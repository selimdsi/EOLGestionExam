﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DAL
{
   public class AppConfiguration2
    {
       public static String ConnectionString
       {
           get
           {
               return ConfigurationManager.ConnectionStrings["DefaultConnectionString2"].ConnectionString;
           }
       }
    }
}

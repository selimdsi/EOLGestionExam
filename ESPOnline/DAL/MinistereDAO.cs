﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ABSEsprit;
using System.Data;
using Oracle.ManagedDataAccess.Client;

namespace DAL
{
  public class MinistereDAO
    {
       #region sing
      static MinistereDAO instance;
        static Object locker = new Object();
        public static MinistereDAO Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new MinistereDAO();
                    }

                    return MinistereDAO.instance;
                }
            }

        }

        private MinistereDAO() { }
        #endregion sing

        #region Connexion
        OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep");
        OracleTransaction myTrans;

        public void openconntrans()
        {
            mySqlConnection.Open();
            myTrans = mySqlConnection.BeginTransaction();

        }

        public void commicttrans()
        {
            myTrans.Commit();
        }

        public void rollbucktrans()
        {
            myTrans.Rollback();
        }
        public void closeConnection()
        {

            mySqlConnection.Close();


        }
        #endregion

      //count masc
        public string Get_NB_ETUD_MASC()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

              //  string cmdQuery = "select count(*) from ESP_INSCRIPTION a inner join esp_etudiant e on a.id_et=e.id_et where a.annee_deb like 2014 and lower(e.etat)='a' and  lower(e.SEXE) like 'm' ";
                string cmdQuery = "select count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl  left join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia'  ) and lower(e.SEXE)='m' and lower(e.ETAT)='a' ";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //count nb feminin
        public string Get_NB_ETUD_fem()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

              //  string cmdQuery = "select count(*) from ESP_INSCRIPTION a inner join esp_etudiant e on a.id_et=e.id_et where a.annee_deb like 2014 and lower(e.etat)='a' and  lower(e.SEXE) like 'f' ";
                string cmdQuery = "select count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl  left join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia'  ) and lower(e.SEXE)='f' and lower(e.ETAT)='a' ";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //count total
        public string Get_NB_ETUD_total()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

               // string cmdQuery = "select count(*) from ESP_INSCRIPTION a inner join esp_etudiant e on a.id_et=e.id_et inner join CLASSES1516 c on c.code_cl like a.code_cl where a.annee_deb like 2014 and lower(e.etat)='a'";


               // string cmdQuery = "select count(*) from ESP_INSCRIPTION a inner join esp_etudiant e on a.id_et=e.id_et where a.annee_deb = 2014 and lower(e.etat)='a' and lower(e.nationalite) like 'tunis%' or upper(e.nationalite) like 'TUINISIENNE%'";
                string cmdQuery = "select count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl left join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia'  )and lower(e.etat)='a' ";

                
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //etudiant etrangère
        public string Get_NB_ETUD_etrange()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

               // string cmdQuery = "select count(*) from ESP_INSCRIPTION a inner join esp_etudiant e on a.id_et=e.id_et inner join CLASSES1516 c on c.code_cl like a.code_cl where a.annee_deb like 2014 and lower(e.etat)='a' and lower(e.NATIONALITE) not like 'tunisienne'";
                // string cmdQuery = "select count(*) from esp_etudiant e left join esp_inscription a on e.id_et=a.id_et where lower(e.nationalite) not like 'tunisienne%' and a.annee_deb='2014' and upper(e.nationalite) not like 'TUINISIENNE%' order by nationalite desc";


                string cmdQuery = "select count(*) from (select * FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl  inner join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and lower(e.etat)='a' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia') MINUS select * FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl inner join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and lower(e.etat)='a' and lower(e.nationalite) like 'tunisienne%' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia'))";
                
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }
      //etudiant de 1ere annee
        public string Get_NB_ETUD_1ERE_MASC()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select count(*) from esp_inscription a inner join esp_etudiant e on a.id_et=e.ID_ET where lower(e.etat) like 'a' and lower(e.sexe) like 'm' and a.annee_deb like '2014' and a.code_cl like '1%'";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //etudiant 1ER FEM
        public string Get_NB_ETUD_1ERE_FEM()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select count(*) from esp_inscription a inner join esp_etudiant e on a.id_et=e.ID_ET where lower(e.etat) like 'a' and lower(e.sexe) like 'f' and a.annee_deb like '2014' and a.code_cl like '1%'";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //count nb ens
        public string Get_NB_ENS()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select count(*) from ESP_ENSEIGNANT where lower(ETAT)='a'";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //nb ens contractuel
        public string Get_NB_contractuel()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select count(*) from ESP_ENSEIGNANT where lower(ETAT)='a' and  lower(type_ens) like 'v'";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

      //nb  ens masc
        public string Get_NB_masc()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select count(*) from ESP_ENSEIGNANT where lower(ETAT)='a' and lower(SEXE_ENS) like 'h'";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

            //nb ens fem
        public string Get_NB_fem()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select count(*) from ESP_ENSEIGNANT where lower(ETAT)='a' and lower(SEXE_ENS) like 'f' ";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

            //repartition des etudiants nouv et redoublant
        public DataTable rep_etudant_arabic()
        {

            DataTable dt = new DataTable();
           // string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select a.Site,DIPLOMEARB,SPECIALITEARB,NIVEAUARB,Niveau,count(*) NB_Classes,sum(Nouveaux_inscrits_masculin) Nouveaux_inscrits_masculin,sum(Nouveaux_inscrits_feminin) Nouveaux_inscrits_feminin,sum(Redoublants_inscrits_masculin) Redoublants_inscrits_masculin,sum(Redoublants_inscrits_feminin) Redoublants_inscrits_feminin,sum(Inscrits) Total from (  select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and lower(e.sexe)='m' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_inscrits_masculin,sum(case when a.type_insc='I' and lower(e.sexe)='f' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_inscrits_feminin,sum(case when a.type_insc='I' and lower(e.sexe)='m' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_inscrits_masculin,sum(case when a.type_insc='I' and lower(e.sexe)='f' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_inscrits_feminin,sum(case when a.type_insc='I' then 1 else 0 end) Inscrits from scoesp09.ESP_INSCRIPTION a left join scoesp09.ESP_INSCRIPTION b on a.id_et=b.id_et  and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join scoesp09.ESP_ETUDIANT e on a.id_et=e.id_et inner join scoesp09.classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and lower(e.ETAT) ='a' and a.code_cl between '1' and '5t' group by c.Site,a.code_cl) a left join scoesp09.classes1516 b on a.classe=b.code_cl group by a.site,niveau,DIPLOMEARB,SPECIALITEARB,NIVEAUARB order by a.site,niveau", con);


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


      //rep etudiant etranger
        public DataTable rep_etudant_etrgr()
        {

            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand(" select  nationalitearb ,C.DIPLOMEARB,C.SPECIALITEARB,NIVEAUARB,C.NIVEAU,sum(case when lower(e.sexe)='m' and lower(e.etat)='a' and upper(nationalite) != 'TUNISIENNE' and nationalite is not null then 1  else 0 end)  masc_inscrits,sum(case when lower(e.sexe)='f' and lower(e.etat)='a' and upper(nationalite) != 'TUNISIENNE' and nationalite is not null then 1 else 0 end)  fem_inscrits,sum(case when lower(e.sexe)='m' and lower(e.etat)='a' and upper(nationalite) != 'TUNISIENNE' and nationalite is not null then 1  else 0 end) + sum(case when lower(e.sexe)='f' and lower(e.etat)='a' and upper(nationalite) != 'TUNISIENNE' and nationalite is not null then 1 else 0 end) as total  from esp_etudiant e inner join esp_inscription p on p.id_et=e.ID_ET inner join CLASSES1516 c on c.CODE_CL=p.CODE_CL where   p.annee_deb =2014 and e.NATIONALITE is not null and lower(e.NATIONALITE) not like 'tunisienne%' and lower(nationalite) not in (SELECT  nationalite FROM    esp_etudiant e WHERE   lower(e.NATIONALITE) like'tunisienne') group by upper(nationalite),e.NATIONALITEarb,C.DIPLOMEARB,C.SPECIALITEARB,C.NIVEAU,NIVEAUARB order by c.NIVEAU", con);

            // string cmdQuery = "select count(*) from (select * FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl  inner join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and lower(e.etat)='a' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia') MINUS select * FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl inner join esp_etudiant e on e.id_et=a.id_et where a.annee_deb='2014' and lower(e.etat)='a' and lower(e.nationalite) like 'tunisienne%' and a.code_cl between '1' and '5t' and( lower(site) like 'ghazala' or lower(site) like 'charguia'))";
                
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

      /// <summary>
      /// ///////////////////////test/////////////////////////
      /// </summary>
      /// <param name="nationalite"></param>
        public void TEST(string nationalite)
        {
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO arabe (C1)VALUES('" +nationalite +"')";
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public DataTable getARABE()
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand(" SELECT * FROM ARABE", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
        public DataTable bind_result()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;

            cmd.CommandText = " select SPECIALITEARB,NIVEAUARB,sum(Inscrits_masculin) Inscrits_masculin,sum(Inscrits_feminin) Inscrits_feminin,sum(P_inscrits_masculin) P_inscrits_masculin,sum(P_inscrits_feminin) P_inscrits_feminin,sum(Reussis_masculin) Reussis_masculin,sum(Reussis_feminin) Reussis_feminin from ( select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and lower(e.sexe)='m'  then 1 else 0 end) Inscrits_masculin,sum(case when a.type_insc='I' and lower(e.sexe)='f'  then 1 else 0 end) Inscrits_feminin,sum(case when a.type_insc='P' and lower(e.sexe)='m'  then 1 else 0 end)  P_inscrits_masculin,sum(case when a.type_insc='P' and lower(e.sexe)='f'  then 1 else 0 end) P_inscrits_feminin, sum(case when a.type_insc='I' and lower(e.sexe)='m' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1)  then 1 else 0 end)  Reussis_masculin,sum(case when a.type_insc='I' and lower(e.sexe)='f' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1)  then 1 else 0 end) Reussis_feminin from scoesp09.ESP_INSCRIPTION a left join scoesp09.ESP_INSCRIPTION b on a.id_et=b.id_et  and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join scoesp09.ESP_ETUDIANT e on a.id_et=e.id_et inner join scoesp09.classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and lower(e.ETAT) ='a' and a.code_cl between '1' and '5t' group by c.Site,a.code_cl) a left join scoesp09.classes1516 b on a.classe=b.code_cl group by SPECIALITEARB,NIVEAUARB order by NIVEAUARB,SPECIALITEARB";
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable Titre_Diplome()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select  diplomearb, specialitearb,sum(Inscrits_masculin) Inscrits_masculins,sum(Inscrits_feminins) Inscrits_feminins,sum(Inscrits_masculin) + sum(Inscrits_feminins) total from ( select distinct  c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and lower(e.sexe)='m'   then 1 else 0 end) Inscrits_masculin,sum(case when a.type_insc='I' and lower(e.sexe)='f'   then 1 else 0 end) Inscrits_feminins from scoesp09.ESP_INSCRIPTION a left join scoesp09.ESP_INSCRIPTION b on a.id_et=b.id_et and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join scoesp09.ESP_ETUDIANT e on a.id_et=e.id_et inner join scoesp09.classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and lower(e.ETAT) ='a' and a.code_cl  between '3' and '5t' group by c.Site,a.code_cl) a left join scoesp09.classes1516 b on a.classe=b.code_cl group by diplomearb, specialitearb order by diplomearb,specialitearb ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using ABSEsprit;

namespace DAL
{
 public   class ExamenDAO
    {
     
       #region sing
        static ExamenDAO instance;
        static Object locker = new Object();
        public static ExamenDAO Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new ExamenDAO();
                    }

                    return ExamenDAO.instance;
                }
            }

        }

        private ExamenDAO() { }
        #endregion sing

        #region Connexion
        OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=selimesp;PASSWORD= tbzr10ep");
        OracleTransaction myTrans;

        public void openconntrans()
        {
            mySqlConnection.Open();
            myTrans = mySqlConnection.BeginTransaction();

        }

        public void commicttrans()
        {
            myTrans.Commit();
        }

        public void rollbucktrans()
        {
            myTrans.Rollback();
        }
        public void closeConnection()
        {

            mySqlConnection.Close();


        }
        #endregion

        public DataTable customerss()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select * from customers";
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }



        public DataTable getsalles(int num, int per,int sem)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT a.CODE_CL, a.CODE_MODULE, a.NUM_SEMESTRE, a.NUM_PERIODE, a.NUM_SEANCE, b.DESIGNATION, c.DATE_SEANCE, c.HEURE_DEBUT FROM ESP_DS_EXAM a, ESP_MODULE b, ESP_SEANCE_EXAMEN c  where a.CODE_MODULE=b.CODE_MODULE and a.NUM_SEANCE=c.NUM_SEANCE and c.NUM_SEANCE='" + num + "' and NUM_PERIODE='" + per + "' and NUM_SEMESTRE='" + sem + "' ORDER BY CODE_CL ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable getsallesmodif(int num, int per, int sem)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT a.CODE_CL, a.MODULE,a.DATE_SEANCE, a.SEMESTRE, a.PERIODE, a.NUM_SEANCE,a.DATE_SEANCE,a.HEURE_DEBUT,a.SALLE_1,a.SALLE_2 FROM ESP_SALLE_EXAMEN a  where NUM_SEANCE='" + num + "' and PERIODE='" + per + "' and SEMESTRE='" + sem + "' ORDER BY CODE_CL ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }


        public DataTable getsurv(int num, int per, int sem)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT a.CODE_CL, a.CODE_MODULE, a.NUM_SEMESTRE, a.NUM_PERIODE, a.NUM_SEANCE, b.DESIGNATION FROM ESP_DS_EXAM a, ESP_MODULE b  where a.CODE_MODULE=b.CODE_MODULE and NUM_SEANCE='" + num + "' and NUM_PERIODE='" + per + "' and NUM_SEMESTRE='" + sem + "' ORDER BY CODE_CL ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable getsurvmodif(int num, int per, int sem)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT a.CODE_CL, a.MODULE, a.SEMESTRE, a.PERIODE, a.NUM_SEANCE, (select NOM_ENS from ESP_ENSEIGNANT  where ID_ENS=a.SURVEILLANT_1 )SURVEILLANT_1,(select NOM_ENS from ESP_ENSEIGNANT  where ID_ENS=a.SURVEILLANT_2 )SURVEILLANT_2  FROM ESP_SURVEILLANT_EXAMEN a where   NUM_SEANCE='" + num + "' and PERIODE='" + per + "' and SEMESTRE='" + sem + "' ORDER BY CODE_CL";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }




        public DataTable bind_calendar_exam()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = " select distinct c.annee_deb ,c.code_cl as Classe from ESP_MODULE_PANIER_CLASSE_SAISO c inner join esp_inscription a on c.CODE_CL=a.CODE_CL where c.ANNEE_DEB='2014' order by FN_TRI_CLASSE(Classe)";
            cmd.CommandText = "select lib_nome,code_nome,CASE  WHEN code_module IN ('MS_40') THEN 'A+' WHEN code_module in ('FR-02') then 'A+' when code_module in ('AS_10','SIM-09') then 'D-' when code_module in 'AP_42' then 'B-' when code_module in 'UP_42' then 'A-' end as A1,CASE WHEN code_module IN ('MS_40') THEN 'A+' WHEN code_module in ('FR-02') then 'A+' when code_module in ('AS_10','SIM-09') then 'D-' when code_module in 'AP_42' then 'B-' when code_module in 'UP_42' then 'A-' end as A2,CASE  WHEN code_module IN ('MS_40') THEN 'A+' WHEN code_module in ('FR-02') then 'A+' when code_module in ('AS_10','SIM-09') then 'D-' when code_module in 'AP_42' then 'B-' when code_module in 'UP_42' then 'A-' end as A3 from ESP_MODULE_PANIER_CLASSE_SAISO,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '1A1' AND ANNEE_DEB = 2016 order by CODE_NOMENCLATURE.CODE_NOME";
                //"select DISTINCT CASE WHEN code_module IN ('SH-42') THEN 'A+' WHEN code_module in ('FR-06') then 'A+' when code_module in ('SIM-08','SIM-09') then 'D-' when code_module in 'DM-13' then 'B-' when code_module in 'WS-09' then 'A-' else 'A+' end as A1,lib_nome,code_nome from esp_module_panier_classe_saiso,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '4SIM1' AND ANNEE_DEB = 2015 order by CODE_NOMENCLATURE.CODE_NOME";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

     //plan d'etude
        public DataTable PLan_etudes(string code_cl)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select distinct m.annee_deb,m.code_module,m.code_cl,num_semestre,nb_heures,coef,date_examen,date_debut,date_fin,m.DATE_RATTRAPAGE,m.HEURE_EXAM,m.SALLE_EXAM,m.SALLE_EXAM2,m.SURVEILLANT,m.SURVEILLANT2,m.CHARGE_P1,m.CHARGE_P2,nb_ects,type_epreuve from esp_module_panier_classe_saiso m inner join esp_inscription c on m.CODE_CL=c.CODE_CL WHERE m.annee_deb='2016' and m.code_cl like '"+code_cl+"' order by m.code_cl,m.num_semestre";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
        public DataTable code_cl()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct m.code_cl from esp_module_panier_classe_saiso m inner join esp_inscription c on m.CODE_CL=c.CODE_CL WHERE m.annee_deb='2016'  order by FN_TRI_CLASSE(m.code_cl)";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
   //Affecter les dates pour passer l'examen

        public DataTable Affecter_exam()
        {
            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
           // cmd.CommandText = "";
            cmd.CommandText = "select lib_nome,code_nome,CASE  WHEN code_module IN ('MS_40') THEN 'A+' WHEN code_module in ('FR-02') then 'A+' when code_module in ('AS_10','SIM-09') then 'D-' when code_module in 'AP_42' then 'B-' when code_module in 'UP_42' then 'A-' end as A1,CASE WHEN code_module IN ('MS_40') THEN 'A+' WHEN code_module in ('FR-02') then 'A+' when code_module in ('AS_10','SIM-09') then 'D-' when code_module in 'AP_42' then 'B-' when code_module in 'UP_42' then 'A-' end as A2,CASE  WHEN code_module IN ('MS_40') THEN 'A+' WHEN code_module in ('FR-02') then 'A+' when code_module in ('AS_10','SIM-09') then 'D-' when code_module in 'AP_42' then 'B-' when code_module in 'UP_42' then 'A-' end as A3 from ESP_MODULE_PANIER_CLASSE_SAISO,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '1A1' AND ANNEE_DEB = 2016 order by CODE_NOMENCLATURE.CODE_NOME";
            
                //"select DISTINCT CASE WHEN code_module IN ('SH-42') THEN 'A+' WHEN code_module in ('FR-06') then 'A+' when code_module in ('SIM-08','SIM-09') then 'D-' when code_module in 'DM-13' then 'B-' when code_module in 'WS-09' then 'A-' else 'A+' end as A1,lib_nome,code_nome from esp_module_panier_classe_saiso,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '4SIM1' AND ANNEE_DEB = 2015 order by CODE_NOMENCLATURE.CODE_NOME";
                //"select distinct LIB_JOURS,lib_nome as HEURE_DEBUT,code_cl,J.SEMAINE_16,J.EXAM_16  FROM CODE_NOMENCLATURE N,esp_jours J,esp_module_panier_classe_saiso C WHERE N.CODE_STR like '92' and C.code_cl like '1A1' AND C.ANNEE_DEB like 2014 AND lower(J.LIB_JOURS) not like 'dimanche' ORDER BY FN_TRI_JOURS(J.LIB_JOURS),N.LIB_NOME";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
        public DataTable bind_semestre()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct num_semestre from esp_module_panier_classe_saiso where annee_deb like '2016'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable bind_modules(string cod_cl,string num_sem)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT c.code_module,designation  FROM ESP_module_panier_classe_saiso c inner join esp_module a on c.CODE_MODULE=a.CODE_MODULE where code_cl like '"+cod_cl+"' and annee_deb like '2016' and num_semestre like '"+num_sem+"' order by num_semestre";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
        //OracleCommand cmd = new OracleCommand("SELECT c.code_module,designation  FROM ESP_module_panier_classe_saiso c inner join esp_module a on c.CODE_MODULE=a.CODE_MODULE where code_cl like '" + ddlClasse.SelectedValue + "' and annee_deb like '2014' and num_semestre like '" + ddlsemestre.SelectedValue + "' order by num_semestre");



     //GET ANnee universitaire

        public DataTable List_etud_2015()
        {

            DataTable dt = new DataTable();
                        OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("SELECT distinct ANNEE_DEB ||'-'|| to_number(nvl(annee_deb,'')+1) ANNEE_UNIVERSITAIRE FROM ESP_INSCRIPTION where to_number(annee_deb)>=2003 order by 1 desc", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public DataTable bind_site(string annee)
        {

            DataTable dt = new DataTable();
            
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT site FROM scoesp09.CLASSES1516 a where a.ANNEE_DEB=" + annee + " union select 'ALL' from dual order by site";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            return dt;
        }


        public DataTable bind_site2(string annee)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT distinct site FROM ESP_DS_EXAM a where annee_deb like '" + annee + "' union select 'ALL' from dual order by site";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            return dt;
        }

        public DataTable bind_niveauGH(string site)
        {

            DataTable dt = new DataTable();
            
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select  distinct substr(CATEGORIE,1,1 ) as niveau from CLASSE a where FILIERE in(01,02,05)  union select 'ALL' from dual order by niveau ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            // con.Close();
            return dt;
        }

        public DataTable bind_classesgh(string niv)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = "select CATEGORIE as code_cl  from CLASSE a where CATEGORIE like'" + niv + "%' and FILIERE in(01,02,05) union select 'ALL' from dual";
            cmd.CommandText = "SELECT distinct CATEGORIE as code_cl from CLASSE where FILIERE in(01,02,05) and CATEGORIE like '" + niv + "%'  union select 'ALL' from dual";

            //cmd.CommandText = "select DISTINCT t1.CODE_CL from ESP_SAISON_CLASSE t1, CLASSE t2 where t1.ANNEE_DEB='2016' and t1.CODE_CL=t2.CODE_CL order by t1.CODE_CL";
            //union select 'ALL' from dual


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }



        public DataTable bind_classesCH(string niv)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select CATEGORIE as code_cl  from CLASSE a where CATEGORIE like'" + niv + "%' and FILIERE in(03,04,07) union select 'ALL' from dual  ";


            //cmd.CommandText = "select DISTINCT t1.CODE_CL from ESP_SAISON_CLASSE t1, CLASSE t2 where t1.ANNEE_DEB='2016' and t1.CODE_CL=t2.CODE_CL order by t1.CODE_CL";
            //union select 'ALL' from dual


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }




        public DataTable bind_classesALL()
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select CATEGORIE as code_cl  from CLASSE union select 'ALL' from dual  ";


            //cmd.CommandText = "select DISTINCT t1.CODE_CL from ESP_SAISON_CLASSE t1, CLASSE t2 where t1.ANNEE_DEB='2016' and t1.CODE_CL=t2.CODE_CL order by t1.CODE_CL";
            //union select 'ALL' from dual


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }



        public DataTable bind_classes(string niv)
        {

            DataTable dt = new DataTable();
          
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = "select NIVEAU as code_cl  from CLASSES1516 a where " + niv + "  union select 'ALL' from dual  ";
            cmd.CommandText = "SELECT distinct CATEGORIE as code_cl from CLASSE where FILIERE in(01,02,05) and CATEGORIE like '" + niv + "%'  union select 'ALL' from dual";


            //cmd.CommandText = "select DISTINCT t1.CODE_CL from ESP_SAISON_CLASSE t1, CLASSE t2 where t1.ANNEE_DEB='2016' and t1.CODE_CL=t2.CODE_CL order by t1.CODE_CL";
            //union select 'ALL' from dual


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }


        public DataTable bind_classesbyclasses(string niv)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select code_cl  from classes1516 a where " + niv + "  union select 'ALL' from dual   ";
            //union select 'ALL' from dual


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }  


        public DataTable bind_exam_aprogrammer(string code_cl,string num_semestr)
        {

            DataTable dt = new DataTable();
            
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2 from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where " + code_cl + "   " + num_semestr + " and a.CHARGE_P1>'0' and  (a.CHARGE_P2='0' or a.CHARGE_P2>'0')  and annee_deb ='2016' order by code_module";
         //   "select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2 from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '" + code_cl + "%' and annee_deb ='2015' and NUM_SEMESTRE='" + num_semestr + "' and a.CHARGE_P1>'0' and  (a.CHARGE_P2='0' or a.CHARGE_P2>'0') order by code_module";
               
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }


        public DataTable bind_exam_aprogrammerbymodule(string code_mod, string num_sem)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = "select distinct b.DESIGNATION ,a.CHARGE_P1,a.CHARGE_P2, a.CODE_CL from ESP_MODULE_PANIER_CLASSE_SAISO a  ,ESP_MODULE b where a.CODE_MODULE=" + code_module + " and NUM_SEMESTRE=" + num_semestr + " and a.CODE_MODULE=b.CODE_MODULE and ANNEE_DEB='2016'";
            cmd.CommandText = "select distinct a.CODE_MODULE,b.DESIGNATION ,a.CHARGE_P1,a.CHARGE_P2, a.CODE_CL from ESP_MODULE_PANIER_CLASSE_SAISO a  ,ESP_MODULE b where a.CODE_MODULE = '" + code_mod.ToString() + "' and a.NUM_SEMESTRE=" + num_sem + " and a.CODE_MODULE=b.CODE_MODULE and ANNEE_DEB='2016' order by a.code_cl";
            
            
            //   "select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2 from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '" + code_cl + "%' and annee_deb ='2015' and NUM_SEMESTRE='" + num_semestr + "' and a.CHARGE_P1>'0' and  (a.CHARGE_P2='0' or a.CHARGE_P2>'0') order by code_module";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }





        public DataTable bind_exam_aprogrammerP2(string code_cl, string num_semestr)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2 from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where "+code_cl+" and annee_deb ='2016' " + num_semestr + " and a.CHARGE_P2>'0'  order by code_module";
            //"select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2,code_cl from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '"+code_cl+"%' and annee_deb ='2015'  order by FN_TRI_CLASSE(a.CODE_CL)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }






        public DataTable bind_exam_aprogrammer(string code_cl)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;

            cmd.CommandText = "select code_cl from esp_ds_exam where A_PROGRAMMER='N' and code_cl like '"+code_cl+"%'";

            //"select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2,code_cl from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '"+code_cl+"%' and annee_deb ='2015'  order by FN_TRI_CLASSE(a.CODE_CL)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }


     //bind code classes


        public DataTable bind_exam_byclasses(string code_cl, string num_semestr)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct code_cl,num_semestre,num_periode from esp_ds_exam where code_cl like '" + code_cl + "%' and num_semestre='" + num_semestr + "'";
            //"select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2,code_cl from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '"+code_cl+"%' and annee_deb ='2015'  order by FN_TRI_CLASSE(a.CODE_CL)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }

        public DataTable bind_exam_byclassesord(string code_cl, string num_semestr)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct b.code_cl,a.code_module,designation,num_semestre,num_periode,case when A_PROGRAMMER like '1' then 'O' when A_PROGRAMMER like '0' then 'N' END as A_PROGRAMMER,TYPE_EXAM from esp_ds_exam b inner join ESP_MODULE a on a.CODE_MODULE=b.CODE_MODULE where code_cl like '" + code_cl + "%' and num_semestre='" + num_semestr + "' order by code_cl";
                
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }




        public DataTable exist(string code_cl,string code_module)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;

            cmd.CommandText = "select * from esp_ds_exam where code_module='"+code_module+"'  and code_cl ='"+code_cl+"' ";

            //"select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2,code_cl from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '"+code_cl+"%' and annee_deb ='2015'  order by FN_TRI_CLASSE(a.CODE_CL)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }





        public DataTable bind_seance_exam()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = " select distinct c.annee_deb ,c.code_cl as Classe from ESP_MODULE_PANIER_CLASSE_SAISO c inner join esp_inscription a on c.CODE_CL=a.CODE_CL where c.ANNEE_DEB='2014' order by FN_TRI_CLASSE(Classe)";
            cmd.CommandText = "select num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode from esp_seance_examen";
            //"select DISTINCT CASE WHEN code_module IN ('SH-42') THEN 'A+' WHEN code_module in ('FR-06') then 'A+' when code_module in ('SIM-08','SIM-09') then 'D-' when code_module in 'DM-13' then 'B-' when code_module in 'WS-09' then 'A-' else 'A+' end as A1,lib_nome,code_nome from esp_module_panier_classe_saiso,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '4SIM1' AND ANNEE_DEB = 2015 order by CODE_NOMENCLATURE.CODE_NOME";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }




        public string Get_annee_deb()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select DISTINCT ANNEE_EN_COURS from esp_parametre where  substr(ANNEE_EN_COURS,4,1) like '6'";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }


        public void affecter_module_examen(string num_seance,string a_programmer, string type_exam, string code_cl, string code_module, int num_semestre, string num_periode, string DS_EXAM, string anne,string s)
        {
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            cmd.CommandText = "insert into esp_ds_exam (NUM_SEANCE,a_programmer,type_exam,code_cl,code_module,num_semestre,num_periode,ds_exam,annee_deb,date_saisie,site) values ('" + num_seance + "','" + a_programmer + "','" + type_exam + "','" + code_cl + "','" + code_module + "','" + num_semestre + "','" + num_periode + "','" + DS_EXAM + "','" + anne + "',to_char(sysdate,'dd/mm/yy'),'"+s+"')";

            cmd.ExecuteNonQuery();
        }

        public void affecter_module_examen_by_module(string num_seance, string a_programmer, string type_exam, string code_cl, string code_module, int num_semestre, string num_periode, string DS_EXAM, string anne, string s)
        {
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            cmd.CommandText = "insert into esp_ds_exam (NUM_SEANCE,a_programmer,type_exam,code_cl,code_module,num_semestre,num_periode,ds_exam,annee_deb,date_saisie,site) values ('" + num_seance + "','" + a_programmer + "','" + type_exam + "','" + code_cl + "','" + code_module + "','" + num_semestre + "','" + num_periode + "','" + DS_EXAM + "','" + anne + "',to_char(sysdate,'dd/mm/yy'),'" + s + "')";

            cmd.ExecuteNonQuery();
        }

        public DataTable fill_jours()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
           cmd.CommandText = "select lib_nome from code_nomenclature where code_str='92'";
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }




        public DataTable fill_Num_jours()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select code_nome from code_nomenclature where code_str='92'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }


        public DataTable existSeance(string num_seance,string periode, int semestre, string dateexam,string annee_deb)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = " select distinct c.annee_deb ,c.code_cl as Classe from ESP_MODULE_PANIER_CLASSE_SAISO c inner join esp_inscription a on c.CODE_CL=a.CODE_CL where c.ANNEE_DEB='2014' order by FN_TRI_CLASSE(Classe)";
            cmd.CommandText = "select * from ESP_SEANCE_EXAMEN where ANNEE_DEB='" + annee_deb + "' and DATE_SEANCE=to_date('" + dateexam.ToString() + "','dd/mm/yyyy hh24:mi:ss') and NUM_SEANCE='" + num_seance + "' and PERIODE='" + periode + "' and SEMESTRE='" + semestre + "'";
            //"select DISTINCT CASE WHEN code_module IN ('SH-42') THEN 'A+' WHEN code_module in ('FR-06') then 'A+' when code_module in ('SIM-08','SIM-09') then 'D-' when code_module in 'DM-13' then 'B-' when code_module in 'WS-09' then 'A-' else 'A+' end as A1,lib_nome,code_nome from esp_module_panier_classe_saiso,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '4SIM1' AND ANNEE_DEB = 2015 order by CODE_NOMENCLATURE.CODE_NOME";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        //select * from ESP_SEANCE_EXAMEN where ANNEE_DEB='2015' and PERIODE='P1' and SEMESTRE='1' and upper(site) like 'GHAZALA';

        public DataTable Cloner(string annee_deb,string periode, int semestre,string site)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            
            cmd.CommandText = "select * from ESP_SEANCE_EXAMEN where ANNEE_DEB='"+annee_deb+"' and PERIODE='"+periode+"' and SEMESTRE='"+semestre+"' and site like '"+site+"'";
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }


     //select annee from esp_parametre
        /*
         */

        //public DataTable get_annee_parametr()
        //{
        //    DataTable dt = new DataTable();

        //    OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
        //    con.Open();
        //    OracleCommand cmd = new OracleCommand();
        //    cmd.Connection = con;

        //    cmd.CommandText = "select  (to_number(SUBSTR(annee_en_cours,1,4))) ||'-'|| ( to_number(nvl(SUBSTR(annee_en_cours,1,4),'')+1))  ANNEE_UNIVERSITAIRE from esp_parametre where  to_number(SUBSTR(annee_en_cours,1,4))>= TO_NUMBER(SUBSTR(annee_0,1,4)) order by 1 desc";

        //    OracleDataAdapter od = new OracleDataAdapter(cmd);
        //    od.Fill(dt);
        //    return dt;
        //}



        public string get_annee_parametr()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select  (to_number(SUBSTR(annee_en_cours,1,4))) ||'-'|| ( to_number(nvl(SUBSTR(annee_en_cours,1,4),'')+1))  ANNEE_DEB from esp_parametre where  to_number(SUBSTR(annee_en_cours,1,4))>= TO_NUMBER(SUBSTR(annee_0,1,4)) order by 1 desc";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }




        public string get_annee_parametrfin()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select (to_number(SUBSTR(annee_en_cours,6,4))) ||'-'|| ( to_number(nvl(SUBSTR(annee_en_cours,6,4),'')+1))  ANNEE_DEB from esp_parametre  where  to_number(SUBSTR(annee_en_cours,6,4))>= TO_NUMBER(SUBSTR(annee_0,6,4)) order by 1 desc";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }


        public string get_annee_0()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select  (to_number(SUBSTR(annee_0,1,4)))  ANNEE_0 from esp_parametre ";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        
        }

        public DataTable getmap_x(string annee,int s,int p,string site)
        {

            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            string x = ("MAPX_" + annee + "_" + s + "" + p + "_" + site + "").Trim();
            //cmd.CommandText = "select * from MAPX_1516_22_Charguia";
            cmd.CommandText = "select * from "+x+"";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;

        }



        public DataTable getmap_detaille(string annee, int s, int p, string site)
        {
            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            string x = "mapdsex_" + annee + "_" + s + "" + p + "_" + site + "";
            //cmd.CommandText = "select * from MAPX_1516_22_Charguia";
            cmd.CommandText = "select * from " + x + "";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;

        }


        public DataTable getpageparsitr(string site)
        {
            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            
            cmd.CommandText = "select distinct  case when page<=9  then CONCAT('0', page) when page>9 then CONCAT('', page) end as page from classes1516 where site like '"+site+"' and page is not null order by page";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;

        }



        public DataTable getmap_detaille(string annee, int s, int p, string site,string page)
        {
            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //mapdsex_
            string x = "MAPDSEX_" + annee + "_" + s + "" + p + "_" + site + "_"+page+"";
            //cmd.CommandText = "select * from MAPX_1516_22_Charguia";
            cmd.CommandText = "select * from " + x + "";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
     //mapX
        public void CreateMapExamenx(string annee,int semestre,int periode,string site)
        {
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand("PS_MAPX_EXAMEN", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(":vannee", OracleDbType.Varchar2).Value = annee;
            cmd.Parameters.Add(":vsemestre", OracleDbType.Int32).Value = semestre;
            cmd.Parameters.Add(":vperiode", OracleDbType.Int32).Value = periode;
            cmd.Parameters.Add(":vsite", OracleDbType.Varchar2).Value = site;
            cmd.ExecuteNonQuery();
        }


     //map
        public void CreateMapExamen(string annee, int semestre, int periode, string site)
        {
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand("PS_MAP_EXAMEN2017", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(":vannee", OracleDbType.Varchar2).Value = annee;
            cmd.Parameters.Add(":vsemestre", OracleDbType.Int32).Value = semestre;
            cmd.Parameters.Add(":vperiode", OracleDbType.Int32).Value = periode;
            cmd.Parameters.Add(":vsite", OracleDbType.Varchar2).Value = site;
            cmd.ExecuteNonQuery();
        }

     //map detaille
        public void CreateMapDetaillee(string annee, int semestre, int periode, string site,string i)
        {
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand("MAP_DET", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(":vannee", OracleDbType.Varchar2).Value = annee;
            cmd.Parameters.Add(":vsemestre", OracleDbType.Int32).Value = semestre;
            cmd.Parameters.Add(":vperiode", OracleDbType.Int32).Value = periode;
            cmd.Parameters.Add(":vsite", OracleDbType.Varchar2).Value = site;
            cmd.Parameters.Add(":i", OracleDbType.Varchar2).Value = i;
            cmd.ExecuteNonQuery();
        }


        public DataTable getmapdsex(string annee, int s, int p, string site)
        {

            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            string x = ("mapdsex_" + annee + "_" + s + "" + p + "_" + site.ToUpper() + "");
            //cmd.CommandText = "select * from MAPX_1516_22_Charguia";
            cmd.CommandText = "select * from " + x + "";
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;

        }





        public DataTable bind_cls(string niv,string site)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select code_cl   from classes1516 a where " + niv + " "+site+" union select 'ALL' from dual  ";
            //union select 'ALL' from dual


            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }



        public string get_salle_dispo()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT SALLE FROM (SELECT SALLE FROM ESP_SALLE_DISPO ORDER BY dbms_random.value) WHERE rownum = 1";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }
        public void affecter_salle_examen(string CODE_CL, string module, string salle1)
        {
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            cmd.CommandText = "insert into test (CODE_CL,CODE_MODULE,SALLE) values ('" + CODE_CL + "','" + module + "','" + salle1 + "')";

            cmd.ExecuteNonQuery();
        }

        public DataTable bind_MODULE_CODECL()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            //cmd.CommandText = " select distinct c.annee_deb ,c.code_cl as Classe from ESP_MODULE_PANIER_CLASSE_SAISO c inner join esp_inscription a on c.CODE_CL=a.CODE_CL where c.ANNEE_DEB='2014' order by FN_TRI_CLASSE(Classe)";
            cmd.CommandText = "SELECT CODE_CL, CODE_MODULE FROM ESP_DS_EXAM ";
            //"select DISTINCT CASE WHEN code_module IN ('SH-42') THEN 'A+' WHEN code_module in ('FR-06') then 'A+' when code_module in ('SIM-08','SIM-09') then 'D-' when code_module in 'DM-13' then 'B-' when code_module in 'WS-09' then 'A-' else 'A+' end as A1,lib_nome,code_nome from esp_module_panier_classe_saiso,CODE_NOMENCLATURE where code_str like '92' and num_semestre='1' and code_cl like '4SIM1' AND ANNEE_DEB = 2015 order by CODE_NOMENCLATURE.CODE_NOME";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }



        public DataTable existDUPLICATION(string MODule, string SEANCE)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;

            cmd.CommandText = "select * from esp_ds_exam where code_cl='"+MODule+"'  and NUM_SEANCE='"+SEANCE+"' and annee_deb='2016' ";

            //"select distinct a.code_module,designation,a.CHARGE_P1,a.CHARGE_P2,code_cl from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.code_module where a.code_cl like '"+code_cl+"%' and annee_deb ='2015'  order by FN_TRI_CLASSE(a.CODE_CL)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }
    }

}

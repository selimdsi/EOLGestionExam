﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ABSEsprit;

namespace DAL
{
  public  class CahierTexteDAO
    {

      
       #region sing
        static CahierTexteDAO instance;
        static Object locker = new Object();
        public static CahierTexteDAO Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new CahierTexteDAO();
                    }

                    return CahierTexteDAO.instance;
                }
            }

        }

        private CahierTexteDAO() { }
        #endregion sing

        #region Connexion
        OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep");
        OracleTransaction myTrans;

        public void openconntrans()
        {
            mySqlConnection.Open();
            myTrans = mySqlConnection.BeginTransaction();

        }

        public void commicttrans()
        {
            myTrans.Commit();
        }

        public void rollbucktrans()
        {
            myTrans.Rollback();
        }
        public void closeConnection()
        {

            mySqlConnection.Close();


        }
        #endregion

        public DataTable bind_cdclens(string _id_ens,int numsem)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select distinct a.code_cl from ESP_MODULE_PANIER_CLASSE_SAISO a where a.id_ens like '"+_id_ens+"' AND a.annee_deb='2015' and num_semestre='"+numsem+"' order BY fn_tri_classe(a.code_cl)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }


        public DataTable bind_module(string _id_ens,string code_cl,int numsemestre)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct DESIGNATION,a.code_module from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.code_module=b.code_module where code_cl like '" + code_cl + "' and id_ens ='" + _id_ens + "' and num_semestre ='" + numsemestre + "' and annee_deb ='2015'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable bind_CRENEAU()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select lib_nome,code_nome from code_nomenclature where CODE_STR='60'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public void add_cahierTEXT(string _CODE_CL, string _CODE_MODULE, DateTime _DATE_SEANCE, string _DUREE, string _CONTENU_TRAITE, string _REMARQUE, string _HEURE_DEBUT, string _HEURE_FIN, string _ID_ENS, string _ANNEE_DEB,int numsem)
        {
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            cmd.CommandText = "INSERT INTO ESP_CAHIER_DE_TEXTE(CODE_CL,CODE_MODULE,DATE_SEANCE,DUREE,CONTENU_TRAITE,REMARQUE,HEURE_DEBUT,HEURE_FIN,ID_ENS,ANNEE_DEB,NUM_SEMESTRE) VALUES ('" + _CODE_CL + "','" + _CODE_MODULE + "',to_date('" +_DATE_SEANCE.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),'" + _DUREE + "','" + _CONTENU_TRAITE + "','" + _REMARQUE + "','" + _HEURE_DEBUT + "','" + _HEURE_FIN + "','" + _ID_ENS + "','" + _ANNEE_DEB + "','"+numsem+"')";

            cmd.ExecuteNonQuery();
        }



        public DataTable GetDataPardate(DateTime DATE_SEANCE, int semestre)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select a.id_ens,nom_ens,code_cl,designation,t.DATE_SEANCE,case t.HEURE_DEBUT when '1' then '9:00 - 10:30' when '2' then '10:45- 12:15' when '3' then '14:00- 15:30' when '4' then '15:45 - 17:15' end as HEURE_DEBUT ,case t.heure_fin when '1' then '9:00 - 10:30' when '2' then '10:45- 12:15' when '3' then '14:00- 15:30' when '4' then '15:45 - 17:15' end as heure_fin ,num_semestre from esp_cahier_de_texte t inner join ESP_ENSEIGNANT a on a.id_ens=t.ID_ENS inner join esp_module b on b.CODE_MODULE=t.CODE_MODULE where to_date(date_seance) =to_date('" + DATE_SEANCE.ToString() + "', 'dd/mm/yy hh24:mi:ss') and num_semestre='" + semestre + "' and annee_deb ='2015'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable GetDataParens(string id_ens , int semestre)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select a.id_ens,nom_ens,code_cl,designation,t.DATE_SEANCE,case t.HEURE_DEBUT when '1' then '9:00 - 10:30' when '2' then '10:45- 12:15' when '3' then '14:00- 15:30' when '4' then '15:45 - 17:15' end as HEURE_DEBUT ,case t.heure_fin when '1' then '9:00 - 10:30' when '2' then '10:45- 12:15' when '3' then '14:00- 15:30' when '4' then '15:45 - 17:15' end as heure_fin ,num_semestre from esp_cahier_de_texte t inner join ESP_ENSEIGNANT a on a.id_ens=t.ID_ENS inner join esp_module b on b.CODE_MODULE=t.CODE_MODULE where a.id_ens= '" + id_ens + "' and num_semestre='" + semestre + "' and annee_deb ='2015'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable GetDataParclasse(string code_cl, int semestre)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select a.id_ens,nom_ens,code_cl,designation,t.DATE_SEANCE,case t.HEURE_DEBUT when '1' then '9:00 - 10:30' when '2' then '10:45- 12:15' when '3' then '14:00- 15:30' when '4' then '15:45 - 17:15' end as HEURE_DEBUT ,case t.heure_fin when '1' then '9:00 - 10:30' when '2' then '10:45- 12:15' when '3' then '14:00- 15:30' when '4' then '15:45 - 17:15' end as heure_fin ,num_semestre from esp_cahier_de_texte t inner join ESP_ENSEIGNANT a on a.id_ens=t.ID_ENS inner join esp_module b on b.CODE_MODULE=t.CODE_MODULE where code_cl = '" + code_cl + "' and num_semestre='" + semestre + "' and annee_deb ='2015'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable VerifCahier(string id_ens, string code_module, string code_cl, int semestre)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from esp_cahier_de_texte where code_cl like '" + code_cl + "' and code_module like '" + code_module + "' and id_ens ='" + id_ens + "' and num_semestre='" + semestre + "' and annee_deb ='2015'";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable Getlistens()
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct a.id_ens,nom_ens from ESP_ENSEIGNANT b inner join esp_module_panier_classe_saiso a on a.id_ens= b.id_ens where annee_deb ='2015' order by nom_ens";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }
        public DataTable Getlistcl(string code_cl)
        {
            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct a.code_cl from esp_module_panier_classe_saiso a inner join classes1516 b on a.code_cl=b.code_cl where  upper(site)='GHAZALA' and a.code_cl like '" + code_cl + "%' order by FN_TRI_CLASSE(a.code_cl)";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            return dt;
        }

        public DataTable bind_niveau()
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select  distinct substr(code_cl,1,1 ) as niveau from classes1516 order by niveau ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            // con.Close();
            return dt;
        }
    }
}

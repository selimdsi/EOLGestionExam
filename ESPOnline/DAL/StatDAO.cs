﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;

using ABSEsprit;
using Oracle.ManagedDataAccess.Client;

namespace DAL
{
   public class StatDAO
    {
         #region sing

        static StatDAO instance;
        static Object locker = new Object();

        public static StatDAO Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new StatDAO();
                    }

                    return StatDAO.instance;
                }
            }

        }
        private StatDAO() { }
        public StatDAO(OracleDataReader myReader)
        {


            if (!myReader.IsDBNull(myReader.GetOrdinal("code_cl")))
            {

                code_cl = myReader.GetString(myReader.GetOrdinal("code_cl"));
            }



            if (!myReader.IsDBNull(myReader.GetOrdinal("NB_PALIER1")))
            {

                code_cl = myReader.GetString(myReader.GetOrdinal("NB_PALIER1"));
            }

            if (!myReader.IsDBNull(myReader.GetOrdinal("NB_PALIER2")))
            {

                code_cl = myReader.GetString(myReader.GetOrdinal("NB_PALIER2"));
            }

            if (!myReader.IsDBNull(myReader.GetOrdinal("NB_PALIER3")))
            {

                code_cl = myReader.GetString(myReader.GetOrdinal("NB_PALIER3"));
            }

            if (!myReader.IsDBNull(myReader.GetOrdinal("NB_PALIER4")))
            {

                code_cl = myReader.GetString(myReader.GetOrdinal("NB_PALIER4"));
            }

            if (!myReader.IsDBNull(myReader.GetOrdinal("NB_PALIER5")))
            {

                code_cl = myReader.GetString(myReader.GetOrdinal("NB_PALIER5"));
            }
            if (!myReader.IsDBNull(myReader.GetOrdinal("CODE_MODULE")))
            {

                _CODE_MODULE = myReader.GetString(myReader.GetOrdinal("CODE_MODULE"));
            }
            if (!myReader.IsDBNull(myReader.GetOrdinal("DESIGNATION")))
            {

                _DESIGNATION = myReader.GetString(myReader.GetOrdinal("DESIGNATION"));
            }
        }
        #endregion

         #region getset

        private string _CODE_MODULE;

        public string CODE_MODULE
        {
            get { return _CODE_MODULE; }
            set { _CODE_MODULE = value; }
        }

        private string _DESIGNATION;

        public string DESIGNATION
        {
            get { return _DESIGNATION; }
            set { _DESIGNATION = value; }
        }
      
        private string code_cl;

        public string Code_cl
        {
            get { return code_cl; }
            set { code_cl = value; }
        }

        private string c_designation;

        public string C_designation
        {
            get { return c_designation; }
            set { c_designation = value; }
        }


        #endregion

        OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= ");
        OracleTransaction myTrans;

        public void openconntrans()
        {
            mySqlConnection.Open();
            myTrans = mySqlConnection.BeginTransaction();

        }

        public void commicttrans()
        {
            myTrans.Commit();
        }

        public void rollbucktrans()
        {
            myTrans.Rollback();
        }
        public void closeConnection()
        {

            mySqlConnection.Close();


        }
        public DataTable GetListNotes(string CODE_CL, string CODE_MODULE,string Annee)
        {


            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT ESP_EVALUATION.PT_FORT , ESP_EVALUATION.PT_FAIBLE , ESP_EVALUATION.PROPOSITION FROM ESP_EVALUATION , ESP_MODULE WHERE ( ESP_MODULE.CODE_MODULE = ESP_EVALUATION.CODE_MODULE ) and ( ( ESP_EVALUATION.ANNEE_DEB = '" +Annee+ "' ) And ( trim(ESP_EVALUATION.CODE_CL) ='" + CODE_CL + "' ) And ( trim(ESP_EVALUATION.CODE_MODULE) ='" + CODE_MODULE + "' ) ) ", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }


        public DataTable GetListNotes2(string CODE_CL, string CODE_MODULE, string ID_ENS,string Annee)
        {


            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT round((ESP_EVAL_CL_MODULE.NB_PALIER1/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) heure_0, round((ESP_EVAL_CL_MODULE.NB_PALIER2/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) heure_1, round((ESP_EVAL_CL_MODULE.NB_PALIER3/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) heure_2, round((ESP_EVAL_CL_MODULE.NB_PALIER4/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) heure_3,  round((ESP_EVAL_CL_MODULE.NB_PALIER5/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) heure_4,   ESP_CRITERE_EVAL.LIB_CRITERE crit FROM ESP_EVAL_CL_MODULE, ESP_CRITERE_EVAL, ESP_MODULE,   ESP_ENSEIGNANT  WHERE  ( substr(ESP_CRITERE_EVAL.CODE_CRITERE,1,3) = substr(ESP_EVAL_CL_MODULE.CODE_CRITERE,1,3 )) and  ( trim(ESP_EVAL_CL_MODULE.CODE_MODULE) = trim(ESP_MODULE.CODE_MODULE )) and  ( trim(ESP_EVAL_CL_MODULE.ID_ENS) = trim(ESP_ENSEIGNANT.ID_ENS )) and  ( ( ESP_EVAL_CL_MODULE.ANNEE_DEB = '" + Annee + "' ) and ESP_ENSEIGNANT.ID_ENS='" + ID_ENS + "' and ESP_MODULE.CODE_MODULE='" + CODE_MODULE + "' and ESP_EVAL_CL_MODULE.CODE_CL='" + CODE_CL + "' and ((SELECT count(*) FROM ESP_INSCRIPTION  WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "') AND ( trim(ESP_INSCRIPTION.CODE_CL) =trim(ESP_EVAL_CL_MODULE.CODE_CL)) )-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) >=0) and ESP_CRITERE_EVAL.LIB_CRITERE like 'Travail%' ", mySqlConnection);
                DataTable myDataTable = new DataTable();


                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }


        public DataTable GetListNotes3(string CODE_CL, string CODE_MODULE, string ID_ENS,string Annee)
        {


            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT ESP_EVAL_CL_MODULE.CODE_CL, round((ESP_EVAL_CL_MODULE.NB_PALIER1/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) p1 , round((ESP_EVAL_CL_MODULE.NB_PALIER2/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) p2 , round((ESP_EVAL_CL_MODULE.NB_PALIER3/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) p3, round((ESP_EVAL_CL_MODULE.NB_PALIER4/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) p4,  round((ESP_EVAL_CL_MODULE.NB_PALIER5/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) p5, ( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5) c_nb_rep, ((SELECT count(*) FROM ESP_INSCRIPTION WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND  ( ESP_INSCRIPTION.CODE_CL = ESP_EVAL_CL_MODULE.CODE_CL))-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) NB_PALIER6    ,   ESP_CRITERE_EVAL.LIB_CRITERE, (SELECT count(*)  FROM ESP_INSCRIPTION   WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND   ( trim(ESP_INSCRIPTION.CODE_CL) = trim(ESP_EVAL_CL_MODULE.CODE_CL))) c_nb_et, ESP_MODULE.DESIGNATION, ESP_ENSEIGNANT.NOM_ENS,   ESP_EVAL_CL_MODULE.ID_ENS FROM ESP_EVAL_CL_MODULE, ESP_CRITERE_EVAL, ESP_MODULE,   ESP_ENSEIGNANT  WHERE  ( substr(ESP_CRITERE_EVAL.CODE_CRITERE,1,3) = substr(ESP_EVAL_CL_MODULE.CODE_CRITERE,1,3 )) and  ( trim(ESP_EVAL_CL_MODULE.CODE_MODULE) = trim(ESP_MODULE.CODE_MODULE )) and  ( trim(ESP_EVAL_CL_MODULE.ID_ENS) = trim(ESP_ENSEIGNANT.ID_ENS )) and  ( ( ESP_EVAL_CL_MODULE.ANNEE_DEB = '" + Annee + "' ) and ESP_ENSEIGNANT.ID_ENS='" + ID_ENS + "' and ESP_CRITERE_EVAL.LIB_CRITERE not like 'Travail%' and ESP_MODULE.CODE_MODULE='" + CODE_MODULE + "' and ESP_EVAL_CL_MODULE.CODE_CL='" + CODE_CL + "' and ((SELECT count(*) FROM ESP_INSCRIPTION  WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND ( trim(ESP_INSCRIPTION.CODE_CL) =trim(ESP_EVAL_CL_MODULE.CODE_CL)) )-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) >=0)", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }


        public DataTable GetListNotes4(string CODE_CL, string CODE_MODULE, string ID_ENS, string Annee)
        {


            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT round((ESP_EVAL_CL_MODULE.NB_PALIER1/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) TI, round((ESP_EVAL_CL_MODULE.NB_PALIER2/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) I, round((ESP_EVAL_CL_MODULE.NB_PALIER3/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) S, round((ESP_EVAL_CL_MODULE.NB_PALIER4/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) B,  round((ESP_EVAL_CL_MODULE.NB_PALIER5/(( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))),2) TB,   ESP_CRITERE_EVAL.LIB_CRITERE crit FROM ESP_EVAL_CL_MODULE, ESP_CRITERE_EVAL, ESP_MODULE,   ESP_ENSEIGNANT  WHERE  ( substr(ESP_CRITERE_EVAL.CODE_CRITERE,1,3) = substr(ESP_EVAL_CL_MODULE.CODE_CRITERE,1,3 )) and  ( trim(ESP_EVAL_CL_MODULE.CODE_MODULE) = trim(ESP_MODULE.CODE_MODULE )) and  ( trim(ESP_EVAL_CL_MODULE.ID_ENS) = trim(ESP_ENSEIGNANT.ID_ENS )) and  ( ( ESP_EVAL_CL_MODULE.ANNEE_DEB = '" + Annee + "' ) and ESP_ENSEIGNANT.ID_ENS='" + ID_ENS + "' and ESP_MODULE.CODE_MODULE='" + CODE_MODULE + "' and ESP_EVAL_CL_MODULE.CODE_CL='" + CODE_CL + "' and ((SELECT count(*) FROM ESP_INSCRIPTION  WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND ( trim(ESP_INSCRIPTION.CODE_CL) =trim(ESP_EVAL_CL_MODULE.CODE_CL)) )-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) >=0) and ESP_CRITERE_EVAL.LIB_CRITERE like 'Global%' ", mySqlConnection);
                DataTable myDataTable = new DataTable();


                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }


        public DataTable GetListNotes5(string ID_ENS,string Annee)
        {


            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand(" select DISTINCT CODE_CL from ESP_MODULE_PANIER_CLASSE_SAISO where ANNEE_DEB='" + Annee + "' and ID_ENS='" + ID_ENS + "'", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }

        public DataTable GetListNotes6(string ID_ENS, string CODE_CL,string Annee)
        {



             using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT ESP_MODULE.CODE_MODULE, ESP_MODULE.DESIGNATION FROM ESP_MODULE_PANIER_CLASSE_SAISO INNER JOIN  ESP_ENSEIGNANT ON ESP_MODULE_PANIER_CLASSE_SAISO.ID_ENS = ESP_ENSEIGNANT.ID_ENS INNER JOIN  ESP_MODULE ON ESP_MODULE_PANIER_CLASSE_SAISO.CODE_MODULE = ESP_MODULE.CODE_MODULE WHERE (ESP_MODULE_PANIER_CLASSE_SAISO.ANNEE_DEB ='" + Annee  + "') AND (ESP_MODULE_PANIER_CLASSE_SAISO.CODE_CL = '" + CODE_CL  + "') AND   (ESP_MODULE_PANIER_CLASSE_SAISO.ID_ENS = '" + ID_ENS  + "' or ESP_MODULE_PANIER_CLASSE_SAISO.ID_ENS2 = '" + ID_ENS  + "' or ESP_MODULE_PANIER_CLASSE_SAISO.ID_ENS3 = '" + ID_ENS  + "' or ESP_MODULE_PANIER_CLASSE_SAISO.ID_ENS4 = '" + ID_ENS + "' or ESP_MODULE_PANIER_CLASSE_SAISO.ID_ENS5 = '" + ID_ENS + "') ", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }


        public DataTable GetListNotes7(string CODE_CL, string CODE_MODULE, string ID_ENS, string Annee)
        {


             using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT ( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5) c_nb_rep FROM ESP_EVAL_CL_MODULE, ESP_CRITERE_EVAL, ESP_MODULE,   ESP_ENSEIGNANT  WHERE  ( substr(ESP_CRITERE_EVAL.CODE_CRITERE,1,3) = substr(ESP_EVAL_CL_MODULE.CODE_CRITERE,1,3 )) and  ( trim(ESP_EVAL_CL_MODULE.CODE_MODULE) = trim(ESP_MODULE.CODE_MODULE )) and  ( trim(ESP_EVAL_CL_MODULE.ID_ENS) = trim(ESP_ENSEIGNANT.ID_ENS )) and  ( ( ESP_EVAL_CL_MODULE.ANNEE_DEB = '" + Annee + "' ) and ESP_ENSEIGNANT.ID_ENS='" + ID_ENS + "' and ESP_CRITERE_EVAL.LIB_CRITERE not like 'Travail%' and ESP_MODULE.CODE_MODULE='" + CODE_MODULE + "' and ESP_EVAL_CL_MODULE.CODE_CL='" + CODE_CL + "' and ((SELECT count(*) FROM ESP_INSCRIPTION  WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND ( trim(ESP_INSCRIPTION.CODE_CL) =trim(ESP_EVAL_CL_MODULE.CODE_CL)) )-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) >=0) and ESP_CRITERE_EVAL.LIB_CRITERE like 'Global%'", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }


        public DataTable GetListNotes8(string CODE_CL, string CODE_MODULE, string ID_ENS,string Annee)
        {


             using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT (SELECT count(*)  FROM ESP_INSCRIPTION   WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "') AND   ( trim(ESP_INSCRIPTION.CODE_CL) = trim(ESP_EVAL_CL_MODULE.CODE_CL))) c_nb_et FROM ESP_EVAL_CL_MODULE, ESP_CRITERE_EVAL, ESP_MODULE,   ESP_ENSEIGNANT  WHERE  ( substr(ESP_CRITERE_EVAL.CODE_CRITERE,1,3) = substr(ESP_EVAL_CL_MODULE.CODE_CRITERE,1,3 )) and  ( trim(ESP_EVAL_CL_MODULE.CODE_MODULE) = trim(ESP_MODULE.CODE_MODULE )) and  ( trim(ESP_EVAL_CL_MODULE.ID_ENS) = trim(ESP_ENSEIGNANT.ID_ENS )) and  ( ( ESP_EVAL_CL_MODULE.ANNEE_DEB = '" + Annee + "' ) and ESP_ENSEIGNANT.ID_ENS='" + ID_ENS + "' and ESP_CRITERE_EVAL.LIB_CRITERE not like 'Travail%' and ESP_MODULE.CODE_MODULE='" + CODE_MODULE + "' and ESP_EVAL_CL_MODULE.CODE_CL='" + CODE_CL + "' and ((SELECT count(*) FROM ESP_INSCRIPTION  WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND ( trim(ESP_INSCRIPTION.CODE_CL) =trim(ESP_EVAL_CL_MODULE.CODE_CL)) )-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) >=0) and ESP_CRITERE_EVAL.LIB_CRITERE like 'Global%'", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }

        public DataTable GetTauxRep(string CODE_CL, string CODE_MODULE, string ID_ENS, string Annee)
        {


             using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("SELECT concat(round(((( ESP_EVAL_CL_MODULE.NB_PALIER1+ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ESP_EVAL_CL_MODULE.NB_PALIER4+ESP_EVAL_CL_MODULE.NB_PALIER5))/((SELECT count(*) FROM ESP_INSCRIPTION WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "') AND ( trim(ESP_INSCRIPTION.CODE_CL) = trim(ESP_EVAL_CL_MODULE.CODE_CL))))*100)),'%') taux_rep FROM ESP_EVAL_CL_MODULE, ESP_CRITERE_EVAL, ESP_MODULE, ESP_ENSEIGNANT  WHERE  ( substr(ESP_CRITERE_EVAL.CODE_CRITERE,1,3) = substr(ESP_EVAL_CL_MODULE.CODE_CRITERE,1,3 )) and  ( trim(ESP_EVAL_CL_MODULE.CODE_MODULE) = trim(ESP_MODULE.CODE_MODULE )) and  ( trim(ESP_EVAL_CL_MODULE.ID_ENS) = trim(ESP_ENSEIGNANT.ID_ENS )) and  ( ( ESP_EVAL_CL_MODULE.ANNEE_DEB = '" + Annee + "' ) and ESP_ENSEIGNANT.ID_ENS='" + ID_ENS + "' and ESP_CRITERE_EVAL.LIB_CRITERE not like 'Travail%' and ESP_MODULE.CODE_MODULE='" + CODE_MODULE + "' and ESP_EVAL_CL_MODULE.CODE_CL='" + CODE_CL + "' and ((SELECT count(*) FROM ESP_INSCRIPTION  WHERE ( ESP_INSCRIPTION.ANNEE_DEB ='" + Annee + "' ) AND ( trim(ESP_INSCRIPTION.CODE_CL) =trim(ESP_EVAL_CL_MODULE.CODE_CL)) )-(ESP_EVAL_CL_MODULE.NB_PALIER1+ ESP_EVAL_CL_MODULE.NB_PALIER2+ ESP_EVAL_CL_MODULE.NB_PALIER3+ ESP_EVAL_CL_MODULE.NB_PALIER4 + ESP_EVAL_CL_MODULE.NB_PALIER5 )) >=0) and ESP_CRITERE_EVAL.LIB_CRITERE like 'Global%'", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }




       //Statistique Anglais pour l'annee 2014/2015
        public DataTable GetNbre_niv_langue(string code_cl)
        {
            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();
                OracleDataAdapter adapter = new OracleDataAdapter();
                adapter.SelectCommand = new OracleCommand("select  NIV_ACQUIS_ANGLAIS,count(*) from esp_inscription where ANNEE_DEB='2014' AND CODE_CL LIKE '"+code_cl+"%' group by NIV_ACQUIS_ANGLAIS ", mySqlConnection);
                DataTable myDataTable = new DataTable();

                try
                {
                    adapter.Fill(myDataTable);

                }
                finally
                {
                    mySqlConnection.Close();
                }
                return myDataTable;
            }
        }

        public string returnCode_cl()
        {
            string lib = " ";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                string cmdQuery = "select distinct CODE_CL from esp_inscription where annee_DEB='2014' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

       //afficher liste charguia
        public DataTable Afficher_list_charguia()
        {

            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select a.code_cl Classe,count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl where annee_deb='2014' and a.code_cl between '1' and '5t'  and site like 'Charguia' group by site,a.code_cl  order BY fn_tri_classe(classe)", con);
           // OracleCommand cmd = new OracleCommand("", con);
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

       //afficher liste gazela

        public DataTable Afficher_list_gazela()
        {

            DataTable dt = new DataTable();
           // string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


           OracleCommand cmd = new OracleCommand("select a.code_cl Classe,count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl where annee_deb='2014' and a.code_cl between '1' and '5t'  and lower(site) like 'ghazala'  group by site,a.code_cl   order BY fn_tri_classe(a.CODE_CL)", con);
           // OracleCommand cmd = new OracleCommand("select a.code_cl Classe,count(*) Effectif FROM ESP_INSCRIPTION a left join scoesp09.classes1516 b on a.code_cl=b.code_cl left join scoesp09.ESP_ETUDIANT e on a.id_et=e.ID_ET where annee_deb='2015' and lower(e.ETAT)='a' and lower(a.code_cl) between '1' and '5t'  and lower(site) like 'ghazala%'  group by site,a.code_cl  order BY fn_tri_classe(a.CODE_CL)",con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public string totalch()
        {
            string lib = " ";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                string cmdQuery = "select count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl where annee_deb='2014' and a.code_cl between '1' and '5t'  and site like 'Charguia' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public string totalgh()
        {
            string lib = " ";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                string cmdQuery = "select count(*) Effectif FROM ESP_INSCRIPTION a left join classes1516 b on a.code_cl=b.code_cl where annee_deb='2014' and a.code_cl between '1' and '5t'  and site like 'Ghazala'";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }



        public DataTable Afficher_listPARniv()
        {

            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) as Nouveaux_inscrits,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) as Redoublants_inscrits,sum(case when a.type_insc='I' then 1 else 0 end) as Inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) as Nouveaux_Pre_inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) as Redoublants_Pre_inscrits,sum(case when a.type_insc='P' then 1 else 0 end) as Pre_Inscrits,count(*) as NB from SCOESP09.ESP_INSCRIPTION a left join SCOESP09.ESP_INSCRIPTION b on a.id_et=b.id_et and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t' group by c.Site,a.code_cl order by case when substr(a.code_cl,length(a.code_cl),1)   not between '0' and '9' then a.code_cl||'100' when substr(a.code_cl,length(a.code_cl)-1,1) not between '0' and '9' then substr(a.code_cl, 1,length(a.code_cl)-1)||to_char(to_number(substr(a.code_cl, length(a.code_cl),1))+100) when substr(a.code_cl,length(a.code_cl)-1,1)     between '0' and '9' then substr(a.code_cl, 1,length(a.code_cl)-2)||to_char(to_number(substr(a.code_cl, length(a.code_cl)-1,2))+100)  else a.code_cl||'100' end", con);
            //OracleCommand cmd = new OracleCommand("", con);
          
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }



        public DataTable Afficher_listetudPARniv()
        {

            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);
            OracleCommand cmd = new OracleCommand("select a.Site,Niveau,sum(Nouveaux_Inscrits) Nouveaux_Inscrits,sum(Redoublants_Inscrits) Redoublants_Inscrits,sum(Inscrits) Inscrits,sum(Nouveaux_Pre_inscrits) Nouveaux_Pre_Inscrits,sum(Redoublants_Pre_Inscrits) Redoublants_Pre_Inscrits,sum(Pré_Inscrits) Pré_Inscrits, sum(Inscrits) + sum(Pré_Inscrits) total from (  select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_inscrits,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_inscrits,sum(case when a.type_insc='I' then 1 else 0 end) Inscrits, sum(case when a.type_insc='P' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_Pre_inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_Pre_inscrits,sum(case when a.type_insc='P' then 1 else 0 end) Pré_Inscrits,count(*) NB  from scoesp09.ESP_INSCRIPTION a left join SCOESP09.ESP_INSCRIPTION b on a.id_et=b.id_et  and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join SCOESP09.ESP_ETUDIANT e on a.id_et=e.id_et left join SCOESP09.classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and lower(e.ETAT) ='a' and a.code_cl between '1' and '5t' group by c.Site,a.code_cl)  a left join SCOESP09.classes1516 b on a.classe=b.code_cl group by a.site,niveau order by a.site,niveau", con);
            
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

       //stat par site

        public DataTable Afficher_stat_site()
        {
            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);

            OracleCommand cmd = new OracleCommand("select Site,sum(Nouveaux_Inscrits) Nouveaux_Inscrits,sum(Redoublants_Inscrits) Redoublants_Inscrits,sum(Inscrits) Inscrits,sum(Nouveaux_Pre_inscrits) Nouveaux_Pre_Inscrits,sum(Redoublants_Pre_Inscrits) Redoublants_Pre_Inscrits,sum(Pré_Inscrits) Pré_Inscrits,sum(Inscrits)+sum(Pré_Inscrits) Total from ( select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_inscrits,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_inscrits,sum(case when a.type_insc='I' then 1 else 0 end) Inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_Pre_inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_Pre_inscrits,sum(case when a.type_insc='P' then 1 else 0 end) Pré_Inscrits,count(*) NB from SCOESP09.ESP_INSCRIPTION a left join SCOESP09.ESP_INSCRIPTION b on a.id_et=b.id_et and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t' group by c.Site,a.code_cl) a group by site order by site", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

       //1
        public DataTable Afficher_total_par_CLASSE()
        {
            DataTable dt = new DataTable();
           // string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select sum(case when a.type_insc='I' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) as Nouveaux_inscrits,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) as Redoublants_inscrits,sum(case when a.type_insc='I' then 1 else 0 end) as Inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) as Nouveaux_Pre_inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) as Redoublants_Pre_inscrits,sum(case when a.type_insc='P' then 1 else 0 end) as Pre_Inscrits,count(*) as NB from SCOESP09.ESP_INSCRIPTION a left join SCOESP09.ESP_INSCRIPTION b on a.id_et=b.id_et and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t'", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

       //2
        public DataTable Afficher_total_par_niv()
        {
            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select count(*) NB_Classes,sum(Nouveaux_Inscrits) Nouveaux_Inscrits,sum(Redoublants_Inscrits) Redoublants_Inscrits,sum(Inscrits) Inscrits,sum(Nouveaux_Pre_inscrits) Nouveaux_Pre_Inscrits,sum(Redoublants_Pre_Inscrits) Redoublants_Pre_Inscrits,sum(Pré_Inscrits) Pré_Inscrits,sum(Inscrits) + sum(Pré_Inscrits) total from ( select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_inscrits,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_inscrits,sum(case when a.type_insc='I' then 1 else 0 end) Inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_Pre_inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_Pre_inscrits,sum(case when a.type_insc='P' then 1 else 0 end) Pré_Inscrits,count(*) NB from SCOESP09.ESP_INSCRIPTION a left join SCOESP09.ESP_INSCRIPTION b on a.id_et=b.id_et  and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t'group by c.Site,a.code_cl)  a left join classes1516 b on a.classe=b.code_cl", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        //3
        public DataTable Afficher_total_DES_TOTAUX()
        {
            DataTable dt = new DataTable();
           // string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select sum(Nouveaux_Inscrits) Nouveaux_Inscrits,sum(Redoublants_Inscrits) Redoublants_Inscrits,sum(Inscrits) Inscrits,sum(Nouveaux_Pre_inscrits) Nouveaux_Pre_Inscrits,sum(Redoublants_Pre_Inscrits) Redoublants_Pre_Inscrits,sum(Pré_Inscrits) Pré_Inscrits,sum(Inscrits)+sum(Pré_Inscrits) Total from ( select c.Site,a.code_cl Classe,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_inscrits,sum(case when a.type_insc='I' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_inscrits,sum(case when a.type_insc='I' then 1 else 0 end) Inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)>substr(nvl(b.code_cl,'0'),1,1) then 1 else 0 end) Nouveaux_Pre_inscrits,sum(case when a.type_insc='P' and substr(a.code_cl,1,1)<=substr(b.code_cl,1,1) then 1 else 0 end) Redoublants_Pre_inscrits,sum(case when a.type_insc='P' then 1 else 0 end) Pré_Inscrits,count(*) NB from SCOESP09.ESP_INSCRIPTION a left join SCOESP09.ESP_INSCRIPTION b on a.id_et=b.id_et and to_number(a.annee_deb)=to_number(b.annee_deb)+1 left join classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t' group by c.Site,a.code_cl) ", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        //Répartition des étudiants par Niveau et Pôle 
        public DataTable ReparEtudiantParPole()
        {
            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            //OracleCommand cmd = new OracleCommand("select substr(a.code_cl,1,1) Niveau, sum(case when lower(site) like 'ghazala'  then 1 else 0 end) as Pôle_TIC,sum(case when lower(site) like 'charguia' then 1 else 0 end) as Pôle_GCEM,count(*) Total from SCOESP09.ESP_INSCRIPTION a left join classes1516 b on a.code_cl = b.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t' group by substr(a.code_cl,1,1) order by substr(a.code_cl,1,1)", con);

            OracleCommand cmd = new OracleCommand(" select substr(a.code_cl,1,1) Niveau, sum(case when lower(site) like 'ghazala'  then 1 else 0 end) as Pôle_TIC,sum(case when lower(site) like 'charguia' then 1 else 0 end) as Pôle_GCEM,count(*) Total from SCOESP09.ESP_INSCRIPTION a left join SCOESP09.classes1516 b on a.code_cl = b.code_cl left join scoesp09.ESP_ETUDIANT e on a.ID_ET=e.ID_ET where a.annee_deb='2015' and lower(e.ETAT)='a' and lower(a.code_cl) between '1' and '5t'group by substr(a.code_cl,1,1) order by substr(a.code_cl,1,1)", con);
           
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
        //total Répartition des étudiants par Niveau et Pôle
        public DataTable titalReparEtudiantParPole()
        {
            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("select sum(case when lower(site) like 'ghazala'  then 1 else 0 end) as Pôle_TIC,sum(case when lower(site) like 'charguia' then 1 else 0 end) as Pôle_GCEM,count(*) Total from SCOESP09.ESP_INSCRIPTION a left join classes1516 b on a.code_cl = b.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t'", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        //Répartition des Classes par Niveau etPôle
        public DataTable ReparclasseParPole()
        {
            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand(" select substr(a.code_cl,1,1) Niveau, sum(case when lower(site) like 'ghazala'  then 1 else 0 end) as Pôle_TIC,sum(case when lower(site) like 'charguia' then 1 else 0 end) as Pôle_GCEM,count(*) Total from(select  site, a.code_cl,count(*) Effectif from SCOESP09.ESP_INSCRIPTION a left join classes1516 b on a.code_cl = b.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t' group by site, a.code_cl) a group by substr(a.code_cl,1,1) order by substr(a.code_cl,1,1)", con);
            //OracleCommand cmd = new OracleCommand("select substr(a.code_cl,1,1) Niveau, sum(case when lower(site) like 'ghazala%'  then 1 else 0 end) as Pôle_TIC,sum(case when lower(site) like 'charguia%' then 1 else 0 end) as Pôle_GCEM,count(*) Total from(select  site, a.code_cl,count(*) Effectif from scoesp09.ESP_INSCRIPTION a left join SCOESP09.classes1516 b on a.code_cl = b.code_cl left join SCOESP09.ESP_ETUDIANT e on a.id_et=e.id_et where a.annee_deb='2015' and lower(e.etat)='a' and lower(a.code_cl) between '1' and '5t' group by site, a.code_cl) a group by substr(a.code_cl,1,1) order by substr(a.code_cl,1,1 )", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        //total Répartition des Classes par Niveau etPôle
        public DataTable totalReparclasseParPole()
        {
            DataTable dt = new DataTable();
            //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("SELECT sum(case when lower(site) like 'ghazala'  then 1 else 0 end) as Pôle_TIC,sum(case when lower(site) like 'charguia' then 1 else 0 end) as Pôle_GCEM,count(*) Total from(select  site, a.code_cl,count(*) Effectif from SCOESP09.ESP_INSCRIPTION a left join classes1516 b on a.code_cl = b.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5t' group by site, a.code_cl) a", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
       //rep nouv inscrits
        public DataTable Rep_nouv_inscrits()
        {
            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


           // OracleCommand cmd = new OracleCommand("select substr(code_cl,1,1) Niveau,sum(TIC) TIC,sum(EM) EM,sum(GC) GC,sum(TIC)+sum(EM)+sum(GC) Total from (select a.code_cl,(case when LOWER(specialite)  like 'info' then 1 else 0 end) TIC,(case when lower(specialite)  like 'em' then 1 else 0 end) EM,(case when LOWER(specialite)  like 'gc' then 1 else 0 end) GC from SCOESP09.ESP_INSCRIPTION a  left join classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and a.code_cl between '1' and '5T' and  a.id_et like '14%') group by substr(code_cl,1,1) order by Niveau", con);

            OracleCommand cmd = new OracleCommand(" select substr(code_cl,1,1) Niveau,sum(TIC) TIC,sum(EM) EM,sum(GC) GC,sum(TIC)+sum(EM)+sum(GC) Total from (select a.code_cl,(case when LOWER(specialite)  like 'info' then 1 else 0 end) TIC,(case when lower(specialite)  like 'em' then 1 else 0 end) EM,(case when LOWER(specialite)  like 'gc' then 1 else 0 end) GC from SCOESP09.ESP_INSCRIPTION a  left join scoesp09.classes1516 c on a.code_cl = c.code_cl left join SCOESP09.ESP_ETUDIANT e on a.id_et=e.id_et where a.annee_deb='2015'  and lower(e.etat) ='a' and lower(a.code_cl) between '1' and '5t' and  a.id_et like '15%')group by substr(code_cl,1,1) order by Niveau", con);
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
       //*****************************PLAN D'ETUDES**************************************************************************************

       //liste par specialite
        public DataTable Afficher_list_ens_affecter(string num_semestre)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select Site,Specialite,Classe,sum(case when enseignant is not null then 1 else 0 end) Enseignants_Affectes,sum(case when enseignant is null then 1 else 0 end) Enseignants_Non_affectes, count(*) Total_Modules  from (SELECT up,d.Site,d.specialite,d.niveau,a.CODE_MODULE,designation module,a.CODE_CL Classe,ANNEE_DEB,a.ID_ENS,a.ID_ENS2, case when NOM_ENS is null or NOM_ENS ='A AFFECTER' then ''else nom_ens end   Enseignant,Charge_p1,Charge_p2,num_semestre Semestre FROM SCOESP09.ESP_MODULE_PANIER_CLASSE_SAISO a left join SCOESP09.ESP_ENSEIGNANT b on a.ID_ENS=b.ID_ENS left join SCOESP09.ESP_MODULE c on a.code_module=c.code_module inner join SCOESP09.classes1516 d on a.code_cl=d.code_cl where a.annee_deb='2014'  " + num_semestre + "  ) group by site,specialite,classe order by site,specialite,Classe", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

       //liste par UP
        public DataTable Afficher_list_ens_UPaffecter(string num_semestre)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);
            OracleCommand cmd = new OracleCommand("select Site,up,Classe,sum(case when enseignant is not null then 1 else 0 end) Enseignants_Affectes,sum(case when enseignant is null then 1 else 0 end) Enseignants_Non_affectes,count(*) Total_Modules from (SELECT up,d.Site,d.specialite,d.niveau,a.CODE_MODULE,designation module,a.CODE_CL Classe,ANNEE_DEB,a.ID_ENS,a.ID_ENS2, case when NOM_ENS is null or NOM_ENS ='A AFFECTER' then ''else nom_ens end  Enseignant,Charge_p1,Charge_p2,num_semestre Semestre FROM SCOESP09.ESP_MODULE_PANIER_CLASSE_SAISO a left join SCOESP09.ESP_ENSEIGNANT b on a.ID_ENS=b.ID_ENS left join SCOESP09.ESP_MODULE c on a.code_module=c.code_module inner join classes1516 d on a.code_cl=d.code_cl where a.annee_deb='2014'  " + num_semestre + "  )group by site,up,classe order by site,up,classe", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        //liste par non ens saisis
        public DataTable Afficher_ens_saisis(string num_semes)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand(" select Site,sum(case when enseignant is not null then 1 else 0 end) Enseignants_Affectes,sum(case when enseignant is null then 1 else 0 end) Enseignants_Non_affectes, count(*) Total_Modules from (SELECT up,d.Site,d.specialite,d.niveau,a.CODE_MODULE,designation module,a.CODE_CL Classe,ANNEE_DEB,a.ID_ENS,a.ID_ENS2,case when NOM_ENS is null or NOM_ENS ='A AFFECTER' then ''else nom_ens end   Enseignant,Charge_p1,Charge_p2,num_semestre Semestre FROM SCOESP09.ESP_MODULE_PANIER_CLASSE_SAISO a left join SCOESP09.ESP_ENSEIGNANT b on a.ID_ENS=b.ID_ENS left join SCOESP09.ESP_MODULE c on a.code_module=c.code_module inner join classes1516 d on a.code_cl=d.code_cl where a.annee_deb='2014' "+num_semes+")group by site order by site", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
        //19_09_2015 STATISTIQUES DES ETUDIANTS INSCRITS
         //*****get annee univer
        public DataTable List_etud_2015()
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand("SELECT distinct ANNEE_DEB ||'-'|| to_number(nvl(annee_deb,'')+1) ANNEE_UNIVERSITAIRE FROM ESP_INSCRIPTION where to_number(annee_deb)>2009 order by 1 desc", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
       //get site
        public DataTable bind_site(string annee)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT site FROM scoesp09.CLASSES1516 a where a.ANNEE_DEB=" + annee + " union select 'ALL' from dual order by site";

           OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            return dt;
        }
       //bind niveau
        public DataTable bind_niveau(string site)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = " select  distinct substr(code_cl,1,1 ) as niveau from classes1516 a where "+site+" union select 'ALL' from dual order by niveau ";

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            // con.Close();
            return dt;
        }
       //bind classes
        public DataTable bind_classes(string niv)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select code_cl from classes1516 a where " + niv + " union select 'ALL' from dual  ";
            //union select 'ALL' from dual

                              
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            // con.Close();
            return dt;
        }

        public DataTable bind_tous_etud(string code_cl)
        {
            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;

            cmd.CommandText = " select distinct row_number() over (partition by a.code_cl order by nom_et, pnom_et) NO, a.code_cl,a.id_et,nom_et as Nom, pnom_et as Prenom, to_char(DATE_NAIS_ET,'dd/MM/yyyy') as Date_de_naissance,LIEU_NAIS_ET as  Lieu_de_naissance,NUM_CIN_PASSEPORT as Cin_Passeport,d.LIB_NOME as Nature_Bac,to_char(DATE_BAC,'dd/MM/yyyy') as Date_Bac, NUM_BAC_ET as Num_Bac, b.ETAB_ORIGINE from   scoesp09.ESP_INSCRIPTION a left join scoesp09.ESP_ETUDIANT b on  a.id_et=b.id_et left join scoesp09.classes1516 c on a.code_cl=c.code_cl left join code_nomenclature d on b.NATURE_BAC=d.CODE_NOME and d.code_str='02' or b.ETAB_ORIGINE=d.CODE_NOME and d.code_str='04'  where " + code_cl + "  order by fn_tri_classe(a.code_cl),NO";
            
            OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);

            // con.Close();
            return dt;
        }

       //bind num_semestre
       public DataTable bind_num_semestre()
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "select distinct TO_char(a.NUM_SEMESTRE) as NUM_SEMESTRE from scoesp09.ESP_MODULE_PANIER_CLASSE_SAISO a  union select 'ALL' from dual";

           OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

           // con.Close();
            return dt;
        }
       /* get arabic data*/
       public DataTable testarabic()
       {

           DataTable dt = new DataTable();
         string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
          OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();
           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           
        //   cmd.CommandText = "select * from arabe";
           cmd.CommandText = "select C1  ب  from arabe ";

           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);
          // con.Close();
           return dt;
       }

       //************************************************2015 ****statistique des niveau de langues ******2015**********************************************************
       public DataTable Affich_list_etud_niveau_ang()
       {

           DataTable dt = new DataTable();
           string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();
           //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


           //OracleCommand cmd = new OracleCommand(" select Niveau,sum(Niveau_A1) Niveau_A1,sum(Niveau_A2) Niveau_A2,sum(Niveau_B1) Niveau_B1,sum(Niveau_B2) Niveau_B2,sum(Niveau_A1) + sum(Niveau_A2) + sum(Niveau_B1) + sum(Niveau_B2) AS total from (select a.code_cl Classe,sum(case when  lower(e.niveau_courant_ANG)='a1'  then 1 else 0 end) as Niveau_A1,sum(case when  lower(e.e.niveau_courant_ANG)='a2'   then 1 else 0 end) as Niveau_A2,sum(case when  lower(e.e.niveau_courant_ANG)='b1'   then 1 else 0 end) as Niveau_B1,sum(case when  lower(e.e.niveau_courant_ANG)='b2'   then 1 else 0 end) as Niveau_B2 from scoesp09.ESP_INSCRIPTION a left join SCOESP09.ESP_ETUDIANT e on a.id_et=e.id_et left join SCOESP09.classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and lower(e.ETAT) ='a' and lower(a.code_cl) between '1' and '4t' and lower(c.SITE) like 'ghazala' group by a.code_cl) a inner join SCOESP09.classes1516 b on a.classe=b.code_cl group by niveau order by niveau", con);
           OracleCommand cmd = new OracleCommand("select Niveau,sum(Niveau_A1) Niveau_A1,sum(Niveau_A2) Niveau_A2,sum(Niveau_B1) Niveau_B1,sum(Niveau_B2) Niveau_B2,sum(Niveau_A1) + sum(Niveau_A2) + sum(Niveau_B1) + sum(Niveau_B2) AS total from (select a.code_cl Classe,sum(case when  lower(e.niveau_courant_ANG)='a1' or ( lower(a.NIV_ACQUIS_ANGLAIS)='a1' AND code_cl like '5%') then 1 else 0 end) as Niveau_A1,sum(case when  lower(e.e.niveau_courant_ANG)='a2' or ( lower(a.NIV_ACQUIS_ANGLAIS)='a2' AND code_cl like '5%')  then 1 else 0 end) as Niveau_A2,sum(case when  lower(e.e.niveau_courant_ANG)='b1' or ( lower(a.NIV_ACQUIS_ANGLAIS)='b1' AND code_cl like '5%')  then 1 else 0 end) as Niveau_B1,sum(case when  lower(e.e.niveau_courant_ANG)='b2' or ( lower(a.NIV_ACQUIS_ANGLAIS)='b2' AND code_cl like '5%')  then 1 else 0 end) as Niveau_B2 from scoesp09.ESP_INSCRIPTION a left join SCOESP09.ESP_ETUDIANT e on a.id_et=e.id_et left join SCOESP09.classes1516 c on a.code_cl = c.code_cl where a.annee_deb='2014' and lower(e.ETAT) ='a' and lower(a.code_cl) between '1' and '5t' and lower(c.SITE) like 'ghazala' group by a.code_cl) a inner join SCOESP09.classes1516 b on a.classe=b.code_cl group by niveau order by niveau", con);


           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);

           con.Close();
           return dt;
       }

       public DataTable Affich_list_etud_niveau_FR()
       {

           DataTable dt = new DataTable();
           string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();
           //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


           OracleCommand cmd = new OracleCommand("     ", con);


           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);

           con.Close();
           return dt;
       }
     //  *******************************************************************MODIFICATION NIVEAU 2014 ET 2015************************************************************************

       //public DataTable bind_niveau_1516()
       //{

       //    DataTable dt = new DataTable();
       //    string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
       //    OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
       //    con.Open();
       //    OracleCommand cmd = new OracleCommand();
       //    cmd.Connection = con;
       //    cmd.CommandText = " select  distinct substr(code_cl,1,1 ) as niveau from classes1516 where annee_deb='2014' order by niveau ";

       //    OracleDataAdapter od = new OracleDataAdapter(cmd);
       //    od.Fill(dt);

       //    // con.Close();
       //    return dt;
       //}

       //public DataTable bind_classes_1516(string niv)
       //{

       //    DataTable dt = new DataTable();
       //    string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
       //    OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
       //    con.Open();
       //    //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


       //    OracleCommand cmd = new OracleCommand();
       //    cmd.Connection = con;
       //    //cmd.CommandText = "select distinct a.code_cl from ESP_MODULE_PANIER_CLASSE_SAISO a left join classes1516 c on a.code_cl =c.code_cl  where a.code_cl like  '" + niv + "%' and lower(c.site) like 'ghazala' order BY fn_tri_classe(a.code_cl) ";
       //    cmd.CommandText = "select distinct a.code_cl from esp_inscription a left join classes1516 c on a.code_cl =c.code_cl  where a.code_cl like  '"+niv+"%' and a.annee_deb='2014' order BY fn_tri_classe(a.code_cl) ";
           
       //    //union select 'ALL' from dual


       //    OracleDataAdapter od = new OracleDataAdapter(cmd);
       //    od.Fill(dt);
       //    // con.Close();
       //    return dt;
       //}


       //modif requete
       public DataTable bind_niveau_1516(string ANNEE_DEB)
       {

           DataTable dt = new DataTable();

           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();
           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           cmd.CommandText = " select  distinct substr(code_cl,1,1 ) as niveau from esp_inscription where annee_deb='" + ANNEE_DEB + "' and substr(code_cl,1,1 ) in ('1','2','3','4','5') order by niveau  ";

           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);

           // con.Close();
           return dt;
       }

       public DataTable bind_classes_1516(string niv,string annee_deb)
       {

           DataTable dt = new DataTable();
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);

           con.Open();
           
           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           
           cmd.CommandText = "select distinct a.code_cl from esp_inscription a  where a.code_cl like  '" + niv + "%' and a.annee_deb='"+annee_deb+"' order BY fn_tri_classe(a.code_cl) ";

           //union select 'ALL' from dual


           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);
           // con.Close();
           return dt;
       }

       public DataTable bind_Nom_Ens(string code_classe,string annee_deb)
       {

           DataTable dt = new DataTable();
          
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();
           //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);


           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           cmd.CommandText = "select (nom_et ||' ' ||pnom_et) as NOM from esp_etudiant e left join esp_inscription a on a.ID_ET=e.id_et where a.code_cl like '" + code_classe + "' and a.annee_deb='" + annee_deb + "' ";
           //union select 'ALL' from dual


           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);
           // con.Close();
           return dt;
       }

       public DataTable bind_niveau_ang_et(string nom_et,string code_clcl,string annee_deb)
       {

           DataTable dt = new DataTable();
           
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();
           
           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           cmd.CommandText = "select distinct a.id_et,(nom_et|| ' '||pnom_et) as NOM,a.NIV_ACQUIS_ANGLAIS,a.NIV_ACQUIS_FRANCAIS from esp_etudiant e left join esp_inscription a on a.ID_ET=e.id_et where a.annee_deb='" + annee_deb + "' and (nom_et|| ' '||pnom_et) like '" + nom_et + "' and code_cl like '" + code_clcl + "'";


           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);
           // con.Close();
           return dt;
       }

       //public string Return_id_et(string code, string name)
       //{

          
       //    string lib = "";

       //    using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
       //    {
       //        mySqlConnection.Open();

       //        string cmdQuery = "select DISTINCT a.id_et from esp_etudiant e left join esp_inscription a on a.id_et=e.id_et  where upper(nom_et || ' ' ||pnom_et) like '" + name + "' and annee_deb='2014' and upper(code_cl) like '" + code + "'";

       //        OracleCommand myCommand = new OracleCommand(cmdQuery);
       //        myCommand.Connection = mySqlConnection;
       //        myCommand.CommandType = CommandType.Text;
       //        lib = myCommand.ExecuteScalar().ToString();
       //        mySqlConnection.Close();
       //    }
       //    return lib;


       //}

       public DataTable bind_list_niveau(string annee_deb)
       {

           DataTable dt = new DataTable();
          
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();

           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           cmd.CommandText = "SELECT  DISTINCT a.NIV_ACQUIS_FRANCAIS FROM ESP_INSCRIPTION a where a.annee_deb='"+annee_deb+"' ORDER BY a.NIV_ACQUIS_FRANCAIS";

           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);
           // con.Close();
           return dt;
       }


       public DataTable bind_list_niveau2(string annee_deb)
       {
           DataTable dt = new DataTable();
          
           OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
           con.Open();

           OracleCommand cmd = new OracleCommand();
           cmd.Connection = con;
           cmd.CommandText = "SELECT  DISTINCT a.NIV_ACQUIS_ANGLAIS FROM ESP_INSCRIPTION a where a.annee_deb='" + annee_deb + "' ORDER BY a.NIV_ACQUIS_ANGLAIS";

           OracleDataAdapter od = new OracleDataAdapter(cmd);
           od.Fill(dt);
           // con.Close();
           return dt;
       }

       public int Update_niv_etud(string id_etud,string niv_fr,string id_ens,string annee_deb)
       {
           OracleConnection cn = new OracleConnection(AppConfiguration.ConnectionString);
           OracleCommand cmd = new OracleCommand("UPDATE  esp_inscription  SET NIV_ACQUIS_FRANCAIS='" + niv_fr + "',user_lang_modif='" + id_ens + "'  where Id_et='" + id_etud + "' and annee_deb='" + annee_deb + "'", cn);
           cn.Open();
           return cmd.ExecuteNonQuery();

       }

       public int Update_niv_etud_ang(string id_etud, string niv_ang, string id_ens)
       {
           OracleConnection cn = new OracleConnection(AppConfiguration.ConnectionString);
           OracleCommand cmd = new OracleCommand("UPDATE  esp_inscription  SET NIV_ACQUIS_ANGLAIS='" + niv_ang + "',user_lang_modif='" + id_ens + "'  where Id_et='" + id_etud + "'", cn);
           cn.Open();
           return cmd.ExecuteNonQuery();

       }

       public string GetUP(string id_ens)
       {

           //string cmdQuery = "select DISTINCT a.id_et from esp_etudiant e left join esp_inscription a on a.id_et=e.id_et  where (nom_et || pnom_et) like '" + name + "' and annee_deb='2014' and code_cl like '" + code + "'";
           string lib = "";

           using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
           {
               mySqlConnection.Open();

               string cmdQuery = "select up from esp_enseignant WHERE ID_ENS='"+id_ens+"'";

               OracleCommand myCommand = new OracleCommand(cmdQuery);
               myCommand.Connection = mySqlConnection;
               myCommand.CommandType = CommandType.Text;
               lib = myCommand.ExecuteScalar().ToString();
               mySqlConnection.Close();
           }
           return lib;


       }

        


    }
}

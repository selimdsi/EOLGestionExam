﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Configuration;
using ABSEsprit;


namespace DAL
{
  public class ToiecDAO


    {
      #region sing
        static ToiecDAO instance;
        static Object locker = new Object();
        public static ToiecDAO Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new ToiecDAO();
                    }

                    return ToiecDAO.instance;
                }
            }

        }

        private ToiecDAO() { }
        #endregion sing

        #region Connexion
        OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep");
        OracleTransaction myTrans;

        public void openconntrans()
        {
            mySqlConnection.Open();
            myTrans = mySqlConnection.BeginTransaction();

        }

        public void commicttrans()
        {
            myTrans.Commit();
        }

        public void rollbucktrans()
        {
            myTrans.Rollback();
        }
        public void closeConnection()
        {

            mySqlConnection.Close();


        }
        #endregion

      //update toic to N:
        public int UpdatEPREPTOICTONo(string _id_etud)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_INSCRIPTION SET PREP_TOEIC='' where ID_ET='" + _id_etud + "' and annee_deb='2014'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }

        public int UpdatETOICToNo(string _id_etud)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_INSCRIPTION SET TEST_TOEIC='' where ID_ET='" + _id_etud + "' AND ANNee_deb='2014'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }
      

        public int UpdatEPREPTOIC(string _id_etud)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_INSCRIPTION SET PREP_TOEIC='2' where ID_ET='" + _id_etud + "' and annee_deb='2014'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }

        public int UpdatETOIC(string _id_etud)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE esp_test_etud set SET TEST_TOEIC='1' where ID_ET='" + _id_etud + "' and annee_deb='2014'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }
        public int UpdatETOICenseign(string _id_ens)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_enseignant SET ESP_enseignant.TEST_TOEIC='O' where ESP_enseignant.ID_ENS='"+_id_ens+"'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }

        public int UpdatEPreparationTOICenseign(string _id_ens)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_enseignant SET ESP_enseignant.prep_TOEIC='O' where ESP_enseignant.ID_ENS='" + _id_ens + "'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }


        public int UpdatETOICToNoenseig(string _id_ens)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_enseignant SET ESP_enseignant.TEST_TOEIC='N' where ESP_enseignant.ID_ENS='" + _id_ens + "'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }

        public int UpdatEPREPTOICToNoenseig(string _id_ens)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_enseignant SET ESP_enseignant.prep_TOEIC='N' where ESP_enseignant.ID_ENS='" + _id_ens + "'", cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }
        public int UpdateEtatInsctestniv(string _id_etud)
        {
            OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ");
            OracleCommand cmd = new OracleCommand("UPDATE ESP_INSCRIPTION SET ETAT_INS_TEST_NIV='Y' where ID_ET='" + _id_etud + "' and annee_deb='2014'" , cn);
            cn.Open();
            return cmd.ExecuteNonQuery();
        }

        public string returnCL(string id_edt)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION WHERE ESP_INSCRIPTION.annee_deb='2014'  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  ";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public void Enreg_inscriTest_Lng(DateTime _DATE_TEST_FR, DateTime _DATE_TEST_ANG, string id_et, string annee_deb)
        {
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            // cmd.CommandText = "INSERT INTO ESP_INSCRIPTION(DATE_TEST_FR,DATE_TEST_ANG) VALUES (to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' andESP_INSCRIPTION.annee_deb='"+annee_deb+"')";
            cmd.CommandText = "update ESP_INSCRIPTION set  DATE_TEST_FR  =to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),DATE_TEST_ANG=to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "' and ESP_INSCRIPTION.CODE_cl LIKE '5%' ";
            //

            // cmd.CommandText = "insert into ESP_INSCRIPTION  (DATE_TEST_FR,DATE_TEST_ANG)values(  to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ( select id_et,annee_deb from ESP_INSCRIPTION where ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "')";


            cmd.ExecuteNonQuery();
          
        }
        public string selectEtatTest( string id_edt)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select ETAT_INS_TEST_NIV from esp_inscription where esp_inscription.id_et='" + id_edt + "' and annee_deb='2014'";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }
        public string selectEtatTTOIEC(string id_edt)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "select TYPE_CHOIX from ESP_TEST_ETUD,esp_inscription where esp_inscription.id_et=ESP_TEST_ETUD.id_et and ESP_TEST_ETUD.id_et='" + id_edt + "' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }



        public string selectEtatTTOIECENS(string id_ens)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "select TYPE_CHOIX from ESP_TEST_ENS,esp_ENSEIGNANT where esp_ENSEIGNANT.id_eNS=ESP_TEST_ENS.id_eNS and ESP_TEST_ENS.id_ens='" + id_ens + "'";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }




        public string selectEtatTPREPTOIEC(string id_edt)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select PREP_TOEIC from esp_inscription where esp_inscription.id_et='" + id_edt + "' and annee_deb='2014'";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }
       
   //enseign
        public string selectEtatTTOIECenseign(string id_ens)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "select TEST_TOEIC from esp_enseignant where esp_enseignant.id_ens='" + id_ens + "' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }

        public string selectPreparationEtatTTOIECenseign(string id_ens)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "select PREP_TOEIC from esp_enseignant where esp_enseignant.id_ens='" + id_ens + "' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }

        public DataTable Afficher_date_exam_etud(string id_etd)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,DATE_TEST_FR,DATE_TEST_ANG,code_cl from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and esp_etudiant.id_et='"+id_etd+"' ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


      //Afficher les etudiants qui inscri meme date
        public DataTable Afficher_date_exam_etud_mamadate(DateTime date)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select distinct ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,DATE_TEST_FR,DATE_TEST_ANG,code_cl from ESP_INSCRIPTION,esp_etudiant,societe,ESP_TOEIC_NB where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and DATE_TEST_FR=DATE_TEST_ANG and DATE_TEST_FR=to_date('" + date.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') AND CODE_CL like '5%'", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }



        public DataTable Affich_ETUD_NULL(DateTime date)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select NIV_ACQUIS_FRANCAIS,NIV_ACQUIS_ANGLAIS from esp_inscription where code_cl like '5%' and annee_deb='2014'", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        public DataTable Afficher_date_exam_etudfrfr(string id_etd)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,DATE_TEST_FR,code_cl from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and esp_etudiant.id_et='" + id_etd + "' AND CODE_CL LIKE '5%'", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        public DataTable Afficher_date_exam_etudangang(string id_etd)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,DATE_TEST_ang,code_cl from ESP_INSCRIPTION,esp_etudiant where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb='2014' and esp_etudiant.id_et='" + id_etd + "' AND CODE_CL LIKE '5%' ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
        public string getANNEEDEBs()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT ANNEE_DEB FROM SOCIETE";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public string getAnneeFiN()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE=DSI-PC:1521/orcl ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT ANNEE_FIN FROM SOCIETE";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public DataTable bindDATEexaM()
        {
            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
           // OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS_ANG,NBMAX FROM ESP_TOEIC_NB  where NB_CONDIDATS_ANG<NBMAX order by DATETEST", con);
           // OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS,NBMAX FROM ESP_TOEIC_NB  WHERE NB_CONDIDATS<NBMAX AND NIV_ACQUIS_ANGLAIS<>'B2' and  NIV_ACQUIS_FRANCAIS<>'B2' order by DATETEST", con);

            OracleCommand cmd = new OracleCommand("SELECT DATETEST FROM ESP_TOEIC_NB WHERE NB_CONDIDATS_FR<NBMAX ",con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable bindDATEexaMANG()
        {
            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            // OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS_ANG,NBMAX FROM ESP_TOEIC_NB  where NB_CONDIDATS_ANG<NBMAX order by DATETEST", con);
            // OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS,NBMAX FROM ESP_TOEIC_NB  WHERE NB_CONDIDATS<NBMAX AND NIV_ACQUIS_ANGLAIS<>'B2' and  NIV_ACQUIS_FRANCAIS<>'B2' order by DATETEST", con);

            OracleCommand cmd = new OracleCommand("SELECT DATETEST FROM ESP_TOEIC_NB WHERE NB_CONDIDATS_ang<NBMAX", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            con.Close();
            return dt;
        }


        public DataTable bindDATEx()
        {
            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            // OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS_ANG,NBMAX FROM ESP_TOEIC_NB  where NB_CONDIDATS_ANG<NBMAX order by DATETEST", con);
            // OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS,NBMAX FROM ESP_TOEIC_NB  WHERE NB_CONDIDATS<NBMAX AND NIV_ACQUIS_ANGLAIS<>'B2' and  NIV_ACQUIS_FRANCAIS<>'B2' order by DATETEST", con);

            OracleCommand cmd = new OracleCommand("SELECT DATETEST FROM ESP_TOEIC_NB ORDER BY DATETEST ", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);
            con.Close();
            return dt;
        }
      

      
        public bool verif(string idate)
        {
            bool exist = false;

            mySqlConnection.Open();

            //"update ESP_INSCRIPTION set  DATE_TEST_FR  =to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),DATE_TEST_ANG=to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "'";

            string cmdQuery = "select * from ESP_TOEIC_NB where DATETEST =to_date('" + idate.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and NB_CONDIDATS_fr<NBMAX  ";

            OracleCommand myCommandAbsence = new OracleCommand(cmdQuery, mySqlConnection);

            OracleDataReader MyReader = myCommandAbsence.ExecuteReader();

            while (MyReader.Read() && !exist)
            {
                exist = true;

                break;

            }
            MyReader.Close();
            mySqlConnection.Close();


            return exist;
        }
        public bool verifang(string idate)
        {
            bool exist = false;

            mySqlConnection.Open();

            //"update ESP_INSCRIPTION set  DATE_TEST_FR  =to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),DATE_TEST_ANG=to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "'";

            string cmdQuery = "select * from ESP_TOEIC_NB where DATETEST =to_date('" + idate.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and NB_CONDIDATS_ang<NBMAX  ";

            OracleCommand myCommandAbsence = new OracleCommand(cmdQuery, mySqlConnection);

            OracleDataReader MyReader = myCommandAbsence.ExecuteReader();

            while (MyReader.Read() && !exist)
            {
                exist = true;

                break;

            }
            MyReader.Close();
            mySqlConnection.Close();


            return exist;
        }


      //selectionner nbcondidat inscrit
      public string  nbCondidatsInscrit(DateTime dateinsc)
        {
            string NB = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT NB_CONDIDATS_fr from ESP_TOEIC_NB where DATETEST=to_date('" + dateinsc.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                NB = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return NB;
        }


      public string nbCondidatsInscritang(DateTime dateinsc)
      {
          string NB = "";

          using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
          {
              mySqlConnection.Open();

              string cmdQuery = "SELECT NB_CONDIDATS_ang from ESP_TOEIC_NB where DATETEST=to_date('" + dateinsc.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ";


              OracleCommand myCommand = new OracleCommand(cmdQuery);
              myCommand.Connection = mySqlConnection;
              myCommand.CommandType = CommandType.Text;
              NB = myCommand.ExecuteScalar().ToString();
              mySqlConnection.Close();
          }
          return NB;
      }

      #region  tester
      //public DataTable bindDATEexam()
      //{
      //    DataTable dt = new DataTable();
      //    string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= ";
      //    OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
      //    con.Open();
      //    //OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS_FR,NBMAX FROM ESP_TOEIC_NB  where NB_CONDIDATS_FR<NBMAX order by DATETEST", con);
      //    OracleCommand cmd = new OracleCommand("SELECT DATETEST,NB_CONDIDATS,NBMAX FROM ESP_TOEIC_NB order by DATETEST", con);

      //    OracleDataAdapter od = new OracleDataAdapter(cmd);
      //    od.Fill(dt);
      //    con.Close();
      //    return dt;
      //}

      ////////////////////////////////////////////////////////////////
      //public bool ajoutnbcandANG(DateTime datetest)
      //{
      //    bool result = false;
      //    openconntrans();

      //    string cmdQuery = " update  ESP_TOEIC_NB set (NB_CONDIDATS_ANG)=( select NB_CONDIDATS_ANG+ 1 from ESP_TOEIC_NB where DATETEST = to_date('" + datetest.ToString() + "', 'dd/mm/yyyy hh24:mi:ss')) ";
      //    Oracle.ManagedDataAccess.Client.OracleCommand myCommand = new OracleCommand(cmdQuery);
      //    myCommand.Connection = mySqlConnection;
      //    myCommand.CommandType = CommandType.Text;
      //    myCommand.Transaction = myTrans;
      //    try
      //    {
      //        myCommand.ExecuteNonQuery();
      //        myTrans.Commit();
      //        result = true;
      //    }
      //    catch (Exception)
      //    {
      //        myTrans.Rollback();
      //        mySqlConnection.Close();
      //        throw;
      //    }
      //    mySqlConnection.Close();
      //    return result;
      //}
      //////////////////////////
      //public string nbCondidatsInscritAng(DateTime dateinsc)
      //{
      //    string NB = "";

      //     using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
      //    {
      //        mySqlConnection.Open();

      //        string cmdQuery = "SELECT NB_CONDIDATS_ANG from ESP_TOEIC_NB where DATETEST=to_date('" + dateinsc.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ";


      //        OracleCommand myCommand = new OracleCommand(cmdQuery);
      //        myCommand.Connection = mySqlConnection;
      //        myCommand.CommandType = CommandType.Text;
      //        NB = myCommand.ExecuteScalar().ToString();
      //        mySqlConnection.Close();
      //    }
      //    return NB;
      //}
      //public bool verifFR(string idate)
      //{
      //    bool exist = false;



      //    mySqlConnection.Open();

      //    //"update ESP_INSCRIPTION set  DATE_TEST_FR  =to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),DATE_TEST_ANG=to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "'";

      //    string cmdQuery = "select * from ESP_TOEIC_NB where DATETEST =to_date('" + idate.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and NB_CONDIDATS_FR<NBMAX  ";

      //    OracleCommand myCommandAbsence = new OracleCommand(cmdQuery, mySqlConnection);

      //    OracleDataReader MyReader = myCommandAbsence.ExecuteReader();

      //    while (MyReader.Read() && !exist)
      //    {
      //        exist = true;

      //        break;

      //    }
      //    MyReader.Close();
      //    mySqlConnection.Close();


      //    return exist;
      //} 
      #endregion

        public bool ajoutnbcandidatsfr(DateTime datetest)
        {
            bool result = false;
            openconntrans();

            string cmdQuery = " update  ESP_TOEIC_NB set (NB_CONDIDATS_fr)=( select NB_CONDIDATS_fr+ 1 from ESP_TOEIC_NB where DATETEST = to_date('" + datetest.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ) ";
            Oracle.ManagedDataAccess.Client.OracleCommand myCommand = new OracleCommand(cmdQuery);
            myCommand.Connection = mySqlConnection;
            myCommand.CommandType = CommandType.Text;
            myCommand.Transaction = myTrans;
            try
            {
                myCommand.ExecuteNonQuery();
                myTrans.Commit();
                result = true;
            }
            catch (Exception)
            {
                myTrans.Rollback();
                mySqlConnection.Close();
                throw;
            }
            mySqlConnection.Close();
            return result;
        }


        public bool ajoutnbcandidatsang(DateTime datetest)
        {
            bool result = false;
            openconntrans();

            string cmdQuery = " update  ESP_TOEIC_NB set (NB_CONDIDATS_ang)=( select NB_CONDIDATS_ang+ 1 from ESP_TOEIC_NB where DATETEST = to_date('" + datetest.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ) ";
            Oracle.ManagedDataAccess.Client.OracleCommand myCommand = new OracleCommand(cmdQuery);
            myCommand.Connection = mySqlConnection;
            myCommand.CommandType = CommandType.Text;
            myCommand.Transaction = myTrans;
            try
            {
                myCommand.ExecuteNonQuery();
                myTrans.Commit();
                result = true;
            }
            catch (Exception)
            {
                myTrans.Rollback();
                mySqlConnection.Close();
                throw;
            }
            mySqlConnection.Close();
            return result;
        }

        public string selectniVeauEDTFR(string id_edt)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT NIV_ACQUIS_FRANCAIS from esp_inscription where esp_inscription.id_et='" + id_edt + "' and annee_deb=2014";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }
        public string selectniVeauEDTANG(string id_edt)
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT NIV_ACQUIS_ANGLAIS from esp_inscription where esp_inscription.id_et='" + id_edt + "' and (annee_deb=2015 or annee_deb='2014')";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }



        public DataTable Afficher_list_condParDateANG(DateTime dateang)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_ANG =to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);
            OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_ANG  from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_ANG=esp_toeic_nb.DATETEST and DATE_TEST_ANG=to_date('" + dateang + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

        public DataTable Afficher_list_condParDateFR(DateTime dateFR)
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            //OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_FR from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_FR =to_date('" + date.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);
            OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL,DATE_TEST_FR  from ESP_INSCRIPTION,esp_etudiant,societe,esp_toeic_nb where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb and ETAT_INS_TEST_NIV='Y' and ESP_INSCRIPTION.DATE_TEST_FR=esp_toeic_nb.DATETEST and DATE_TEST_FR=to_date('" + dateFR + "', 'dd/mm/yyyy hh24:mi:ss') and code_cl like '5%'", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public string nbtoiec()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = " SELECT count(*) from esp_inscription,societe where TEST_toeic='O' and esp_inscription.annee_deb=societe.annee_deb AND (code_cl like '4%' or code_cl like '5%')";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }

        public string nbPREPtoiec()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT count(*) from esp_inscription,societe where prep_toeic='O' and esp_inscription.annee_deb=societe.annee_deb AND (code_cl like '4%' or code_cl like '5%')";


                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;

        }

        public string returnCLSUPP(string id_edt)
        {
            string lib = " ";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();


                string cmdQuery = "select  MAX(code_cl) from esp_inscription,esp_etudiant  where esp_inscription.id_et=esp_etudiant.id_et  and ESP_ETUDIANT.etat='A' AND  esp_inscription.id_et='" + id_edt + "' and (annee_DEB='2015' or annee_DEB='2014') ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }


      //return code_cl 14


        public string returnCLSUPPmax(string id_edt)
        {
            string lib = " ";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();


                string cmdQuery = "select  MAX(code_cl) from esp_inscription,esp_etudiant  where esp_inscription.id_et=esp_etudiant.id_et  and ESP_ETUDIANT.etat='A' AND  esp_inscription.id_et='" + id_edt + "' and (annee_DEB='2015' or annee_DEB='2014') ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }


        public void Enreg_inscriTest_fr(DateTime _DATE_TEST_FR, string id_et, string annee_deb)
        {
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            // cmd.CommandText = "INSERT INTO ESP_INSCRIPTION(DATE_TEST_FR,DATE_TEST_ANG) VALUES (to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' andESP_INSCRIPTION.annee_deb='"+annee_deb+"')";
            cmd.CommandText = "update ESP_INSCRIPTION set DATE_TEST_FR=to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "' and ESP_INSCRIPTION.CODE_cl like '5%' ";
            
            cmd.ExecuteNonQuery();

        }

        public void Enreg_inscriTest_ANG(DateTime _DATE_TEST_ANG, string id_et, string annee_deb)
        {
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            // cmd.CommandText = "INSERT INTO ESP_INSCRIPTION(DATE_TEST_FR,DATE_TEST_ANG) VALUES (to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' andESP_INSCRIPTION.annee_deb='"+annee_deb+"')";
            cmd.CommandText = "update ESP_INSCRIPTION set DATE_TEST_ANG=to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "' and ESP_INSCRIPTION.CODE_cl like '5%' ";

            cmd.ExecuteNonQuery();

        }


        public void Addnbcondidatfr(DateTime date)
        {
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
           //  cmd.CommandText = "update  ESP_TOEIC_NB set (NB_CONDIDATS_fr)=( select (NB_CONDIDATS_fr+ 1) from ESP_TOEIC_NB where DATETEST = to_date('" + date.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ) ";

            cmd.CommandText = "update  ESP_TOEIC_NB set NB_CONDIDATS_fr=NB_CONDIDATS_fr+1 where DATETEST = to_date('" + date.ToString() + "', 'dd/mm/yyyy hh24:mi:ss')  ";
           

            

            cmd.ExecuteNonQuery();

        }


        public void AddnbcondidatANG(DateTime dateang)
        {
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
             //cmd.CommandText = " update  ESP_TOEIC_NB set (NB_CONDIDATS_ang)=( select (NB_CONDIDATS_ang+ 1) from ESP_TOEIC_NB where DATETEST = to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') ) ";

            cmd.CommandText = " update  ESP_TOEIC_NB set NB_CONDIDATS_ang=NB_CONDIDATS_ang+ 1 where DATETEST = to_date('" + dateang.ToString() + "', 'dd/mm/yyyy hh24:mi:ss')  ";


            cmd.ExecuteNonQuery();

        }

        public string countNBTOIEC()
        {
            //OracleConnection cn = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep");
            //OracleCommand cmd = new OracleCommand("SELECT COUNT(*) from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.ANNEE_DEB=SOCIETE.ANNEE_DEB AND  TEST_TOEIC='O'", cn);
            //cn.Open();
            //return cmd.ExecuteNonQuery();

            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                string cmdQuery = "SELECT COUNT(*) from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.ANNEE_DEB=SOCIETE.ANNEE_DEB AND  TEST_TOEIC='O' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public string countNBPREPTOIEC()
        {
           

            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
            {
                mySqlConnection.Open();

                // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                string cmdQuery = "SELECT COUNT(*) from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.ANNEE_DEB=SOCIETE.ANNEE_DEB AND  PREP_TOEIC='O' ";
                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }



        public DataTable Afficher_list_toiec()
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and TEST_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

            OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='1' AND (CODE_CL LIKE '4%' or code_cl like '5%') and esp_test_etud.TYPE_TEST='T'", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }
        public DataTable Afficher_list_PREP_toiec()
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
           // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

            OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='2' AND (CODE_CL LIKE '4%' or code_cl like '5%')and esp_test_etud.TYPE_TEST='T'", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

       public DataTable Afficher_list_commune()
        {

            DataTable dt = new DataTable();
            string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

            OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='3' AND (CODE_CL LIKE '4%' or code_cl like '5%') and (esp_test_etud.TYPE_TEST='T')", con);
            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


       public string countNB_TOIEC()
       {
           string lib = "";

           using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
           {
               mySqlConnection.Open();

               // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


               string cmdQuery = " select sum(type_choix) from ( select count(*) type_choix from esp_TEST_ENS where  (type_choix='1' or type_choix='3') union all SELECT count(*) type_choix from esp_TEST_ETUD where (type_choix='1' or type_choix='3' ) and (esp_TEST_ETUD.TYPE_TEST='T'))";
               
               OracleCommand myCommand = new OracleCommand(cmdQuery);
               myCommand.Connection = mySqlConnection;
               myCommand.CommandType = CommandType.Text;
               lib = myCommand.ExecuteScalar().ToString();
               mySqlConnection.Close();
           }
           return lib;
       }

      public string countNBPrep_TOIEC()
       {
           string lib = "";

           using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
           {
               mySqlConnection.Open();

               // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


               string cmdQuery = "select sum(type_choix) from ( select count(*) type_choix from esp_TEST_ENS where  (type_choix='2' or type_choix='3') union all SELECT count(*) type_choix from esp_TEST_ETUD where (type_choix='2' or type_choix='3' ) and (esp_TEST_ETUD.TYPE_TEST='T'))";
               
               OracleCommand myCommand = new OracleCommand(cmdQuery);
               myCommand.Connection = mySqlConnection;
               myCommand.CommandType = CommandType.Text;
               lib = myCommand.ExecuteScalar().ToString();
               mySqlConnection.Close();
           }
           return lib;
       }


      public string countNBPrep_TOIEC_et_prep()
      {
          string lib = "";

          using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
          {
              mySqlConnection.Open();

              // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


              string cmdQuery = "select sum(type_choix) from ( select count(*) type_choix from esp_TEST_ENS where  (type_choix='3') union all SELECT count(*) type_choix from esp_TEST_ETUD  where (type_choix='3' )and (esp_TEST_ETUD.TYPE_TEST='T'))";

              OracleCommand myCommand = new OracleCommand(cmdQuery);
              myCommand.Connection = mySqlConnection;
              myCommand.CommandType = CommandType.Text;
              lib = myCommand.ExecuteScalar().ToString();
              mySqlConnection.Close();
          }
          return lib;
      }
      

      public DataTable Afficher_list_ens_toeic()
      {

          DataTable dt = new DataTable();
          string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
          OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
          con.Open();
          // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

          OracleCommand cmd = new OracleCommand("select nom_ens,esp_enseignant.id_ens from esp_enseignant,esp_test_ens where etat='A'  and esp_test_ens.ID_ENS=esp_enseignant.ID_ENS and type_choix='1'", con);
          OracleDataAdapter od = new OracleDataAdapter(cmd);
          od.Fill(dt);

          con.Close();
          return dt;
      }

      public DataTable Afficher_list_ens_prep_toeic()
      {

          DataTable dt = new DataTable();
          string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
          OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
          con.Open();
          // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

          OracleCommand cmd = new OracleCommand("select nom_ens,esp_enseignant.id_ens from esp_enseignant,esp_test_ens where etat='A'  and esp_test_ens.ID_ENS=esp_enseignant.ID_ENS and type_choix='2'", con);
          OracleDataAdapter od = new OracleDataAdapter(cmd);
          od.Fill(dt);

          con.Close();
          return dt;
      }

             public DataTable Afficher_list_ens_commun()
      {

          DataTable dt = new DataTable();
          string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
          OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
          con.Open();
          // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

          OracleCommand cmd = new OracleCommand("select nom_ens,esp_enseignant.id_ens from esp_enseignant,esp_test_ens where etat='A'  and esp_test_ens.ID_ENS=esp_enseignant.ID_ENS and type_choix='3'", con);
          OracleDataAdapter od = new OracleDataAdapter(cmd);
          od.Fill(dt);

          con.Close();
          return dt;
      }

             public void Enreg_etud_toeic( string id_et, string _choix)
             {
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                 cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'))";
                
                 cmd.ExecuteNonQuery();

             }

             //public void Enreg_etud_FORMATION(string id_et, string _choix)
             //{
             //    string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
             //    OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
             //    conn.Open();
             //    OracleCommand cmd = conn.CreateCommand();
             //   // cmd.CommandText = "INSERT INTO ESP_FORMATION_ETUD_LANGUE(ID_ET,TYPE_CHOIX,DATE_CHOIX) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'))";

             //    cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'))";
                
                 
             //    cmd.ExecuteNonQuery();

             //}

             //public DataTable get_id_etud_formation(string id_et)
             //{

             //    DataTable dt = new DataTable();
             //    string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
             //    OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
             //    con.Open();
             //    // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

             //    //OracleCommand cmd = new OracleCommand("select * from ESP_FORMATION_ETUD_LANGUE where ESP_FORMATION_ETUD_LANGUE.id_et='" + id_et + "' and DATE_CHOIX=to_date(sysdate,'dd/MM/yyyy') ", con);

             //    OracleCommand cmd = new OracleCommand("select * from ESP_TEST_ETUD where ESP_TEST_ETUD.id_et='" + id_et + "' and DATE_CHOIX=to_date(sysdate,'dd/MM/yyyy') ", con);
                 
             //    OracleDataAdapter od = new OracleDataAdapter(cmd);
             //    od.Fill(dt);

             //    con.Close();
             //    return dt;
             //}

             //public string getChoixLangues(string id_edt)
             //{
             //    string lib = "";

             //    using (OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep"))
             //    {
             //        mySqlConnection.Open();

             //        string cmdQuery = "select TYPE_CHOIX from ESP_TEST_ETUD,esp_inscription where esp_inscription.id_et=ESP_TEST_ETUD.id_et and ESP_TEST_ETUD.id_et='" + id_edt + "' and DATE_CHOIX=to_date(sysdate,'dd/MM/yyyy') ";
             //        OracleCommand myCommand = new OracleCommand(cmdQuery);
             //        myCommand.Connection = mySqlConnection;
             //        myCommand.CommandType = CommandType.Text;
             //        lib = myCommand.ExecuteScalar().ToString();
             //        mySqlConnection.Close();
             //    }
             //    return lib;

             //}
            

             public void Enreg_ens_toeic(string id_ens, string _choix)
             {
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                 cmd.CommandText = "INSERT INTO ESP_TEST_Ens(ID_Ens,TYPE_CHOIX,DATE_CHOIX) VALUES ('" + id_ens + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'))";

                 cmd.ExecuteNonQuery();

             }


             public bool verifexistet()
             {
                 bool exist = false;

                 mySqlConnection.Open();

                 //"update ESP_INSCRIPTION set  DATE_TEST_FR  =to_date('" + _DATE_TEST_FR.ToString() + "', 'dd/mm/yyyy hh24:mi:ss'),DATE_TEST_ANG=to_date('" + _DATE_TEST_ANG.ToString() + "', 'dd/mm/yyyy hh24:mi:ss') where ESP_INSCRIPTION.CODE_CL='" + code_cl + "' and ESP_INSCRIPTION.id_et='" + id_et + "' and ESP_INSCRIPTION.annee_deb='" + annee_deb + "'";

                 string cmdQuery = " SELECT distinct esp_inscription.id_et FROM esp_inscription WHERE EXISTS (SELECT esp_test_etud.ID_ET FROM esp_test_etud WHERE esp_inscription.id_et = esp_test_etud.id_et) ";
                 
                 OracleCommand myCommandAbsence = new OracleCommand(cmdQuery, mySqlConnection);

                 OracleDataReader MyReader = myCommandAbsence.ExecuteReader();

                 while (MyReader.Read() && !exist)
                 {
                     exist = true;

                     break;

                 }
                 MyReader.Close();
                 mySqlConnection.Close();


                 return exist;
             }

             public string getidtest_toeic()
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                     string cmdQuery = "SELECT ID_ET FROM ESP_TEST_ETUD  ";

                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;
             }


             public DataTable Aff_list_inscrit(string id_et)
             {

                 DataTable dt = new DataTable();
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                 // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

                 OracleCommand cmd = new OracleCommand("select * from esp_test_etud where esp_test_etud.id_et='"+id_et+"' ", con);
                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }


             public DataTable Aff_list_inscrit_ens(string id_ens)
             {

                 DataTable dt = new DataTable();
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                 // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

                 OracleCommand cmd = new OracleCommand("select * from esp_test_ens where esp_test_ens.id_ens='" + id_ens + "' ", con);
                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }
             public int UpdatE_choix(string _id_etud,string choix)
             {
                 OracleConnection cn = new OracleConnection(AppConfiguration.ConnectionString);
                 OracleCommand cmd = new OracleCommand("UPDATE ESP_TEST_ETUD SET DATE_CHOIX=to_date(sysdate,'dd/MM/yyyy'), TYPE_CHOIX='" + choix + "' where ID_ET='" + _id_etud + "'", cn);
                 cn.Open();
                              return cmd.ExecuteNonQuery();
             }



      //fonction consernant la formation francais et anglais 2014/2015


             public DataTable Afficher_list_commune_formation()
             {

                 DataTable dt = new DataTable();
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                 // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

               //  OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='3' AND (CODE_CL LIKE '5GC%' or code_cl like '5EM%') AND DATE_CHOIX > TO_DATE('20-07-2015','DD-MM-YYYY')", con);

                 OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and esp_test_etud.TYPE_TEST='F' and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='3' AND (CODE_CL LIKE '5GC%' or code_cl like '5EM%') AND TO_DATE(DATE_CHOIX) > TO_DATE('20/07/2015','DD/MM/YYYY')",con);
                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }

             public DataTable Afficher_format_ang()
             {

                 DataTable dt = new DataTable();
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                 // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and PREP_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

                // OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='2' AND (CODE_CL LIKE '5GC%' or code_cl like '5EM%') AND DATE_CHOIX > TO_DATE('20-07-2015','DD-MM-YYYY') and type_test='F'", con);
                 OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and esp_test_etud.TYPE_TEST='F' and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='2' AND (CODE_CL LIKE '5GC%' or code_cl like '5EM%') AND TO_DATE(DATE_CHOIX) > TO_DATE('20/07/2015','DD/MM/YYYY')",con);
                 
                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }
             public DataTable Afficher_format_fr()
             {

                 DataTable dt = new DataTable();
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                 // OracleCommand cmd = new OracleCommand("select ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL  from ESP_INSCRIPTION,esp_etudiant,societe where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb=societe.annee_deb  and TEST_TOEIC='O' and (CODE_CL LIKE '4%' or code_cl like '5%')  ", con);

               //  OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et  and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='1' AND (CODE_CL LIKE '5GC%' or code_cl like '5EM%') AND DATE_CHOIX > TO_DATE('20-07-2015','DD-MM-YYYY') ", con);

                 OracleCommand cmd = new OracleCommand("SELECT ESP_INSCRIPTION.id_et,NOM_ET,PNOM_ET,ESP_INSCRIPTION.CODE_CL FROM ESP_INSCRIPTION,esp_etudiant,esp_test_etud where ESP_INSCRIPTION.id_et=esp_etudiant.id_et and esp_test_etud.TYPE_TEST='F' and ESP_INSCRIPTION.annee_deb='2014' and esp_test_etud.ID_ET=ESP_INSCRIPTION.ID_ET and type_choix='1' AND (CODE_CL LIKE '5GC%' or code_cl like '5EM%') AND TO_DATE(DATE_CHOIX) > TO_DATE('20/07/2015','DD/MM/YYYY')",con);
                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }
      //inscrit au formation
             public void Enreg_etud_FORMATION(string id_et, string _choix)
             {
                 string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                 
                 // cmd.CommandText = "INSERT INTO ESP_FORMATION_ETUD_LANGUE(ID_ET,TYPE_CHOIX,DATE_CHOIX) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'))";

                 cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date('29/02/16','dd/MM/yyyy'),'F')";


                 cmd.ExecuteNonQuery();

             }




      


             public DataTable get_id_etud_formation(string id_et)
             {

                 DataTable dt = new DataTable();
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                 
                 OracleCommand cmd = new OracleCommand("select * from ESP_TEST_ETUD where ESP_TEST_ETUD.id_et='" + id_et + "' and DATE_CHOIX=to_date(sysdate,'dd/MM/yy') ", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }

             public string getChoixLangues(string id_edt)
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     string cmdQuery = "select TYPE_CHOIX from ESP_TEST_ETUD,esp_inscription where esp_inscription.id_et=ESP_TEST_ETUD.id_et and ESP_TEST_ETUD.id_et='" + id_edt + "' and DATE_CHOIX=to_date(sysdate,'dd/MM/yyyy') ";
                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;

             }

             public string countNB_fr()
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                     string cmdQuery = " SELECT count(*) type_choix from esp_TEST_ETUD where (type_choix='1' or type_choix='3' ) AND TO_DATE(DATE_CHOIX) > TO_DATE('20/07/2015','DD/MM/YYYY') and type_test='F'";

                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;
             }

             public string countNB_ang()
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                     string cmdQuery = "SELECT count(*) type_choix from esp_TEST_ETUD where (type_choix='2' or type_choix='3' ) AND TO_DATE(DATE_CHOIX) > TO_DATE('20/07/2015','DD/MM/YYYY') and type_test='F'";

                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;
             }


             public string countNB_fr_ang()
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                     string cmdQuery = " SELECT count(*) type_choix from esp_TEST_ETUD where (type_choix='3' ) AND TO_DATE(DATE_CHOIX) > TO_DATE('21/07/2015','DD/MM/YYYY')  and type_test='F';";

                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;
             }

      //formation fr et ang pour nos enseignants esprit
             public void Enreg_etud_FORMATION_enseign(string id_ens, string _choix)
             {
                 //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep";
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                 // cmd.CommandText = "INSERT INTO ESP_FORMATION_ETUD_LANGUE(ID_ET,TYPE_CHOIX,DATE_CHOIX) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'))";

                 cmd.CommandText = "INSERT INTO ESP_TEST_ENS(ID_ENS,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_ens+ "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'F')";


                 cmd.ExecuteNonQuery();

             }


             public DataTable verifier(string id_ens)
             {

                 DataTable dt = new DataTable();
                 //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();
                
                 OracleCommand cmd = new OracleCommand("select * from esp_test_ens where id_ens='" + id_ens + "' and type_test='F' and DATE_CHOIX=to_date(sysdate,'dd/MM/yy') ", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }


             public DataTable verifieretud(string id_et)

             {

                 DataTable dt = new DataTable();
                 //string source = "DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD=tbzr10ep ";
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();

                 OracleCommand cmd = new OracleCommand("select * from esp_test_etud where id_et='" + id_et + "' and type_test='F' and to_date(DATE_CHOIX)=to_date('29/02/16','dd/MM/yy') ", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }

             public void Enreg_etud_FORMAt_test(string id_et, string _choix)
             {
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                // OracleCommand cmd1 = conn.CreateCommand();
                 cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'D')";
                 //cmd1.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'T')";
                 cmd.ExecuteNonQuery();
                 //cmd1.ExecuteNonQuery();

             }

      //test fr par date:le 10/02/2016


             public void Enreg_etud_FORMAt_testparDATE(string id_et,string date)
             {
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                 // OracleCommand cmd1 = conn.CreateCommand();
                 cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','0','"+date+"','T')";
                 //cmd1.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'T')";
                 cmd.ExecuteNonQuery();
                 //cmd1.ExecuteNonQuery();

             }


             //public void Enreg_etud_FORMAt_testparDATEANG(string id_et, string date)
             //{
             //    OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
             //    conn.Open();
             //    OracleCommand cmd = conn.CreateCommand();
             //    // OracleCommand cmd1 = conn.CreateCommand();
             //    cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','0','" + date + "','T')";
             //    //cmd1.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'T')";
             //    cmd.ExecuteNonQuery();
             //    //cmd1.ExecuteNonQuery();

             //}

      //enreg test
             public void Enreg_etud_test(string id_et, string _choix)
             {
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                 conn.Open();
                 OracleCommand cmd = conn.CreateCommand();
                 OracleCommand cmd1 = conn.CreateCommand();
                // cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'F')";
                 cmd1.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','" + _choix + "',to_date(sysdate,'dd/MM/yyyy'),'T')";
               
                 cmd1.ExecuteNonQuery();
             }


             public string countnbinscrfr_date()
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     string cmdQuery = " SELECT count(*) from esp_test_etud where to_date(DATE_CHOIX)=to_date('13/04/2016','dd/MM/YYYY') and type_choix='0'";

                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;

             }

             public string countnbinscrANG_date()
             {
                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     string cmdQuery = " SELECT count(*) from esp_test_etud where (to_date(DATE_CHOIX)=to_date('12/07/16','dd/MM/YY') or to_date(DATE_CHOIX)=to_date('19/07/16','dd/MM/YY')) and type_choix='0'";

                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;

             }




             public DataTable get_id_etud_formatioParDate(string id_et,String date_choix)
             {

                 DataTable dt = new DataTable();
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();

                 OracleCommand cmd = new OracleCommand("select * from ESP_TEST_ETUD where ESP_TEST_ETUD.id_et='" + id_et + "' and DATE_CHOIX='13/04/16'", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }


             public DataTable get_id_etud_formatioParDateANG(string id_et, String date_choix)
             {

                 DataTable dt = new DataTable();
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();

                 OracleCommand cmd = new OracleCommand("select * from ESP_TEST_ETUD where ESP_TEST_ETUD.id_et='" + id_et + "' and (DATE_CHOIX=to_date('"+date_choix+"'),'dd/MM/yy')", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }



      //17/06/16inscription fr et ang

             public void Enreg_fr1219(string id_et, string date)
             {
                 OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
                
                 conn.Open();

                 OracleCommand cmd = conn.CreateCommand();
                
                 cmd.CommandText = "INSERT INTO ESP_TEST_ETUD(ID_ET,TYPE_CHOIX,DATE_CHOIX,TYPE_TEST) VALUES ('" + id_et + "','F','" + date + "','T')";
                 
                 cmd.ExecuteNonQuery();
                

             }

             public DataTable get_inscrit1219(string id_et)
             {

                 DataTable dt = new DataTable();
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();

                 OracleCommand cmd = new OracleCommand("select * from ESP_TEST_ETUD where ESP_TEST_ETUD.id_et='"+id_et+"' and( DATE_CHOIX=to_date('12/07/16','dd/MM/yy' )  or  (DATE_CHOIX=to_date('19/07/16','dd/MM/yy')))", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }

             public DataTable get_date(string id_et)
             {

                 DataTable dt = new DataTable();
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                 con.Open();

                 OracleCommand cmd = new OracleCommand("select date_choix from ESP_TEST_ETUD where ESP_TEST_ETUD.id_et='" + id_et + "' and type_choix='F'", con);

                 OracleDataAdapter od = new OracleDataAdapter(cmd);
                 od.Fill(dt);

                 con.Close();
                 return dt;
             }


             public int Updatechoix1219(string _id_etud,string date)
             {
              OracleConnection cn = new OracleConnection(AppConfiguration.ConnectionString);
              OracleCommand cmd = new OracleCommand("UPDATE esp_test_etud set to_date(DATE_CHOIX)=to_date('" + date.ToString() + "','dd/MM/yy' ) where type_choix='F' and TYPE_TEST='T' and  id_et='" + _id_etud + "'", cn);
              cn.Open();
              return cmd.ExecuteNonQuery();
             }

             public string get_etat_finanviere(string id_et)
             {


                 string lib = "";

                 using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
                 {
                     mySqlConnection.Open();

                     // string cmdQuery = "SELECT CODE_CL from ESP_INSCRIPTION,SOCIETE WHERE ESP_INSCRIPTION.annee_deb=societe.annee_deb  and ESP_INSCRIPTION.ID_ET='" + id_edt + "'  and ESP_INSCRIPTION.code_cl like '5%' ";


                     string cmdQuery = "Select fs_is_student_authorised_new(id_et) from esp_etudiant where id_et='"+id_et+"' ";
                     OracleCommand myCommand = new OracleCommand(cmdQuery);
                     myCommand.Connection = mySqlConnection;
                     myCommand.CommandType = CommandType.Text;
                     lib = myCommand.ExecuteScalar().ToString();
                     mySqlConnection.Close();
                 }
                 return lib;
             }


    }

    }


    


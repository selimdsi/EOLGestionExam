﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ABSEsprit;

namespace DAL
{
 public   class EncadDAO
    {

      #region sing
        static EncadDAO instance;
        static Object locker = new Object();
        public static EncadDAO Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new EncadDAO();
                    }

                    return EncadDAO.instance;
                }
            }

        }

        private EncadDAO() { }
        #endregion sing

        #region Connexion
        OracleConnection mySqlConnection = new OracleConnection("DATA SOURCE= ;PERSIST SECURITY INFO=True;USER ID=SCOESP09;PASSWORD= tbzr10ep");
        OracleTransaction myTrans;

        public void openconntrans()
        {
            mySqlConnection.Open();
            myTrans = mySqlConnection.BeginTransaction();

        }

        public void commicttrans()
        {
            myTrans.Commit();
        }

        public void rollbucktrans()
        {
            myTrans.Rollback();
        }
        public void closeConnection()
        {

            mySqlConnection.Close();


        }
        #endregion


        public DataTable Getmodule()
        {

            DataTable dt = new DataTable();
            
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select distinct p.code_module,designation from esp_module_panier_classe_saiso p inner join esp_module e  on p.code_module=e.code_module where annee_deb='2015' order by designation,code_module ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public DataTable Gettechnologie()
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select lib_nome,code_nome from code_nomenclature where code_str='65' ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public DataTable GetMethodologie()
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select lib_nome,code_nome from code_nomenclature where code_str='64' ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public DataTable Gettypeprojet()
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select lib_nome,code_nome from code_nomenclature where code_str='79' ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }

     //isert projet

        public void insert_Project(string _Id_projet,string _ANNEE_DEB,string _nom_projet,string _description_projet,string _technologies,string _methodologie,string _type_projet,string duree, string _code_module,string _id_enseignant,string s,string p)
        {
                        OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = conn.CreateCommand();
            cmd.CommandText = "insert into ESP_PROJET_N(Id_projet,ANNEE_DEB,nom_projet,description_projet,technologies,methodologie,type_projet,duree,code_module,id_ens,date_saisie,semestre,periode) values ('" + _Id_projet + "','" + _ANNEE_DEB + "','" + _nom_projet + "','" + _description_projet + "','" + _technologies + "','" + _methodologie + "','" + _type_projet + "','"+duree+"','" + _code_module + "','" + _id_enseignant + "',to_date(sysdate,'dd/MM/yyyy HH24:MI:SS'),'"+s+"','"+p+"')";

            cmd.ExecuteNonQuery();
        }

        public string get_an()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select annee_deb from societe";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public string inc_id_projet()
        {
            string lib = "";

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "SELECT COUNT(*) AS NB FROM ESP_PROJET_N";

                OracleCommand myCommand = new OracleCommand(cmdQuery);
                myCommand.Connection = mySqlConnection;
                myCommand.CommandType = CommandType.Text;
                lib = myCommand.ExecuteScalar().ToString();
                mySqlConnection.Close();
            }
            return lib;
        }

        public decimal inc_id_projetd()
        {

            decimal x = 0;
            OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString);
            
            mySqlConnection.Open();
            string cmdQuery = "SELECT COUNT(*) AS NB FROM ESP_PROJET_N";

            Oracle.ManagedDataAccess.Client.OracleCommand myCommand = new OracleCommand(cmdQuery);
            myCommand.Connection = mySqlConnection;
            myCommand.CommandType = CommandType.Text;
            myCommand.Transaction = myTrans;

            using (OracleDataReader myReader = myCommand.ExecuteReader())
            {
                if (myReader.HasRows) 
                {
                    x = myReader.GetDecimal(myReader.GetOrdinal("NB"));
                }


            return x;
            }

        }

     //add

        public DataTable bindlsprof(string idens)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            // OracleCommand cmd = new OracleCommand(" select * from esp_projet_n where id_ens='"+idens+"' ", con);


            OracleCommand cmd = new OracleCommand(" select a.id_projet,NOM_PROJET,TYPE_PROJET,id_et,lib_nome as libel_proj,t.NOM_ET ||' '||t.PNOM_ET as nom from esp_projet_n a inner join ESP_PROJET_ETUDIANT e on e.ID_PROJET=a.ID_PROJET inner join code_nomenclature c on c.CODE_NOME=a.TYPE_PROJET inner join esp_etudiant t on t.ID_ET=e.ID_ET where id_ens='" + idens + "' and c.CODE_STR='79'", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


        public DataTable getlistProjet(string an,string id_ens)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select nom_projet,id_projet from esp_projet_n where annee_deb='" + an + "' and ID_ENS='"+id_ens+"' ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


    // select NOM_ET||' '||PNOM_ET || ' '||e2.ID_ET ||'  '|| e1.code_cl as NOM, e2.ID_ET as ID_ET from esp_inscription e1,esp_etudiant e2 where annee_deb=2014  AND e2.id_et=e1.id_et AND e2.etat='A' order by e1.code_cl"
       
     public DataTable getliststudents(string an)
        {

            DataTable dt = new DataTable();

            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            OracleCommand cmd = new OracleCommand(" select NOM_ET||' '||PNOM_ET || ' '||e2.ID_ET ||'  '|| e1.code_cl as NOM, e2.ID_ET as ID_ET from esp_inscription e1,esp_etudiant e2 where annee_deb='"+an+"'  AND e2.id_et=e1.id_et AND e2.etat='A' order by e1.code_cl ", con);

            OracleDataAdapter od = new OracleDataAdapter(cmd);
            od.Fill(dt);

            con.Close();
            return dt;
        }


     //select lib_nome from esp_projet_n a inner join CODE_NOMENCLATURE c on c.CODE_NOME=a.TYPE_PROJET where id_projet='PROJ6' and c.CODE_str='79';

     public DataTable gettypeprojet(string an, string idprooo)
     {

         DataTable dt = new DataTable();

         OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
         con.Open();
         OracleCommand cmd = new OracleCommand("select lib_nome,type_projet from esp_projet_n a inner join CODE_NOMENCLATURE c on c.CODE_NOME=a.TYPE_PROJET where id_projet='"+idprooo+"' and c.CODE_str='79' and annee_deb='"+an+"'", con);

         OracleDataAdapter od = new OracleDataAdapter(cmd);
         od.Fill(dt);

         con.Close();
         return dt;
     }

     public void affecter_project(string _Id_projet, string id_et)
     {
         OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
         conn.Open();
         OracleCommand cmd = conn.CreateCommand();
         cmd.CommandText = "insert into esp_projet_etudiant(Id_projet,id_et,date_saisie) values ('" + _Id_projet + "','"+id_et+"',to_date(sysdate,'dd/MM/yyyy HH24:MI:SS'))";

         cmd.ExecuteNonQuery();
     }
 
 }
}

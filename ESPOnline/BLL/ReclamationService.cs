﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;

namespace BLL
{
    public class ReclamationService
    {
        ReclamationDAO dao = ReclamationDAO.Instance;

        public void ajouterReclamation(RECLAMATIONN rec)
        {
            dao.ajouterReclamation(rec);
        }

        public void modifierReclamation(RECLAMATIONN rec)
        {
            dao.modifierReclamation(rec);
        }
        public void supprimerReclamation(RECLAMATIONN rec)
        {
            dao.supprimerReclamation(rec);
        }
        public List<RECLAMATIONN> listerReclamation()
        {
            return dao.listerReclamation();
        }

        public List<RECLAMATIONN> listerRReclamationParType(decimal id)
        {
            return dao.listerReclamation().Where(p => p.ID_RECLAMTION == id).ToList<RECLAMATIONN>();
        }
        public List<RECLAMATIONN> listerRReclamationParType(string traiter)
        {
            return dao.listerReclamation().Where(p => p.TRAITER == traiter).ToList<RECLAMATIONN>();
        }
        public List<RECLAMATIONN> listerREntete_ReclamationParType(decimal identete)
        {
            return dao.listerReclamation().Where(p => p.ID_ENTETE_RECLAMATION == identete).ToList<RECLAMATIONN>();
        }
        public List<RECLAMATIONN> listerRReclamationPardescription(string description)
        {
            return dao.listerReclamation().Where(p => p.DESCRIPTION == description).ToList<RECLAMATIONN>();
        }

    }
}

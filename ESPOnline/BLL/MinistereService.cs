﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
   public class MinistereService
    {
       MinistereDAO service = MinistereDAO.Instance;
        DataTable dt = new DataTable();

       //masculin
        public string Get_NB_ETUD_MASC()
        {
            return service.Get_NB_ETUD_MASC();
        }
       //fem
        public string Get_NB_ETUD_fem()
        {
            return service.Get_NB_ETUD_fem();
        }
       //total
        public string Get_NB_ETUD_total()
        {
            return service.Get_NB_ETUD_total();
        }
       //etranger
        public string Get_NB_ETUD_etrange()
        {
            return service.Get_NB_ETUD_etrange();
        }

       //1ere masc
        public string Get_NB_ETUD_1ERE_MASC()
        {
            return service.Get_NB_ETUD_1ERE_MASC();
        }

       //1ere fem
        public string Get_NB_ETUD_1ERE_FEM()
        {
            return service.Get_NB_ETUD_1ERE_FEM();
        }
       //nbens
        public string Get_NB_ENS()
        {
            return service.Get_NB_ENS();
        }

       //nb ens contractuel
        public string Get_NB_contractuel()
        {
            return service.Get_NB_contractuel();
        }
       //nb ens masc
        public string Get_NB_masc()
        {
            return service.Get_NB_masc();
        }

       //nb ens fem
        public string Get_NB_fem()
        {
            return service.Get_NB_fem();
        }

       //bind rep en arab

        public DataTable rep_etudant_arabic()
        {
            dt = service.rep_etudant_arabic();
            return dt;
        }

        public DataTable rep_etudant_etrgr()
        {
            dt = service.rep_etudant_etrgr();
            return dt;
        }

        public void TEST(string nationalite)
        {
            service.TEST(nationalite);
        }

        public DataTable getARABE()
        {
            dt = service.getARABE();
            return dt;
        }

        public DataTable bind_result()
        {
            dt = service.bind_result();
            return dt;
        }

        public DataTable Titre_Diplome()
        {
            dt = service.Titre_Diplome();
            return dt;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
  public  class CahietTextService
    {
      CahierTexteDAO dao = CahierTexteDAO.Instance;

      DataTable dt = new DataTable();

      public DataTable bind_cdclens(string _id_ens,int num)
      { 
          dt=dao.bind_cdclens(_id_ens,num);
          return dt;
      }

      public DataTable bind_module(string _id_ens, string code_cl,int numsemestre)
      {
          dt = dao.bind_module(_id_ens,code_cl,numsemestre);
          return dt;
      }

      public DataTable bind_CRENEAU()
      {
          dt = dao.bind_CRENEAU();
          return dt;
      }


      public void add_cahierTEXT(string _CODE_CL, string _CODE_MODULE, DateTime _DATE_SEANCE, string _DUREE, string _CONTENU_TRAITE, string _REMARQUE, string _HEURE_DEBUT, string _HEURE_FIN, string _ID_ENS, string _ANNEE_DEB,int num_sem)
      {
          dao.add_cahierTEXT(_CODE_CL, _CODE_MODULE, _DATE_SEANCE, _DUREE, _CONTENU_TRAITE, _REMARQUE, _HEURE_DEBUT, _HEURE_FIN, _ID_ENS, _ANNEE_DEB, num_sem);
      }


       public DataTable VerifCahier(string id_ens, string code_module, string code_cl, int semestre)
         {
             dt = dao.VerifCahier(id_ens, code_module, code_cl, semestre);
             return dt;
         }


       public DataTable GetDataParclasse(string code_cl, int semestre)
       {
           dt = dao.GetDataParclasse(code_cl, semestre);
           return dt;
       
       }

       public DataTable GetDataParens(string id_ens, int semestre)
       {
           dt = dao.GetDataParens(id_ens, semestre);
           return dt;
       }

       public DataTable GetDataPardate(DateTime DATE_SEANCE, int semestre)
       {
           dt = dao.GetDataPardate(DATE_SEANCE, semestre);
           return dt;
       
       }

       public DataTable Getlistens()
       {
           dt = dao.Getlistens();
           return dt;
       }

       public DataTable Getlistcl(string code_cl)
       {
           dt = dao.Getlistcl(code_cl);
           return dt;
       }

       public DataTable bind_niveau()
       {
           dt = dao.bind_niveau();
           return dt;
       }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DAL;

namespace BLL
{

    public class ServiceEncadrement
    {
        EncadDAO dao = EncadDAO.Instance;
        DataTable dt;

        public DataTable Getmodule()
        {
            dt = dao.Getmodule();
            return dt;
        }

        public DataTable Gettechnologie()
        {
            dt = dao.Gettechnologie();
            return dt;
        }

        public DataTable GetMethodologie()
        {
            dt = dao.GetMethodologie();
            return dt;
        }

        public DataTable Gettypeprojet()
        {
            dt = dao.Gettypeprojet();
            return dt;
       
        }

        public void insert_Project(string _Id_projet, string _ANNEE_DEB, string _nom_projet, string _description_projet, string _technologies, string _methodologie, string _type_projet, string duree, string _code_module, string _id_enseignant, string s, string p)
        { 
        dao.insert_Project( _Id_projet, _ANNEE_DEB, _nom_projet,  _description_projet, _technologies, _methodologie, _type_projet,  duree,  _code_module, _id_enseignant, s,p);
        
        }


        public string get_an()
        {
            return dao.get_an();
        }


        public decimal inc_id_projetd()
        {
            return dao.inc_id_projetd();
        }
        public string inc_id_projet()
        {
            return dao.inc_id_projet();
        
        }
        public DataTable getlistProjet(string an,string id_ens)
        {

            dt = dao.getlistProjet(an,id_ens);
            return dt;
        
        }

        public DataTable getliststudents(string an)
        {
            dt = dao.getliststudents(an);
            return dt;
        }

        public DataTable gettypeprojet(string an,string idproo)
        {
            dt = dao.gettypeprojet(an,idproo);
            return dt;
        }


        public DataTable bindlsprof(string id_ens)
        {
            dt = dao.bindlsprof(id_ens);
            return dt;
        
        }
        public void affecter_project(string _Id_projet, string id_et)
        {

            dao.affecter_project(_Id_projet, id_et);
           
        }
    }
}

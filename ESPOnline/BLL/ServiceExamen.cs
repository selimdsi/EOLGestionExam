﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using System.Data;

namespace BLL
{
  public  class ServiceExamen
    {

      ExamenDAO service = ExamenDAO.Instance;
      DataTable dt = new DataTable();

      public DataTable getsalles(int numero, int semestre, int periode)
      {
          dt = service.getsalles(numero, semestre, periode);
          return dt;
      }

      public DataTable getsallesmodif(int numero, int semestre, int periode)
      {
          dt = service.getsallesmodif(numero, semestre, periode);
          return dt;
      }

      public DataTable getsurv(int numero, int semestre, int periode)
      {
          dt = service.getsurv(numero, semestre, periode);
          return dt;
      }

      public DataTable getsurvmodif(int numero, int semestre, int periode)
      {
          dt = service.getsurvmodif(numero, semestre, periode);
          return dt;
      }

      public DataTable bind_MODULE_CODECL()
      {
          dt = service.bind_MODULE_CODECL();
          return dt;
      }
      public void affecter_salle_examen(string CODE_CL, string module, string salle1)
      {
          service.affecter_salle_examen( CODE_CL,  module,  salle1);
      }
      public string get_salle_dispo()
      { 
      return service.get_salle_dispo();
      }

      public DataTable bind_calendar_exam()
      {
          dt = service.bind_calendar_exam();
          return dt;
      }

      public DataTable customerss()
      {
          dt = service.customerss();
          return dt;
      }

      public DataTable PLan_etudes(string cl)
      {
          dt = service.PLan_etudes(cl);
          return dt;
      }

      public DataTable code_cl()
      {
          dt = service.code_cl();
          return dt;
      }


      public DataTable Affecter_exam()
      {
          dt = service.Affecter_exam();
          return dt;
      }

      public DataTable bind_modules(string cod_cl, string num_sem)
      {
          dt = service.bind_modules(cod_cl,num_sem);
          return dt;
      }

      public DataTable bind_semestre()
      {
          dt = service.bind_semestre();
          return dt;
      }

      public DataTable List_etud_2015()
      {
          DataTable dt = new DataTable();
          dt = service.List_etud_2015();
          return dt;
      }

      public DataTable bind_site(string annee)
      {
          
          dt = service.bind_site(annee);
          return dt;
      }

      public DataTable bind_niveauGH(string site)
      {
          
          dt = service.bind_niveauGH(site);
          return dt;
      }

      public DataTable bind_classesgh(string niv)
      {

          dt = service.bind_classesgh(niv);
          return dt;

      }



      public DataTable bind_classes(string niv)
      {
          dt = service.bind_classes(niv);
          return dt;

      }
      public DataTable bind_classesCH(string niv)
      {
          dt = service.bind_classesCH(niv);
          return dt;
      }

      public DataTable bind_classesALL()
      {
          dt = service.bind_classesALL();
          return dt;
      }


      public DataTable bind_exam_aprogrammer(string code_cl,string num_semestre)
      {
          
          dt = service.bind_exam_aprogrammer(code_cl,num_semestre);
          return dt;

      }

      public DataTable bind_exam_aprogrammerbymodule(string code_mod, string num_sem)
      {

          dt = service.bind_exam_aprogrammerbymodule(code_mod, num_sem);
          return dt;

      }

      public DataTable bind_exam_aprogrammerP2(string code_cl, string num_semestr)
      {

          dt = service.bind_exam_aprogrammerP2(code_cl, num_semestr);
          return dt;

      }


      public DataTable bind_exam_byclasses(string code_cl, string num_semestr)
      {

          dt = service.bind_exam_byclasses(code_cl, num_semestr);
          return dt;


      }

      public DataTable bind_exam_byclassesord(string code_cl, string num_semestr)
      {
          dt = service.bind_exam_byclassesord(code_cl, num_semestr);
          return dt;

      
      }


      public DataTable bind_exam_aprogrammer(string code_cl)
      {
        
          dt = service.bind_exam_aprogrammer(code_cl);
          return dt;

      
      }

      public DataTable exist(string code_cl, string code_module)
      {

          dt = service.exist(code_cl, code_module);
          return dt;
      }


         public DataTable bind_classesbyclasses(string niv)
        {
              dt = service.bind_classesbyclasses(niv);
          return dt;
         }


         public DataTable bind_seance_exam()
         {
             dt = service.bind_seance_exam();
             return dt;
         }

         public string Get_annee_deb()
         {

             return service.Get_annee_deb();
         }

         public DataTable bind_site2(string annee)
         {
             dt = service.bind_site2(annee);
             return dt;
         }

         public void affecter_module_examen(string numseance, string a_programmer, string type_exam, string code_cl, string code_module, int num_semestre, string num_periode, string DS_EXAM, string anne,string s)
         {

             service.affecter_module_examen(numseance,a_programmer, type_exam, code_cl, code_module, num_semestre, num_periode, DS_EXAM, anne,s);
             

         }

         public void affecter_module_examen_by_module(string numseance, string a_programmer, string type_exam, string code_cl, string code_module, int num_semestre, string num_periode, string DS_EXAM, string anne, string s)
         {

             service.affecter_module_examen_by_module(numseance, a_programmer, type_exam, code_cl, code_module, num_semestre, num_periode, DS_EXAM, anne, s);


         }



         public DataTable fill_jours()
         {
             dt = service.fill_jours();
             return dt;
         
         }



         public DataTable fill_Num_jours()
         {


             dt = service.fill_Num_jours();
             return dt;
         
         }


         public DataTable existSeance(string num_seance, string periode, int semestre, string dateexam, string annee_deb)
         { 
            dt=service. existSeance(num_seance,periode,semestre,dateexam, annee_deb);
      return dt;
         
         }


         public DataTable Cloner(string annee_deb, string periode, int semestre, string site)
         {
             dt = service.Cloner(annee_deb, periode, semestre, site);
             return dt;
         }

         //public DataTable get_annee_parametr()
         //{
         //    dt = service.get_annee_parametr();
         //    return dt;

         //}

         public string get_annee_parametr()
         {
             return service.get_annee_parametr();
         }

         public string get_annee_parametrfin()
         {
             return service.get_annee_parametrfin();
         }

         public string get_annee_0()
         {
             return service.get_annee_0();
         }


         public DataTable getmap_x(string annee, int s, int p, string site)
         {
             dt = service.getmap_x(annee,s,p,site);
             return dt;
         
         }

         public DataTable getmap_detaille(string annee, int s, int p, string site)
         {
             dt = service.getmap_detaille(annee, s, p, site);
             return dt;
         
         }
         public DataTable getpageparsite(string site)
         {
             dt = service.getpageparsitr(site);
             return dt;
         }
         public DataTable getmap_detaille(string annee, int s, int p, string site, string page)
         {
             dt = service.getmap_detaille(annee,  s,  p,  site, page);
             return dt;
         }

         public void CreateMapExamenx(string annee, int semestre, int periode, string site)
         { 
         service.CreateMapExamenx( annee,  semestre,  periode,  site);
         }

         public void CreateMapDetaillee(string annee, int semestre, int periode, string site,string i)
         {
             service.CreateMapDetaillee(annee, semestre, periode, site,i);
         }

         public void CreateMapExamen(string annee, int semestre, int periode, string site)
         {

             service.CreateMapExamen(annee, semestre, periode, site);
         }


         public DataTable getmapdsex(string annee, int s, int p, string site)
         {

             dt = service.getmapdsex(annee, s, p, site);
             return dt;
         }


         public DataTable bind_cls(string niv, string site)
         {
             dt = service.bind_cls(niv,site);
             return dt;
         
         }

         public DataTable existDUPLICATION(string MODule, string SEANCE)
         {
             dt = service.existDUPLICATION( MODule,  SEANCE);
             return dt;
         
         }



        
}
    }


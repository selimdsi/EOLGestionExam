﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;

namespace BLL
{
    public class StatService
    {
        StatDAO notedao = StatDAO.Instance;
       public DataTable GetserviceListNotes(string CODE_CL, string CODE_MODULE, string Annee)
       {
           return notedao.GetListNotes(CODE_CL, CODE_MODULE,Annee);
       }

       public DataTable GetserviceListNotes2(string CODE_CL, string CODE_MODULE, string ID_ENS, string Annee)
       {
           return notedao.GetListNotes2(CODE_CL, CODE_MODULE, ID_ENS,Annee);
       }

       public DataTable GetserviceListNotes3(string CODE_CL, string CODE_MODULE, string ID_ENS, string Annee)
       {
           return notedao.GetListNotes3(CODE_CL, CODE_MODULE, ID_ENS,Annee);
       }

       public DataTable GetserviceListNotes4(string CODE_CL, string CODE_MODULE, string ID_ENS,string Annee)
       {
           return notedao.GetListNotes4(CODE_CL, CODE_MODULE, ID_ENS,Annee);
       }

       public DataTable GetserviceListNotes5(string ID_ENS,string Annee)
       {
           return notedao.GetListNotes5(ID_ENS,Annee);
       }

    

       public DataTable GetserviceListNotes7(string CODE_CL, string CODE_MODULE, string ID_ENS,string Annee)
       {
           return notedao.GetListNotes7(CODE_CL, CODE_MODULE, ID_ENS,Annee);
       }

       public DataTable GetserviceListNotes8(string CODE_CL, string CODE_MODULE, string ID_ENS,string Annee)
       {
           return notedao.GetListNotes8(CODE_CL, CODE_MODULE, ID_ENS,Annee);
       }


       public DataTable GetserviceListNotes6(string ID_ENS, string CODE_CL, string Annee)
       {
           return notedao.GetListNotes6(ID_ENS, CODE_CL,Annee);
       }

       public DataTable GetserviceTauxRep(string CODE_CL, string CODE_MODULE, string ID_ENS, string Annee)
       {
           return notedao.GetTauxRep(CODE_CL, CODE_MODULE, ID_ENS, Annee);
       }

       public DataTable GetNbre_niv_langue(string code_cl)
       {
           DataTable dt = new DataTable();
           dt = notedao.GetNbre_niv_langue(code_cl);
           return dt;
       }

       public string returnCode_cl()
       {
           return notedao.returnCode_cl();
       }


       public DataTable Afficher_list_charguia()
       { DataTable dt = new DataTable();
       dt = notedao.Afficher_list_charguia();

       return dt;
       }

       public DataTable Afficher_list_ghazela()
       {
           DataTable dt = new DataTable();
           dt = notedao.Afficher_list_gazela();

           return dt;
       }

       public string totalgh()
       {
           return notedao.totalgh();
       }


       public string totalch()
       {
           return notedao.totalch();
       }

       public DataTable Afficher_listPARniv()
       {
           DataTable dt = new DataTable();
           dt = notedao.Afficher_listPARniv();
           return dt;
       }

       public DataTable Afficher_list_ens_UPaffecter(string num_S)
       {
           DataTable dt = new DataTable();
           dt = notedao.Afficher_list_ens_UPaffecter(num_S);
           return dt;
       }
       public DataTable Afficher_ens_saisis(string num_semes)
       {
           DataTable dt = new DataTable();
           dt = notedao.Afficher_ens_saisis(num_semes);
           return dt;
       }
       public DataTable Afficher_list_ens_affecter(string num_semestre)
       {

           DataTable dt = new DataTable();
           dt = notedao.Afficher_list_ens_affecter(num_semestre);
           return dt;
       }
       public DataTable Afficher_listetudPARniv()
       {
           DataTable dt = new DataTable();
           dt = notedao.Afficher_listetudPARniv();
           return dt;
       
       }



        //afficher stat site
       public DataTable Afficher_stat_site()
       {
           DataTable dt = new DataTable();
           dt = notedao.Afficher_stat_site();
           return dt;
       
       }


       public DataTable Afficher_total_par_CLASSE()
       {

           DataTable dt = new DataTable();
           dt = notedao.Afficher_total_par_CLASSE();
           return dt;
       }


       public DataTable Afficher_total_par_niv()
       {

           DataTable dt = new DataTable();
           dt = notedao.Afficher_total_par_niv();
           return dt;
       }

       public DataTable Afficher_total_DES_TOTAUX()
       {

           DataTable dt = new DataTable();
           dt = notedao.Afficher_total_DES_TOTAUX();
           return dt;
       }

        //******************************

       //Répartition des étudiants par Niveau et Pôle 
       public DataTable ReparEtudiantParPole()
       {
           DataTable dt = new DataTable();
           dt = notedao.ReparEtudiantParPole();
           return dt;
       }

          //total Répartition des étudiants par Niveau et Pôle
       public DataTable titalReparEtudiantParPole()
       {
           DataTable dt = new DataTable();
           dt = notedao.titalReparEtudiantParPole();
           return dt;
       }

          //Répartition des Classes par Niveau etPôle
       public DataTable ReparclasseParPole()
       {
           DataTable dt = new DataTable();
           dt = notedao.ReparclasseParPole();
           return dt;
       }

        //total Répartition des Classes par Niveau etPôle
       public DataTable totalReparclasseParPole()
       {
           DataTable dt = new DataTable();
           dt = notedao.totalReparclasseParPole();
           return dt;
       }

        //rep nouv inscrits

       public DataTable Rep_nouv_inscrits()
       {
           DataTable dt = new DataTable();
           dt = notedao.Rep_nouv_inscrits();
           return dt;
       }

       public DataTable List_etud_2015()
       {
           DataTable dt = new DataTable();
           dt = notedao.List_etud_2015();
           return dt;
       }

       public DataTable testarabic()
       {
           DataTable dt = new DataTable();
           dt = notedao.testarabic();
           return dt;
       }



       public DataTable bind_tous_etud(string annee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_tous_etud(annee_deb);
           return dt;
       }
       public DataTable bind_classes(string niv)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_classes(niv);
           return dt;
       
       }
       public DataTable bind_niveau(string site)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_niveau(site);
           return dt;
       }
       public DataTable bind_site(string annee)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_site(annee);
           return dt;
       }
       public DataTable bind_num_semestre()
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_num_semestre();
           return dt;
       }

       public DataTable bind_tous_etudss(string annee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_tous_etud(annee_deb);
           return dt;
       }
//****************************************************************service stat********************************************************************
       public DataTable Affich_list_etud_niveau_ang()
       {
           DataTable dt = new DataTable();
           dt = notedao.Affich_list_etud_niveau_ang();
           return dt;
       }

       public DataTable Affich_list_etud_niveau_FR()
       {
           DataTable dt = new DataTable();
           dt = notedao.Affich_list_etud_niveau_FR();
           return dt;
       }




        //*********************************************************************Modification annee 2014*********************************************************************

       public DataTable bind_niveau_1516(string annee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_niveau_1516(annee_deb);
           return dt;
       }
       public DataTable bind_classes_1516(string niv,string annee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_classes_1516(niv, annee_deb);
           return dt;
       }

       public DataTable bind_Nom_Ens(string code_classe,string annee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_Nom_Ens(code_classe, annee_deb);
           return dt;
       }

       public DataTable bind_niveau_ang_et(string namess,string code_cl,string anneee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_niveau_ang_et(namess, code_cl, anneee_deb);
           return dt;
       }
       //public string Return_id_et(string code, string name)
       //{
       //    return notedao.Return_id_et(code,name);
       //}

       public DataTable bind_list_niveau(string annee_deb)
       {
           DataTable dt = new DataTable();
           dt = notedao.bind_list_niveau(annee_deb);
           return dt;
       
       }

       public DataTable bind_list_niveau2(string annee_deb)
       {

           DataTable dt = new DataTable();
           dt = notedao.bind_list_niveau2(annee_deb);
           return dt;
       }

       public int Update_niv_etud(string id_etud, string niv_fr, string id_ens,string annee_deb)
       {
           return notedao.Update_niv_etud(id_etud, niv_fr, id_ens, annee_deb);
       }

       public int Update_niv_etud_ang(string id_etud, string niv_ang, string id_ens)
       {
           return notedao.Update_niv_etud_ang(id_etud, niv_ang, id_ens);
       }

       public string GetUP(string id_ens)
       {
           return notedao.GetUP(id_ens);
       }
    }
    }


﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetMap.aspx.cs" 
Inherits="ESPOnline.Direction.GetMap" MasterPageFile="~/Direction/Site2.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

      
          .style3
        {
            color: #000000;
        }
        .style4
        {
            color: #CC0000;
        }
        .style5
        {
            color: #0066FF;
        }
        .style6
        {
            color: #0000FF;
        }

      
          .style7
        {
            color: #FF0000;
        }

      
          .style8
        {
            color: #003300;
        }

      .grid td, .grid th{
  text-align:center;
          </style>

           <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <%--  <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>--%>
<script src="../Contents/Scripts/ScrollableGridPlugin_ASP.NetAJAX_2.0.js" type="text/javascript"></script>
    <%-- <script type="text/javascript">
    $(document).ready(function () {
        $('#<%=Gridtoiec.ClientID %>').Scrollable({
            ScrollHeight: 300,
         
        });
    });
    </script>--%>


   <%-- <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $("#<%=GridView1.ClientID %> input:checkbox").each(
        function () {

            this.onclick = function () {
                if (this.checked)
                    this.parentNode.parentNode.style.backgroundColor = '#00BFFF';
                else
                    this.parentNode.parentNode.style.backgroundColor = 'transparent';

            }

        })
        });
    </script>--%>

    <style type="text/css">
     lblanneedeb.Size = new Size(24, 72);
                lblanneedeb.Font = new Font("Verdana", 12);
    </style>


    <style type="text/css"> 
.styled-button-10 {
	background:#ff0000;
	background:-moz-linear-gradient(top,#ff0000 0%,#ff0000 100%);
	background:-weNeue',sans-serif;
	font-size:16px;bkit-gradient(linear,left top,left bottom,color-stop(0%,#ff0000),color-stop(100%,#ff0000)): "Helvetica 
	border-radius:5px";
	background:-webkit-linear-gradient(top,#ff0000 0%,#ff0000 100%);
	background:-o-linear-gradient(top,#f65c6c 0%,#ff0000 100%);
	background:-ms-linear-gradient(top,#ff0000 0%,#ff0000 100%);
	background:#ff0000;
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#ff0000', endColorstr='#ff0000',GradientType=0);
	padding:10px 15px;
	color:#fff;
	font-family:'Helvetica ';
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #a70f1f
}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
<h3>Generation automatique de Map_X</h3>
<br />
<asp:GridView runat="server" ID="griDMaPX"  ></asp:GridView>
</asp:Content>
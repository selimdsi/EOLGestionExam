﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class Affectation_seance : System.Web.UI.Page
    {



        private void SetInitialRow()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));
            dt.Columns.Add(new DataColumn("Column5", typeof(string)));
            dt.Columns.Add(new DataColumn("Column6", typeof(string)));
            dt.Columns.Add(new DataColumn("Column7", typeof(string)));
            dt.Columns.Add(new DataColumn("Column8", typeof(string)));
            dt.Columns.Add(new DataColumn("Column9", typeof(string)));
            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;
            dr["Column3"] = string.Empty;

            dr["Column4"] = string.Empty;
            dr["Column5"] = string.Empty;
            dr["Column6"] = string.Empty;

            dr["Column7"] = string.Empty;
            dr["Column8"] = string.Empty;
            dr["Column9"] = string.Empty;



            dt.Rows.Add(dr);

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            Gridview1.DataSource = dt;
            Gridview1.DataBind();
        }






        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];

                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox2");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox3");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox4");

                        TextBox box11 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("TextBox5");
                        TextBox box21 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("TextBox6");
                        TextBox box31 = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("TextBox7");

                        TextBox box111 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("TextBox8");
                        TextBox box211 = (TextBox)Gridview1.Rows[rowIndex].Cells[8].FindControl("TextBox9");
                        TextBox box311= (TextBox)Gridview1.Rows[rowIndex].Cells[9].FindControl("TextBox10");
                        
                        
                       

                        drCurrentRow = dtCurrentTable.NewRow();
                        drCurrentRow["RowNumber"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["Column1"] = box1.Text;
                        dtCurrentTable.Rows[i - 1]["Column2"] = box2.Text;
                        dtCurrentTable.Rows[i - 1]["Column3"] = box3.Text;

                        dtCurrentTable.Rows[i - 1]["Column4"] = box11.Text;
                        dtCurrentTable.Rows[i - 1]["Column5"] = box21.Text;
                        dtCurrentTable.Rows[i - 1]["Column6"] = box31.Text;


                        dtCurrentTable.Rows[i - 1]["Column7"] = box111.Text;
                        dtCurrentTable.Rows[i - 1]["Column8"] = box211.Text;
                        dtCurrentTable.Rows[i - 1]["Column9"] = box311.Text;


                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }
        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TextBox box1 = (TextBox)Gridview1.Rows[rowIndex].Cells[1].FindControl("TextBox2");
                        TextBox box2 = (TextBox)Gridview1.Rows[rowIndex].Cells[2].FindControl("TextBox3");
                        TextBox box3 = (TextBox)Gridview1.Rows[rowIndex].Cells[3].FindControl("TextBox4");

                        TextBox box11 = (TextBox)Gridview1.Rows[rowIndex].Cells[4].FindControl("TextBox5");
                        TextBox box21 = (TextBox)Gridview1.Rows[rowIndex].Cells[5].FindControl("TextBox6");
                        TextBox box31 = (TextBox)Gridview1.Rows[rowIndex].Cells[6].FindControl("TextBox7");

                        TextBox box111 = (TextBox)Gridview1.Rows[rowIndex].Cells[7].FindControl("TextBox8");
                        TextBox box211 = (TextBox)Gridview1.Rows[rowIndex].Cells[8].FindControl("TextBox9");
                        TextBox box311 = (TextBox)Gridview1.Rows[rowIndex].Cells[9].FindControl("TextBox10");
                        
                        box1.Text = dt.Rows[i]["Column1"].ToString();
                        box2.Text = dt.Rows[i]["Column2"].ToString();
                        box3.Text = dt.Rows[i]["Column3"].ToString();

                        box11.Text = dt.Rows[i]["Column4"].ToString();
                        box21.Text = dt.Rows[i]["Column5"].ToString();
                        box31.Text = dt.Rows[i]["Column6"].ToString();

                        box111.Text = dt.Rows[i]["Column7"].ToString();
                        box211.Text = dt.Rows[i]["Column8"].ToString();
                        box311.Text = dt.Rows[i]["Column9"].ToString();



                        rowIndex++;
                    }
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetInitialRow();
            }
        }
        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }

        protected void btnvalid_Click(object sender, EventArgs e)
        {
                           
                    
                      foreach (GridViewRow row in Gridview1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {

                            OracleCommand cmd = new OracleCommand("insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode) values (:num_seance,:annee_deb,to_date(:date_seance,'dd/MM/yy'),:semestre,:num_jours,:jours,:heure_debut,:heure_fin,:site,:periode) ");
                                    //("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam,code_module=:code_module where code_cl =:code_cl");


                            cmd.Parameters.Add(":num_seance", row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":annee_deb", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add("to_date(:date_seance,'dd/mm/yy')", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":semestre", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":num_jours", row.Cells[4].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":jours", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":heure_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":heure_fin", row.Cells[8].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":site", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":periode", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);
                               
                               
                              
                                this.ExecuteQuery(cmd, "UPDATE");

                            }
                                        
                   
                    //rebind grid maha
                    //gvCustomers.DataSource = service.bind_niveau_etudiantbycl(ddclasse2.SelectedValue, ddlannee_debM.SelectedValue);
                    //gvCustomers.DataBind();
                }

                      Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");
                   

    }
        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }







      }
    }

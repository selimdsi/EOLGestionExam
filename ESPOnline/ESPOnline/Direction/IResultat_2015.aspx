﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IResultat_2015.aspx.cs" Inherits="ESPOnline.Direction.IResultat_2015"

MasterPageFile="~/Direction/Site2.Master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

      
          .grid td, .grid th{
     text-align:center;
      
          }
              
          .style5
        {
            color: #FF0000;
            font-size: x-large;
            font-weight: normal;
        }
              
          </style>

        
</asp:Content>


<asp:Content ID="Content11" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
<h2>بطاقة عـــدد 8</h2>
<h3 class="style5">
نتائج الإمتحانات(الإرتقاء) في آخر السنة الجامعية 2014/2015</h3>
<br />
 <h2 class="style6">
توزيع الطلبة بين جدد و راسبين</h2>
&nbsp;<h3 class="style7">السنة الجامعية:2016/2015</h3>
<br />
<asp:GridView runat="server" ID="GridView1" AutoGenerateColumns="False" dir="rtl" 
                                                      
                                                         Style="border-bottom: white 2px ridge; border-left: white 2px ridge;
                                                        background-color: white; border-top: white 2px ridge; border-right: white 2px ridge;"
                                                        BorderWidth="0px" BorderColor="Red"  CssClass="grid"
                                                        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="Both" EmptyDataRowStyle-CssClass="ItemStyle"  BackColor="#0099CC"
                                                    onrowdatabound="GridView1_RowDataBound"
                                                    ShowFooter="true"
                                                       >
                                                        <EmptyDataTemplate>
                                                            Pas d'enregistrement.
                                                        </EmptyDataTemplate>
                                                        
                                                        <HeaderStyle HorizontalAlign="Center" Height="40px" Width="100px" BackColor="Red" />
                                                        <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                                                        <FooterStyle CssClass="ItemStyle" />
                                                        <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                                                        <RowStyle CssClass="GridItemStyle" />
                                                        <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        <Columns>


               <%-- <asp:BoundField DataField="DIPLOMEARB" HeaderText="عنوان الشهادة (*)" ReadOnly="True" 
                SortExpression="DIPLOMEARB" HeaderStyle-Width="200px" HeaderStyle-Height="30px" />--%>
                
                 <asp:BoundField DataField="SPECIALITEARB" HeaderText="الإختصاص" ReadOnly="True" 
                SortExpression="SPECIALITEARB" HeaderStyle-Width="150px" />
                
                <asp:BoundField DataField="NIVEAUARB" HeaderText="المستوى الدراسي" ReadOnly="True" 
                SortExpression="NIVEAUARB" HeaderStyle-Width="150px" /> 
                <%-- <asp:BoundField DataField="Niveau" HeaderText="المستوى" ReadOnly="True" 
                SortExpression="Niveau" HeaderStyle-Width="150px" />--%>

               <%-- <asp:BoundField DataField=" NB_Classes" HeaderText="عدد الأقسام" ReadOnly="True" 
                SortExpression=" NB_Classes" />--%>


                <asp:TemplateField HeaderText="المسجلون:ذكور" HeaderStyle-Width="100px">
        <ItemTemplate>
            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("Inscrits_masculin")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>


                <asp:TemplateField HeaderText="المسجلون:إناث" HeaderStyle-Width="100px">
         <ItemTemplate>
            <asp:Label ID="lblUnitsInStock" runat="server" Text='<%# Eval("Inscrits_feminin") %>' />
         </ItemTemplate>                   
         <FooterTemplate>
            <asp:Label ID="lblTotalUnitsInStock" runat="server" />
         </FooterTemplate>
      </asp:TemplateField>


              
                <asp:TemplateField HeaderText="المترشحون:ذكور" HeaderStyle-Width="100px">
         <ItemTemplate>
            <asp:Label ID="lblUnitsInStockGC" runat="server" Text='<%# Eval("P_inscrits_masculin") %>' />
         </ItemTemplate>                   
         <FooterTemplate>
            <asp:Label ID="lblTotalUnitsInStockGC" runat="server" />
         </FooterTemplate>
      </asp:TemplateField>

                 <asp:TemplateField HeaderText=" المترشحون:إناث" HeaderStyle-Width="100px">
         <ItemTemplate>
            <asp:Label ID="lblUnitsInStockGCTotal" runat="server" Text='<%# Eval("P_inscrits_feminin") %>' />
         </ItemTemplate>                   
         <FooterTemplate>
            <asp:Label ID="lblTotalUnitsInStockGCTotal" runat="server" />
         </FooterTemplate>
      </asp:TemplateField>


                <asp:TemplateField HeaderText="الناجحون:ذكور" HeaderStyle-Width="100px">
         <ItemTemplate>
            <asp:Label ID="lblUnitsInStockNB" runat="server" Text='<%# Eval("Reussis_masculin") %>' />
         </ItemTemplate>                   
         <FooterTemplate>
            <asp:Label ID="lblTotalUnitsInStockNB" runat="server" />
         </FooterTemplate>
      </asp:TemplateField>

      
                <asp:TemplateField HeaderText="الناجحون:إناث" HeaderStyle-Width="100px">
         <ItemTemplate>
            <asp:Label ID="lblUnitsInStockNBFEMM" runat="server" Text='<%# Eval("Reussis_feminin") %>' />
         </ItemTemplate>                   
         <FooterTemplate>
            <asp:Label ID="lblTotalUnitsInStockNBFEMM" runat="server" />
         </FooterTemplate>
      </asp:TemplateField>


                <%--  <asp:BoundField DataField="Nouveaux_inscrits_masculin" HeaderText="جدد ذكور" ReadOnly="True" 
                SortExpression="Nouveaux_inscrits_masculin" /> --%>

                <%--  <asp:BoundField DataField="Nouveaux_inscrits_feminin" HeaderText="جدد إناث" ReadOnly="True" 
                SortExpression="Nouveaux_inscrits_feminin" /> --%>
               
               <%-- <asp:BoundField DataField="Redoublants_inscrits_masculin" HeaderText="راسبون ذكور" ReadOnly="True" 
                SortExpression="Redoublants_inscrits_masculin" /> --%>

                 <%--<asp:BoundField DataField="Redoublants_inscrits_feminin" HeaderText="راسبون إناث" ReadOnly="True" 
                SortExpression="Redoublants_inscrits_feminin" /> --%>


                <%--<asp:BoundField DataField="total" HeaderText="المجموع" ReadOnly="True" 
                SortExpression="total" /> --%>


               <%-- nouv travail--%>

                     </Columns>
                     
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <RowStyle ForeColor="#000000"  />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                         <FooterStyle BackColor="Red" Font-Bold="True" ForeColor="White" HorizontalAlign="Center"  />
   <HeaderStyle BackColor="Red" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <RowStyle ForeColor="#000000"  />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        
                                                    </asp:GridView>

<br />
<br />

 <table>

                                           
                                           <tr>
                                           <td>
                                             
                                                    <h4>
                                             
                                                    <asp:Button  ID="Btnprint" runat="server" width="160px"
         Text="طباعة" 
Height="50px" ForeColor="Black" BackColor="#CCCCCC"  CssClass="text-info" onclick="Btnprint_Click" />
                                                    </h4>
                                           </td>
                                           <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; </td>
                                          <td>
                                          <asp:Button  ID="Button1" runat="server" Text="Exporter en excel" 
Height="50px" ForeColor="Black" BackColor="#CCCCCC"  CssClass="text-info" onclick="BuTT2_Click" />
                                          </td>  
                                            
                                            </tr>
                                            </table>  
                                            <table>
                                            <tr>
                                            <td>
                                            <br />
                                            <br />
                                            <br />
                                            <br />
                                            </td>
                                            </tr>
                                            </table>


</asp:Content>
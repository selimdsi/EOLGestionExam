﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class Consulter_seanceV1 : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                bind_lst_ex();
            }

            
        }

        public void bind_lst_ex()
        {

            gvCustomers.DataSource = service.bind_seance_exam();
            gvCustomers.DataBind();
        }



        protected void OnCheckedChangedDDD(object sender, EventArgs e)
        {
        }

        protected void OnCheckedChanged(object sender, EventArgs e)
        {


            bool isUpdateVisible = false;
//Label1.Text = string.Empty;

            //Loop through all rows in GridView
            foreach (GridViewRow row in gvCustomers.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                        row.RowState = DataControlRowState.Edit;

                    for (int i = 6; i < row.Cells.Count; i++)
                    {
                        row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                        }
                        if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                        }

                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }

                    }

                }





            }




            // UpdatePanel1.Update();
            //btnUpdate.Visible = isUpdateVisible;
        }



        protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {
        }

            //modifier les seance affectées
        





        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void btnadd_Click(object sender, EventArgs e)
        {
       
            //if exist faire le update
            foreach (GridViewRow row in gvCustomers.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {

                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                    {
                        //dt = service.exist(row.Cells[1].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value, row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text);
                        //if (dt.Rows.Count != 0)
                        //{
                        OracleCommand cmd = new OracleCommand("update esp_seance_examen set date_seance=:date_seance,heure_debut=:heure_debut,heure_fin=:heure_fin,num_jours=:num_jours,jours=:jours where num_seance =:num_seance");

                        cmd.Parameters.Add(":date_seance", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":heure_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":heure_fin", row.Cells[8].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":num_jours", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":jours", row.Cells[10].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":num_seance", row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text);
                                               
                        
                        
                        
                        this.ExecuteQuery(cmd, "SELECT");
                        Response.Write(@"<script language='javascript'>alert('Modification avec succès');</script>");

                        bind_lst_ex();
                    }
                }

            }
        }



    }
}
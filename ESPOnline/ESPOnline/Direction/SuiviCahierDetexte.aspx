﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SuiviCahierDetexte.aspx.cs" 
Inherits="ESPOnline.Direction.SuiviCahierDetexte" MasterPageFile="~/Direction/Site2.Master"  EnableEventValidation = "false" %>

 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">


.custom-calendar .ajax__calendar_container   
{
    background-color:#ffc; /* pale yellow */ border:solid 1px #666;z-index : 1000 ; 
    font-family:arial,helvetica,clean,sans-serif;
 
    } 
 
.custom-calendar .ajax__calendar_title {  background-color:#cf9; /* pale green */ height:20px;  color:#333; } 
 
.custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next {  background-color:#aaa; /* darker gray */ height:20px;  width:20px;
} 
 
.custom-calendar .ajax__calendar_day {  color:#333; /* normal day - darker gray color */} .custom-calendar .ajax__calendar_other .ajax__calendar_day {  color:#666; /* day not actually in this month - lighter gray color */} 
 
.custom-calendar .ajax__calendar_today {  background-color:#cff;  /* pale blue */ height:20px; }

</style>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
     
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <%--  <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>--%>
<script src="../Contents/Scripts/ScrollableGridPlugin_ASP.NetAJAX_2.0.js" type="text/javascript"></script>
     <script type="text/javascript">
    $(document).ready(function () {
        $('#<%=Gridexam.ClientID %>').Scrollable({
            ScrollHeight: 300,
         
        });
    });
    </script>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: normal;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: normal;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}
.grid td, .grid th{
  text-align:center;
      
          }
        .style1
        {
            color: #800000;
            font-size: large;
        }
      
          .style2
        {
            font-size: large;
        }
      
          </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <b>
    
<br class="style2" />
    </b>
<center>
<h3 class="style1">Suivi Cahier de textes des enseignants</h3>
<br />

              <%--<table><tr> <td class="style2">
                Semestre:
                </td>
            <td class="style5">
            
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"
                                                            AutoPostBack="True" >
                                                            <asp:ListItem Value="1" >Semestre 1</asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True">Semestre 2</asp:ListItem>
                                                       
                                                        </asp:RadioButtonList>
                                                                    </td></tr></table>--%>

  
<asp:Panel ID="plchoix" runat="server" Visible="false" >
<table>

<tr>
<td>
<asp:Label ID="Label11" runat="server" Text="Veuillez suivre le cahier de texte soit: "
                            ></asp:Label></td>

                                                     
   <td>          <asp:RadioButtonList ID="Rdchoix" runat="server"   
            RepeatDirection="Horizontal" width="500px" >
                 <asp:ListItem Value="1">Par date</asp:ListItem>
                 <asp:ListItem Value="2">Par enseignant</asp:ListItem>
                 <asp:ListItem Value="3">Par classe</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 <td></td>
 
            <td class="style2">
                Semestre:
                </td>
            <td class="style5">
            
                                                        <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal"
                                                            AutoPostBack="True" >
                                                            <asp:ListItem Value="1" >Semestre 1</asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True">Semestre 2</asp:ListItem>
                                                       
                                                        </asp:RadioButtonList>
                                                                    </td>
           
        

            <td><asp:Button  ID="btnvalid" runat="server" Text="Valider" Height="35px" 
                    Width="120px" onclick="btnvalid_Click" /></td>
</tr>
</table>
</asp:Panel>

<br />
<asp:Panel ID="pldate" runat="server" Visible="false">
<center><b>Suivi des enseignants par date de séance</b></center>
<table>

<tr>
<td><asp:Label runat="server" ID="lbldate" Text="Veuillez choisir une date: "></asp:Label>
<asp:TextBox ID="TBdateseance" runat="server" CssClass="form-inline form-control" 
        ontextchanged="TBdateseance_TextChanged" AutoPostBack="true"></asp:TextBox>
                <asp:CalendarExtender ID="TBdateseance_CalendarExtender" runat="server" 
                    Enabled="True" Format="dd/MM/yyyy" TargetControlID="TBdateseance" CssClass="custom-calendar">
                </asp:CalendarExtender></td>
</tr>

<tr>
<td>
<asp:GridView runat="server" ID="Gridexam" AutoGenerateColumns="False" 
                                                          
                                                         Style="border-bottom: black 2px ridge; border-left: black 2px ridge;
                                                        background-color: white; border-top: black 2px ridge; border-right: black 2px ridge;"
                                                        BorderWidth="0px" 
        BorderColor="Red"  CssClass="grid"
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="Both" 
        EmptyDataRowStyle-CssClass="ItemStyle"  BackColor="#0099CC"
  
                                                   
                                                       >
                                                        <EmptyDataTemplate>
                                                            Pas d'enregistrement.
                                                        </EmptyDataTemplate>
                                                        
                                                        <HeaderStyle HorizontalAlign="Center" Height="20px" Width="100px" BackColor="Red" />
                                                        <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                                                        <FooterStyle CssClass="ItemStyle" />
                                                        <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                                                        <RowStyle CssClass="GridItemStyle" />
                                                        <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        <Columns>
<asp:TemplateField ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderText="Nº">
  <ItemTemplate>
    <%# Container.DataItemIndex + 1 %>
  </ItemTemplate>
</asp:TemplateField>
           
      <asp:TemplateField HeaderText="Id enseignant" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("id_ens")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
       
      <asp:TemplateField HeaderText="Nom enseignant" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("nom_ens")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

        <asp:TemplateField HeaderText="Module" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Designation")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
       
      <asp:TemplateField HeaderText="Classe" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("code_cl")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Date seance" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Date_seance")%>' DataFormatString="{0:d}" />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

         <asp:TemplateField HeaderText="heure début" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("heure_debut")%>'  />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      <asp:TemplateField HeaderText="heure fin" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("heure_fin")%>'  />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      

                            
         </Columns>
                      
                           <HeaderStyle CssClass="GridHeaderStyle" />
                     <RowStyle ForeColor="#000000"  />
             <SelectedRowStyle CssClass="GridSelectedStyle" />
      <AlternatingRowStyle BackColor="#d2c1bf"  />

                                                    </asp:GridView>
</td>
</tr>
</table>


</asp:Panel>
<asp:Panel ID="plens" runat="server" Visible="false">

<b><center>Suivi par enseignant</center></b>
<br />
<table>
<asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>
<tr>
<td><asp:Label runat="server" ID="Label1" Text="Veuillez choisir un enseignant:"></asp:Label>


<telerik:RadComboBox ID="rdens" runat="server" AutoPostBack="True" 
                                                            
   Filter="Contains"  EnableLoadOnDemand="True" width="200px"
                                                            
     EmptyMessage="Tapez le nom de l'enseignant" 
         onselectedindexchanged="RadComboBox2_SelectedIndexChanged" >
        </telerik:RadComboBox>
</td>
</tr>
<tr>
<td>
<asp:GridView runat="server" ID="gridens" AutoGenerateColumns="False" 
                                                          
                                                         Style="border-bottom: black 2px ridge; border-left: black 2px ridge;
                                                        background-color: white; border-top: black 2px ridge; border-right: black 2px ridge;"
                                                        BorderWidth="0px" 
        BorderColor="Red"  CssClass="grid"
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="Both" 
        EmptyDataRowStyle-CssClass="ItemStyle"  BackColor="#0099CC"
  
                                                   
                                                       >
                                                        <EmptyDataTemplate>
                                                            Pas d'enregistrement.
                                                        </EmptyDataTemplate>
                                                        
                                                        <HeaderStyle HorizontalAlign="Center" Height="20px" Width="100px" BackColor="Red" />
                                                        <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                                                        <FooterStyle CssClass="ItemStyle" />
                                                        <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                                                        <RowStyle CssClass="GridItemStyle" />
                                                        <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        <Columns>
<asp:TemplateField ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderText="Nº">
  <ItemTemplate>
    <%# Container.DataItemIndex + 1 %>
  </ItemTemplate>
</asp:TemplateField>
           
      <asp:TemplateField HeaderText="Id enseignant" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("id_ens")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
       
      <asp:TemplateField HeaderText="Nom enseignant" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("nom_ens")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

        <asp:TemplateField HeaderText="Module" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Designation")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
       
      <asp:TemplateField HeaderText="Classe" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("code_cl")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Date seance" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Date_seance")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="heure début" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("heure_debut")%>'  />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      <asp:TemplateField HeaderText="heure fin" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("heure_fin")%>'  />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
      

                            
         </Columns>
                      
                           <HeaderStyle CssClass="GridHeaderStyle" />
                     <RowStyle ForeColor="#000000"  />
             <SelectedRowStyle CssClass="GridSelectedStyle" />
      <AlternatingRowStyle BackColor="#d2c1bf"  />

                                                    </asp:GridView>
</td>
</tr>
</table>
</asp:Panel>
<asp:Panel ID="plclasse" runat="server" Visible="false">
<center><b>Suivi des enseignants par classe</b></center>
<table>

<tr>
<td>
    <span class="style5">Niveau:</span>
<asp:DropDownList ID="ddlniv" runat="server" Width="150px"
   Height="30px"
        AutoPostBack="true" onselectedindexchanged="ddlniv_SelectedIndexChanged" 
        CssClass="style3">

</asp:DropDownList>
</td>
<td>
<span class="style5">Classe:</span>


<telerik:RadComboBox ID="rdclasse" runat="server" AutoPostBack="True" 
                                                            
   Filter="Contains"  EnableLoadOnDemand="True" 
                                                            
     EmptyMessage="Tapez la classe consernée" onselectedindexchanged="rdclasse_SelectedIndexChanged" 
          
       >
        </telerik:RadComboBox>

</td>
</tr>
<tr>
<td>
<asp:GridView runat="server" ID="gridclasse" AutoGenerateColumns="False" 
                                                          
                                                         Style="border-bottom: black 2px ridge; border-left: black 2px ridge;
                                                        background-color: white; border-top: black 2px ridge; border-right: black 2px ridge;"
                                                        BorderWidth="0px" 
        BorderColor="Red"  CssClass="grid"
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="Both" 
        EmptyDataRowStyle-CssClass="ItemStyle"  BackColor="#0099CC"
 
                                                   
                                                       >
                                                        <EmptyDataTemplate>
                                                            Pas d'enregistrement.
                                                        </EmptyDataTemplate>
                                                        
                                                        <HeaderStyle HorizontalAlign="Center" Height="20px" Width="100px" BackColor="Red" />
                                                        <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                                                        <FooterStyle CssClass="ItemStyle" />
                                                        <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                                                        <RowStyle CssClass="GridItemStyle" />
                                                        <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        <Columns>
<asp:TemplateField ItemStyle-Width="20px" ItemStyle-HorizontalAlign="Center" HeaderText="Nº">
  <ItemTemplate>
    <%# Container.DataItemIndex + 1 %>
  </ItemTemplate>
</asp:TemplateField>
           
      <asp:TemplateField HeaderText="Id enseignant" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("id_ens")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
       
      <asp:TemplateField HeaderText="Nom enseignant" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("nom_ens")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

        <asp:TemplateField HeaderText="Module" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Designation")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
       
      <asp:TemplateField HeaderText="Classe" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("code_cl")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Date seance" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Date_seance")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="heure début" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("heure_debut")%>'  />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

      <asp:TemplateField HeaderText="heure fin" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("heure_fin")%>'  />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

                            
         </Columns>
                      
                           <HeaderStyle CssClass="GridHeaderStyle" />
                     <RowStyle ForeColor="#000000"  />
             <SelectedRowStyle CssClass="GridSelectedStyle" />
      <AlternatingRowStyle BackColor="#d2c1bf"  />

                                                    </asp:GridView>
</td>
</tr>
</table>
</asp:Panel>


</center>
</asp:Content>

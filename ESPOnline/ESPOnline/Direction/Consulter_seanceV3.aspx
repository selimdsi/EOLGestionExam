﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Consulter_seanceV3.aspx.cs" 
Inherits="ESPOnline.Direction.Consulter_seance"MasterPageFile="~/Direction/Site2.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>



<script src="../Contents/Scripts/ScrollableGridPlugin_ASP.NetAJAX_2.0.js" type="text/javascript"></script>
     <script type="text/javascript">
    $(document).ready(function () {
        $('#<%=gvCustomers.ClientID %>').Scrollable({
            ScrollHeight: 300,
         
        });
    });
    </script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

      
          .style1
        {
            color: #660066;
        }

      
          .style2
        {
            color: #006699;
        }
        .style3
        {
            color: #000000;
        }

      
          </style>
          <script type="text/javascript">


              function calendarShown(sender, args) {
                  sender._popupBehavior._element.style.zIndex = 1000;
              }
  </script>

           <script src="styles/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="styles/Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
<script src="styles/Scripts/JScript1.js" type="text/javascript"></script>
<link href="styles/Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<link href="styles/Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<script type = "text/javascript">
    $(document).ready(function () {
        $(".Calender").dynDateTime({
            showsTime: true,
            ifFormat: "%d/%m/%Y",

            align: "BR",
            electric: false,
            singleClick: false,
            displayArea: ".siblings('.dtcDisplayArea')",
            button: ".next()"
        });
    });
</script> 


 <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.5.js" type="text/javascript"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/redmond/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(bindPicker);
            bindPicker();
        });
        function bindPicker() {
            $("input[type=text][id*=DateTimeValue]").datepicker();
        }
    </script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
<center>

<h3 class="style1"> Consultation et modification des séances des examens</h3>
<br />

<asp:Panel ID="plrechercher" runat="server">
<h2>Veuiller choisir le mode de recherche:</h2>
<br />
<table>

<tr><td>
    <span class="style5"><span class="style2">Année Universitaire:</span>
</span>
<asp:DropDownList ID="ddlan_univer" runat="server" AppendDataBoundItems="true"  Width="150px"
   Height="30px"     AutoPostBack="true" 
        onselectedindexchanged="ddlan_univer_SelectedIndexChanged" CssClass="style3">
<asp:ListItem>Veuillez choisir
</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
</td>


<td>
    <span class="style2">Site:</span><span class="style3"> </span>
<asp:DropDownList ID="ddlsite" runat="server" 
       Width="150px"
   Height="30px"
    AutoPostBack="true" CssClass="style3"    >

</asp:DropDownList>

</td>

</table>
<br />
<table>
<tr>
<td>
<asp:Label ID="Label11" runat="server" Text="Semestre: "
              ForeColor="#044652">
              </asp:Label></td>

                                                     
   <td>          <asp:RadioButtonList ID="Rdsemestre" runat="server"   
            RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="1">S1</asp:ListItem>
                 <asp:ListItem Value="2">S2</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 





<td>
<asp:Label ID="Label6" runat="server" Text="Periode: "
      ForeColor="#044652"    ></asp:Label></td>

<td><asp:RadioButtonList ID="rdperiode" runat="server"   
             RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="P1">P1</asp:ListItem>
                 <asp:ListItem Value="P2">P2</asp:ListItem>               
            </asp:RadioButtonList></td>
           </tr>
</table>
<br />
 
<asp:Button  ID="Btnconsult" runat="server" Text="Consulter" Height="35px" 
                    Width="120px" onclick="Btnconsult_Click"/>
</asp:Panel>



<asp:Panel ID="plconsult"  runat="server">
<h4>Modification des séances des examens Par année,semestre et periode</h4>
<br />
<table>

<tr>
<td>
    <span class="style5"><span class="style2">Année Universitaire:</span>
</span>
<asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true"  Width="150px"
   Height="30px"     AutoPostBack="true" 
        onselectedindexchanged="ddlan_univercloner_SelectedIndexChanged" CssClass="style3">
<asp:ListItem>Veuillez choisir
</asp:ListItem>
</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
</td>


<td>
    <span class="style2">Site:</span><span class="style3"> </span>
<asp:DropDownList ID="DropDownList2" runat="server" 
       Width="150px"
   Height="30px"
    AutoPostBack="true" CssClass="style3"    >

</asp:DropDownList>

</td>
</tr>
</table>
<br />
<table>
<tr>
<td>
<asp:Label ID="Label1" runat="server" Text="Semestre: "
              ForeColor="#044652">
              </asp:Label></td>

                                                     
   <td>          <asp:RadioButtonList ID="RadioButtonList1" runat="server"   
            RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="1">S1</asp:ListItem>
                 <asp:ListItem Value="2">S2</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 





<td>
<asp:Label ID="Label2" runat="server" Text="Periode: "
      ForeColor="#044652"    ></asp:Label></td>

<td><asp:RadioButtonList ID="RadioButtonList2" runat="server"   
             RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="P1">P1</asp:ListItem>
                 <asp:ListItem Value="P2">P2</asp:ListItem>               
            </asp:RadioButtonList></td>
           </tr>
</table>

<table>
<tr><td>

<asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="false" 
OnRowDataBound = "OnRowDataBoundS" 
 Style="border-bottom: white 2px ridge; border-left: white 2px ridge; background-color: white; 
     border-top: white 2px ridge; border-right: white 2px ridge;"
  BorderWidth="0px" 
        BorderColor="White" CellSpacing="1" CellPadding="3" CssClass="grid"
        
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="None" 
        EmptyDataRowStyle-CssClass="ItemStyle" 
        onselectedindexchanged="gvCustomers_SelectedIndexChanged"  >
    <Columns>
       
        <asp:TemplateField ItemStyle-Width="20px">
      
     <HeaderTemplate>
                <asp:CheckBox ID = "chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChangedDDD" Visible="false"/>
            </HeaderTemplate>
           <ItemTemplate> 
              <asp:CheckBox ID="CheckBox1"  runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChanged" />
           </ItemTemplate>
        </asp:TemplateField>
        
<%--insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode
--%>




<%--
        <asp:BoundField DataField="num_seance" HeaderText="Nº séance" SortExpression="num_seance"
            ItemStyle-Width="25px" />--%>


            <%-- <asp:TemplateField HeaderText="Nº séance">
                <ItemTemplate>--%>
                   <%-- <asp:BoundField DataField="RowNumber" HeaderText="Nº séance"  />--%>
                    <%--<asp:Label ID="lb" runat="server" Text='<%# Eval("num_seance")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>



<%--<asp:BoundField DataField="annee_deb" HeaderText="annee_deb" SortExpression="annee_deb" />
<asp:BoundField DataField="semestre" HeaderText="semestre" SortExpression="semestre" />--%>

<%--<asp:BoundField DataField="site" HeaderText="site" SortExpression="site" />--%>

<%--<asp:BoundField DataField="periode" HeaderText="periode" SortExpression="periode" />--%>

        
       
             <asp:TemplateField HeaderText=" heure_debut">

             <ItemTemplate>
                <asp:Label ID = "lblCountry2" runat="server" Text='<%# Eval("heure_debut") %>'></asp:Label>
                <asp:TextBox ID="ddhb" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>

                           <asp:TemplateField HeaderText="heure_fin">

             <ItemTemplate>
                <asp:Label ID = "lblCountry3" runat="server" Text='<%# Eval("heure_fin") %>'></asp:Label>
                <asp:TextBox ID="ddlhf" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>


                           
                           <asp:TemplateField HeaderText="Nº jours">

             <ItemTemplate>
                <asp:Label ID = "lblCountry4" runat="server" Text='<%# Eval("NUM_jours") %>'></asp:Label>
                <asp:TextBox ID="ddlnumj" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>

                           
                           <asp:TemplateField HeaderText="jours">

             <ItemTemplate>
                <asp:Label ID = "lblCountry5" runat="server" Text='<%# Eval("jours") %>'></asp:Label>
                <asp:TextBox ID="ddljours" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>



<%--
                            <asp:TemplateField HeaderText="Date" SortExpression="Date">
                        <ItemTemplate>
                            <asp:Label ID="Date" runat="server" Text='<%# Eval("date_seance","{0:dd/MM/yyyy}") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="DateTimeValue" runat="server"  class = "Calender" Text='<%# Bind("date_seance","{0:dd/MM/yyyy}") %>'></asp:TextBox>
                        
                         <img src="styles/calender.png" alt="" />
                        </EditItemTemplate>
                    </asp:TemplateField>--%>




                            <asp:TemplateField HeaderText="date seance">
                <ItemTemplate>
                <asp:Label ID="lblCountry" runat="server"  Text='<%# Eval("date_seance","{0:dd/MM/yyyy}") %>'></asp:Label>
               <asp:TextBox  
               ID="ddldate" runat="server"  Visible = "false"  class = "Calender"
               
                OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" >
               </asp:TextBox>
                 <img src="styles/calender.png" alt="" />
            </ItemTemplate>
            </asp:TemplateField>
    </Columns>
</asp:GridView>
</ContentTemplate>
</asp:UpdatePanel>
</td>
<td>&nbsp;&nbsp;</td>
<td>
<asp:Button  ID="btncloner" runat="server" Text="Cloner" onclick="btncloner_Click"/>
</td>
</tr>
</table>

</asp:Panel>

</center>

</asp:Content>

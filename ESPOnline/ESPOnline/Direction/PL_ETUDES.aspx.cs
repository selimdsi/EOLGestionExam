﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Direction
{
    public partial class PL_ETUDES : System.Web.UI.Page
    {
        ServiceExamen server = new ServiceExamen();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ddl1.DataValueField = "code_cl";
                ddl1.DataTextField = "code_cl";
                ddl1.DataSource = server.code_cl();
                
                ddl1.DataBind();

            }
        }

        protected void ddl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(ddl1.SelectedValue != null)
            {
            grdbindPl.DataSource = server.PLan_etudes(ddl1.SelectedValue);
            grdbindPl.DataBind();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Direction
{
    public partial class Info_General : System.Web.UI.Page
    {
        MinistereService service = new MinistereService();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                lblmasc.Text = service.Get_NB_ETUD_MASC();
                lblfem.Text = service.Get_NB_ETUD_fem();
                lbltotal.Text = service.Get_NB_ETUD_total();
                Lbletranger.Text = service.Get_NB_ETUD_etrange();
                lbpremmasc.Text = service.Get_NB_ETUD_1ERE_MASC();
                lbpremfem.Text = service.Get_NB_ETUD_1ERE_FEM();
                lbenstotal.Text = service.Get_NB_ENS();

                lbenstotal.Text = service.Get_NB_ENS();
                lbensmasc.Text = service.Get_NB_masc();
                lbensfem.Text = service.Get_NB_fem();
                lbenscontract.Text = service.Get_NB_contractuel();

            }
        }
    }
}
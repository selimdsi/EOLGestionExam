﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Info_General.aspx.cs" Inherits="ESPOnline.Direction.Info_General" 

MasterPageFile="~/Direction/Site2.Master"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

      
          .grid td, .grid th{
     text-align:center;
      
          }
              
          .style2
        {
            font-size: large;
            width: 421px;
        }
        .style3
        {
            color: #990000;
            width: 421px;
        }
              
          .style4
        {
            color: #660066;
        }
              
          </style>

        
</asp:Content>


<asp:Content ID="Content11" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <span class="style4"><b>
 <br />
 <br />
    </b>
 <center>
 <h2>
بطاقة عـــدد 1 </h2>
 <br />
 <h2>معلومات عامة عن المؤسسة</span></h2>
 <br />
 <table border="1px">
 <tr>
 <td>
 <table dir="rtl">
 <tr>
 <td class="style3">

     <h2>1-

     العدد الجملي للطلبة المسجلين خلال السنة الجامعية 2016/2015
     </h2>
     <br />
 </td>
 </tr>
 <tr>
 <td class="style2">ذكور:</td>
 <td><asp:Label ID="lblmasc" runat="server"></asp:Label></td>
 </tr>
 <tr>
 <td class="style2">إناث:</td>
 <td><asp:Label ID="lblfem" runat="server"></asp:Label></td>
 </tr>
 <tr>
 <td class="style2"> منهم طلبة أجانب:</td>
 <td><asp:Label ID="Lbletranger" runat="server"></asp:Label></td>
 </tr>
 <tr>
 <td class="style2">العدد الجملي:</td>
 <td><b><asp:Label ID="lbltotal" runat="server" ForeColor="#084B8A"></asp:Label></b></td>
 
 </tr>
 
 </table>

 <table dir="rtl">
 <tr>
 <td class="style3">

     <h2>2-
عدد الطلبة المسجلون الجدد بالسنة الأولى 
     </h2>
     <br />
 </td>
 </tr>
 
  <tr>
 <td class="style2">ذكور:</td>
 <td><asp:Label ID="lbpremmasc" runat="server"></asp:Label></td>
 </tr>
 <tr>
 <td class="style2">إناث:</td>
 <td><asp:Label ID="lbpremfem" runat="server"></asp:Label></td>
 </tr>
 </table>

 <table dir="rtl" >
 <tr>
 <td class="style3">

     <h2>3-
العدد الجملي للمدرسين: 
     </h2>
     <br />
 </td>
 
 <td><b><asp:Label ID="lbenstotal" runat="server" ForeColor="#084B8A"></asp:Label></b></td>
 </tr>
 
  <tr>
 <td class="style2">ذكور:</td>
 <td><asp:Label ID="lbensmasc" runat="server"></asp:Label></td>
 </tr>
 <tr>
 <td class="style2">إناث:</td>
 <td><asp:Label ID="lbensfem" runat="server"></asp:Label></td>
 </tr>

  <tr>
 <td class="style2">منهم مدرسون عرضيون(vacataires):</td>
 <td><asp:Label ID="lbenscontract" runat="server"></asp:Label></td>
 </tr>
 </table>

 </td></tr>
 <tr><td><br /></td></tr>
 </table>
 
 </center>

 <br />
 <br />
</asp:Content>



﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Affecter_seance_exam.aspx.cs" 
Inherits="ESPOnline.Direction.Affecter_seance_exam" MasterPageFile="~/Direction/Site2.Master" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td 
   {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    

        
  <script type="text/javascript">


      function calendarShown(sender, args) {
          sender._popupBehavior._element.style.zIndex = 1000;
      }
  </script>


   <script src="styles/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="styles/Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
<script src="styles/Scripts/JScript1.js" type="text/javascript"></script>
<link href="styles/Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<link href="styles/Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<script type = "text/javascript">
    $(document).ready(function () {
        $(".Calender").dynDateTime({
            showsTime: true,
            ifFormat: "%d/%m/%Y",
           
            align: "BR",
            electric: false,
            singleClick: false,
            displayArea: ".siblings('.dtcDisplayArea')",
            button: ".next()"
        });
    });
</script> 


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


<asp:Panel ID="plvalider" runat="server">
<center>
<h3>Affectetion des séances des examens</h3>

</center>
<table>
<tr>
<td>
<asp:Label ID="Label11" runat="server" Text="Semestre: "
              ForeColor="#044652">
              </asp:Label></td>

                                                     
   <td>          <asp:RadioButtonList ID="Rdsemestre" runat="server"   
            RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="1">S1</asp:ListItem>
                 <asp:ListItem Value="2">S2</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 





<td>
<asp:Label ID="Label6" runat="server" Text="Periode: "
      ForeColor="#044652"    ></asp:Label></td>

<td><asp:RadioButtonList ID="rdperiode" runat="server"   
             RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="1">P1</asp:ListItem>
                 <asp:ListItem Value="2">P2</asp:ListItem>               
            </asp:RadioButtonList></td>
            <td><asp:Button  ID="Button1" runat="server" Text="Valider" Height="35px" 
                    Width="120px" onclick="btnvalid_Click"/></td>
</tr>
</table>
</asp:Panel>

<asp:Panel ID="Plchoixannee" runat="server" Visible="false">
<table><tr><td><asp:Label ID="Label7" runat="server" Text="Veuillez choisir l'année auquel vous avez faire l'affectation: "
                             ForeColor="#044652"  ></asp:Label>
</td>
<td> <asp:RadioButtonList ID="Rdcours" runat="server"   
            RepeatDirection="Horizontal" width="180px" >
            <asp:ListItem value="C">Année en cours</asp:ListItem>
                 <asp:ListItem value="A">Autre</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

              <td><asp:Button  ID="Btnsuivant" runat="server" Text="Suivant" Height="35px" 
                    Width="120px" onclick="Btnsuivant_Click" /></td>
</tr></table>

                                                     
           

</asp:Panel>


<asp:Panel ID="plannee" runat="server" Visible="false">


<table>

<tr><td>
    <span class="style5">Année Universitaire:
</span>
<asp:DropDownList ID="ddlan_univer" runat="server" AppendDataBoundItems="true"  Width="150px"
   Height="30px"     AutoPostBack="true" 
        onselectedindexchanged="ddlan_univer_SelectedIndexChanged" CssClass="style3">

</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
</td>


<td>
    <span class="style5">Site:</span><span class="style3"> </span>
<asp:DropDownList ID="ddlsite" runat="server" 
        onselectedindexchanged="ddlsite_SelectedIndexChanged"  Width="150px"
   Height="30px"
    AutoPostBack="true" CssClass="style3"    >

</asp:DropDownList>

</td>



</tr>


</table>


</asp:Panel>

<br />

<asp:Panel ID="Plannee2" runat="server" Visible="false">


<table>


<tr>




<td>
    <span class="style5">Site:</span><span class="style3"> </span>
<asp:DropDownList ID="DropDownList2" runat="server"  AppendDataBoundItems="true" 
          Width="150px"
   Height="30px"
    AutoPostBack="true" CssClass="style3" 
        onselectedindexchanged="DropDownList2_SelectedIndexChanged"    >
    <asp:ListItem>Veuillez choisir
</asp:ListItem>
</asp:DropDownList>

</td>




</tr>



</table>


</asp:Panel>

<br />
<asp:Panel ID="pl2"  runat="server" Visible="false">


<center>
<h2>Site: <asp:Label ID="lblSite" runat="server" ForeColor="CadetBlue"></asp:Label></h2>
<h2>Affectation des séances des examens pour l'année :
<asp:Label ID="lblan" runat="server" ForeColor="CadetBlue"></asp:Label>
, Semestre : <asp:Label ID="lblsem" runat="server" ForeColor="CadetBlue"></asp:Label>

,Periode: <asp:Label ID="lblperiod" runat="server" ForeColor="CadetBlue"></asp:Label></h2></center>

 <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>


       
<asp:gridview ID="Gridview1"  runat="server"  ShowFooter="true"
                              AutoGenerateColumns="false"   OnRowDataBound = "OnRowDataBoundS" 
                              CssClass="carousel" onselectedindexchanged="Gridview1_SelectedIndexChanged" 
      
      
      >
        <Columns>
      <%--  <asp:BoundField DataField="RowNumber" HeaderText="Nº séance" />--%>

       <asp:TemplateField HeaderText="Nº séance">
                <ItemTemplate>
                   <%-- <asp:BoundField DataField="RowNumber" HeaderText="Nº séance"  />--%>
                    <asp:Label  ID="lb" runat="server" Text='<%# Eval("RowNumber")%>' ></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

      <%--  <asp:TemplateField HeaderText="Date Examen">
        <ItemTemplate>
            <asp:TextBox ID="TextBox1" runat="server" Width="175px" />
            <asp:Calendar ID="DateTakenCalendar" runat="server" OnSelectionChanged="Calendar_Changed">
            
            
            </asp:Calendar>
        
        
        </ItemTemplate>
        </asp:TemplateField>
--%>
         
       
         <asp:TemplateField HeaderText="Date Examen">
            <ItemTemplate >
                <asp:TextBox ID="TextBox1" runat="server"   class = "Calender" ontextchanged="txt_TextChanged" AutoPostBack="true" />
                <img src="styles/calender.png" alt="" />
            </ItemTemplate>
        </asp:TemplateField>
         
        <%--<asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                 <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1" 
                                                          PopupButtonID="TextBox1" Format="dd/MM/yyyy">
                                                          </asp:CalendarExtender>

            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="Nº Jours">
            

            <ItemTemplate>
                <asp:DropDownList ID="DropDownList1" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>

        </asp:TemplateField>--%>

        



        <asp:TemplateField HeaderText="Nº Jours">
              
                <ItemTemplate>
               
               <asp:DropDownList ID="ddlCountries" runat="server"  
               OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" 
               AutoPostBack="true" AppendDataBoundItems="true" >
               </asp:DropDownList>
            </ItemTemplate>
            </asp:TemplateField>






             <asp:TemplateField HeaderText="Jours">
              
                <ItemTemplate>
               
               <asp:DropDownList ID="ddlCountries2" runat="server"  
             
               AutoPostBack="true" AppendDataBoundItems="true" Enabled="False" >
               </asp:DropDownList>
            </ItemTemplate>
            </asp:TemplateField>


       <%-- <asp:TemplateField  HeaderText="Jours">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList2" runat="server"
                         AppendDataBoundItems="true">
                     <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>--%>


                <asp:TemplateField  HeaderText="Heure début">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList3" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="-1">Select</asp:ListItem>

                      <asp:ListItem Value="08">08</asp:ListItem>
                     <asp:ListItem Value="09">09</asp:ListItem>
                     <asp:ListItem Value="10">10</asp:ListItem>
                     <asp:ListItem Value="11">11</asp:ListItem>
                     <asp:ListItem Value="12">12</asp:ListItem>

                     <asp:ListItem Value="13">13</asp:ListItem>

                     <asp:ListItem Value="14">14</asp:ListItem>     
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="16">16</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>



        <asp:TemplateField  HeaderText="Minute début">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList4" runat="server"
                                  AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                      <asp:ListItem Value="00">00</asp:ListItem>
                     <asp:ListItem Value="15">15</asp:ListItem>
                     <asp:ListItem Value="30">30</asp:ListItem>
                     <asp:ListItem Value="45">45</asp:ListItem>
                    


                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>


         <asp:TemplateField  HeaderText="Heure fin">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList5" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="-1">Select</asp:ListItem>

                       <asp:ListItem Value="08">08</asp:ListItem>
                     <asp:ListItem Value="09">09</asp:ListItem>
                     <asp:ListItem Value="10">10</asp:ListItem>
                     <asp:ListItem Value="11">11</asp:ListItem>
                     <asp:ListItem Value="12">12</asp:ListItem>

                     <asp:ListItem Value="13">13</asp:ListItem>

                     <asp:ListItem Value="14">14</asp:ListItem>     
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="16">16</asp:ListItem>
                    <asp:ListItem Value="16">17</asp:ListItem>
                    <asp:ListItem Value="16">18</asp:ListItem>
                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Minute fin">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList6" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="-1">Select</asp:ListItem>

                     <asp:ListItem Value="00">00</asp:ListItem>
                     <asp:ListItem Value="15">15</asp:ListItem>
                     <asp:ListItem Value="30">30</asp:ListItem>
                     <asp:ListItem Value="45">45</asp:ListItem>
                    
                </asp:DropDownList>
            </ItemTemplate>
            <FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
                 <asp:Button ID="ButtonAdd" runat="server" 
                             Text="Ajouter séance" 
                             onclick="ButtonAdd_Click" />
            </FooterTemplate>
        </asp:TemplateField>
        </Columns>
</asp:gridview> 

<br />
<br />

<center>
<asp:Button  ID="btnaffecter" runat="server" Text="Affecter" Height="40px" 
        Width="100px" onclick="btnaffecter_Click" />
</center>
<br />
<br />
<br />

</asp:Panel>

</asp:Content>
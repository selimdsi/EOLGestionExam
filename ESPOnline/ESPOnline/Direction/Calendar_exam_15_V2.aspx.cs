﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class Calendar_exam_15_V2 : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView1.Visible = true;
                //bind_code_cl();
                GridView1.DataSource = service.Affecter_exam();
                GridView1.DataBind();
           

                //bindSemestre();
            }
        }

        //calendar
        protected void Cal1_SelectionChanged(object sender, EventArgs e)
        {
            Calendar cal = (Calendar)sender;
            TextBox text1 = (TextBox)((GridViewRow)cal.Parent.Parent).FindControl("text1");

            text1.Text = cal.SelectedDate.ToShortDateString();
        }

       

        
        //protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        //{
        //    txtdebutDate_PopupControlExtender1.Commit(Calendar1.SelectedDate.ToShortDateString());
        //}


        



        protected void btnload_Click(object sender, EventArgs e)
        {
            GridView1.Visible = true;
            GridView1.DataSource = service.Affecter_exam();
            GridView1.DataBind();
           
        }
        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        public void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //TableCell statusCell = e.Row.Cells[2];
                //if (statusCell.Text == "A")
                //{
                //    statusCell.Text = "Absent";
                //}
                //if (statusCell.Text == "P")
                //{
                //    statusCell.Text = "Present";
                //}
            }
        }

       

    }
}
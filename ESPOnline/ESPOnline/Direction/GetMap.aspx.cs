﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;
using System.Configuration;

namespace ESPOnline.Direction
{
    public partial class GetMap : System.Web.UI.Page
    {
        OracleCommand cmd;
        OracleDataAdapter da;
        DataSet ds;

        //string connectionstring;
        OracleConnection con;

        private void loaddata()
        {
            con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            try 
            {

                cmd = new OracleCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SPemp";
                cmd.Connection = con;
               
                var reader = cmd.ExecuteReader();
                griDMaPX.DataSource = reader;
                griDMaPX.DataBind();
                cmd.Dispose();
                con.Close();


            }
            catch(Exception ex)
            {
                Response.Write(ex.Message);
            }

        
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)

            {
                loaddata();
                /*
 
 
 
                 OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                                OracleDataAdapter da = new OracleDataAdapter("SP_getdata", con);
                                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                                DataSet ds = new DataSet();
                                da.Fill(ds, "SP_getdata");
                                griDMaPX.DataSource = ds.Tables["SP_getdata"];
                                griDMaPX.DataBind(); 
                 */

                //bind0();
               // String strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
              /* OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
                OracleCommand cmd = new OracleCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SP_getdata";
                cmd.Connection = con;
                try
                {
                    con.Open();
                    griDMaPX.EmptyDataText = "Pas d'enregistrement";
                    griDMaPX.DataSource = cmd.ExecuteReader();
                    griDMaPX.DataBind();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    con.Close();
                    con.Dispose();
                }
               * */
                /*
                OracleCommand command = new OracleCommand();
                OracleDataAdapter adapter = new OracleDataAdapter();
                DataSet ds = new DataSet();
                int i = 0;
                string sql = null;
               // string connetionString = "Data Source=.;Initial Catalog=pubs;User ID=sa;Password=*****";
                OracleConnection connection = new OracleConnection(AppConfiguration.ConnectionString);
                connection.Open();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "SP_getdata";
                adapter = new OracleDataAdapter(command);
                adapter.Fill(ds);
                connection.Close();
                griDMaPX.DataSource = ds;
                griDMaPX.DataBind();
                 * */

            }
            
        }

        public void bind0()
        {
            DataTable dt = new DataTable();
            // String conStr = "Data Source=172.16.6.173;Initial Catalog=servion_hari;User  ID=sa;Password=Servion@123";
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            OracleCommand com = new OracleCommand("SP_getdata", con);
            com.CommandType = CommandType.StoredProcedure;
          OracleDataAdapter da = new OracleDataAdapter(com);
            try
            {
                con.Open();
                da.Fill(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
            griDMaPX.DataSource = dt;
            griDMaPX.DataBind();

        }


    }
}
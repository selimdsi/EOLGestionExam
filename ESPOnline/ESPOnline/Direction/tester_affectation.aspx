﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tester_affectation.aspx.cs" Inherits="ESPOnline.Direction.tester_affectation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
<asp:gridview ID="Gridview1"  runat="server"  ShowFooter="true"
                              AutoGenerateColumns="false"   OnRowDataBound = "OnRowDataBoundS" 
                              CssClass="carousel" onselectedindexchanged="Gridview1_SelectedIndexChanged" 
      
      
      >
        <Columns>
      <%--  <asp:BoundField DataField="RowNumber" HeaderText="Nº séance" />--%>

       <asp:TemplateField HeaderText="Nº séance">
                <ItemTemplate>
                   <%-- <asp:BoundField DataField="RowNumber" HeaderText="Nº séance"  />--%>
                    <asp:Label ID="lb" runat="server" Text='<%# Eval("RowNumber")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

      <%--  <asp:TemplateField HeaderText="Date Examen">
        <ItemTemplate>
            <asp:TextBox ID="TextBox1" runat="server" Width="175px" />
            <asp:Calendar ID="DateTakenCalendar" runat="server" OnSelectionChanged="Calendar_Changed">
            
            
            </asp:Calendar>
        
        
        </ItemTemplate>
        </asp:TemplateField>
--%>
         
       
         <asp:TemplateField HeaderText="Date Examen">
            <ItemTemplate >
                <asp:TextBox ID="TextBox1" runat="server"   class = "Calender" ontextchanged="txt_TextChanged" AutoPostBack="true" />
                <img src="styles/calender.png" alt="" />
            </ItemTemplate>
        </asp:TemplateField>
         
        <%--<asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                 <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1" 
                                                          PopupButtonID="TextBox1" Format="dd/MM/yyyy">
                                                          </asp:CalendarExtender>

            </ItemTemplate>
        </asp:TemplateField>--%>
        <%--<asp:TemplateField HeaderText="Nº Jours">
            

            <ItemTemplate>
                <asp:DropDownList ID="DropDownList1" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>

        </asp:TemplateField>--%>

        



        <asp:TemplateField HeaderText="Nº Jours">
              
                <ItemTemplate>
               
               <asp:DropDownList ID="ddlCountries" runat="server"  
               OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" 
               AutoPostBack="true" AppendDataBoundItems="true" >
               </asp:DropDownList>
            </ItemTemplate>
            </asp:TemplateField>






             <asp:TemplateField HeaderText="Jours">
              
                <ItemTemplate>
               
               <asp:DropDownList ID="ddlCountries2" runat="server"  
             
               AutoPostBack="true" AppendDataBoundItems="true" Enabled="False" >
               </asp:DropDownList>
            </ItemTemplate>
            </asp:TemplateField>


       <%-- <asp:TemplateField  HeaderText="Jours">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList2" runat="server"
                         AppendDataBoundItems="true">
                     <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>--%>


                <asp:TemplateField  HeaderText="Heure début">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList3" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="-1">Select</asp:ListItem>

                      <asp:ListItem Value="08">08</asp:ListItem>
                     <asp:ListItem Value="09">09</asp:ListItem>
                     <asp:ListItem Value="10">10</asp:ListItem>
                     <asp:ListItem Value="11">11</asp:ListItem>
                     <asp:ListItem Value="12">12</asp:ListItem>

                     <asp:ListItem Value="13">13</asp:ListItem>

                     <asp:ListItem Value="14">14</asp:ListItem>     
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="16">16</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>



        <asp:TemplateField  HeaderText="Minute début">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList4" runat="server"
                                  AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select</asp:ListItem>
                      <asp:ListItem Value="00">00</asp:ListItem>
                     <asp:ListItem Value="15">15</asp:ListItem>
                     <asp:ListItem Value="30">30</asp:ListItem>
                     <asp:ListItem Value="45">45</asp:ListItem>
                    


                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>


         <asp:TemplateField  HeaderText="Heure fin">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList5" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="-1">Select</asp:ListItem>

                       <asp:ListItem Value="08">08</asp:ListItem>
                     <asp:ListItem Value="09">09</asp:ListItem>
                     <asp:ListItem Value="10">10</asp:ListItem>
                     <asp:ListItem Value="11">11</asp:ListItem>
                     <asp:ListItem Value="12">12</asp:ListItem>

                     <asp:ListItem Value="13">13</asp:ListItem>

                     <asp:ListItem Value="14">14</asp:ListItem>     
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="16">16</asp:ListItem>
                    <asp:ListItem Value="16">17</asp:ListItem>
                    <asp:ListItem Value="16">18</asp:ListItem>
                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Minute fin">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList6" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="-1">Select</asp:ListItem>

                     <asp:ListItem Value="00">00</asp:ListItem>
                     <asp:ListItem Value="15">15</asp:ListItem>
                     <asp:ListItem Value="30">30</asp:ListItem>
                     <asp:ListItem Value="45">45</asp:ListItem>
                    
                </asp:DropDownList>
            </ItemTemplate>
            <FooterStyle HorizontalAlign="Right" />
            <FooterTemplate>
                 <asp:Button ID="ButtonAdd" runat="server" 
                             Text="Ajouter séance" 
                             onclick="ButtonAdd_Click" />
            </FooterTemplate>
        </asp:TemplateField>
        </Columns>
</asp:gridview>
    </div>
    </form>
</body>
</html>

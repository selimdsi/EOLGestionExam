﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;
using BLL;

namespace ESPOnline.Direction
{
    public partial class program_module_MAJ : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();

        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                get_annee_univ();
            }
        }

        public void get_annee_univ()
        {
            ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataSource = service.List_etud_2015();
            ddlan_univer.DataBind();


        }


        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }
        }

        protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlsite.SelectedValue.ToString() != null)
            {
                if (ddlsite.SelectedValue.ToString() != "ALL")
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_niveau(Label1.Text.ToString() + " and lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'");
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;
                    Label2.Text = " lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
                }
                else
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_niveau(Label1.Text.ToString());
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                }
            }
        }

        protected void ddlniv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlniv.SelectedValue != null)
            {
                if (ddlniv.SelectedValue.ToString() != "ALL")
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    if (Label2.Text == "")
                    {

                        ddclasse.DataSource = service.bind_classesbyclasses("lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'");
                        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    }
                    else
                    {

                        ddclasse.DataSource = service.bind_classesbyclasses("lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() + " " + "and" + " " + Label2.Text.ToString());

                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;

                        Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    }
                }
                else
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    if (Label2.Text.ToString() != "")
                    {

                        ddclasse.DataSource = service.bind_classesbyclasses(Label1.Text.ToString() + " and " + Label2.Text.ToString());
                    }
                    else
                    {
                        ddclasse.DataSource = service.bind_classesbyclasses(Label1.Text.ToString());
                    }
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddclasse.DataBind();
                    ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddclasse.SelectedItem.Selected = false;
                    ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                    Label3.Text = "";
                }

            }
        }

        protected void ddclasse_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindData();
            btnUpdate.Visible = true;



        }

        private void BindData()
        {
            Gridexam.DataSource = service.bind_exam_byclassesord(ddclasse.SelectedValue, Rdsemestre.SelectedValue);
            Gridexam.DataBind();
        }

        protected void EditCustomer(object sender, GridViewEditEventArgs e)
        {
            Gridexam.EditIndex = e.NewEditIndex;
            this.BindData();
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Gridexam.EditIndex = -1;
            BindData();
        }



        protected void UpdateCustomer(object sender, GridViewUpdateEventArgs e)
        {
            //string shipperId = (Gridexam.Rows[e.RowIndex].FindControl("rblShippers") as RadioButtonList).SelectedItem.Value;
            //string orderId = Gridexam.DataKeys[e.RowIndex].Value.ToString();
            //string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            //using (OracleConnection con = new OracleConnection(strConnString))
            //{
            //    string query = "UPDATE Orders SET Shipvia = @ShipperId WHERE OrderId = @OrderId";
            //    using (OracleCommand cmd = new OracleCommand(query))
            //    {
            //        cmd.Connection = con;
            //        // a voir  cmd.Parameters.AddWithValue("@OrderId", orderId);
            //        // a voir   cmd.Parameters.AddWithValue("@ShipperId", shipperId);
            //        con.Open();
            //        cmd.ExecuteNonQuery();
            //        con.Close();
            //        Response.Redirect(Request.Url.AbsoluteUri);
            //    }
            //}
        }

        protected void Gridexam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddlaprogrammer = (e.Row.FindControl("ddlaprogrammer") as DropDownList);


                OracleCommand cmdd = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='96' and (code_nome='01' or code_nome='02')");

                ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
                ddlaprogrammer.DataTextField = "LIB_NOME";
                ddlaprogrammer.DataValueField = "LIB_NOME";

                ddlaprogrammer.DataBind();




                DropDownList ddltypeexam = (e.Row.FindControl("ddltypeexam") as DropDownList);


                OracleCommand cmdx = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='97' and (code_nome='01' or code_nome='02' or code_nome='03') ");

                ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
                ddltypeexam.DataTextField = "LIB_NOME";
                ddltypeexam.DataValueField = "LIB_NOME";

                ddltypeexam.DataBind();




            }
        }



            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    DropDownList ddlCountries = (e.Row.FindControl("ddlCountries") as DropDownList);

            //    Label lblcodecl = (e.Row.FindControl("lblcodecl") as Label);
            //    OracleCommand cmd = new OracleCommand("SELECT  code_module FROM esp_ds_exam where code_cl like '" + lblcodecl.Text+ "' order by code_module");

            //    ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
            //    ddlCountries.DataTextField = "code_module";
            //    ddlCountries.DataValueField = "code_module";

            //    ddlCountries.DataBind();


            ////bind a prog
            //DropDownList ddlaprogrammer = (e.Row.FindControl("ddlaprogrammer") as DropDownList);

            //Label lblcodecl11 = (e.Row.FindControl("lblcodecl") as Label);
            //OracleCommand cmdd = new OracleCommand("SELECT LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='96' and (code_nome='01' or code_nome='02')");

            //ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
            //ddlaprogrammer.DataTextField = "LIB_NOME";
            //ddlaprogrammer.DataValueField = "LIB_NOME";

            //ddlaprogrammer.DataBind();



            ////bind type exam

            //DropDownList ddltypeexam = (e.Row.FindControl("ddltypeexam") as DropDownList);

            //Label lblco = (e.Row.FindControl("lblcodecl") as Label);
            //OracleCommand cmdx = new OracleCommand("SELECT LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='97' and (code_nome='01' or code_nome='02' or code_nome='03') ");

            //ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
            //ddltypeexam.DataTextField = "LIB_NOME";
            //ddltypeexam.DataValueField = "LIB_NOME";

            //ddltypeexam.DataBind();


            //}

        





        protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        {

            //foreach (GridViewRow row in Gridexam.Rows)
            //{
            //    if (row.RowType == DataControlRowType.DataRow)
            //    {

            //        DropDownList ddldesign = (row.FindControl("ddldesign") as DropDownList);
            //        DropDownList ddlCountries1 = (row.FindControl("ddlCountries") as DropDownList);
            //        Label lblcodecl = (row.FindControl("lblcodecl") as Label);
            //        OracleCommand cmd2 = new OracleCommand("SELECT  distinct designation from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.CODE_MODULE where a.code_module like '"+ddlCountries1.SelectedValue+"' and code_cl like '"+lblcodecl.Text+"'");

            //        ddldesign.DataSource = this.ExecuteQuery(cmd2, "SELECT");
            //        ddldesign.DataTextField = "designation";
            //        ddldesign.DataValueField = "designation";

            //        ddldesign.DataBind();




            //        DropDownList ddlaprogrammer = (row.FindControl("ddlaprogrammer") as DropDownList);

            //        Label lblcodecl11 = (row.FindControl("lblcodecl") as Label);
            //        OracleCommand cmdd = new OracleCommand("SELECT A_programmer from esp_ds_exam where code_cl like '" + lblcodecl11.Text + "' and code_module like '" + ddlaprogrammer.SelectedValue + "'");

            //        ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
            //        ddlaprogrammer.DataTextField = "A_programmer";
            //        ddlaprogrammer.DataValueField = "A_programmer";

            //        ddlaprogrammer.DataBind();



            //        //bind type exam

            //        DropDownList ddltypeexam = (row.FindControl("ddltypeexam") as DropDownList);

            //        Label lblco = (row.FindControl("lblcodecl") as Label);
            //        OracleCommand cmdx = new OracleCommand("SELECT type_exam from esp_ds_exam where code_cl like '" + lblco.Text + "' and code_module like '" + ddlaprogrammer.SelectedValue + "'");

            //        ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
            //        ddltypeexam.DataTextField = "type_exam";
            //        ddltypeexam.DataValueField = "type_exam";

            //        ddltypeexam.DataBind();

            //    }
            //}
        }


        protected void ddlmodule_SelectedIndexChanged(object sender, EventArgs e)
        {


        }





        //editer in a gridview$


        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void Update(object sender, EventArgs e)
        {

            //if exist faire le update
            foreach (GridViewRow row in Gridexam.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {

                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                    {
                        //dt = service.exist(row.Cells[1].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value, row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text);
                        //if (dt.Rows.Count != 0)
                        //{
                            OracleCommand cmd = new OracleCommand("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam where code_cl =:code_cl and code_module=:code_module");

                            cmd.Parameters.Add("a_programmer", row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value);
                            cmd.Parameters.Add(":type_exam", row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value);

                            cmd.Parameters.Add(":code_cl", row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":code_module", row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text);
                            // cmd.Parameters.Add(":code_cl", Gridexam.DataKeys[row.RowIndex].Value);
                            //DropDownList ddlCountries = (e.row.FindControl("ddlCountries") as DropDownList);
                            this.ExecuteQuery(cmd, "SELECT");
                            Response.Write(@"<script language='javascript'>alert('Modification avec succès');</script>");

                            BindData();
                        //}

                        //else
                        //{
                        //    Response.Write(@"<script language='javascript'>alert('Module non affecté');</script>");
                        //}

                    }
                }

            }
        }




        //rebind grid maha
        //gvCustomers.DataSource = service.bind_niveau_etudiantbycl(ddclasse2.SelectedValue, ddlannee_debM.SelectedValue);
        //gvCustomers.DataBind();

        protected void OnCheckedChangedDDD(object sender, EventArgs e)
        {
            bool isUpdateVisible = false;
            CheckBox chk = (sender as CheckBox);
            if (chk.ID == "chkAll")
            {
                foreach (GridViewRow row in Gridexam.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked = chk.Checked;
                    }
                }
            }
            CheckBox chkAll = (Gridexam.HeaderRow.FindControl("chkAll") as CheckBox);
            chkAll.Checked = true;
            foreach (GridViewRow row in Gridexam.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    for (int i = 3; i < row.Cells.Count; i++)
                    {
                        row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                        }
                        if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                        }
                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }
                        if (!isChecked)
                        {
                            chkAll.Checked = false;
                        }
                    }
                }
            }
            btnUpdate.Visible = isUpdateVisible;
        }


        protected void OnCheckedChanged(object sender, EventArgs e)
        {
           

                bool isUpdateVisible = false;
                Label1.Text = string.Empty;

                //Loop through all rows in GridView
                foreach (GridViewRow row in Gridexam.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                        if (isChecked)
                            row.RowState = DataControlRowState.Edit;

                        for (int i = 6; i < row.Cells.Count; i++)
                        {
                            row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                            if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                            {
                                row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                            }
                            if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                            {
                                row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                            }

                            if (isChecked && !isUpdateVisible)
                            {
                                isUpdateVisible = true;
                            }

                            //bind a prog


                        }

                    }



                    //DropDownList ddlaprogrammer = (row.FindControl("ddlaprogrammer") as DropDownList);


                    //OracleCommand cmdd = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='96' and (code_nome='01' or code_nome='02')");

                    //ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
                    //ddlaprogrammer.DataTextField = "LIB_NOME";
                    //ddlaprogrammer.DataValueField = "LIB_NOME";

                    //ddlaprogrammer.DataBind();



                    //bind type exam

                    //DropDownList ddltypeexam = (row.FindControl("ddltypeexam") as DropDownList);


                    //OracleCommand cmdx = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='97' and (code_nome='01' or code_nome='02' or code_nome='03') ");

                    //ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
                    //ddltypeexam.DataTextField = "LIB_NOME";
                    //ddltypeexam.DataValueField = "LIB_NOME";

                    //ddltypeexam.DataBind();



                }




                // UpdatePanel1.Update();
                btnUpdate.Visible = isUpdateVisible;
            }




        

    }
}


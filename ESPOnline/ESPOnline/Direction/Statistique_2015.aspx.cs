﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using ClosedXML.Excel;
using System.IO;
using System.Data;
using System.Drawing;
namespace ESPOnline.Direction
{
    public partial class Statistique_2015 : System.Web.UI.Page
    {
        StatService service = new StatService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_DECID"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

           
                if (!IsPostBack)
                {
                   lblch.Text = service.totalch();
                    Lblgh.Text = service.totalgh();
                    bindcharguia();
                    bindghazela();
                    //lbl2.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    lbl2.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"); 
                   // lbl1.Text = DateTime.Now.ToString("h:mm:ss");
                   
                }
            
        }
        protected void gridViewtoiec_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Gridtoiec.PageIndex = e.NewPageIndex;
            Gridtoiec.DataBind();
            Gridtoiec.DataSource = service.Afficher_list_ghazela();
            Gridtoiec.DataBind();
        }

        protected void gridViewtoiec62_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;
            GridView1.DataBind();
            GridView1.DataSource = service.Afficher_list_charguia();
            GridView1.DataBind();
        }

        public void bindcharguia()
        {
            GridView1.DataSource = service.Afficher_list_charguia();
            GridView1.DataBind();
        }

        public void bindghazela()
        {
            Gridtoiec.DataSource = service.Afficher_list_ghazela();
            Gridtoiec.DataBind();
        }

        protected void Gridtoiec_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void BUTT1_Click(object sender, EventArgs e)
        {
            
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Effectif_ghazala.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                Gridtoiec.AllowPaging = false;
                bindghazela();

                Gridtoiec.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in Gridtoiec.HeaderRow.Cells)
                {
                    cell.BackColor = Gridtoiec.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in Gridtoiec.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = Gridtoiec.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = Gridtoiec.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                Gridtoiec.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        protected void BuTT2_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Effectif_charguia.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView1.AllowPaging = false;
                bindcharguia();

                GridView1.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = GridView1.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView1.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }

        protected void Btntoiec_Click(object sender, EventArgs e)
        {

        }

        decimal totalPrice = 0M;
     
        int totalItems = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPrice = (Label)e.Row.FindControl("lblPrice");
               

                decimal price = Decimal.Parse(lblPrice.Text);
              

                totalPrice += price;
              
                totalItems += 1;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalPrice = (Label)e.Row.FindControl("lblTotalPrice");
               

                lblTotalPrice.Text = totalPrice.ToString();
               
                // lblAveragePrice.Text = (totalPrice / totalItems).ToString("F");
            }
        }


        decimal totalPrice2 = 0M;

        int totalItems3 = 0;
        protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPrice = (Label)e.Row.FindControl("lblPrice");


                decimal priceS = Decimal.Parse(lblPrice.Text);


                totalPrice2 += priceS;

                totalItems3 += 1;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalPrice = (Label)e.Row.FindControl("lblTotalPrice");


                lblTotalPrice.Text = totalPrice2.ToString();

                // lblAveragePrice.Text = (totalPrice / totalItems).ToString("F");
            }
        }

       


    }
}
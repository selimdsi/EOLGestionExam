﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class AA_TestProcedure : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void valider(object sender, EventArgs e)
        {
            OracleConnection conn = new OracleConnection(AppConfiguration.ConnectionString);
            conn.Open();
            OracleCommand cmd = new OracleCommand("CALCUL", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(":a", OracleDbType.Int32);
           cmd.Parameters.Add(":b", OracleDbType.Int32);
            cmd.Parameters.Add(":p", OracleDbType.Int32);
            cmd.Parameters.Add(":s", OracleDbType.Int32);

            cmd.Parameters[":a"].Direction = ParameterDirection.Input;
           cmd.Parameters[":b"].Direction = ParameterDirection.Input;
           cmd.Parameters[":p"].Direction = ParameterDirection.Output;
            cmd.Parameters[":s"].Direction = ParameterDirection.Output;

            cmd.Parameters[":a"].Value = Convert.ToInt32(txt1.Text);
           cmd.Parameters[":b"].Value = Convert.ToInt32(txt2.Text);
            OracleDataReader dr = cmd.ExecuteReader();
            lbl1.Text = cmd.Parameters[":p"].Value.ToString();
            lbl2.Text = cmd.Parameters[":s"].Value.ToString();
        }
    }
}
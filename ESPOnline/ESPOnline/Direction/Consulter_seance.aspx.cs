﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class Consulter_seance : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //bind_lst_ex();
                get_annee_univ();
                plconsult.Visible = false;
            }

            
        }

        public void get_annee_univ()
        {
            ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataSource = service.List_etud_2015();
            ddlan_univer.DataBind();


        }

        public void get_anneecloner()
        {
            DropDownList1.DataTextField = "ANNEE_UNIVERSITAIRE";
            DropDownList1.DataValueField = "ANNEE_UNIVERSITAIRE";
            DropDownList1.DataSource = service.List_etud_2015();
            DropDownList1.DataBind();


        }

        protected void ddlan_univercloner_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != null)
            {
                DropDownList2.DataTextField = "SITE";
                DropDownList2.DataValueField = "SITE";

                DropDownList2.DataSource = service.bind_site(DropDownList1.SelectedValue.ToString().Substring(0, 4));

                DropDownList2.DataBind();
                DropDownList2.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                DropDownList2.SelectedItem.Selected = false;
                DropDownList2.Items.FindByText("Veuillez choisir").Selected = true;
                //Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }
        }

        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                //Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }
        }


        public void bind_lst_ex()
        {

            gvCustomers.DataSource = service.bind_seance_exam();
            gvCustomers.DataBind();
        }



        protected void OnCheckedChangedDDD(object sender, EventArgs e)
        {
        }

        protected void OnCheckedChanged(object sender, EventArgs e)
        {

            bool isUpdateVisible = false;
//Label1.Text = string.Empty;

            //Loop through all rows in GridView
            foreach (GridViewRow row in gvCustomers.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                        row.RowState = DataControlRowState.Edit;

                    for (int i = 6; i < row.Cells.Count; i++)
                    {
                        row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                        }
                        if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                        }

                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }

                    }

                }





            }




            UpdatePanel1.Update();
           // btnUpdate.Visible = isUpdateVisible;
        }



        protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {
        }

            //modifier les seance affectées
        





        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        #region updateall
        protected void btnadd_Click(object sender, EventArgs e)
        {

            //if exist faire le update
            foreach (GridViewRow row in gvCustomers.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {

                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                    {
                        //dt = service.exist(row.Cells[1].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value, row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text);
                        //if (dt.Rows.Count != 0)
                        //{
                        OracleCommand cmd = new OracleCommand("update esp_seance_examen set date_seance=:date_seance,heure_debut=:heure_debut,heure_fin=:heure_fin,num_jours=:num_jours,jours=:jours where num_seance =:num_seance");

                        cmd.Parameters.Add(":date_seance", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":heure_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":heure_fin", row.Cells[8].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":num_jours", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":jours", row.Cells[10].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        cmd.Parameters.Add(":num_seance", row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text);




                        this.ExecuteQuery(cmd, "SELECT");
                        Response.Write(@"<script language='javascript'>alert('Modification avec succès');</script>");

                        bind_lst_ex();
                    }
                }

            }
        }


        
        #endregion



        protected void Btnconsult_Click(object sender, EventArgs e)
        {
            get_anneecloner();
            
            gvCustomers.DataSource = service.Cloner(ddlan_univer.SelectedValue.Substring(0,4), rdperiode.SelectedValue,Convert.ToInt32(Rdsemestre.SelectedValue), ddlsite.SelectedValue);

            gvCustomers.DataBind();
            plconsult.Visible = true;
            plrechercher.Visible = false;
        }

        protected void gvCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btncloner_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in gvCustomers.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {

                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                    {
                        //dt = service.exist(row.Cells[1].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value, row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text);
                        //if (dt.Rows.Count != 0)
                        //{

                        string dateSeance = row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text;
                       string num_seance =row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text;

                        int semestre = Convert.ToInt32(RadioButtonList1.SelectedValue);
                        string periode = RadioButtonList2.SelectedValue;
                        string site = DropDownList2.SelectedValue;
                        string annee = DropDownList1.SelectedValue.Substring(0,4);

                        string heure_debut = row.Cells[2].Controls.OfType<Label>().FirstOrDefault().Text;

                        string heure_fin = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;

                        string num_jours = row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text;

                        string jours = row.Cells[5].Controls.OfType<Label>().FirstOrDefault().Text;


                      OracleCommand cmd = new OracleCommand("insert into esp_seance_examen ( Num_seance,date_seance,semestre,periode,annee_deb,site,NUM_JOURS,JOURS,HEURE_DEBUT,HEURE_FIN) values('" + num_seance + "','" + dateSeance + "','" + semestre + "','" + periode + "','" + annee + "','" + site + "','"+num_jours+"','"+jours+"','"+heure_debut+"','"+heure_fin+"')");

                       //cmd.Parameters.Add(":date_seance", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":heure_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":heure_fin", row.Cells[8].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":num_jours", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":jours", row.Cells[10].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":num_seance", row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text);

                        this.ExecuteQuery(cmd, "SELECT");
                                       
                    }
                }
            }
            Response.Write(@"<script language='javascript'>alert('Modification avec succès');</script>");

            gvCustomers.DataSource = service.Cloner(ddlan_univer.SelectedValue.Substring(0, 4), rdperiode.SelectedValue, Convert.ToInt32(Rdsemestre.SelectedValue), ddlsite.SelectedValue);

            gvCustomers.DataBind();
     

        }



    }
}
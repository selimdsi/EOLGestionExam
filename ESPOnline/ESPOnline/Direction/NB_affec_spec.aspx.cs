﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using ClosedXML.Excel;
using System.IO;
using System.Data;
using System.Text;
using System.Drawing;

namespace ESPOnline.Direction
{
    public partial class NB_affec_spec : System.Web.UI.Page
    {
        StatService service = new StatService();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["ID_DECID"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }


            if (!IsPostBack)
            {
                //get_nb_affecparspecialite();
               
                Btnprint.Visible = false;
                Button1.Visible = false;
                lbltitle.Visible = false;

                lbl2.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                Gridtoiec.Visible = false;
              

                ddlsem.DataTextField = "NUM_SEMESTRE";
                ddlsem.DataValueField = "NUM_SEMESTRE";
                ddlsem.DataSource = service.bind_num_semestre() ;
                ddlsem.DataBind();
            }

        }
        protected void gridViewtoiec_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Gridtoiec.PageIndex = e.NewPageIndex;
            Gridtoiec.DataBind();
            Gridtoiec.DataSource = service.Afficher_list_ens_affecter(lblsemestre.Text.ToString());
            Gridtoiec.DataBind();
        }

        public void get_nb_affecparspecialite()
        {
            Gridtoiec.DataSource = service.Afficher_list_ens_affecter(lblsemestre.Text.ToString());
            Gridtoiec.DataBind();
        }

        protected void BuTT2_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Nb_ens_par_spec.xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                Gridtoiec.AllowPaging = false;
                get_nb_affecparspecialite();

                Gridtoiec.HeaderRow.BackColor = Color.White;
                foreach (TableCell cell in Gridtoiec.HeaderRow.Cells)
                {
                    cell.BackColor = Gridtoiec.HeaderStyle.BackColor;
                }
                foreach (GridViewRow row in Gridtoiec.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = Gridtoiec.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = Gridtoiec.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                Gridtoiec.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }

        }


        protected void Btnprint_Click(object sender, EventArgs e)
        {
            Gridtoiec.AllowPaging = false;
            Gridtoiec.DataSource = service.Afficher_list_ens_affecter(lblsemestre.Text.ToString());
            Gridtoiec.DataBind();
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            Gridtoiec.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'")
                .Replace(System.Environment.NewLine, "");
            StringBuilder sb = new StringBuilder();
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            sb.Append(gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            Gridtoiec.AllowPaging = true;
            Gridtoiec.DataSource = service.Afficher_list_ens_affecter(lblsemestre.Text.ToString());
            Gridtoiec.DataBind();

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }

   
 
          decimal totalPrice = 0M;
                decimal totalStock = 0M;
                decimal totalgc = 0M;
              
                int totalItems = 0;
                protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Label lblPrice = (Label)e.Row.FindControl("lblPrice");
                        Label lblUnitsInStock = (Label)e.Row.FindControl("lblUnitsInStock");
                        Label lblUnitsInStockGC = (Label)e.Row.FindControl("lblUnitsInStockGC");
                        

                        decimal price = Decimal.Parse(lblPrice.Text);
                        decimal stock = Decimal.Parse(lblUnitsInStock.Text);
                        decimal sgc = Decimal.Parse(lblUnitsInStockGC.Text);
                     


                        totalPrice += price;
                        totalStock += stock;
                        totalgc += sgc;
                       
                        totalItems += 1;
                    }

                    if (e.Row.RowType == DataControlRowType.Footer)
                    {
                        Label lblTotalPrice = (Label)e.Row.FindControl("lblTotalPrice");
                        Label lblTotalUnitsInStock = (Label)e.Row.FindControl("lblTotalUnitsInStock");
                        Label lblTotalUnitsInStockGC = (Label)e.Row.FindControl("lblTotalUnitsInStockGC");
                       


                        lblTotalPrice.Text = totalPrice.ToString();
                        lblTotalUnitsInStock.Text = totalStock.ToString();
                        lblTotalUnitsInStockGC.Text = totalgc.ToString();
                       

                        // lblAveragePrice.Text = (totalPrice / totalItems).ToString("F");
                    }
                }

                protected void ddlsem_SelectedIndexChanged(object sender, EventArgs e)
                {
                    lblsemestre.Text = ddlsem.SelectedValue.ToString(); 
                    if (ddlsem.SelectedValue != null)
                    {
                        if (lblsemestre.Text.ToString() != "ALL")
                        {

                            Gridtoiec.DataSource = service.Afficher_list_ens_affecter("and a.NUM_semestre =" + lblsemestre.Text.ToString());
                            Gridtoiec.DataBind();
                            Gridtoiec.Visible = true;
                            Btnprint.Visible = true;
                            Button1.Visible = true;
                            lbltitle.Visible = true;
                            lblsemestre.Visible = true;
                        }
                        else
                        {
                            Gridtoiec.DataSource = service.Afficher_list_ens_affecter("");
                            Gridtoiec.DataBind();
                            Gridtoiec.Visible = true;

                            Btnprint.Visible = true;
                            Button1.Visible = true;
                            lbltitle.Visible = true;
                            lblsemestre.Visible = true;
                        }
                            
                        }
                    }
                }

 


    }


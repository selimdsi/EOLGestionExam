﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
namespace ESPOnline.Direction
{
    public partial class SuiviCahierDetexte : System.Web.UI.Page
    {
        CahietTextService service = new CahietTextService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)

            {
                plchoix.Visible = true;

            }
        }

        protected void TBdateseance_TextChanged(object sender, EventArgs e)
        {
            Gridexam.DataSource = service.GetDataPardate(Convert.ToDateTime(TBdateseance.Text),2);
            Gridexam.DataBind();
           
        }

        protected void RadComboBox1_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            //gridens.DataSource = service.GetDataParens(rdens.SelectedValue, Convert.ToInt32(RadioButtonList3.SelectedValue));
            //gridens.DataBind();
          
        }

        protected void RadComboBox2_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {

            gridens.DataSource = service.GetDataParens(rdens.SelectedValue, Convert.ToInt32(RadioButtonList3.SelectedValue));
            gridens.DataBind();
           
        }

        protected void btnvalid_Click(object sender, EventArgs e)
        {
            if (Rdchoix.SelectedValue == "1")
            {
                pldate.Visible = true;
                //plchoix.Visible = false;
                plclasse.Visible = false;
                plens.Visible = false;
            }

            else
                if (Rdchoix.SelectedValue == "2")
                {
                    bindEns();

                    plens.Visible = true;
                    pldate.Visible = false;
                    plclasse.Visible = false;
                }
                else
                    if (Rdchoix.SelectedValue == "3")
                    {
                        bind_niveau();
                        plclasse.Visible = true;
                        //plchoix.Visible = false;
                        pldate.Visible = false;
                        plens.Visible = false;

                    }
        }

        public void bindEns()
        {
            rdens.DataTextField = "nom_ens";
            rdens.DataValueField = "id_ens";
            rdens.DataSource = service.Getlistens();
            rdens.DataBind();
           // tempcl.Update();
        }

        public void bind_niveau()
        {

            ddlniv.DataTextField = "niveau";
            ddlniv.DataValueField = "niveau";
            ddlniv.DataSource = service.bind_niveau();
            
            ddlniv.DataBind();
            ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "0"));
            ddlniv.SelectedItem.Selected = false;
        }

        public void bindcl()
        {
            rdclasse.DataTextField = "code_cl";
            rdclasse.DataValueField = "code_cl";
            rdclasse.DataSource = service.Getlistcl(ddlniv.SelectedValue);
            rdclasse.DataBind();
        
        }

        protected void ddlniv_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindcl();
        }

        protected void rdclasse_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            gridclasse.DataSource = service.GetDataParclasse(rdclasse.SelectedValue, Convert.ToInt32(RadioButtonList3.SelectedValue));
            gridclasse.DataBind(); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.IO;
using System.Text;
using System.Drawing;

namespace ESPOnline.Direction
{
    public partial class IREP_ETUD_ETGR : System.Web.UI.Page
    {
        MinistereService service = new MinistereService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView1.DataSource = service.rep_etudant_etrgr();
                GridView1.DataBind();
            }
        }

        protected void Btnprint_Click(object sender, EventArgs e)
        {
            GridView1.Style[HtmlTextWriterStyle.Direction] = "rtl";

            GridView1.Rows[0].Cells[1].Style[HtmlTextWriterStyle.Direction] = "rtl";

            GridView1.AllowPaging = false;
            GridView1.DataSource = service.rep_etudant_etrgr();
            GridView1.DataBind();
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            GridView1.RenderControl(hw);
            string gridHTML = sw.ToString().Replace("\"", "'")
                .Replace(System.Environment.NewLine, "");
            StringBuilder sb = new StringBuilder();
            sb.Append("<x:DisplayRightToLeft/>\n");
            sb.Append("<script type = 'text/javascript'>");
            sb.Append("window.onload = new function(){");
            sb.Append("var printWin = window.open('', '', 'left=0");
            sb.Append(",top=0,width=1000,height=600,status=0');");
            sb.Append("printWin.document.write(\"");
            sb.Append(gridHTML);
            sb.Append("\");");
            sb.Append("printWin.document.close();");
            sb.Append("printWin.focus();");
            sb.Append("printWin.print();");
            sb.Append("printWin.close();};");
            sb.Append("</script>");
            ClientScript.RegisterStartupScript(this.GetType(), "GridPrint", sb.ToString());
            GridView1.AllowPaging = false;
            GridView1.DataSource = service.rep_etudant_etrgr();
            GridView1.DataBind();

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }


        protected void BuTT2_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=Repartition_etrangeres.xls");
            Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />");
            //  Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);

                //To Export all pages
                GridView1.AllowPaging = false;
                GridView1.DataSource = service.rep_etudant_etrgr();
                GridView1.DataBind();

                GridView1.HeaderRow.BackColor = Color.White;


                foreach (TableCell cell in GridView1.HeaderRow.Cells)
                {
                    cell.BackColor = GridView1.HeaderStyle.BackColor;


                }
                foreach (GridViewRow row in GridView1.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView1.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView1.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }
                GridView1.RenderControl(hw);

                //style to format numbers to string

                string style = @"<style> .textmode { } </style>";

                Response.Write(style);

                Response.Output.Write(sw.ToString());

                Response.Flush();

                Response.End();

                AddExcelStyling();

            }

        }



        decimal totalPrice = 0M;
        decimal totalStock = 0M;
        decimal totalgc = 0M;
        decimal totaltt = 0M;
        //decimal redOUB = 0M;
        //decimal PINS = 0M;
        decimal NBS = 0M;
        int totalItems = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPrice = (Label)e.Row.FindControl("lblPrice");
                Label lblUnitsInStock = (Label)e.Row.FindControl("lblUnitsInStock");
                Label lblUnitsInStockGC = (Label)e.Row.FindControl("lblUnitsInStockGC");
                //Label lblUnitsInStockGCTotal = (Label)e.Row.FindControl("lblUnitsInStockGCTotal");

                //Label lblUnitsInStockGCTredoub = (Label)e.Row.FindControl("lblUnitsInStockGCTredoub");
                //Label lblUnitsInStockGCTprins = (Label)e.Row.FindControl("lblUnitsInStockGCTprins");
                //Label lblUnitsInStockNB = (Label)e.Row.FindControl("lblUnitsInStockNB");

                decimal price = Decimal.Parse(lblPrice.Text);
                decimal stock = Decimal.Parse(lblUnitsInStock.Text);
                decimal sgc = Decimal.Parse(lblUnitsInStockGC.Text);
                //decimal tt = Decimal.Parse(lblUnitsInStockGCTotal.Text);

                ////decimal tt1 = Decimal.Parse(lblUnitsInStockGCTredoub.Text);
                ////decimal tt2 = Decimal.Parse(lblUnitsInStockGCTprins.Text);
                //decimal tt3 = Decimal.Parse(lblUnitsInStockNB.Text);


                totalPrice += price;
                totalStock += stock;
                totalgc += sgc;
                //totaltt += tt;
                ////redOUB += tt1;
                ////PINS += tt2;
                //NBS += tt3;
                totalItems += 1;
            }

            if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblTotalPrice = (Label)e.Row.FindControl("lblTotalPrice");
                Label lblTotalUnitsInStock = (Label)e.Row.FindControl("lblTotalUnitsInStock");
                Label lblTotalUnitsInStockGC = (Label)e.Row.FindControl("lblTotalUnitsInStockGC");
                //Label lblTotalUnitsInStockGCTotal = (Label)e.Row.FindControl("lblTotalUnitsInStockGCTotal");

                ////Label lblTotalUnitsInStockredoub = (Label)e.Row.FindControl("lblTotalUnitsInStockredoub");
                ////Label lblTotalUnitsInStockprins = (Label)e.Row.FindControl("lblTotalUnitsInStockprins");
                //Label lblTotalUnitsInStockNB = (Label)e.Row.FindControl("lblTotalUnitsInStockNB");


                lblTotalPrice.Text = totalPrice.ToString();
                lblTotalUnitsInStock.Text = totalStock.ToString();
                lblTotalUnitsInStockGC.Text = totalgc.ToString();
                //lblTotalUnitsInStockGCTotal.Text = totaltt.ToString();

                ////lblTotalUnitsInStockredoub.Text = redOUB.ToString();
                ////lblTotalUnitsInStockprins.Text = PINS.ToString();
                //lblTotalUnitsInStockNB.Text = NBS.ToString();

                // lblAveragePrice.Text = (totalPrice / totalItems).ToString("F");
            }
        }

        //methode of export to excel

        private string AddExcelStyling()
        {

            StringBuilder sb = new StringBuilder();

            sb.Append("<html xmlns:o='urn:schemas-microsoft-com:office:office'\n" +

            "xmlns:x='urn:schemas-microsoft-com:office:excel'\n" +

            "xmlns='http://www.w3.org/TR/REC-html40'>\n" +

            "<head>\n");

            sb.Append("<style>\n");

            sb.Append("@page");

            sb.Append("mso-page-orientation:landscape;}\n");

            sb.Append("</style>\n");

            sb.Append("<!--[if gte mso 9]><xml>\n");

            sb.Append("<x:ExcelWorkbook>\n");

            sb.Append("<x:ExcelWorksheets>\n");

            sb.Append("<x:ExcelWorksheet>\n");

            sb.Append("<x:Name>Sheet Name</x:Name>\n");

            sb.Append("<x:WorksheetOptions>\n");

            sb.Append("<x:Print>\n");

            sb.Append("<x:HorizontalResolution>600</x:HorizontalResolution\n");

            sb.Append("<x:VerticalResolution>600</x:VerticalResolution\n");

            sb.Append("</x:Print>\n");

            sb.Append("<x:Selected/>\n");

            sb.Append("<x:DisplayRightToLeft/>\n");

            sb.Append("<x:DoNotDisplayGridlines/>\n");

            sb.Append("</x:WorksheetOptions>\n");

            sb.Append("</x:ExcelWorksheet>\n");

            sb.Append("</x:ExcelWorksheets>\n");

            sb.Append("</x:ExcelWorkbook>\n");

            sb.Append("</xml><![endif]-->\n");

            sb.Append("</head>\n");

            sb.Append("<body>\n");

            return sb.ToString();
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Direction
{
    public partial class List_etud_inscrit_2015 : System.Web.UI.Page
    {
        StatService service = new StatService();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                get_annee_univ();
                Gridtoiec.Visible =false;
                
            }
        }

        public void get_annee_univ()
        {
            ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataSource = service.List_etud_2015();
            ddlan_univer.DataBind();
        
        
        }

        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";
               
                ddlsite.DataSource = service.bind_site( ddlan_univer.SelectedValue.ToString().Substring(0, 4));
   
                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                Label1.Text ="a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4)+"'";
            

            }
        }

        protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlsite.SelectedValue.ToString() != null)
            {
                if (ddlsite.SelectedValue.ToString() != "ALL")
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_niveau(Label1.Text.ToString() + " and lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'");
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;
                    Label2.Text = " lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
                }
                else
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_niveau(Label1.Text.ToString());
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                }
            }
        }

        protected void ddlniv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlniv.SelectedValue != null)
            {
                if (ddlniv.SelectedValue.ToString() != "ALL")
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    if (Label2.Text == "")
                    {
                        
                        ddclasse.DataSource = service.bind_classes("lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString());
                        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    }
                    else 
                    {
                       
                        ddclasse.DataSource = service.bind_classes("lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() + " " + "and" + " " + Label2.Text.ToString());
                     
                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                        
                        Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    }
                                    }
                else 
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    if (Label2.Text.ToString() != "")
                    {
                       
                        ddclasse.DataSource = service.bind_classes(Label1.Text.ToString() + " and " + Label2.Text.ToString());
                    }
                    else
                    {
                        ddclasse.DataSource = service.bind_classes(Label1.Text.ToString()); 
                    }
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddclasse.DataBind();
                    ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddclasse.SelectedItem.Selected = false;
                    ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                    Label3.Text = "";
                }
               
            }
        }


        public void get_ETUD(string a, string code_cl)
        {
            //(and lower(a.code_cl) like '" + code_cl + "%');
           
        }

        protected void ddclasse_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddclasse.SelectedValue != null)
            {

                if (ddclasse.SelectedValue.ToString() != "ALL" )

                {
                    if (Label2.Text.ToString() != "" && Label3.Text.ToString() != "")
                    {

                        Gridtoiec.DataSource = service.bind_tous_etud("lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() + " " + "and" + " " + Label2.Text.ToString() + " " + "and" + " " + Label3.Text.ToString());
                        //+ " " + "and" + " " + Label3.Text.ToString()
                        Gridtoiec.DataBind();
                        Gridtoiec.Visible = true;
                        Label4.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'";
                        Label5.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() + " " + "and" + " " + Label2.Text.ToString() + " " + "and" + " " + Label3.Text.ToString();
                    }
                    else
                   
                        if (Label2.Text.ToString() == "" && Label3.Text.ToString() != "")
                        {
                            Gridtoiec.DataSource = service.bind_tous_etud("lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() +" and " +Label3.Text.ToString());
                            //+ " " + "and" + " " + Label3.Text.ToString()
                            Gridtoiec.DataBind();
                            Gridtoiec.Visible = true;
                            Label4.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'";
                        }

                        else

                            if (Label2.Text.ToString() != "" && Label3.Text.ToString() == "")
                            {
                                Gridtoiec.DataSource = service.bind_tous_etud("lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString());
                                //+ " " + "and" + " " + Label3.Text.ToString()
                                Gridtoiec.DataBind();
                                Gridtoiec.Visible = true;
                                Label4.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'";
                            }
                }
                else 
                {
                    if (Label3.Text.ToString() == "" && Label2.Text.ToString()=="")
                    {
                        Gridtoiec.DataSource = service.bind_tous_etud(Label1.Text.ToString());
                       
                    }
                    else
                    if (Label3.Text.ToString() != "" && Label2.Text.ToString() == "")
                    {
                        Gridtoiec.DataSource = service.bind_tous_etud(Label1.Text.ToString()+"  and "+Label3.Text.ToString());

                    }
                    else
                    if (Label3.Text.ToString() == "" && Label2.Text.ToString() != "")
                    {
                        Gridtoiec.DataSource = service.bind_tous_etud(Label1.Text.ToString() + "  and " + Label2.Text.ToString());

                    }

                    else
                    {
                        Gridtoiec.DataSource = service.bind_tous_etud(Label1.Text.ToString() + " and " + Label2.Text.ToString() + " and " + Label3.Text.ToString());
                       
                    }
                    Gridtoiec.DataBind();
                    Gridtoiec.Visible = true;
                    Label4.Text = "";
                }
               
            }
          
        }
    }
}
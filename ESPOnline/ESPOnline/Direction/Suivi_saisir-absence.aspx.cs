﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace ESPOnline.Direction
{
    public partial class Suivi_saisir_absence : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_DECID"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }
              
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox1.Text = DateTime.Parse(TextBox2.Text.Trim()).ToString("dd-MMM-yy", CultureInfo.InvariantCulture).ToUpper() ;

            GridView1.DataSourceID = "SqlDataSource1";
           
            GridView1.DataBind();
            if (GridView1.Rows.Count == 0) { Label2.Visible = true; } else { Label2.Visible = false; }
            GridView2.DataSourceID = "SqlDataSource2";

            GridView2.DataBind();
            if (GridView2.Rows.Count == 0) { Label3.Visible = true; } else { Label3.Visible = false; }
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        //protected void Button2_Click(object sender, EventArgs e)
        //{
        //    TextBox1.Text = DateTime.Parse(TextBox2.Text.Trim()).ToString("dd-MMM-yy", CultureInfo.InvariantCulture).ToUpper();

           
        //}

     
    }
}
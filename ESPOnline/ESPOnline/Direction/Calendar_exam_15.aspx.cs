﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;

namespace ESPOnline.Direction
{
    public partial class Calendar_exam_15 : System.Web.UI.Page
    {
        ServiceExamen server = new ServiceExamen();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                BindGrid(dt,true);
                ViewState["dt"] = dt;
                BindGrid(dt, false);

            }
        }

    


        private void BindGrid(DataTable dt, bool rotate)
        {
            GridView1.ShowHeader = !rotate;
           
            dt = server.bind_calendar_exam();
            GridView1.DataSource = dt;
            GridView1.DataBind();

            if (dt.Rows.Count > 0)
            {
                //Bind the First GridView with the original data from the DataTable
                GridView1.DataSource = dt;
                GridView1.DataBind();

                //Pivot the Original data from the DataTable by calling the
                //method PivotTable and pass the dt as the parameter

                DataTable pivotedTable = ToPivotTable(dt);
                GridView2.DataSource = pivotedTable;
                GridView2.DataBind();
                GridView1.Visible = false;
            }
            //if (rotate)
            //{
            //    foreach (GridViewRow row in GridView1.Rows)
            //    {
            //        row.Cells[0].CssClass = "header";
            //    }
            //}
        }


        /* protected void Convert(object sender, EventArgs e)
         {
             //DataTable dt  = server.bind_calendar_exam();

             DataTable dt = (DataTable)ViewState["dt"];
             if ((sender as Button).CommandArgument == "1")
             {
                 btnConvert1.Visible = false;
                 btnConvert2.Visible = true;
                 DataTable dt2 = new DataTable();
                 for (int i = 0; i <= dt.Rows.Count; i++)
                 {
                     dt2.Columns.Add();
                 }
                 for (int i = 0; i < dt.Columns.Count; i++)
                 {
                     dt2.Rows.Add();
                     dt2.Rows[i][0] = dt.Columns[i].ColumnName;
                 }
                 for (int i = 0; i < dt.Columns.Count; i++)
                 {
                     for (int j = 0; j < dt.Rows.Count; j++)
                     {
                         dt2.Rows[i][j + 1] = dt.Rows[j][i];
                     }
                 }
                 BindGrid(dt2, true);
             }
             else
             {
                 btnConvert1.Visible = true;
                 btnConvert2.Visible = false;
                 BindGrid(dt, false);
             }
         }*/

        public static DataTable ToPivotTable(DataTable sourceTable)
        {
            DataTable pivotTable = new DataTable();
            // First Column is the params column:
            pivotTable.Columns.Add("Parameter", typeof(string));
            // Next Columns are each line first item value
            foreach (DataRow row in sourceTable.Rows)
            {
                string columnName = row.ItemArray[0].ToString();
                string newColumnName = columnName;
                int id = 2;
                while (pivotTable.Columns.Contains(newColumnName))
                {
                    newColumnName = columnName + " [" + id + "]";
                    ++id;
                }
                pivotTable.Columns.Add(newColumnName, typeof(string));
            }
            // Fills empty rows - number of column - first column
            // The first column in the new table is the paramer - the name of the column
            for (int columnIndex = 1; columnIndex < sourceTable.Columns.Count; columnIndex++)
            {
                DataRow row = pivotTable.NewRow();
                row[columnIndex] = sourceTable.Columns[columnIndex].ColumnName;
                pivotTable.Rows.Add(row);
            }
            // Fills the pivot table by source values
            for (int sourceRowIndex = 0; sourceRowIndex < sourceTable.Rows.Count; sourceRowIndex++)
            {
                // Starts from 1 becuase the 0 column is the row headers in the pivot
                for (int sourceColumnIndex = 1; sourceColumnIndex < sourceTable.Columns.Count; sourceColumnIndex++)
                {
                    pivotTable.Rows[sourceColumnIndex - 1][sourceRowIndex + 1] =
                      sourceTable.Rows[sourceRowIndex].ItemArray[sourceColumnIndex];
                }
            }

            return pivotTable;
        }
    }
}

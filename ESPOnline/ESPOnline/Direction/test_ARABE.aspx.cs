﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Direction
{
    public partial class test_ARABE : System.Web.UI.Page
    {
        MinistereService service = new MinistereService();
        protected void Page_Load(object sender, EventArgs e)
        {
            Gridview1.DataSource = service.getARABE();
            Gridview1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            service.TEST(TextBox1.Text);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SuiviCahierDetexteff.aspx.cs"
 Inherits="ESPOnline.Direction.SuiviCahierDetexteff" MasterPageFile="~/Direction/Site2.Master"  EnableEventValidation = "false" %>

 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<style type="text/css">


.custom-calendar .ajax__calendar_container   
{
    background-color:#ffc; /* pale yellow */ border:solid 1px #666;z-index : 1000 ; 
    font-family:arial,helvetica,clean,sans-serif;
 
    } 
 
.custom-calendar .ajax__calendar_title {  background-color:#cf9; /* pale green */ height:20px;  color:#333; } 
 
.custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next {  background-color:#aaa; /* darker gray */ height:20px;  width:20px;
} 
 
.custom-calendar .ajax__calendar_day {  color:#333; /* normal day - darker gray color */} .custom-calendar .ajax__calendar_other .ajax__calendar_day {  color:#666; /* day not actually in this month - lighter gray color */} 
 
.custom-calendar .ajax__calendar_today {  background-color:#cff;  /* pale blue */ height:20px; }

</style>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
     

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<h3 class="style1">Suivi Cahier de textes des enseignants</h3>
<br />
<asp:Panel ID="plchoix" runat="server" >
<table>

<tr>
<td>
<asp:Label ID="Label11" runat="server" Text="Veuillez suivre le cahier de texte soit: "
                            ></asp:Label></td>

                                                     
   <td>          <asp:RadioButtonList ID="Rdchoix" runat="server"   
            RepeatDirection="Horizontal" width="500px" >
                 <asp:ListItem Value="1">Par date</asp:ListItem>
                 <asp:ListItem Value="2">Par enseignant</asp:ListItem>
                 <asp:ListItem Value="3">Par classe</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 <td></td>
 
           
        

            <td><asp:Button  ID="btnvalid" runat="server" Text="Valider" Height="35px" 
                    Width="120px" onclick="btnvalid_Click" /></td>
</tr>
</table>
</asp:Panel>

    </asp:Content>
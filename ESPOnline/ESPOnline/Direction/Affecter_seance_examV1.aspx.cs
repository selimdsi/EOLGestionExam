﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using BLL;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class Affecter_seance_examV1 : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();

        #region static data
        //private ArrayList GetDummyData()
        //{

        //    ArrayList arr = new ArrayList();

        //    arr.Add(new ListItem("Item1", "1"));
        //    arr.Add(new ListItem("Item2", "2"));
        //    arr.Add(new ListItem("Item3", "3"));
        //    arr.Add(new ListItem("Item4", "4"));
        //    arr.Add(new ListItem("Item5", "5"));

        //    return arr;
        //}

        //private void FillDropDownList(DropDownList ddl)
        //{
        //    ArrayList arr = GetDummyData();

        //    foreach (ListItem item in arr)
        //    {
        //        ddl.Items.Add(item);
        //    }
        //} 
        #endregion

        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;

            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));//for TextBox value 
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));//for TextBox value 
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));//for DropDownList selected item 
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));//for DropDownList selected item 
            dt.Columns.Add(new DataColumn("Column5", typeof(string)));//for DropDownList selected item 

            dt.Columns.Add(new DataColumn("Column6", typeof(string)));//for DropDownList selected item 

            dt.Columns.Add(new DataColumn("Column7", typeof(string)));//for DropDownList selected item 


            dr = dt.NewRow();
            dr["RowNumber"] = 1;
            dr["Column1"] = string.Empty;
            dr["Column2"] = string.Empty;

            dr["Column3"] = string.Empty;
            dr["Column4"] = string.Empty;

            dr["Column5"] = string.Empty;
            dr["Column6"] = string.Empty;

            dr["Column7"] = string.Empty;
           
            dt.Rows.Add(dr);

            //Store the DataTable in ViewState for future reference 

            ViewState["CurrentTable"] = dt;

            //Bind the Gridview 
            Gridview1.DataSource = dt;
            Gridview1.DataBind();

            //After binding the gridview, we can then extract and fill the DropDownList with Data 

            DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[2].FindControl("DropDownList1");
            DropDownList ddl2 = (DropDownList)Gridview1.Rows[0].Cells[3].FindControl("DropDownList2");
            DropDownList ddl3 = (DropDownList)Gridview1.Rows[0].Cells[4].FindControl("DropDownList3");
            DropDownList ddl4 = (DropDownList)Gridview1.Rows[0].Cells[5].FindControl("DropDownList4");
            DropDownList ddl5 = (DropDownList)Gridview1.Rows[0].Cells[6].FindControl("DropDownList5");
            DropDownList ddl6 = (DropDownList)Gridview1.Rows[0].Cells[7].FindControl("DropDownList6");
            //FillDropDownList(ddl1);
            //FillDropDownList(ddl2);
            
        }

        private void AddNewRowToGrid()
        {

            if (ViewState["CurrentTable"] != null)
            {

                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;

                if (dtCurrentTable.Rows.Count > 0)
                {
                    drCurrentRow = dtCurrentTable.NewRow();
                    drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

                    //add new row to DataTable 
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    //Store the current data to ViewState for future reference 

                    ViewState["CurrentTable"] = dtCurrentTable;


                    for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
                    {

                        //extract the TextBox values 

                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[1].FindControl("TextBox1");
                        //TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox2");

                        dtCurrentTable.Rows[i]["Column1"] = box1.Text;
                        //dtCurrentTable.Rows[i]["Column2"] = box2.Text;

                        //extract the DropDownList Selected Items 

                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[i].Cells[2].FindControl("ddlCountries");
                        DropDownList ddl2 = (DropDownList)Gridview1.Rows[i].Cells[3].FindControl("ddlCountries2");

                        DropDownList ddl3 = (DropDownList)Gridview1.Rows[i].Cells[4].FindControl("DropDownList3");

                        DropDownList ddl4 = (DropDownList)Gridview1.Rows[i].Cells[5].FindControl("DropDownList4");

                        DropDownList ddl5 = (DropDownList)Gridview1.Rows[i].Cells[6].FindControl("DropDownList5");
                        DropDownList ddl6 = (DropDownList)Gridview1.Rows[i].Cells[7].FindControl("DropDownList6");



                        // Update the DataRow with the DDL Selected Items 

                        dtCurrentTable.Rows[i]["Column2"] = ddl1.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Column3"] = ddl2.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Column4"] = ddl3.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Column5"] = ddl4.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Column6"] = ddl5.SelectedItem.Text;
                        dtCurrentTable.Rows[i]["Column7"] = ddl6.SelectedItem.Text;

                    }
                    //fillJours();
                   // fillnumJours();

                    //Rebind the Grid with the current data to reflect changes 
                    Gridview1.DataSource = dtCurrentTable;
                    Gridview1.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");

            }
            //Set Previous Data on Postbacks 
            SetPreviousData();
        }

        private void SetPreviousData()
        {

            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[1].FindControl("TextBox1");
                        //TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox2");

                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[rowIndex].Cells[2].FindControl("ddlCountries");
                        DropDownList ddl2 = (DropDownList)Gridview1.Rows[rowIndex].Cells[3].FindControl("ddlCountries2");

                        DropDownList ddl3 = (DropDownList)Gridview1.Rows[rowIndex].Cells[4].FindControl("DropDownList3");
                        DropDownList ddl4 = (DropDownList)Gridview1.Rows[rowIndex].Cells[5].FindControl("DropDownList4");


                        DropDownList ddl5 = (DropDownList)Gridview1.Rows[rowIndex].Cells[6].FindControl("DropDownList5");
                        DropDownList ddl6 = (DropDownList)Gridview1.Rows[rowIndex].Cells[7].FindControl("DropDownList6");



                        //Fill the DropDownList with Data 
                        //FillDropDownList(ddl1);
                        //FillDropDownList(ddl2);
                       // fillnumJours();
                        //fillJours();

                        if (i < dt.Rows.Count - 1)
                        {

                            //Assign the value from DataTable to the TextBox 
                            box1.Text = dt.Rows[i]["Column1"].ToString();
                            //box2.Text = dt.Rows[i]["Column2"].ToString();

                            //Set the Previous Selected Items on Each DropDownList  on Postbacks 
                            ddl1.ClearSelection();
                            ddl1.Items.FindByText(dt.Rows[i]["Column2"].ToString()).Selected = true;

                            ddl2.ClearSelection();
                            ddl2.Items.FindByText(dt.Rows[i]["Column3"].ToString()).Selected = true;

                            ddl3.ClearSelection();
                            ddl3.Items.FindByText(dt.Rows[i]["Column4"].ToString()).Selected = true;


                            ddl4.ClearSelection();
                            ddl4.Items.FindByText(dt.Rows[i]["Column5"].ToString()).Selected = true;


                            ddl5.ClearSelection();
                            ddl5.Items.FindByText(dt.Rows[i]["Column6"].ToString()).Selected = true;

                            ddl6.ClearSelection();
                            ddl6.Items.FindByText(dt.Rows[i]["Column7"].ToString()).Selected = true;


                        }

                        rowIndex++;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetInitialRow();
                //fillJours();
                //fillnumJours();
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
        }


        public void fillJours()
        {
            DropDownList ddl2 = (DropDownList)Gridview1.Rows[0].Cells[3].FindControl("DropDownList2");
            ddl2.DataSource = service.fill_jours();
            ddl2.DataBind();
        }


        public void fillnumJours()
        {
            DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[2].FindControl("DropDownList1");
            ddl1.DataSource = service.fill_Num_jours();
            ddl1.DataBind();
        }


        public void fillHD()

        { 
        
        }


        public void fillMD()
        {

        }

        public void fillHF()
        {
        }


        public void fillMF()
        {

        }

        

        protected void Calendar_Changed(object sender, EventArgs e)
        {
            Calendar DateTakenCalendar = (Calendar)sender;
            TextBox DateTakenTextBox = (TextBox)((GridViewRow)((Calendar)(sender)).Parent.Parent).FindControl("TextBox1");

            DateTakenTextBox.Text = DateTakenCalendar.SelectedDate.ToString();

        }


       

        


        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                OracleCommand cmd = new OracleCommand("select code_nome from code_nomenclature where code_str='92'");
                DropDownList ddlCountries = (e.Row.FindControl("ddlCountries") as DropDownList);
                ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
                ddlCountries.DataTextField = "code_nome";
                ddlCountries.DataValueField = "code_nome";
                // ddlCountries.Items.Add("NN");
                // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                ddlCountries.DataBind();


                OracleCommand cmd2 = new OracleCommand("select lib_nome from code_nomenclature where code_str='92'");
                DropDownList ddlCountries2 = (e.Row.FindControl("ddlCountries2") as DropDownList);
                ddlCountries2.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                ddlCountries2.DataTextField = "lib_nome";
                ddlCountries2.DataValueField = "lib_nome";
                // ddlCountries.Items.Add("NN");
                // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                ddlCountries2.DataBind();
            }


            
        
        
        }

      




        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void btnaffecter_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow row in Gridview1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    DataTable dtt;

                    if (Rdcours.SelectedValue == "C")


                    {
                        

                        string semestre = Rdsemestre.SelectedValue;

                        string anneesub = service.Get_annee_deb();
                        string annee = anneesub.Substring(0,4);

                        string periode = rdperiode.SelectedValue;
                        string site = DropDownList2.SelectedValue;
                        string heuredeb = row.Cells[4].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                        string mindebut = row.Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                        string dp = ":";
                        string hdeb = string.Concat(heuredeb, dp, mindebut);
                        string heurefin = row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                        string mindefin = row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                        string hfin = string.Concat(heurefin, dp, mindefin) ;


                        string numseance = row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text;
                        string date_seanceup=row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text;

                        DateTime date_seance2 = Convert.ToDateTime(date_seanceup);

                        string date_seance = date_seance2.ToString("dd/MM/yyyy");
                        string num_jours = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                        string jours = row.Cells[3].Controls.OfType<DropDownList>().FirstOrDefault().Text;




                        dtt = service.existSeance(numseance,periode,Convert.ToInt32( semestre),date_seance,annee);


                        if (dtt.Rows.Count != 0)
                        {
                            Response.Write(@"<script language='javascript'>alert('Affectation dejà existe');</script>");
                        }


                        else

                        {
                            OracleCommand cmd = new OracleCommand("insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode) values ('" + numseance + "','" + annee + "',to_date('" + date_seance + "','dd/MM/yy'),'" + semestre + "','" + num_jours + "','" + jours + "','" + hdeb + "','" + hfin + "','" + site + "','" + periode + "') ");

                            this.ExecuteQuery(cmd, "UPDATE");
                           // Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");

                        }
                        //("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam,code_module=:code_module where code_cl =:code_cl");

                       

                        //cmd.Parameters.Add(":num_seance", row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text);
                        ////cmd.Parameters.Add(":annee_deb", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add("to_date(:date_seance,'dd/mm/yy')", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        ////cmd.Parameters.Add(":semestre", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":num_jours", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":jours", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);
                       
                        //cmd.Parameters.Add(":heure_debut", row.Cells[4].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":min_debut", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);


                        //cmd.Parameters.Add(":heure_fin", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":min_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        
                        //cmd.Parameters.Add(":site", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":periode", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);

                      

                        
                    }

                    else

                    {

                        if (Rdcours.SelectedValue == "A")
                        {
                            string semestre1 = Rdsemestre.SelectedValue;
                            string annee1 = ddlan_univer.SelectedValue.Substring(0,4);
                            string periode = rdperiode.SelectedValue;
                            string site = ddlsite.SelectedValue;


                            string heuredeb = row.Cells[4].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            string mindebut = row.Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                            string dp = ":";
                            string hdeb = string.Concat(heuredeb, dp, mindebut);
                            string heurefin = row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            string mindefin = row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            string hfin = string.Concat(heurefin, dp, mindefin);


                            string numseance = row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text;
                            string date_seanceup = row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text;

                            DateTime date_seance2 = Convert.ToDateTime(date_seanceup);

                            string date_seance = date_seance2.ToString("dd/MM/yyyy");
                            string num_jours = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                            string jours = row.Cells[3].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            dtt = service.existSeance(numseance,periode,Convert.ToInt32( semestre1),date_seance,annee1);


                            if (dtt.Rows.Count != 0)
                            {
                                Response.Write(@"<script language='javascript'>alert('Affectation dejà existe');</script>");
                            }


                            else
                            {


                                OracleCommand cmd = new OracleCommand("insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode) values ('" + numseance + "','" + annee1 + "',to_date('" + date_seance + "','dd/MM/yy'),'" + semestre1 + "','" + num_jours + "','" + jours + "','" + hdeb + "','" + hfin + "','" + site + "','" + periode + "') ");
                                //("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam,code_module=:code_module where code_cl =:code_cl");
                                this.ExecuteQuery(cmd, "UPDATE");
                                //Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");
                            }
                            //cmd.Parameters.Add(":num_seance", row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":annee_deb", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add("to_date(:date_seance,'dd/mm/yy')", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":semestre", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":num_jours", row.Cells[4].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":jours", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":heure_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":heure_fin", row.Cells[8].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":site", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":periode", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);





                        }
                    }
                    
                }


                //rebind grid maha
                //gvCustomers.DataSource = service.bind_niveau_etudiantbycl(ddclasse2.SelectedValue, ddlannee_debM.SelectedValue);
                //gvCustomers.DataBind();
            }

            Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");
                           

        }

        protected void btnvalid_Click(object sender, EventArgs e)
        {
            if (Rdsemestre.SelectedValue == "" || rdperiode.SelectedValue == "")
            {
                Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

            }
            //

            else
            plvalider.Visible = false;
            Plchoixannee.Visible = true;
        }

        protected void Btnsuivant_Click(object sender, EventArgs e)
        {
            plvalider.Visible = false;
            Plchoixannee.Visible = false;

            if (Rdcours.SelectedValue == "C")
            {

                Plannee2.Visible = true;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";

                string annee =service.Get_annee_deb();

                DropDownList2.DataSource = service.bind_site2(annee.Substring(0, 4));

                DropDownList2.DataBind();

            }

            else
            {
                if (Rdcours.SelectedValue == "A")
                    plannee.Visible = true;

                ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
                ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
                ddlan_univer.DataSource = service.List_etud_2015();
                ddlan_univer.DataBind();


            {
                
            }
            }


        }

        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                //Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }
        }

        protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
        {
            pl2.Visible = true;
            plannee.Visible = false;

            lblan.Text = ddlan_univer.SelectedValue.Substring(0,4);
            lblperiod.Text = rdperiode.SelectedValue;
            lblsem.Text = Rdsemestre.SelectedValue;
            lblSite.Text = ddlsite.SelectedValue;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            pl2.Visible = true;
            Plannee2.Visible = false;

            lblan.Text = service.Get_annee_deb();
            lblperiod.Text = rdperiode.SelectedValue;
            lblsem.Text = Rdsemestre.SelectedValue;
            lblSite.Text = DropDownList2.SelectedValue;

        }

        //ajouter les seances




        protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
            DropDownList duty = (DropDownList)gvr.FindControl("ddlCountries");
            DropDownList duty2 = (DropDownList)gvr.FindControl("ddlCountries2");

            if (duty.SelectedValue == "01")
            {
                duty2.SelectedValue = "Lundi";

            }
            else
                if (duty.SelectedValue == "02")
                {

                    duty2.SelectedValue = "Mardi";
                }
                else
                    if (duty.SelectedValue == "03")
                    {
                        duty2.SelectedValue = "Mercredi";

                    }
                    else
                        if (duty.SelectedValue == "04")
                        {
                            duty2.SelectedValue = "Jeudi";

                        }
                        else
                            if (duty.SelectedValue == "05")
                            {
                                duty2.SelectedValue = "Vendredi";


                            }
                            else if (duty.SelectedValue == "06")
                            {
                                duty2.SelectedValue = "Samedi";

                            }
        }
       






    }



}
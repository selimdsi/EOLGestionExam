﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List_etud_inscrit_2015.aspx.cs" Inherits="ESPOnline.Direction.List_etud_inscrit_2015"
MasterPageFile="~/Direction/Site2.Master"  EnableEventValidation = "false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
     
 <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <%--  <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>--%>
<script src="../Contents/Scripts/ScrollableGridPlugin_ASP.NetAJAX_2.0.js" type="text/javascript"></script>
     <script type="text/javascript">
    $(document).ready(function () {
        $('#<%=Gridtoiec.ClientID %>').Scrollable({
            ScrollHeight: 300,
         
        });
    });
    </script>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: normal;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: normal;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}
.grid td, .grid th{
  text-align:center;
      
          </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />

<table>
<tr>
<td><asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label></td>
</tr>
<tr><td> <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"></asp:Label></td></tr>
<tr><td><asp:Label ID="Label3" runat="server" Text="Label" Visible="false"></asp:Label></td></tr>
<tr><td> <asp:Label ID="Label4" runat="server" Text="Label" Visible="false"></asp:Label></td></tr>

<tr><td> <asp:Label ID="Label5" runat="server" Text="Label" Visible="false"></asp:Label></td></tr>
</table>
    <table>
<tr>
<td>
Année Universitaire:
<asp:DropDownList ID="ddlan_univer" runat="server" AppendDataBoundItems="true"  Width="150px"
   Height="30px"     AutoPostBack="true" onselectedindexchanged="ddlan_univer_SelectedIndexChanged">
<asp:ListItem>Veuillez choisir
</asp:ListItem>
</asp:DropDownList>
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
Site:
<asp:DropDownList ID="ddlsite" runat="server" onselectedindexchanged="ddlsite_SelectedIndexChanged"  Width="150px"
   Height="30px"
    AutoPostBack="true"    >

</asp:DropDownList>

</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
Niveau:
<asp:DropDownList ID="ddlniv" runat="server" Width="150px"
   Height="30px"
        AutoPostBack="true" onselectedindexchanged="ddlniv_SelectedIndexChanged">

</asp:DropDownList>
</td>
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td>
Classes:
<asp:DropDownList ID="ddclasse" runat="server" Width="150px"
   Height="30px"
        AutoPostBack="true" onselectedindexchanged="ddclasse_SelectedIndexChanged">

</asp:DropDownList>
</td>

</tr>
</table>
<br />
<center>
<table width="100%">
<tr>
<td>

<asp:GridView runat="server" ID="Gridtoiec" AutoGenerateColumns="False" 
                                                          
                                                         Style="border-bottom: black 2px ridge; border-left: black 2px ridge;
                                                        background-color: white; border-top: black 2px ridge; border-right: black 2px ridge;"
                                                        BorderWidth="0px" BorderColor="Red"  CssClass="grid"
                                                        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="Both" EmptyDataRowStyle-CssClass="ItemStyle"  BackColor="#0099CC"
 
                                                   
                                                       >
                                                        <EmptyDataTemplate>
                                                            Pas d'enregistrement.
                                                        </EmptyDataTemplate>
                                                        
                                                        <HeaderStyle HorizontalAlign="Center" Height="20px" Width="100px" BackColor="Red" />
                                                        <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                                                        <FooterStyle CssClass="ItemStyle" />
                                                        <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                                                        <RowStyle CssClass="GridItemStyle" />
                                                        <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        <Columns>
          

                
               

               
                  <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("NO")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="code classe" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice1" runat="server" Text='<%# Eval("code_cl")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="ID ET" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice2" runat="server" Text='<%# Eval("ID_ET")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice2" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="NOM" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice3" runat="server" Text='<%# Eval("NOM")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice3" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="PRENOM" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice4" runat="server" Text='<%# Eval("PRENOM")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice4" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="Date de naissance" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice5" runat="server" Text='<%# Eval("Date_de_naissance")%>' DataTextFormatString="{0:d}" />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice5" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="Lieu de naissance" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice6" runat="server" Text='<%# Eval("Lieu_de_naissance")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice6" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="Cin/Passeport" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice7" runat="server" Text='<%# Eval("Cin_Passeport")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice7" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>

       <asp:TemplateField HeaderText="Nature Bac" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice8" runat="server" Text='<%# Eval("Nature_Bac")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice8" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>


       <asp:TemplateField HeaderText="Date Bac" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice9" runat="server" Text='<%# Eval("Date_Bac")%>' DataTextFormatString="{0:d} "/>
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice9" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>


       <asp:TemplateField HeaderText="N° Bac" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("Num_Bac")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice11" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
              
               <asp:TemplateField HeaderText="Etablissement d'origine" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice12" runat="server" Text='<%# Eval("ETAB_ORIGINE")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice12" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>
              

               </Columns>
                      <%--<FooterStyle BackColor="Red" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
   <HeaderStyle BackColor="Red" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />--%>
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <RowStyle ForeColor="#000000"  />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        
                                                    </asp:GridView>
</td>
</tr>

</table>

</center>


</asp:Content>


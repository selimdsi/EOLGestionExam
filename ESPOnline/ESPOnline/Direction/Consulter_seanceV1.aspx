﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Consulter_seanceV1.aspx.cs" 
Inherits="ESPOnline.Direction.Consulter_seance"MasterPageFile="~/Direction/Site2.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>



<script src="../Contents/Scripts/ScrollableGridPlugin_ASP.NetAJAX_2.0.js" type="text/javascript"></script>
     <script type="text/javascript">
    $(document).ready(function () {
        $('#<%=gvCustomers.ClientID %>').Scrollable({
            ScrollHeight: 300,
         
        });
    });
    </script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

      
          </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
<br />
<center>

<h3> Consulter et modifier les séances affectées</h3>
<br />
<br />
<table>
<tr><td>

<asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="false" 
OnRowDataBound = "OnRowDataBoundS" 
 Style="border-bottom: white 2px ridge; border-left: white 2px ridge; background-color: white; 
     border-top: white 2px ridge; border-right: white 2px ridge;"
  BorderWidth="0px" 
        BorderColor="White" CellSpacing="1" CellPadding="3" CssClass="grid"
        
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="None" 
        EmptyDataRowStyle-CssClass="ItemStyle"  >
    <Columns>
       
        <asp:TemplateField ItemStyle-Width="20px">
      
     <HeaderTemplate>
                <asp:CheckBox ID = "chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChangedDDD" Visible="false"/>
            </HeaderTemplate>
           <ItemTemplate> 
              <asp:CheckBox ID="CheckBox1"  runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChanged" />
           </ItemTemplate>
        </asp:TemplateField>
        
<%--insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode
--%>




<%--
        <asp:BoundField DataField="num_seance" HeaderText="Nº séance" SortExpression="num_seance"
            ItemStyle-Width="25px" />--%>


             <asp:TemplateField HeaderText="Nº séance">
                <ItemTemplate>
                   <%-- <asp:BoundField DataField="RowNumber" HeaderText="Nº séance"  />--%>
                    <asp:Label ID="lb" runat="server" Text='<%# Eval("num_seance")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>



<asp:BoundField DataField="annee_deb" HeaderText="annee_deb" SortExpression="annee_deb" />
<asp:BoundField DataField="semestre" HeaderText="semestre" SortExpression="semestre" />

<asp:BoundField DataField="site" HeaderText="site" SortExpression="site" />

<asp:BoundField DataField="periode" HeaderText="periode" SortExpression="periode" />

        
        <asp:TemplateField HeaderText="date seance">
                <ItemTemplate>
                <asp:Label ID="lblCountry" runat="server"  Text='<%# Eval("date_seance","{0:dd/MM/yyyy}") %>'></asp:Label>
               <asp:TextBox  
               ID="ddldate" runat="server"  Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" >
               </asp:TextBox>
            </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText=" heure_debut">

             <ItemTemplate>
                <asp:Label ID = "lblCountry2" runat="server" Text='<%# Eval("heure_debut") %>'></asp:Label>
                <asp:TextBox ID="ddhb" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>

                           <asp:TemplateField HeaderText="heure_fin">

             <ItemTemplate>
                <asp:Label ID = "lblCountry3" runat="server" Text='<%# Eval("heure_fin") %>'></asp:Label>
                <asp:TextBox ID="ddlhf" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>


                           
                           <asp:TemplateField HeaderText="Nº jours">

             <ItemTemplate>
                <asp:Label ID = "lblCountry4" runat="server" Text='<%# Eval("NUM_jours") %>'></asp:Label>
                <asp:TextBox ID="ddlnumj" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>

                           
                           <asp:TemplateField HeaderText="jours">

             <ItemTemplate>
                <asp:Label ID = "lblCountry5" runat="server" Text='<%# Eval("jours") %>'></asp:Label>
                <asp:TextBox ID="ddljours" runat="server" Visible = "false" OnSelectedIndexChanged="ddlDropDownList122_SelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true">
                </asp:TextBox>
            </ItemTemplate>
                           </asp:TemplateField>
    </Columns>
</asp:GridView>
</ContentTemplate>
</asp:UpdatePanel></td>

<td>
<asp:Button  ID="btnadd" runat="server" Text="Modifier" onclick="btnadd_Click"/>
</td>
</tr>

</table>



</center>

<</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;

namespace ESPOnline.Direction
{
    public partial class Pivot_Data_TABLE : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindGridView();
            }
        }

        private void BindGridView()
        {
            DataTable dt = new DataTable();
            OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString);
            try
            {
                con.Open();
                OracleCommand cmd = new OracleCommand();
                cmd.Connection = con;
                cmd.CommandText = " select * from customers";

                OracleDataAdapter od = new OracleDataAdapter(cmd);
                od.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    //Bind the First GridView with the original data from the DataTable
                    GridView1.DataSource = dt;
                    GridView1.DataBind();

                    //Pivot the Original data from the DataTable by calling the
                    //method PivotTable and pass the dt as the parameter

                    DataTable pivotedTable = ToPivotTable(dt);
                    GridView2.DataSource = pivotedTable;
                    GridView2.DataBind();
                }
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                string msg = "Fetch Error:";
                msg += ex.Message;
                throw new Exception(msg);
            }
            finally
            {
                con.Close();
            }
        }
        /*  private DataTable PivotTable(DataTable origTable)
        {

            DataTable newTable = new DataTable();
            DataRow dr = null;
            //Add Columns to new Table
            for (int i = 0; i <= origTable.Rows.Count; i++)
            {
                newTable.Columns.Add(new DataColumn(origTable.Columns[i].ColumnName, typeof(String)));
            }

            //Execute the Pivot Method
            for (int cols = 0; cols < origTable.Columns.Count; cols++)
            {
                dr = newTable.NewRow();
                for (int rows = 0; rows < origTable.Rows.Count; rows++)
                {
                    if (rows < origTable.Columns.Count)
                    {
                        dr[0] = origTable.Columns[cols].ColumnName; // Add the Column Name in the first Column
                        dr[rows + 1] = origTable.Rows[rows][cols];
                    }
                }
                newTable.Rows.Add(dr); //add the DataRow to the new Table rows collection
            }
            return newTable;
        }*/ 
    





        public static DataTable ToPivotTable(DataTable sourceTable)
        {
            DataTable pivotTable = new DataTable();
            // First Column is the params column:
            pivotTable.Columns.Add("Parameter", typeof(string));
            // Next Columns are each line first item value
            foreach (DataRow row in sourceTable.Rows)
            {
                string columnName = row.ItemArray[0].ToString();
                string newColumnName = columnName;
                int id = 2;
                while (pivotTable.Columns.Contains(newColumnName))
                {
                    newColumnName = columnName + " [" + id + "]";
                    ++id;
                }
                pivotTable.Columns.Add(newColumnName, typeof(string));
            }
            // Fills empty rows - number of column - first column
            // The first column in the new table is the paramer - the name of the column
            for (int columnIndex = 1; columnIndex < sourceTable.Columns.Count; columnIndex++)
            {
                DataRow row = pivotTable.NewRow();
                row[0] = sourceTable.Columns[columnIndex].ColumnName;
                pivotTable.Rows.Add(row);
            }
            // Fills the pivot table by source values
            for (int sourceRowIndex = 0; sourceRowIndex < sourceTable.Rows.Count; sourceRowIndex++)
            {
                // Starts from 1 becuase the 0 column is the row headers in the pivot
                for (int sourceColumnIndex = 1; sourceColumnIndex < sourceTable.Columns.Count; sourceColumnIndex++)
                {
                    pivotTable.Rows[sourceColumnIndex - 1][sourceRowIndex + 1] =
                      sourceTable.Rows[sourceRowIndex].ItemArray[sourceColumnIndex];
                }
            }

            return pivotTable;
        }

    }
}
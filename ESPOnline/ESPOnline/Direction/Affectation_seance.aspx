﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Affectation_seance.aspx.cs" 

Inherits="ESPOnline.Direction.Affectation_seance" MasterPageFile="~/Direction/Site2.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    

        
  


    


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<br />

<br />
<center>
<h3>Affectation des séances des examens</h3>
<br />
<table width="300px"><tr><td width="200px"><asp:gridview ID="Gridview1" width="200px" runat="server" ShowFooter="true" AutoGenerateColumns="false">
            <Columns>

                         <asp:TemplateField HeaderText="Nº séance">
                <ItemTemplate>
                   <%-- <asp:BoundField DataField="RowNumber" HeaderText="Nº séance"  />--%>
                    <asp:Label ID="lb" runat="server" Text='<%# Eval("RowNumber")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <%-- <asp:TemplateField HeaderText="Année deb">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>--%>


            <%-- <asp:TemplateField HeaderText="Semestre">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>--%>


           <%--  <asp:TemplateField HeaderText="Période">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
--%>
            <asp:TemplateField HeaderText="Nº jours">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField HeaderText="Jours">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="Date">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>


             <asp:TemplateField HeaderText="Heure début">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>

             <asp:TemplateField HeaderText="Heure Fin">
                <ItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="Site">
                <ItemTemplate>
                     <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                </ItemTemplate>
                <FooterStyle HorizontalAlign="Right" />
                <FooterTemplate>
                 <asp:Button ID="ButtonAdd" runat="server" Text="Ajouter séance" 
                        onclick="ButtonAdd_Click" />
                </FooterTemplate>
            </asp:TemplateField>
            </Columns>
        </asp:gridview></td></tr></table>
 
</center>
<br />
<br />




<center>
<asp:Button  ID="btnvalid" runat="server" Text="Affecter" Height="40px" 
        Width="100px" onclick="btnvalid_Click"/>
</center>
<br />
<br />
<br />
</asp:Content>

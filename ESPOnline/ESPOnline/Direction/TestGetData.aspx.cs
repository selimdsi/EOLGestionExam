﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;
using BLL;

namespace ESPOnline.Direction
{
    public partial class TestGetData : System.Web.UI.Page
    {
        ServiceExamen server = new ServiceExamen();
        OracleCommand cmd;
        OracleDataAdapter da;
        DataSet ds;

        //string connectionstring;
        OracleConnection con;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //loaddata();

                //geidmapx.DataSource = server.getmap_x();
                //geidmapx.DataBind();
            }
        }

        private void loaddata()
        {
            con = new OracleConnection(AppConfiguration.ConnectionString);
            con.Open();
            try
            {

                cmd = new OracleCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "SPemp";
                cmd.Connection = con;

                var reader = cmd.ExecuteReader();
                Gridtoiec.DataSource = reader;
                Gridtoiec.DataBind();
                cmd.Dispose();
                con.Close();


            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }




        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using DAL;
using BLL;

namespace ESPOnline.EnseignantsCUP
{
    public partial class Entretiensession2 : System.Web.UI.Page
    {
        string idt;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }


            // Button3.Attributes.Add("onClick", "return confirmSubmit();");

            idt = Session["ID_ENS"].ToString().Trim();
            Label3.Text = idt;

            Label3.Visible = false;


        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (DropDownList1.SelectedItem != null)
                {

                    GridView3.DataSource = SqlDataSource3;
                    GridView3.DataBind();
                }
                else
                {
                    Response.Write("<script LANGUAGE='JavaScript' >alert('Il faut choisir un candidat')</script>");
                }
            }
            catch
            {

            }

        }
        protected void GridView3_test(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                foreach (TableCell cell in e.Row.Cells)
                {

                    cell.Attributes.CssStyle["text-align"] = "center";
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            TextBox1.Text = (Convert.ToDecimal(DropDownList2.SelectedValue) * 5 + Convert.ToDecimal(DropDownList5.SelectedValue) * 5 + Convert.ToDecimal(DropDownList6.SelectedValue) * 5 + Convert.ToDecimal(DropDownList7.SelectedValue) * 5).ToString();

        }

        protected void butsubmit_Click(object sender, EventArgs e)
        {
            try
            {

                //if (hide.Value.ToUpper() == "OUI")
                //{

                string id = DropDownList1.SelectedValue;


                ESP_TEMP_ETUDIANT_ENREG re = new ESP_TEMP_ETUDIANT_ENREG();
                //ESP_ re = ec.ESP_ETUDIANT_ENREG.First(p => p.ID_ET == id);
                re.ID_ET = id;
                re.SCORE_ENTRETIEN = Convert.ToDecimal(TextBox1.Text);
                re.ID_ENS_ENTRETIEN = Label3.Text;
                re.DATE_SAISIE = DateTime.Now;
                using (session2 ec = new session2())
                {
                    ec.ESP_TEMP_ETUDIANT_ENREG.AddObject(re);
                    ec.SaveChanges();
                    // Response.Write("<script LANGUAGE='JavaScript' >alert('enregistrement avec succé ')</script>");
                    GridView3.DataSource = null;
                    GridView3.DataBind();
                    DropDownList1.ClearSelection();
                    DropDownList2.ClearSelection(); DropDownList5.ClearSelection(); DropDownList2.ClearSelection(); DropDownList6.ClearSelection();
                    DropDownList7.ClearSelection(); TextBox1.Text = "";
                    DropDownList1.DataBind();

                }
            }
            //else
            //{
            //    hide.Value = "OUI";
            //}
            //}
            catch
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('Il faut choisir un candidat et lui donner une note ')</script>");
                GridView3.DataSource = null;
                GridView3.DataBind();
                DropDownList1.ClearSelection();
                DropDownList2.ClearSelection(); DropDownList5.ClearSelection(); DropDownList2.ClearSelection(); DropDownList6.ClearSelection();
                DropDownList7.ClearSelection(); TextBox1.Text = "";
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Etudiants
{
    public partial class Test_toeic_prep : System.Web.UI.Page
    {
        string id_et;
        ToiecService service = new ToiecService();
        string ID_ET;

        string veriflabeltoeic;
        string veriflabelprepTOEIC;
        string nbenregtoiec;
        string nbenregtpreptoiec;
        protected void Page_Load(object sender, EventArgs e)
        {
            plrep.Visible = false;

            if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

                lblanneedeb.Text = service.getANNEEDEBs();

                lblanneefin.Text = service.getAnneeFiN();

                id_et = Session["ID_ET"].ToString();
              
                nbenregtoiec = service.countNB_TOIEC();
                nbenregtpreptoiec = service.countNBPrep_TOIEC();

                if (!IsPostBack)
                { 
                    lblcountpreptoiec.Text=nbenregtpreptoiec;
                    lblcounttoiec.Text = nbenregtoiec;

                    int nbtoiec = Convert.ToInt32(nbenregtoiec);

                    int nbprep = Convert.ToInt32(nbenregtpreptoiec);

                    if (nbtoiec <= 10 && nbprep <= 10)
                    {
                        Panelfrang.Visible = true;
                    }

                    if (nbtoiec <= 10 || nbprep <= 10)
                    {
                        if (nbtoiec <= 10)
                        {
                            paneltoiec.Visible = true;
                            Panelfrang.Visible = true;
                        }
                        else
                        {
                            paneltoiec.Visible = false;
                            chkTOIEC.Enabled = false;
                            //Panelfrang.Visible = true;
                            panelmsg.Visible = true;
                            // Label2.Text = "le nombre est atteint 300 candidats au test toiec";
                            Response.Write(@"<script language='javascript'>alert('le nombre est atteint 300 candidats au test toiec,passer preparation');</script>");

                        }

                        if (nbprep <= 10)
                        {
                            panelprep.Visible = true;
                            Panelfrang.Visible = true;

                        }
                        else
                        {
                            //Panelfrang.Visible = true;
                            panelprep.Visible = false;
                            panelmsg.Visible = true;
                            //Label2.Text = "le nombre est atteint 300 candidats au test PREPARATION toiec";
                            Response.Write(@"<script language='javascript'>alert('le nombre est atteint 300 candidats au test preparation toiec,passer test toiec');</script>");

                        }

                    }






                    else
                    {
                        panelmsg.Visible = true;
                        Panelfrang.Visible = false;
                        Response.Write(@"<script language='javascript'>alert('Session fermée,le nombre est atteint 300 candidats dans les deux test toiec et preparation toiec');</script>");


                    }

                    //panelmsg.Visible = false;
                    //Response.Write(@"<script language='javascript'>alert('Session fermée,le nombre est atteint 300 candidats dans les deux test toiec et preparation toiec');</script>");

                }



        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            
            
            veriflabeltoeic = service.selectEtatTTOIEC(id_et);

            veriflabelprepTOEIC = service.selectEtatTPREPTOIEC(id_et);

            ID_ET = Session["ID_ET"].ToString();


                if (veriflabeltoeic == "O")
                {
                    lbltoiec.Text = "Vous êtes inscrit au test TOEIC";
                    plrep.Visible = true;

                }
                else
                {
                    lbltoiec.Text = "Vous n'êtes pas inscrit au test TOEIC";
                    plrep.Visible = true;

                }
                if (veriflabelprepTOEIC == "O")
                {
                    lblpreptoiec.Text = "Vous êtes inscrit au test PREP TOEIC";
                    plrep.Visible = true;

                }
                else
                {
                    lblpreptoiec.Text = "Vous n'êtes  pas inscrit au test PREP TOEIC";
                    plrep.Visible = true;

                }
               
        }


        protected void chkTOIEC_CheckedChanged(object sender, EventArgs e)
        {

            if (Convert.ToInt32(nbenregtoiec) <= 17)
            {
                if (chkTOIEC.Checked)
                {
                    service.UpdatETOIC(id_et);
                }
                else
                {
                    service.UpdatETOICToNo(id_et);
                }
            }
            else
            {
                chkTOIEC.Visible = false;
            }
           
        }

        protected void chkprepTOIEC_CheckedChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(nbenregtpreptoiec) <= 1)
            {
                if (chkprepTOIEC.Checked)
                {
                    service.UpdatEPREPTOIC(id_et);
                }
                else
                {
                    service.UpdatEPREPTOIC(id_et);
                }
            }
            else 
            {
                chkprepTOIEC.Visible = false;
            }
           
        }


        }
    
    
    }
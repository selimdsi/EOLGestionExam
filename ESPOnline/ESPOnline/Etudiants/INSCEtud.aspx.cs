﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using Oracle.ManagedDataAccess.Types;
using BLL;

namespace ESPOnline.Etudiants
{
    public partial class INSCEtud : System.Web.UI.Page
    {

        public string ID_ET;
        public string Code_Cl;
        List<string> classeall = new List<string> { "3A1", "3A10", "3A11", "3A12", "3A13", "3A14", "3A15", "3A16", "3A17", "3A18", "3A19", "3A2", "3A20", "3A21", "3A3", "3A4", "3A5", "3A6", "3A7", "3A8", "3A9", "3B1", "3B2", "3B3", "3B4", "3B5" };
        List<string> classeA = new List<string> { "3A1", "3A10", "3A11", "3A12", "3A13", "3A14", "3A15", "3A16", "3A17", "3A18", "3A19", "3A2", "3A20", "3A21", "3A3", "3A4", "3A5", "3A6", "3A7", "3A8", "3A9" };
        List<string> classeB = new List<string> { "3B1", "3B2", "3B3", "3B4", "3B5" };

        List<string> lisrempinfA = new List<string> { "GL", "ERP-BI", "SIM", "ArcTic" };
        List<string> lisremptelA = new List<string> { "ISEM", "IRT" };

        List<string> lisrempInfB = new List<string> { "GL" };
        List<string> lisremptelB = new List<string> { "ISEM", "IRT" };
        List<string> lischoix = new List<string> { };
        OrientationDAO orientdao = new OrientationDAO();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }
            ID_ET = Session["ID_ET"].ToString();
            Code_Cl = DAL.OrientationDAO.getlcodecl(ID_ET, "2013");

            LabCodeCl.Text = Code_Cl;
            BtnInfB.Enabled = false;
            BtnInfoA.Enabled = false;
            BtntelA.Enabled = false;
            BtnTelB.Enabled = false;
            BtnInfoAmodif.Enabled = false;
            BtntelAmodif.Enabled = false;
            BtnTelBmodif.Enabled = false;
            LinkButton13.Enabled = false;

            // GridViewAffOr.DataBind();

            if (!Page.IsPostBack)
            {
                if (!classeall.Contains(Code_Cl))
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert('Page Pour Les 3èmes A et B Informatique et Telecom')</script>");

                    DropDownListSpec.Enabled = false;
                    LabCodeCl.BackColor = System.Drawing.Color.Red;
                    LabCodeCl.Text = "Votre Classe est : " + LabCodeCl.Text + "  " + " Page Pour Les 3èmes A et B Informatique et Telecom";


                    //    Response.Redirect("~/Accueil.aspx");

                }

                else
                {
                    if (orientdao.veriforientation(ID_ET, "2013"))
                    {
                        Response.Write("<script LANGUAGE='JavaScript'> alert('Vous avez deja effectué Lorientation Vous pouvez Modifié votre Choix jusquau 18/04/2014  ')</script>");

                        #region bind infoA

                        lischoix.Clear();

                        DDChinfA1.DataSource = lisrempinfA.OrderBy(s => Guid.NewGuid());
                        DDChinfA1.DataBind();
                        lischoix.Add(DDChinfA1.SelectedValue);


                        DDChinfA2.DataSource = lisrempinfA.Except(lischoix).OrderBy(s => Guid.NewGuid());

                        DDChinfA2.DataBind();
                        lischoix.Add(DDChinfA2.SelectedValue);

                        DDChinfA3.DataSource = lisrempinfA.Except(lischoix).OrderBy(s => Guid.NewGuid());

                        DDChinfA3.DataBind();
                        lischoix.Add(DDChinfA3.SelectedValue);

                        DDChinfA4.DataSource = lisrempinfA.Except(lischoix).OrderBy(s => Guid.NewGuid());

                        DDChinfA4.DataBind();
                        lischoix.Add(DDChinfA4.SelectedValue);
                        #endregion

                        #region bind 3 tel a

                        lischoix.Clear();

                        DDChTelA1.DataSource = lisremptelA.OrderBy(s => Guid.NewGuid());
                        DDChTelA1.DataBind();
                        lischoix.Add(DDChTelA1.SelectedValue);

                        DDChTelA2.DataSource = lisremptelA.Except(lischoix).OrderBy(s => Guid.NewGuid());
                        DDChTelA2.DataBind();
                        lischoix.Add(DDChTelA2.SelectedValue);

                        #endregion

                        #region bind 3 inf B

                        lischoix.Clear();

                        DDChinfB1.DataSource = lisrempInfB.OrderBy(s => Guid.NewGuid());
                        DDChinfB1.DataBind();
                        lischoix.Add(DDChinfB1.SelectedValue);


                        #endregion

                        #region bind 3 tel B

                        lischoix.Clear();

                        DDChTelB1.DataSource = lisremptelB.OrderBy(s => Guid.NewGuid());
                        DDChTelB1.DataBind();
                        lischoix.Add(DDChTelB1.SelectedValue);

                        DDChTelB2.DataSource = lisremptelB.Except(lischoix).OrderBy(s => Guid.NewGuid());
                        DDChTelB2.DataBind();
                        lischoix.Add(DDChTelB2.SelectedValue);

                        #endregion

                        //BtnInfB.Enabled = false;
                        //BtnInfoA.Enabled = false;
                        //BtntelA.Enabled = false;
                        //BtnTelB.Enabled = false;







                    }
                    else
                        #region bind infoA

                        lischoix.Clear();

                    DDChinfA1.DataSource = lisrempinfA.OrderBy(s => Guid.NewGuid());
                    DDChinfA1.DataBind();
                    lischoix.Add(DDChinfA1.SelectedValue);


                    DDChinfA2.DataSource = lisrempinfA.Except(lischoix).OrderBy(s => Guid.NewGuid());

                    DDChinfA2.DataBind();
                    lischoix.Add(DDChinfA2.SelectedValue);

                    DDChinfA3.DataSource = lisrempinfA.Except(lischoix).OrderBy(s => Guid.NewGuid());

                    DDChinfA3.DataBind();
                    lischoix.Add(DDChinfA3.SelectedValue);

                    DDChinfA4.DataSource = lisrempinfA.Except(lischoix).OrderBy(s => Guid.NewGuid());

                    DDChinfA4.DataBind();
                    lischoix.Add(DDChinfA4.SelectedValue);
                        #endregion

                    #region bind 3 tel a

                    lischoix.Clear();

                    DDChTelA1.DataSource = lisremptelA.OrderBy(s => Guid.NewGuid());
                    DDChTelA1.DataBind();
                    lischoix.Add(DDChTelA1.SelectedValue);

                    DDChTelA2.DataSource = lisremptelA.Except(lischoix).OrderBy(s => Guid.NewGuid());
                    DDChTelA2.DataBind();
                    lischoix.Add(DDChTelA2.SelectedValue);

                    #endregion

                    #region bind 3 inf B

                    lischoix.Clear();

                    DDChinfB1.DataSource = lisrempInfB.OrderBy(s => Guid.NewGuid());
                    DDChinfB1.DataBind();
                    lischoix.Add(DDChinfB1.SelectedValue);


                    #endregion

                    #region bind 3 tel B

                    lischoix.Clear();

                    DDChTelB1.DataSource = lisremptelB.OrderBy(s => Guid.NewGuid());
                    DDChTelB1.DataBind();
                    lischoix.Add(DDChTelB1.SelectedValue);

                    DDChTelB2.DataSource = lisremptelB.Except(lischoix).OrderBy(s => Guid.NewGuid());
                    DDChTelB2.DataBind();
                    lischoix.Add(DDChTelB2.SelectedValue);

                    #endregion
                }




            }
        }





        protected void DropDownListSpec_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListSpec.SelectedValue == "TELECOM")
            {
                if (classeA.Contains(Code_Cl))
                {
                    PanelBTEL.Visible = false;
                    panelATELECOM.Visible = true;
                    P3Ainfo.Visible = false;
                    PanelBinfo.Visible = false;
                }
                else if (classeB.Contains(Code_Cl))
                {
                    panelATELECOM.Visible = false;
                    PanelBTEL.Visible = true;



                    P3Ainfo.Visible = false;
                    PanelBinfo.Visible = false;


                }

            }
            else if (DropDownListSpec.SelectedValue == "INFORMATIQUE")
            {
                if (classeA.Contains(Code_Cl))
                {
                    PanelBinfo.Visible = false;
                    P3Ainfo.Visible = true;

                    PanelBTEL.Visible = false;
                    panelATELECOM.Visible = false;


                }
                else if (classeB.Contains(Code_Cl))
                {
                    P3Ainfo.Visible = false;
                    PanelBinfo.Visible = true;

                    PanelBTEL.Visible = false;
                    panelATELECOM.Visible = false;


                }
            }
        }

        protected void DDChinfA1_SelectedIndexChanged(object sender, EventArgs e)
        {

            lischoix.Clear();

            lischoix.Add(DDChinfA1.SelectedValue);

            DDChinfA2.DataSource = lisrempinfA.Except(lischoix);
            DDChinfA2.DataBind();

            lischoix.Add(DDChinfA2.SelectedValue);

            DDChinfA3.DataSource = lisrempinfA.Except(lischoix);

            DDChinfA3.DataBind();

            lischoix.Add(DDChinfA3.SelectedValue);



            DDChinfA4.DataSource = lisrempinfA.Except(lischoix);

            DDChinfA4.DataBind();
            lischoix.Add(DDChinfA4.SelectedValue);
        }

        protected void DDChinfA2_SelectedIndexChanged(object sender, EventArgs e)
        {
            lischoix.Clear();

            lischoix.Add(DDChinfA1.SelectedValue);



            lischoix.Add(DDChinfA2.SelectedValue);

            DDChinfA3.DataSource = lisrempinfA.Except(lischoix);

            DDChinfA3.DataBind();

            lischoix.Add(DDChinfA3.SelectedValue);



            DDChinfA4.DataSource = lisrempinfA.Except(lischoix);

            DDChinfA4.DataBind();
            lischoix.Add(DDChinfA4.SelectedValue);
        }

        protected void DDChinfA3_SelectedIndexChanged(object sender, EventArgs e)
        {

            lischoix.Clear();

            lischoix.Add(DDChinfA1.SelectedValue);



            lischoix.Add(DDChinfA2.SelectedValue);



            lischoix.Add(DDChinfA3.SelectedValue);



            DDChinfA4.DataSource = lisrempinfA.Except(lischoix);

            DDChinfA4.DataBind();
            lischoix.Add(DDChinfA4.SelectedValue);

        }

        protected void DDChinfA4_SelectedIndexChanged(object sender, EventArgs e)
        {
            lischoix.Clear();

            lischoix.Add(DDChinfA1.SelectedValue);



            lischoix.Add(DDChinfA2.SelectedValue);



            lischoix.Add(DDChinfA3.SelectedValue);




            lischoix.Add(DDChinfA4.SelectedValue);
        }

        protected void DDChTelA1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lischoix.Clear();

            lischoix.Add(DDChTelA1.SelectedValue);

            DDChTelA2.DataSource = lisremptelA.Except(lischoix);
            DDChTelA2.DataBind();

            lischoix.Add(DDChTelA2.SelectedValue);



        }

        protected void DDChTelA2_SelectedIndexChanged(object sender, EventArgs e)
        {

            lischoix.Clear();

            lischoix.Add(DDChTelA1.SelectedValue);



            lischoix.Add(DDChTelA2.SelectedValue);
        }

        protected void DDChTelB1_SelectedIndexChanged(object sender, EventArgs e)
        {
            lischoix.Clear();

            lischoix.Add(DDChTelB1.SelectedValue);

            DDChTelB2.DataSource = lisremptelB.Except(lischoix);
            DDChTelB2.DataBind();

            lischoix.Add(DDChTelB2.SelectedValue);

        }

        protected void LinkButton11_Click(object sender, EventArgs e)
        {
            //OrientationDAO erionDAO = new OrientationDAO();
            //ESP_ORIENTATION esporient = new ESP_ORIENTATION();

            //esporient.ANNEE_DEB = "2013";
            //esporient.CODE_CL = Code_Cl;
            //esporient.ID_ET = ID_ET;
            //esporient.SPECIALITE = DropDownListSpec.SelectedValue;
            //esporient.CH1 = DDChinfA1.SelectedValue;
            //esporient.CH2 = DDChinfA2.SelectedValue;
            //esporient.CH3 = DDChinfA3.SelectedValue;
            //esporient.CH4 = DDChinfA4.SelectedValue;

            //          if ( erionDAO.veriforientation(ID_ET, "2013"))
            //          {
            //              Response.Write("<script LANGUAGE='JavaScript'> alert('Vous avez deja   ')</script>");
            //          }
            //          else
            //          {
            //              if (erionDAO.addorient(esporient))
            //              {
            //                  Response.Write("<script LANGUAGE='JavaScript'> alert('ok   ')</script>");
            //              }
            //              else
            //              {
            //                  Response.Write("<script LANGUAGE='JavaScript'> alert('no  ')</script>");
            //              }
            //          }
            ESP_ORIENTATION re = new ESP_ORIENTATION();
            re.ANNEE_DEB = "2013";
            re.CH1 = DDChinfA1.SelectedValue;
            re.CH2 = DDChinfA2.SelectedValue;
            re.CH3 = DDChinfA3.SelectedValue;
            re.CH4 = DDChinfA4.SelectedValue;
            re.DATE_SAISIE = OracleDate.GetSysDate().Value;
            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;



            //BLL.orient ajt = new orient();
            OrientationDAO orientdao = new OrientationDAO();


            if (orientdao.addorient(re))
            {

                Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

            }

            else
            {
                Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
            }




        }

        protected void LinkButton12_Click(object sender, EventArgs e)
        {
            ESP_ORIENTATION re = new ESP_ORIENTATION();
            re.ANNEE_DEB = "2013";
            re.CH1 = DDChTelA1.SelectedValue;
            re.CH2 = DDChTelA2.SelectedValue;
            re.CH3 = "";
            re.CH4 = "";
            re.DATE_SAISIE = OracleDate.GetSysDate().Value;

            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;






            //BLL.orient ajt = new orient();
            OrientationDAO orientdao = new OrientationDAO();


            if (orientdao.addorient(re))
            {

                Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

            }

            else
            {
                Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
            }

        }

        protected void LinkButton13_Click(object sender, EventArgs e)
        {
            ESP_ORIENTATION re = new ESP_ORIENTATION();
            re.ANNEE_DEB = "2013";
            re.CH1 = DDChTelB1.SelectedValue;
            re.CH2 = DDChTelB2.SelectedValue;
            re.CH3 = "";
            re.CH4 = "";
            re.DATE_SAISIE = OracleDate.GetSysDate().Value;
            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;
            ;



            OrientationDAO orientdao = new OrientationDAO();


            if (orientdao.addorient(re))
            {

                Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

            }

            else
            {
                Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
            }

        }

        protected void LinkButton14_Click(object sender, EventArgs e)
        {
            ESP_ORIENTATION re = new ESP_ORIENTATION();
            re.ANNEE_DEB = "2013";
            re.CH1 = DDChinfB1.SelectedValue;
            re.CH2 = "";
            re.CH3 = "";
            re.CH4 = "";
             re.DATE_SAISIE = OracleDate.GetSysDate().Value;
            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;
            ;



            OrientationDAO orientdao = new OrientationDAO();


            if (orientdao.addorient(re))
            {

                Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

            }

            else
            {
                Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
            }

        }

        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                // this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked YES!')", true);


                ESP_ORIENTATION re = new ESP_ORIENTATION();
                re.ANNEE_DEB = "2013";
                re.CH1 = DDChinfA1.SelectedValue;
                re.CH2 = DDChinfA2.SelectedValue;
                re.CH3 = DDChinfA3.SelectedValue;
                re.CH4 = DDChinfA4.SelectedValue;
                re.DATE_SAISIE = OracleDate.GetSysDate().Value;
                re.SPECIALITE = DropDownListSpec.SelectedValue;
                re.CODE_CL = Code_Cl;

                re.ID_ET = ID_ET;



                //BLL.orient ajt = new orient();
                //OrientationDAO orientdao = new OrientationDAO();

                if (orientdao.deleteorient(ID_ET, "2013"))
                {


                    if (orientdao.addorient(re))
                    {

                        Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

                    }

                    else
                    {
                        Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
                    }
                }
                else
                {

                }



                // OrientationDAO orientdao = new OrientationDAO();

                DataList1.DataBind();
            }






        }

        protected void LinkButtonModif_Click(object sender, EventArgs e)
        {
            ESP_ORIENTATION re = new ESP_ORIENTATION();

            re.ANNEE_DEB = "2013";
            re.CH1 = DDChTelA1.SelectedValue;
            re.CH2 = DDChTelA2.SelectedValue;
            re.CH3 = "";
            re.CH4 = "";
            re.DATE_SAISIE = OracleDate.GetSysDate().Value;

            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;

            if (orientdao.deleteorient(ID_ET, "2013"))
                {


                    if (orientdao.addorient(re))
                    {

                        Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

                    }

                    else
                    {
                        Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
                    }
                }
                else
                {

                }

            DataList1.DataBind();

                // OrientationDAO orientdao = new OrientationDAO();


            }

        protected void LinkButtonmodifBtel_Click(object sender, EventArgs e)
        {

            ESP_ORIENTATION re = new ESP_ORIENTATION();

            re.ANNEE_DEB = "2013";
            re.CH1 = DDChTelB1.SelectedValue;
            re.CH2 = DDChTelB2.SelectedValue;
            re.CH3 = "";
            re.CH4 = "";
            re.DATE_SAISIE = OracleDate.GetSysDate().Value;

            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;

            if (orientdao.deleteorient(ID_ET, "2013"))
            {


                if (orientdao.addorient(re))
                {

                    Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

                }

                else
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
                }
            }
            else
            {

            }

            DataList1.DataBind();

            // OrientationDAO orientdao = new OrientationDAO();

        }

        protected void LinkButtonupdatinfoB_Click(object sender, EventArgs e)
        {
            ESP_ORIENTATION re = new ESP_ORIENTATION();

            re.ANNEE_DEB = "2013";
            re.CH1 = DDChinfB1.SelectedValue;
            re.CH2 = "";
            re.CH3 = "";
            re.CH4 = "";
            re.DATE_SAISIE = OracleDate.GetSysDate().Value;

            re.SPECIALITE = DropDownListSpec.SelectedValue;
            re.CODE_CL = Code_Cl;

            re.ID_ET = ID_ET;

            if (orientdao.deleteorient(ID_ET, "2013"))
            {


                if (orientdao.addorient(re))
                {

                    Response.Write("<script LANGUAGE='JavaScript'> alert('Choix Envoyer avec succés')</script>");

                }

                else
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert(' Envoie echoué')</script>");
                }
            }
            else
            {

            }

            DataList1.DataBind();
        }







        }
    }

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
namespace ESPOnline.Etudiants
{
    public partial class TestFrAng : System.Web.UI.Page
    {
        ToiecService service = new ToiecService();
        string id_etud;
        string veriflabeltoeic;
        string veriflabelprepTOEIC;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }
            id_etud = Session["ID_ET"].ToString();
            veriflabeltoeic = service.selectEtatTTOIEC(id_etud);
            veriflabelprepTOEIC = service.selectEtatTPREPTOIEC(id_etud);

            if (!Page.IsPostBack)
            {

                HttpContext.Current.Response.AddHeader("p3p", "CP=\"CAO PSA OUR\"");
                if (Session["ID_ET"] == null)
                {
                    Response.Redirect("~/Online/default.aspx");
                }
                else
                {
                    //Gridbord.Visible = false;

                }

                string nivetudiantang = service.selectniVeauEDTANG(id_etud);
                string nivetudiantfr = service.selectniVeauEDTFR(id_etud);

                if (nivetudiantang == "B2" && nivetudiantfr == "B2")
                {
                    Response.Write(@"<script language='javascript'>alert('Votre niveau en français est B2 ,qu\'en anglais ,passer un test TOIEC OU PREP TOEIC');</script>");
                    Response.Redirect("~/Etudiants/Test_toeic_prep.aspx");


                }


                else
                {
                    if (nivetudiantfr == "B2")
                    {
                        Response.Write(@"<script language='javascript'>alert('Votre niveau en français est B2,vous n\'avez le droit  de passer que l\'anglais, TOIEC OU PREP TOEIC');</script>");
                
                        Panelfrang.Visible = false;
                        Panelfr.Visible = false;
                    }
                    else
                    {
                        if (nivetudiantang == "B2")
                        {
                            Response.Write(@"<script language='javascript'>alert(' Votre niveau en anglais est B2,vous n\'avez le droit  de passer que le Français, TOIEC OU PREP TOEIC');</script>");

                            Panelfrang.Visible = false;
                            Panelanglais.Visible = false;
                            linkbutonfrang.Visible = false;
                            LinkButtonfr.Visible = false;
                        }

                        else
                        {
                            if (nivetudiantang != "B2" && nivetudiantfr != "B2")
                            {
                                Response.Write(@"<script language='javascript'>alert('Votre niveau est moins B2 dans les deux matieres');</script>");


                                Panelfr.Visible = false;
                                Panelanglais.Visible = false;
                                linkbutonfrang.Visible = false;
                                LinkButtonfr.Visible = false;
                            }
                        }
                    }
                }

            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;

namespace ESPOnline.Etudiants
{
 
    public partial class Toeic_etudiant : System.Web.UI.Page
    {
        ToiecService service = new ToiecService();
       

        string ID_ET;

        string veriflabeltoeic;
        string veriflabelprepTOEIC;
        string id_et;
        string nbenregtoiec;
        string nbenregtpreptoiec;
        string typechoix;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

            id_et = Session["ID_ET"].ToString();
            lblanneedeb.Text = service.getANNEEDEBs();

            lblanneefin.Text = service.getAnneeFiN();
            plrst.Visible = false;


            nbenregtoiec = service.countNB_TOIEC();
            nbenregtpreptoiec = service.countNBPrep_TOIEC();
            lblcounttoiec.Text = nbenregtoiec;
            lblcountpreptoiec.Text = nbenregtpreptoiec;


            string id_etud = Session["ID_ET"].ToString();
            string codecl = service.returnCLSUPP(id_etud);

            //bool exist=service.verifexistet();
            DataTable dt;
            dt = service.Aff_list_inscrit(id_etud);
            //string veriflabelprepTOEIC = service.selectEtatTPREPTOIEC(id_etud);
            string nivetudiantang = service.selectniVeauEDTANG(id_etud);
            string nivetudiantfr = service.selectniVeauEDTFR(id_etud);

            if (dt.Rows.Count != 0)
            {
                Response.Write(@"<script language='javascript'>alert('Vous êtes déjà inscrit,mais tu peux modifier votre choix');</script>");
                Response.Redirect("~/Etudiants/Toeic_modif.aspx");

            }

            else
            {
                if (codecl.StartsWith("5") || codecl.StartsWith("4"))
                {
                    if (nivetudiantang == "A1")
                    {

                        Response.Write(@"<script language='javascript'>alert('Votre niveau d\'anglais est A1');</script>");
                    }
                    else
                    {
                        int nbtoiec = Convert.ToInt32(nbenregtoiec);

                        int nbprep = Convert.ToInt32(nbenregtpreptoiec);


                        if (nbtoiec < 10 && nbprep < 10)
                        {
                            ddlchoix.Visible = true;
                        }
                        else
                        {
                            if (nbtoiec < 10 || nbprep < 10)
                            {
                                if (nbtoiec < 10)
                                {
                                    Response.Redirect("~/Etudiants/Inscrit_TOEIC.aspx");

                                }
                                else
                                {

                                    Response.Write(@"<script language='javascript'>alert('le nombre est atteint 300 candidats au certification toiec,passer preparation');</script>");

                                }
                                if (nbprep < 10)
                                {
                                    Response.Redirect("~/Etudiants/Inscrit_TOEIC_PREP.aspx");

                                }
                                else
                                {
                                    //Panelfrang.Visible = true;
                                    panelprep.Visible = false;
                                    //panelmsg.Visible = true;
                                    lblprep.Visible = false;
                                    //Label2.Text = "le nombre est atteint 300 candidats au test PREPARATION toiec";
                                    Response.Write(@"<script language='javascript'>alert('le nombre est atteint 300 candidats au  prep certification toiec,passer certification toiec');</script>");

                                }
                            }

                            else
                            {

                                Panelfrang.Visible = false;
                                Response.Write(@"<script language='javascript'>alert('Session fermée,le nombre est atteint 300 candidats dans les deux certification toiec et preparation toiec');</script>");


                            

                        }


                    }


 
                }
               
 
            }
              else
                {
                    Response.Write(@"<script language='javascript'>alert('Sauf les classes de 5 ème et de 4 ème année ont le droit de passer le test');</script>");

                }

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                service.Enreg_etud_toeic(id_et, ddlchoix.SelectedValue);

                Response.Write(@"<script language='javascript'>alert('Vous êtes enregistré avec succée');</script>");

                typechoix = service.selectEtatTTOIEC(id_et);

                if (typechoix == "1")
                {
                    lbltpd.Text = "Vous êtes inscrit au certification toeic,Bonne chance";
                    plrst.Visible = true;
                    Button1.Visible = false;
                    ddlchoix.Visible = false;
                    paneltoiec.Visible = false;
                    panelprep.Visible = false;
                    lblprep.Visible = false;
                    //lbltoiec.Visible = false;
                    lblchoix.Visible = false;
                }
                else
                    if (typechoix == "2")
                    {
                        lbltpd.Text = "Vous êtes inscrit au prep toeic,Bonne chance";
                        plrst.Visible = true;
                        Button1.Visible = false;
                        ddlchoix.Visible = false; paneltoiec.Visible = false;
                        panelprep.Visible = false;
                        lblprep.Visible = false;
                        //lbltoiec.Visible = false;
                        lblchoix.Visible = false;
                    }
                    else
                        if (typechoix == "3")
                        {
                            lbltpd.Text = "Vous êtes inscritdans les deux certifications,Bonne chance";
                            plrst.Visible = true;
                            Button1.Visible = false;
                            ddlchoix.Visible = false;
                            paneltoiec.Visible = false;
                            panelprep.Visible = false;
                            lblprep.Visible = false;
                            //lbltoiec.Visible = false;
                            lblchoix.Visible = false;
                        }



            }
            catch (Exception ex)
            {
                Response.Write(@"<script language='javascript'>alert('Vous n\'avez pas le droit de modifier');</script>");
                Panelfrang.Visible = false;
               

            }
           
            }


           
        }

       
    }

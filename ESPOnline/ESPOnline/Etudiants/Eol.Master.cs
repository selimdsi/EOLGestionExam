﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DAL;
using BLL;
using System.Data;

namespace ESPOnline
{
    public partial class Eol : System.Web.UI.MasterPage
    {
        string id_etud;
        string NOM_ET;
        string PRENOM_ET;
        string NUM_CIN_PASSEPORT;
        string ADRESSE_MAIL_ESP;
        ToiecService service = new ToiecService();
        protected void Page_Load(object sender, EventArgs e)
        {
            EncadDAO dt = EncadDAO.Instance;
            id_etud = Session["ID_ET"].ToString();
            NOM_ET = Session["NOM_ET"].ToString();
            PRENOM_ET = Session["PNOM_ET"].ToString();
            NUM_CIN_PASSEPORT = Session["CIN_PASS"].ToString();
            A1.Visible = false;
            //if ((dt.getClasse(id_etud).StartsWith("5")))
            //{
            //    test3.Visible = false;
            //    A1.Visible = true;
            //}
            try
            {
                ADRESSE_MAIL_ESP = Session["ADRESSE_MAIL_ESP"].ToString();
            }
            catch { Response.Write("<script LANGUAGE='JavaScript'> alert('Vous devez avoir une adresse mail ESPRIT  ')</script>"); }
           
            Label2.Text = PRENOM_ET + " " + NOM_ET;
            string nivetudiantang = service.selectniVeauEDTANG(id_etud);
        }
        protected void testBtn_Click(object sender, EventArgs e)
        {
          //  id_etud = Session["ID_ET"].ToString();
            string codecl = service.returnCLSUPP(id_etud);
            string nivetudiantang = service.selectniVeauEDTANG(id_etud);

            string nivetudiantfr = service.selectniVeauEDTFR(id_etud);
            if (codecl.StartsWith("5"))
            {
                if (nivetudiantang == "" || nivetudiantfr == "")
                {
                    Response.Write(@"<script language='javascript'>alert('Veuillez contacter l'administration');</script>");

                }

                else
                {
                    if (nivetudiantang == "B2" && nivetudiantfr == "B2")
                    {
                        Response.Write(@"<script language='javascript'>alert('Votre niveau est B2 Vous navez pas le droit de passer le test,si vous voulez passer un test TOIEC OU PREP TOEIC');</script>");
                        Response.Redirect("~/Etudiants/TestFrAng.aspx");
                    }

                   // Response.Redirect("~/Etudiants/Inscrip_Test_Langue.aspx");
                    else
                    {
                        if (nivetudiantfr == "B2")
                        {
                            Response.Write(@"<script language='javascript'>alert('Votre niveau en français est B2,vous n\'avez le droit  de passer que l\'anglais, TOIEC OU PREP TOEIC');</script>");
                            Response.Redirect("~/Etudiants/TestFrAng.aspx");
                            
                        }
                        else
                        {
                            if (nivetudiantang == "B2")
                            {
                                Response.Write(@"<script language='javascript'>alert(' Votre niveau en anglais est B2,vous n\'avez le droit  de passer que le Français, TOIEC OU PREP TOEIC');</script>");

                                Response.Redirect("~/Etudiants/TestFrAng.aspx");
                            }

                            else
                            {
                                if (nivetudiantang != "B2" && nivetudiantfr != "B2")
                                {
                                    Response.Write(@"<script language='javascript'>alert('Votre niveau est moins B2 dans les deux matieres');</script>");
                                    Response.Redirect("~/Etudiants/TestFrAng.aspx");
                                }
                            }
                        }
                       

                    }
                }
            }

            else
            {
                Response.Write("<script LANGUAGE='JavaScript'> alert('Sauf les classes de 5 emme années ont invite de faire le test')</script>");


            }

        }


        protected void clickbtn_Click(object sender, EventArgs e)
        
        
    {

        string id_etud = Session["ID_ET"].ToString();
        string codecl = service.returnCLSUPP(id_etud);

        //bool exist=service.verifexistet();
        DataTable dt;
        dt = service.Aff_list_inscrit(id_etud);
        //string veriflabelprepTOEIC = service.selectEtatTPREPTOIEC(id_etud);
        string nivetudiantang = service.selectniVeauEDTANG(id_etud);
        string nivetudiantfr = service.selectniVeauEDTFR(id_etud);
        if (dt.Rows.Count != 0)
        {
            Response.Write(@"<script language='javascript'>alert('Vous êtes déjà inscrit,mais tu peux modifier votre choix');</script>");
            Response.Redirect("~/Etudiants/Toeic_modif.aspx");

        }
        else
        {
            if (codecl.StartsWith("5") || codecl.StartsWith("4"))
            {
                if (nivetudiantang == "A1")
                {

                    Response.Write(@"<script language='javascript'>alert('Votre niveau d\'anglais est A1');</script>");
                }



                else
                {
                    Response.Redirect("~/Etudiants/Toeic_etudiant.aspx");
                }



            }

            else
            {
                Response.Write(@"<script language='javascript'>alert('Sauf les classes de 5 ème et de 4 ème année ont le droit de passer le test');</script>");

            }

        }

       
    }
        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Session.Clear();
            Response.Redirect("~/Online/Accueil.aspx");
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Consult_Niv_2015.aspx.cs" 
Inherits="ESPOnline.Etudiants.Consult_Niv_2015" 
 MasterPageFile="~/Etudiants/Eol.Master" %>

  <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
      <link rel="stylesheet" type="text/css" href="../Styles/style.css" />
      <style type="text/css">
          .style1
              color: #FFFFFF;
          }
          .style2
          {
              color: #FF0000;
          }
      </style>


      <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

    
          .style4
          {
              color: #CC0000;
          }
           .grid td, .grid th{
  text-align:center; 

      
          </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <br />
    <br />
    <br />
<center>
<h3 class="style4">Consultation niveaux de langue pour l'année universitaire 2015/2016</h3>
<br />
<asp:GridView runat="server" ID="GridView1" AutoGenerateColumns="False" 
                                                          
                                                         Style="border-bottom: white 2px ridge; border-left: white 2px ridge;
                                                        background-color: white; border-top: white 2px ridge; border-right: white 2px ridge;"
                                                        BorderWidth="0px" 
        BorderColor="Red" CellSpacing="1" CellPadding="3" CssClass="grid"
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="Both" 
        EmptyDataRowStyle-CssClass="ItemStyle"  BackColor="#0099CC" Height="70px" Width="500px"
                                                        >
                                                        <EmptyDataTemplate>
                                                            Pas d'enregistrement.
                                                        </EmptyDataTemplate>
                                                        
                                                        <HeaderStyle HorizontalAlign="Center" Height="20px" Width="150px" BackColor="Red" />
                                                        <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                                                        <FooterStyle CssClass="ItemStyle" />
                                                        <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                                                        <RowStyle CssClass="GridItemStyle" />
                                                        <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        <Columns>

                   <asp:TemplateField HeaderText="IDENTIFIANT" ItemStyle-HorizontalAlign="Center"  HeaderStyle-Width="150px">
        <ItemTemplate>
            <asp:Label ID="lblPrice1" runat="server" Text='<%# Eval("ID_ET")%>' />
         </ItemTemplate>
         <FooterTemplate>
            <asp:Label ID="lblTotalPrice1" runat="server" />
         </FooterTemplate>                   
      </asp:TemplateField>


                  <asp:BoundField DataField="NOM" HeaderText="NOM ET PRENOM" ReadOnly="True" 
                SortExpression="NOM" HeaderStyle-Width="500px" ItemStyle-HorizontalAlign="Center"/> 
               
                <asp:BoundField DataField="niveau_courant_fr" HeaderText="NIVEAU FRANCAIS" ReadOnly="True" 
                SortExpression="niveau_courant_fr"  ItemStyle-Font-Bold="true" HeaderStyle-Width="500px" ItemStyle-HorizontalAlign="Center"
                /> 

                 <asp:BoundField DataField="niveau_courant_ang" HeaderText="NIVEAU ANGLAIS" ReadOnly="True" 
                SortExpression="niveau_courant_ang" ItemStyle-Font-Bold="true" HeaderStyle-Width="500px" ItemStyle-HorizontalAlign="Center"/> 

              

                     </Columns>
                     
                                                        <HeaderStyle CssClass="GridHeaderStyle" />
                                                        <RowStyle ForeColor="#000000"  />
                                                        <SelectedRowStyle CssClass="GridSelectedStyle" />
                                                        
                                                    </asp:GridView>
                                                    <br />
                                                    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Etudiants/Accueil.aspx"  runat="server" ForeColor="#0033CC" > Retour à la page d'Accueil</asp:HyperLink>

                                                    <br />
                                                    </center>
</asp:Content>
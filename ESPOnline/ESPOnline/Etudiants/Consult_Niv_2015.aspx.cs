﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Etudiants
{
    public partial class Consult_Niv_2015 : System.Web.UI.Page
    {
        LangueService service = new LangueService();
        string id_et;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
                {
                    Response.Redirect("~/Online/default.aspx");
                }
                else
                {
                    id_et = Session["ID_ET"].ToString();
                    GridView1.DataSource = service.Consult_niv_ang(id_et);
                    GridView1.DataBind();
                }
            }
        }

       
    }
}
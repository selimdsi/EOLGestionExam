﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestFrAng.aspx.cs" Inherits="ESPOnline.Etudiants.TestFrAng" 
MasterPageFile="~/Etudiants/Eol.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>

      <style type="text/css">
          .style1
          {
              color: #800000;
              font-size: medium;
          }
          </style>


      <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: white;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}

      
          .style6
          {
              width: 372px;
          }

      
          .style7
          {
              width: 101px;
          }
          .style9
          {
              width: 163px;
          }
          .style10
          {
              width: 211px;
          }

      
          .style11
          {
              color: #FF0000;
          }

      
          .style12
          {
              font-size: medium;
          }
          .style13
          {
              color: #000000;
              font-size: medium;
          }

      
          .style14
          {
              font-size: x-large;
          }

      
          .style15
          {
              color: #FF0000;
              font-size: medium;
          }

      
          </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server" >
    <br />
<br />
<br />
<br />
<center>
<asp:Panel ID="Panelfrang" runat="server"  BackColor="#BABABA" Height="600px" Width="1000px">
<center>
<table><tr><td class="style13"><h1 class="style1"><span class="style14">INSCRIPTION 
    AU TEST DU LANGUE POUR L&#39;ANNEE UNIVERSITAIRE:
    <asp:Label ID="lblanneedeb" runat="server" CssClass="style7"></asp:Label>
    /<asp:Label ID="lblanneefin" runat="server" CssClass="style7"></asp:Label>
    </span></h1></td></tr>
            
       <tr><td class="style13">
       
        <h3 class="style12" >
            <span class="style11">*</span><span class="text-info"> Veuillez choisir votre 
            date de passage de l&#39;examen d&#39;évaluation de test de niveau (Choix entre trois 
            dates uniques).</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> Après validation, vos 
            choix ne seront plus modifiables.</span><br 
                class="text-info" />
            <span class="style11">* </span><span class="text-info">Le nombre d&#39;inscription 
            ne doit pas dépasser 150 personnes par date.</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> les étudiants auront 
            droit à une seule inscription par épreuve, les absences ne seront pas donc 
            tolérées.</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> les étudiants auront un 
            niveau B2 seront invités à passer le test TOIEC ou bien PREP TOIEC.</span><br 
                class="text-info" />
            </h3>
       
       </td></tr>     
            
            </table>
            </center>
            <br />
   
<asp:Panel ID="plTestfr" runat="server"  BackColor="#CC0000" Height="300px" Width="800px">

<br />
<br /><br />
<center>
            <table style="background-color: #999999; width: 701px;" Height="200px">
            <tr><td class="style6" colspan="3">Veuillez choisir la date correspondante pour passer le test du Français ,de même l'anglais</td></tr>
            <tr>
             <td class="style9" >
                 <asp:Label ID="lblfr" runat="server" Text="Test Français" CssClass="text-info"></asp:Label>
             </td>
             <td>
                 <asp:DropDownList ID="ddltestfr" runat="server" AppendDataBoundItems="true" 
                     AutoPostBack="true" dataTextFormatString="{0:dd/MM/yyyy}" 
                     >
                     <asp:ListItem>--Choisir la date du test Français--</asp:ListItem>
                     <asp:ListItem>11/05/2015</asp:ListItem>
                     <asp:ListItem>12/05/2015</asp:ListItem>
                     <asp:ListItem>13/05/2015</asp:ListItem>
                 </asp:DropDownList>
             </td>
             <td class="style10"> <asp:Label ID="Label6" runat="server" Text="Nombre d'Inscription :" 
                     CssClass="text-info"></asp:Label>
                                    <asp:Label ID="lblcountfr" runat="server" ForeColor="Red" 
                     CssClass="text-info"></asp:Label><asp:Label ID="lbl" runat="server" 
                     Text=":étudiant(e)s" CssClass="text-info"></asp:Label></td>
         </tr>

         <tr>
             <td class="style9" >
                 <asp:Label ID="lblang" runat="server" Text="Test Anglais" CssClass="text-info"></asp:Label>
             </td>
             <td>
                 <asp:DropDownList ID="ddlang" runat="server" AppendDataBoundItems="true" 
                     AutoPostBack="true" dataTextFormatString="{0:dd/MM/yyyy}" 
                     >
                     <asp:ListItem>--Choisir la date du test Anglais--</asp:ListItem>
                      <asp:ListItem>11/05/2015</asp:ListItem>
                     <asp:ListItem>12/05/2015</asp:ListItem>
                     <asp:ListItem>13/05/2015</asp:ListItem>
                 </asp:DropDownList>
             </td>
             <td class="style10"> <asp:Label ID="Label2" runat="server" Text="Nombre d'Inscription :"></asp:Label>
                                    <asp:Label ID="lblcountang" runat="server" ForeColor="Red"></asp:Label><asp:Label ID="Label4" runat="server" Text=":étudiant(e)s"></asp:Label></td>
         </tr>

           
            </table>
            </center>

           <br />
         <asp:Button ID="btnOk" runat="server" CssClass="style12" Height="38px" 
             Text="Ajouter" Width="91px"    />
            
    </asp:Panel>
    <br />
    <br />
    </asp:Panel>
    
             <span class="style15"><strong>Si vous êtes interesser de passer le test toeic et le test prep toeic cliquer sur le lien si dessus:</strong></span><br />
             <asp:LinkButton ID="linkbutonfrang" runat="server" PostBackUrl="~/Etudiants/Test_toeic_prep.aspx">Cliquez-ici</asp:LinkButton>
</center>

<%--Pour ceux qui vont passer le test du français--%>
<center>
<asp:Panel ID="Panelfr" runat="server"  BackColor="#BABABA" Height="600px" Width="1000px">
<center>
<table><tr><td class="style13"><h1 class="style1"><span class="style14">INSCRIPTION 
    AU TEST DU LANGUE POUR L&#39;ANNEE UNIVERSITAIRE:
    <asp:Label ID="Label1" runat="server" CssClass="style7"></asp:Label>
    /<asp:Label ID="Label3" runat="server" CssClass="style7"></asp:Label>
    </span></h1></td></tr>
            
       <tr><td class="style13">
       
        <h3 class="style12" >
            <span class="style11">*</span><span class="text-info"> Veuillez choisir votre 
            date de passage de l&#39;examen d&#39;évaluation de test de niveau (Choix entre trois 
            dates uniques).</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> Après validation, vos 
            choix ne seront plus modifiables.</span><br 
                class="text-info" />
            <span class="style11">* </span><span class="text-info">Le nombre d&#39;inscription 
            ne doit pas dépasser 150 personnes par date.</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> les étudiants auront 
            droit à une seule inscription par épreuve, les absences ne seront pas donc 
            tolérées.</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> les étudiants auront un 
            niveau B2 seront invités à passer le test TOIEC ou bien PREP TOIEC.</span><br 
                class="text-info" />
            </h3>
       
       </td></tr>     
            
            </table>
            </center>
            <br />
   
<asp:Panel ID="Panelang" runat="server"  BackColor="#CC0000" Height="300px" Width="800px">

<br />
<br /><br />
<center>
            <table style="background-color: #999999; width: 701px;" Height="200px">
            <tr><td class="style6" colspan="3">Veuillez choisir la date correspondante pour passer le test du Français</td></tr>
            <tr>
             <td class="style9" >
                 <asp:Label ID="Label5" runat="server" Text="Test Français" CssClass="text-info"></asp:Label>
             </td>
             <td>
                 <asp:DropDownList ID="DropDownList1" runat="server" AppendDataBoundItems="true" 
                     AutoPostBack="true" dataTextFormatString="{0:dd/MM/yyyy}" 
                     >
                     <asp:ListItem>--Choisir la date du test Français--</asp:ListItem>
                     <asp:ListItem>11/05/2015</asp:ListItem>
                     <asp:ListItem>12/05/2015</asp:ListItem>
                     <asp:ListItem>13/05/2015</asp:ListItem>
                 </asp:DropDownList>
             </td>
             <td class="style10"> <asp:Label ID="Label7" runat="server" Text="Nombre d'Inscription :" 
                     CssClass="text-info"></asp:Label>
                                    <asp:Label ID="Label8" runat="server" ForeColor="Red" 
                     CssClass="text-info"></asp:Label><asp:Label ID="Label9" runat="server" 
                     Text=":étudiant(e)s" CssClass="text-info"></asp:Label></td>
         </tr>

         

           
            </table>
            </center>

           <br />
         <asp:Button ID="Button1" runat="server" CssClass="style12" Height="38px" 
             Text="Ajouter" Width="91px"    />
            
    </asp:Panel>
    <br />
    <br />
    </asp:Panel>
    
             <span class="style15"><strong>Si vous êtes interesser de passer le test toeic et le test prep toeic cliquer sur le lien si dessus:</strong></span><br />
             <asp:LinkButton ID="LinkButtonfr" runat="server" PostBackUrl="~/Etudiants/Test_toeic_prep.aspx">Cliquez-ici</asp:LinkButton>
</center>
<%--Pour ceux qui vont passer le test d'anglais--%>

<center>
<asp:Panel ID="Panelanglais" runat="server"  BackColor="#BABABA" Height="600px" Width="1000px">
<center>
<table><tr><td class="style13"><h1 class="style1"><span class="style14">INSCRIPTION 
    AU TEST DU LANGUE POUR L&#39;ANNEE UNIVERSITAIRE:
    <asp:Label ID="Label14" runat="server" CssClass="style7"></asp:Label>
    /<asp:Label ID="Label15" runat="server" CssClass="style7"></asp:Label>
    </span></h1></td></tr>
            
       <tr><td class="style13">
       
        <h3 class="style12" >
            <span class="style11">*</span><span class="text-info"> Veuillez choisir votre 
            date de passage de l&#39;examen d&#39;évaluation de test de niveau (Choix entre trois 
            dates uniques).</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> Après validation, vos 
            choix ne seront plus modifiables.</span><br 
                class="text-info" />
            <span class="style11">* </span><span class="text-info">Le nombre d&#39;inscription 
            ne doit pas dépasser 150 personnes par date.</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> les étudiants auront 
            droit à une seule inscription par épreuve, les absences ne seront pas donc 
            tolérées.</span><br 
                class="text-info" />
            <span class="style11">*</span><span class="text-info"> les étudiants auront un 
            niveau B2 seront invités à passer le test TOIEC ou bien PREP TOIEC.</span><br 
                class="text-info" />
            </h3>
       
       </td></tr>     
            
            </table>
            </center>
            <br />
   
<asp:Panel ID="Panel4" runat="server"  BackColor="#CC0000" Height="300px" Width="800px">

<br />
<br /><br />
<center>
            <table style="background-color: #999999; width: 701px;" Height="200px">
            <tr><td class="style6" colspan="3">Veuillez choisir la date correspondante pour passer le test d'anglais</td></tr>
            

         <tr>
             <td class="style9" >
                 <asp:Label ID="Label20" runat="server" Text="Test Anglais" CssClass="text-info"></asp:Label>
             </td>
             <td>
                 <asp:DropDownList ID="DropDownList4" runat="server" AppendDataBoundItems="true" 
                     AutoPostBack="true" dataTextFormatString="{0:dd/MM/yyyy}" 
                     >
                     <asp:ListItem>--Choisir la date du test Anglais--</asp:ListItem>
                      <asp:ListItem>11/05/2015</asp:ListItem>
                     <asp:ListItem>12/05/2015</asp:ListItem>
                     <asp:ListItem>13/05/2015</asp:ListItem>
                 </asp:DropDownList>
             </td>
             <td class="style10"> <asp:Label ID="Label21" runat="server" Text="Nombre d'Inscription :"></asp:Label>
                                    <asp:Label ID="Label22" runat="server" ForeColor="Red"></asp:Label><asp:Label ID="Label23" runat="server" Text=":étudiant(e)s"></asp:Label></td>
         </tr>

           
            </table>
            </center>

           <br />
         <asp:Button ID="Button2" runat="server" CssClass="style12" Height="38px" 
             Text="Ajouter" Width="91px"    />
            
    </asp:Panel>
    <br />
    <br />
    </asp:Panel>
    
             <span class="style15"><strong>Si vous êtes interesser de passer le test toeic et le test prep toeic cliquer sur le lien si dessus:</strong></span><br />
             <asp:LinkButton ID="LinkButtonang" runat="server" PostBackUrl="~/Etudiants/Test_toeic_prep.aspx">Cliquez-ici</asp:LinkButton>
</center>

</asp:Content>


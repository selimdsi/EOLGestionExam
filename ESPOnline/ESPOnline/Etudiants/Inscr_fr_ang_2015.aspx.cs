﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;

namespace ESPOnline.Etudiants
{
    public partial class Inscr_fr_ang_2015 : System.Web.UI.Page
    {
        string id_et;
        string typechoix;
        DataTable dt = new DataTable();
        ToiecService service = new ToiecService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }
            lblanneedeb.Text = service.getANNEEDEBs();
            lblanneefin.Text = service.getAnneeFiN();

            if(!IsPostBack)
            {

                id_et = Session["ID_ET"].ToString();

                string codecl = service.returnCLSUPP(id_et);
                dt = service.get_id_etud_formation(id_et);
                panelddr.Visible = false;
                panel1.Visible = false;

                if (dt.Rows.Count != 0)
                {

                    //Response.Redirect("~/Etudiants/Accueil.aspx");
                    Response.Write(@"<script language='javascript'>alert('Vous êtes déjà inscrit');</script>");
                    panelddr.Visible = false;
                    panel1.Visible = true;
                }
                else
                {
                    if (codecl.StartsWith("5GC") || codecl.StartsWith("5EM"))
                    {
                        Response.Write(@"<script language='javascript'>alert('Veuillez choisir la formation que vous voulez passer');</script>");
                        panelddr.Visible = true;
                        panel1.Visible = false;
                    }
                    else
                    {
                        Response.Write(@"<script language='javascript'>alert('Vous n\'avez pas le droit de passer la formation');</script>");
                        panelddr.Visible = false;
                        panel1.Visible = true;
                        


                    }
            }
           
                

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try 
            {
                

                id_et = Session["ID_ET"].ToString();
                service.Enreg_etud_FORMATION(id_et, ddlchoix.SelectedValue);
                Response.Write(@"<script language='javascript'>alert('Vous êtes enregistré avec succès');</script>");
                panelddr.Visible = false;
                panel1.Visible = true;
            
            }

            catch
            {
                Response.Write(@"<script language='javascript'>alert('Erreur de serveur');</script>");
            
            }
            
            
        }
    }
}
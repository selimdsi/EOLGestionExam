﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;

namespace ESPOnline.Etudiants
{
    public partial class Insc_FR1219 : System.Web.UI.Page
    {
        ToiecService service = new ToiecService();
        string id_et;
        DataTable dt;

        string count_nbfr;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["ID_ET"] == null || Session["CIN_PASS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

            if (!IsPostBack)
            {

                id_et = Session["ID_ET"].ToString();
                dt = service.get_inscrit1219(id_et);

                if (dt.Rows.Count != 0)
                {
                    Response.Write(@"<script language='javascript'>alert('Veuillez modifier votre choix!!');</script>");
                    
                    pllo.Visible = false;
                    plmodif.Visible = true;

                    //Apres modification panel1.Visible = true;
                    //Bind choix
                }
            }
        }



        protected void ddltestfr_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }



        protected void Imgvalid_Click1(object sender, EventArgs e)
        {

             try
            {

                if (ddlchoix.SelectedValue == "0")
                {
                    Response.Write(@"<script language='javascript'>alert('Veuillez choisir la date');</script>");

                }
                else
                {
                    pllo.Visible = true;
                    id_et = Session["ID_ET"].ToString();
                    service.Enreg_fr1219(id_et, ddlchoix.SelectedValue);
                    Response.Write(@"<script language='javascript'>alert('Vous êtes enregistré avec succès,Bonne chance :)');</script>");
                    panel1.Visible = true;
                    pllo.Visible = false;

                }
            }

             catch
             {
                 Response.Write(@"<script language='javascript'>alert('Erreur de serveur');</script>");

             }
        }

        protected void btnmodif_Click(object sender, EventArgs e)
        {
            try
            {

                if (DropDownList1.SelectedValue == "0")
                {
                    Response.Write(@"<script language='javascript'>alert('Veuillez choisir la date');</script>");

                }
                else
                {
                    
                    id_et = Session["ID_ET"].ToString();
                    service.Updatechoix1219(id_et, DropDownList1.SelectedValue);
                    Response.Write(@"<script language='javascript'>alert('Votre choix sera modifié,Bonne chance :)');</script>");
                    panel1.Visible = true;
                    pllo.Visible = false;
                    

                }
            }

            catch
            {
                Response.Write(@"<script language='javascript'>alert('Erreur de serveur');</script>");

            }
        }
    }
}
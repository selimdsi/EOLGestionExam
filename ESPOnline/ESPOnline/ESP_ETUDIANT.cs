﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Oracle.ManagedDataAccess.Client;
using System.ComponentModel;
using ABSEsprit;

namespace AffichageClasse
{
    //   SELECT     NOM_ET, PNOM_ET, ID_ET, NUM_CIN_PASSEPORT
    //FROM         SCOESP09.ESP_ETUDIANT



    public class ESP_ETUDIANT
    {
        #region sing

        static ESP_ETUDIANT instance;
        static Object locker = new Object();
        //InscriptionOnLineESPRIT manager = new GestionEnquêtesEntities();
        public static ESP_ETUDIANT Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new ESP_ETUDIANT();
                    }

                    return ESP_ETUDIANT.instance;
                }
            }

        }
        private ESP_ETUDIANT() { }

        #endregion
        #region public private methodes

        private string _ID_ET;

        public string ID_ET
        {
            get { return _ID_ET; }
            set { _ID_ET = value; }
        }
        private string _NUM_CIN_PASSEPORT;

        public string NUM_CIN_PASSEPORT
        {
            get { return _NUM_CIN_PASSEPORT; }
            set { _NUM_CIN_PASSEPORT = value; }
        }
        private string _PWD_ET;

        public string PWD_ET
        {
            get { return _PWD_ET; }
            set { _PWD_ET = value; }
        }
        private string _NOM_ET;

        public string NOM_ET
        {
            get { return _NOM_ET; }
            set { _NOM_ET = value; }
        }
        private string _PRENOM_ET;

        public string PRENOM_ET
        {
            get { return _PRENOM_ET; }
            set { _PRENOM_ET = value; }
        }
        private string _ADRESSE_MAIL_ESP;

        public string ADRESSE_MAIL_ESP
        {
            get { return _ADRESSE_MAIL_ESP; }
            set { _ADRESSE_MAIL_ESP = value; }
        }



        #endregion

        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public ESP_ETUDIANT loginET(string _ID_ET, string _NUM_CIN_PASSEPORT, string _PWD_ET)
        {
            bool exist = false;
            string Name = "x";
            ESP_ETUDIANT etu = null;

            using (OracleConnection mySqlConnection = new OracleConnection(AppConfiguration.ConnectionString))
            {
                mySqlConnection.Open();

                string cmdQuery = "select * from ESP_ETUDIANT WHERE  (ID_ET ='" + _ID_ET + "' or trim(NUM_CIN_PASSEPORT)='" + _NUM_CIN_PASSEPORT + "') and pwd_et=FS_CRYPT_DECRYPT('" + _PWD_ET + "')  and ETAT='A'";


                OracleCommand myCommand = new OracleCommand(cmdQuery, mySqlConnection);


                OracleDataReader MyReader = myCommand.ExecuteReader();

                while (MyReader.Read() && !exist)
                {
                    // String Name = MyReader["Username"].ToString();


                    etu = new ESP_ETUDIANT(MyReader);
                    break;

                }
                MyReader.Close();
                mySqlConnection.Close();
                return etu;
            }



        }

        public ESP_ETUDIANT(OracleDataReader myReader)
        {
            if (!myReader.IsDBNull(myReader.GetOrdinal("ID_ET")))
            {
                _ID_ET = myReader.GetString(myReader.GetOrdinal("ID_ET"));

            }

            if (!myReader.IsDBNull(myReader.GetOrdinal("NUM_CIN_PASSEPORT")))
            {

                _NUM_CIN_PASSEPORT = myReader.GetString(myReader.GetOrdinal("NUM_CIN_PASSEPORT"));
            }
            if (!myReader.IsDBNull(myReader.GetOrdinal("NOM_ET")))
            {

                _NOM_ET = myReader.GetString(myReader.GetOrdinal("NOM_ET"));
            }
            if (!myReader.IsDBNull(myReader.GetOrdinal("PWD_ET")))
            {

                _PWD_ET = myReader.GetString(myReader.GetOrdinal("PWD_ET"));
            }
            if (!myReader.IsDBNull(myReader.GetOrdinal("PNOM_ET")))
            {

                _PRENOM_ET = myReader.GetString(myReader.GetOrdinal("PNOM_ET"));
            }
            if (!myReader.IsDBNull(myReader.GetOrdinal("ADRESSE_MAIL_ESP")))
            {

                _ADRESSE_MAIL_ESP = myReader.GetString(myReader.GetOrdinal("ADRESSE_MAIL_ESP"));
            }


        }

        public ESP_ETUDIANT(string ID_ET, string NUM_CIN_PASSEPORT, string NOM_ET, string PRENOM_ET, string ADRESSE_MAIL_ESP, string PWD_ET)
        {
            this._ID_ET = ID_ET;
            this._NUM_CIN_PASSEPORT = NUM_CIN_PASSEPORT;
            this._NOM_ET = NOM_ET;
            this._PRENOM_ET = PRENOM_ET;
            this._ADRESSE_MAIL_ESP = ADRESSE_MAIL_ESP;
            this._PWD_ET = PWD_ET;

        }
    }

}

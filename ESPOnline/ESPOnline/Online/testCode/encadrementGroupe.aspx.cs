﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ESPSuiviEncadrement;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;


namespace ESPOnline.testCode
{
    public partial class encadrementGroupe : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            
        }
        /*************************************************************************creation encadrement*******************************************************/
        protected void Button1_Click(object sender, EventArgs e)
        {
            
            OracleDate date1 = OracleDate.GetSysDate();
            OracleTimeStamp date2 = OracleTimeStamp.GetSysDate();
            

            ESP_ENCADREMENT_GP.Instance.openconntrans();
            decimal id = ESP_ENCADREMENT_GP.Instance.inc_id_encadrement_groupe();
            if (ESP_ENCADREMENT_GP.Instance.create_Encadrement_ESP("2014", "12", "WAEL SAIDI", "1254", date1, date2, date2, date2, 10, 10, 10, 10, 10, "AB", "rien", "rient", 15, "GPROJ1") == true)
                Label1.Text = "done";
            else
                Label1.Text = "none";
            ESP_ENCADREMENT_GP.Instance.closeConnection();
        }

        /*************************************************************************Filtration d'encadrement *******************************************************/
        protected void Button2_Click(object sender, EventArgs e)
        {
            bindGroupeProjetParIDgroupeprojet();
        }
        protected void bindGroupeProjetParIDgroupeprojet()
        {
            List<ESP_ENCADREMENT_GP> ls = new List<ESP_ENCADREMENT_GP>();
            ls = ESP_ENCADREMENT_GP.GetListByGROUPE("GPROJ1");
            if (ls != null)
            {
                DropDownList1.DataSource = ls;
                DropDownList1.DataTextField = "NOM_GROUPE";
                DropDownList1.DataValueField = "ID_GROUPE_PROJET";
                DropDownList1.DataBind();
                DropDownList1.Items.Insert(0, new ListItem("Choisir", "0"));
            }
            else
                DropDownList1.Items.Insert(0, new ListItem("N/A", "0"));

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            bindGroupeProjetParIDgroupeprojetIDprojet();

        }
        protected void bindGroupeProjetParIDgroupeprojetIDprojet()
        {
            List<ESP_ENCADREMENT_GP> ls = new List<ESP_ENCADREMENT_GP>();
            ls = ESP_ENCADREMENT_GP.GetProjByGROUPEProjet("PROJ1", "GPROJ1");
            DropDownList1.DataSource = ls;
            DropDownList1.DataTextField = "NOM_GROUPE";
            DropDownList1.DataValueField = "ID_GROUPE_PROJET";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Choisir", "0"));
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            bindGroupeProjetParClasse();

        }
        protected void bindGroupeProjetParClasse()
        {
            List<ESP_ENCADREMENT_GP> ls = new List<ESP_ENCADREMENT_GP>();
            ls = ESP_ENCADREMENT_GP.GetGROUPProjByClass("5INFOA3", "GPROJ1");
            DropDownList1.DataSource = ls;
            DropDownList1.DataTextField = "NOM_GROUPE";
            DropDownList1.DataValueField = "ID_GROUPE_PROJET";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Choisir", "0"));
        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            bindGroupeProjetParType();
        }
        protected void bindGroupeProjetParType()
        {
            List<ESP_ENCADREMENT_GP> ls = new List<ESP_ENCADREMENT_GP>();
            ls = ESP_ENCADREMENT_GP.GetListByEtudiantType("GPROJ1","12");
            DropDownList1.DataSource = ls;
            DropDownList1.DataTextField = "NOM_GROUPE";
            DropDownList1.DataValueField = "ID_GROUPE_PROJET";
            DropDownList1.DataBind();
            DropDownList1.Items.Insert(0, new ListItem("Choisir", "0"));
        }

      
        /*************************************************************************Creation Groupe Projet *******************************************************/

        protected void Button6_Click(object sender, EventArgs e)
        {
            ESP_GP_PROJET.Instance.openconntrans();
            decimal id = ESP_GP_PROJET.Instance.inc_id_groupe_projet();
            if (ESP_GP_PROJET.Instance.create_groupe_projet_ESP("GPROJ" + id, "Omar", "PFE", 1, "PFE") == true)
                Label2.Text = "created";
            else
                Label2.Text = "not created";
            ESP_GP_PROJET.Instance.closeConnection();
        }

        protected void Button7_Click(object sender, EventArgs e)
        {
            //ESP_ETUDIANT_NOTE_GROUPE.Instance.openconntrans();
            //if (ESP_ETUDIANT_NOTE_GROUPE.Instance.verif_Groupe_projet_ESP(DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString()) == false)
            //{
            //    ESP_ETUDIANT_NOTE_GROUPE.Instance.affect_etudiant_groupe_projet_ESP(DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString(), 0, "Zied Alaya");
            //    Label3.Text = "affected";
            //}
            //else
            //    Label3.Text = "not affected";
            
            //ESP_ETUDIANT_NOTE_GROUPE.Instance.closeConnection();
        }

        protected void Button8_Click(object sender, EventArgs e)
        {
            ESP_GP_PROJET.Instance.openconntrans();
            List<EtudiantClasses> listeE = EtudiantClasses.GetList();
            List<ESP_GP_PROJET> listeP = ESP_GP_PROJET.GetListGROUPE();
            ESP_GP_PROJET.Instance.closeConnection();
            DropDownList1.DataSource = listeE;
            DropDownList1.DataTextField = "NOM_ET";
            DropDownList1.DataValueField = "ID_ET";
            DropDownList1.DataBind();

            DropDownList2.DataSource = listeP;
            DropDownList2.DataTextField = "NOM_GROUPE";
            DropDownList2.DataValueField = "ID_GROUPE_PROJET";
            DropDownList2.DataBind();
            etudiant.Text = DropDownList1.SelectedItem.ToString();
            textBox1.Visible = true;
            Button9.Visible = true;
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            etudiant.Text = DropDownList1.SelectedItem.ToString();
        }

        protected void Button9_Click(object sender, EventArgs e)
        {
            ESP_ETUDIANT_NOTE_GROUPE.Instance.openconntrans();
            ESP_ETUDIANT_NOTE_GROUPE.Instance.update_NOTE_etudiant_projet_ESp(DropDownList2.SelectedValue.ToString(), DropDownList1.SelectedValue.ToString(), decimal.Parse(textBox1.Text),"Zied Alaya");
            ESP_ETUDIANT_NOTE_GROUPE.Instance.closeConnection();
        }
        /*************************************************************************Creation+affectation un etudiant au Groupe Projet *******************************************************/


    }
}
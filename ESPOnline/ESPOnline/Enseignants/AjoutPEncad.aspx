﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AjoutPEncad.aspx.cs" Inherits="ESPOnline.Enseignants.AjoutPEncad"
    MasterPageFile="~/Enseignants/Ens.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Contents/Css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Contents/Scripts/JScript1.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "../EnseignantsCUP/Styles/minus.png");
        });
        $("[src*=minus]").live("click", function () {
            $(this).attr("src", "../EnseignantsCUP/Styles/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
    <style type="text/css">
        table.grid tbody tr:hover
        {
            background-color: #e5ecf9;
        }
        .GridHeaderStyle
        {
            color: #FEF7F7;
            background-color: #9c1919;
            font-weight: bold;
        }
        .GridItemStyle
        {
            background-color: #eeeeee;
            color: #333;
        }
        .GridAlternatingStyle
        {
            background-color: #dddddd;
            color: black;
        }
        .GridSelectedStyle
        {
            background-color: #d6e6f6;
            color: black;
        }
        
        
        .GridStyle
        {
            border-bottom: white 2px ridge;
            border-left: white 2px ridge;
            background-color: white;
            width: 100%;
            border-top: white 2px ridge;
            border-right: white 2px ridge;
        }
        .ItemStyle
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        
        .ItemStyle td
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        .FixedHeaderStyle
        {
            background-color: #7591b1;
            color: #FFFFFF;
            font-weight: bold;
            position: relative;
            top: expression(this.offsetParent.scrollTop);
            z-index: 10;
        }
        .Caption_1_Customer
        {
            background-color: #beccda;
            color: #000000;
            width: 30%;
            height: 20px;
        }
        
        
        .grid td, .grid th
        {
            text-align: center;</style>
    <%-- style row color chnged--%>
    <style type="text/css">
        .successMerit
        {
            background-color: #1fa756;
            border: medium none;
            color: White;
        }
        .defaultColor
        {
            background-color: white;
            color: black;
        }
        .dangerFailed
        {
            background-color: #f2283a;
            color: White;
        }
        .style10
        {
            color: #0000CC;
        }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <%--  <script src="Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>--%>
    <script src="../Contents/Scripts/ScrollableGridPlugin_ASP.NetAJAX_2.0.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $('#<%=Gridstudent.ClientID %>').Scrollable({
            ScrollHeight: 300,
         
        });
    });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
    <center>
        <script language='JavaScript' type='text/JavaScript'>

            function OpenPopWindow() {
                var url = "Add_project2017.aspx";

                window.open(url, "popUp", "width=480, height=500, top=0, left=0, menubar=no, resizable=no, " +
                        "scrollbars=yes, status=no", '');
            }
 
        </script>
    </center>
    <center>

    <asp:Panel runat="server" ID="pl10" BackColor="#C0C0C0">
        <asp:Image ID="Image1" ImageUrl="~/bootstrap-3.1.1-dist/css/Sans titre3.png" runat="server" />
        <br />
        <br />
       
            <table>
                <tr>
                    <td>
                        <span class="style10">Titre de projet:</span>
                        <telerik:RadComboBox ID="ddlprojet" runat="server" AutoPostBack="True" EnableLoadOnDemand="True"
                            EmptyMessage="Tappez le titre de votre projet" Filter="Contains" Width="290px" Height="120px"
                            OnSelectedIndexChanged="ddlprojet_SelectedIndexChanged" DataTextField="nom_projet"
                            DataValueField="id_projet">
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlprojet"
                            ForeColor="Red" ErrorMessage="Value Required!" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="style10">Type de projet: </span>
                        <telerik:RadComboBox ID="ddltype" runat="server" EnableLoadOnDemand="True" EmptyMessage="Type de projet"
                            Filter="Contains" Width="290px" Height="120px">
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddltype"
                            ForeColor="Red" ErrorMessage="Value Required!" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="style10">Liste des étudiants:</span>
                        <telerik:RadComboBox ID="ddlstudentt" runat="server" AutoPostBack="True" EnableLoadOnDemand="True"
                            EmptyMessage="Tappez le nom de l'étudiant" Filter="Contains" Width="290px" Height="120px">
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstudentt"
                            ForeColor="Red" ErrorMessage="Value Required!" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <asp:Button ID="btnoki" runat="server" Text="Affecter Projet" Width="220px" Height="50px"
                OnClick="btnoki_Click" />
                <br />
                <br />
        </asp:Panel>
    </center>
    <br />
    <center>
    <h3>Liste des Étudiants affectés aux projet</h3>
        <asp:ScriptManager ID="DefaultMasterScriptManager" runat="server" />
        <asp:UpdatePanel ID="UpdateP3345" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView runat="server" ID="Gridstudent" AutoGenerateColumns="False" AllowPaging="True"
                    Visible="false" Style="border-bottom: white 2px ridge; border-left: white 2px ridge;
                    background-color: white; border-top: white 2px ridge; border-right: white 2px ridge;"
                    BorderWidth="0px" BorderColor="White" CellSpacing="1" CellPadding="3" CssClass="grid"
                    RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" GridLines="None"
                    EmptyDataRowStyle-CssClass="ItemStyle" DataKeyNames="id_projet" 
                    OnRowEditing="GrdEmpData_RowEditing"
                    OnRowCancelingEdit="GridView1_RowCancelingEdit"
                     OnRowUpdating="GrdEmpData_RowUpdating">
                    <EmptyDataTemplate>
                        Empty Result.
                    </EmptyDataTemplate>
                    <HeaderStyle HorizontalAlign="Center" Height="20" />
                    <RowStyle HorizontalAlign="Center" CssClass="ItemStyle"></RowStyle>
                    <FooterStyle CssClass="ItemStyle" />
                    <EmptyDataRowStyle CssClass="ItemStyle"></EmptyDataRowStyle>
                    <RowStyle CssClass="GridItemStyle" />
                    <AlternatingRowStyle CssClass="GridAlternatingStyle" />
                    <HeaderStyle CssClass="GridHeaderStyle" />
                    <SelectedRowStyle CssClass="GridSelectedStyle" />
                    <Columns>

                      <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <asp:ImageButton runat="server" ID="imgAddNew" ImageUrl="~/EnseignantsCUP/Styles/add.png" alt="" CausesValidation="false"
                                                                        Style="cursor: hand;" ImageAlign="Middle"  OnClientClick="return OpenPopWindow();"  ToolTip="add"
                                                                        />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="IbDelete" ImageUrl="~/EnseignantsCUP/Styles/delete.png" CausesValidation="false"
                                                                        ImageAlign="Middle" CommandName="Delete" OnClientClick="return confirm('Ëtes-vous sûr de vouloir supprimer cet équipe?');"
                                                                         ToolTip="Delete this row"  />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="20px" ControlStyle-Width="20px" FooterStyle-Width="20px">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton runat="server" ID="imgEdit" ImageUrl="~/EnseignantsCUP/Styles/edit.png" 
                                                                        Style="cursor: hand;" ImageAlign="Middle"  Enabled="true"  Text="Edit"   CommandName="Edit"  ToolTip="Edit"/>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                             <asp:TemplateField HeaderStyle-Width="20px" ControlStyle-Width="20px" FooterStyle-Width="20px">
                                                             
                                                            <EditItemTemplate>   
                                <asp:ImageButton ID="btnUpdate" runat="server" Text="Update"  CausesValidation="false"  CommandName="Update" ImageUrl="~/EnseignantsCUP/Styles/save.png" ToolTip="Update"
                                 OnClientClick = "return confirm('Ëtes-vous sûr de vouloir modifier cet équipe?')"></asp:ImageButton>
                                  <asp:ImageButton runat="server" ID="BtnCancelUpdateCrewSalaryPayment" ImageAlign="Middle" 
                                                                    CausesValidation="false" ImageUrl="~/EnseignantsCUP/Styles/redo.png" />
                                                            
</EditItemTemplate>    
</asp:TemplateField>
             
             
                  
             
                      
                        <asp:TemplateField HeaderText="Code projet" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                            <ItemTemplate>
                                <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("id_projet")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPrice1" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nom de projet" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="150px">
                            <ItemTemplate>
                                <asp:Label ID="lblPrice13" runat="server" Text='<%# Eval("NOM_PROJET")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPrice11" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Code type de projet" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label ID="lblPrice12" runat="server" Text='<%# Eval("TYPE_PROJET")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblTotalPrice111" runat="server" />
                            </FooterTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField   HeaderText = "Identifiant etud">
    <ItemTemplate>
        <asp:Label ID="lblNatureIntervention" runat="server"
                Text='<%# Eval("id_et")%>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
         <asp:DropDownList runat="server" ID="ddltype" DataTextField="type_projet" DataValueField="lib_nome">
                    </asp:DropDownList>
    </EditItemTemplate> 
    <FooterTemplate>
        <asp:TextBox ID="txtnature" runat="server"></asp:TextBox>
   </FooterTemplate>
</asp:TemplateField>



                          <asp:TemplateField   HeaderText = "Nom etudiant">
    <ItemTemplate>
        <asp:Label ID="lblNatureIntervention" runat="server"
                Text='<%# Eval("Nom")%>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
         <asp:DropDownList runat="server" ID="ddltype" DataTextField="type_projet" DataValueField="lib_nome">
                    </asp:DropDownList>
    </EditItemTemplate> 
    <FooterTemplate>
        <asp:TextBox ID="txtnature" runat="server"></asp:TextBox>
   </FooterTemplate>
</asp:TemplateField>


 <asp:TemplateField   HeaderText = "Type de projet">
    <ItemTemplate>
        <asp:Label ID="lblNatureIntervention" runat="server"
                Text='<%# Eval("libel_proj")%>'></asp:Label>
    </ItemTemplate>
    <EditItemTemplate>
         <asp:DropDownList runat="server" ID="ddltype" DataTextField="type_projet" DataValueField="lib_nome">
                    </asp:DropDownList>
    </EditItemTemplate> 
    <FooterTemplate>
        <asp:TextBox ID="txtnature" runat="server"></asp:TextBox>
   </FooterTemplate>
</asp:TemplateField>

                    </Columns>
                    <HeaderStyle CssClass="GridHeaderStyle" />
                    <RowStyle CssClass="GridItemStyle" />
                    <SelectedRowStyle CssClass="GridSelectedStyle" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>

    <br />

    <center>
    
    
    <table>

<tr>
<td>


<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<asp:GridView ID="gvCustomers" runat="server" AutoGenerateColumns="false" OnRowDataBound = "OnRowDataBoundS" DataKeyNames = "id_et"
 Style="border-bottom: white 2px ridge; border-left: white 2px ridge; background-color: white; 
     border-top: white 2px ridge; border-right: white 2px ridge;"
  BorderWidth="0px" 
        BorderColor="White" CellSpacing="1" CellPadding="3" CssClass="grid"
        Visible="false"
                                                        
        RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" 
                                                        GridLines="None" 
        EmptyDataRowStyle-CssClass="ItemStyle"  >
    <Columns>
       <%-- <asp:TemplateField>
            <HeaderTemplate>
                <asp:CheckBox ID = "chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChanged" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChanged" />
            </ItemTemplate>
        </asp:TemplateField>--%>

        <asp:TemplateField ItemStyle-Width="20px">
       <%-- <HeaderTemplate>
      <asp:CheckBox ID="checkAll" runat="server" onclick = "checkAll(this);"  AutoPostBack="true"/>
    </HeaderTemplate>--%>

     <HeaderTemplate>
                <asp:CheckBox ID = "chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChangedDDD" Visible="false"/>
            </HeaderTemplate>
           <ItemTemplate> 
              <asp:CheckBox ID="CheckBox1"  runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChanged" />
           </ItemTemplate>
        </asp:TemplateField>

        <asp:BoundField DataField="ID_ET" HeaderText="ID Etudiant" SortExpression="ID_ET"
            ItemStyle-Width="25px" />
<asp:BoundField DataField="NOM" HeaderText="NOM et PRENOM" SortExpression="NOM" />
        <%-- <asp:TemplateField HeaderText="Identifiant" ItemStyle-HorizontalAlign  ="Left"  ItemStyle-Width ="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblId" runat="server" Text='<% # Eval("ID_ET") %>'></asp:Label>
                        </ItemTemplate>
                     </asp:TemplateField>  
                                                            
<asp:TemplateField HeaderText="Nom et prenom de l'étudiant">
                <ItemTemplate>
                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("NOM") %>'></asp:Label>
                </ItemTemplate>
              
            </asp:TemplateField>--%>

        <asp:TemplateField HeaderText="Niveau Français">
                <%--<ItemTemplate>
                    <asp:Label ID="lablNature" runat="server" Text='<%# Eval("niveau_courant_fr") %>'></asp:Label>
                </ItemTemplate>--%>
               <%-- <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="ddlniveau_fr" DataTextField="niveau_courant_fr" DataValueField="niveau_courant_fr" Visible="false">
                    </asp:DropDownList>
                </EditItemTemplate>--%>
                <ItemTemplate>
                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("niveau_courant_fr") %>'></asp:Label>
               <asp:DropDownList ID="ddlCountries" runat="server" Visible = "false"  AutoPostBack="true" AppendDataBoundItems="true" >
               </asp:DropDownList>
            </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Niveau Anglais">

             <ItemTemplate>
                <asp:Label ID = "lblCountry2" runat="server" Text='<%# Eval("niveau_courant_ang") %>'></asp:Label>
                <asp:DropDownList ID="ddlCountries2" runat="server" Visible = "false"  AutoPostBack="true" AppendDataBoundItems="true">
                </asp:DropDownList>
            </ItemTemplate>

            <EditItemTemplate>
            
             
                <asp:Label ID = "lblCountry2" runat="server" Text='<%# Eval("niveau_courant_ang") %>'></asp:Label>
                <asp:DropDownList ID="ddlCountries2" runat="server" Visible = "false"  AutoPostBack="true" AppendDataBoundItems="true">
                </asp:DropDownList>
           
            
            </EditItemTemplate>
                <%--<ItemTemplate>
                    <asp:Label ID="labang" runat="server" Text='<%# Eval("niveau_courant_ang") %>'></asp:Label>
                </ItemTemplate>--%>
               <%-- <EditItemTemplate>
                    <asp:DropDownList runat="server" ID="ddlniveau_ang" DataTextField="niveau_courant_ang" DataValueField="niveau_courant_ang "  Visible="false">
                    </asp:DropDownList>
                </EditItemTemplate>--%>
            </asp:TemplateField>
    </Columns>
</asp:GridView>
</ContentTemplate>
</asp:UpdatePanel>

</td>
<td><br /></td>
<td><asp:Button ID="btnUpdate" runat="server" Text="Modifier" OnClick = "Update" Visible="false" height="44px"/></td>


</tr>
</table>
    
    </center>
</asp:Content>

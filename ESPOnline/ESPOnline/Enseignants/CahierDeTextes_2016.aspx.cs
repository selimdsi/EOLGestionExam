﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;

namespace ESPOnline.Enseignants
{
    public partial class CahierDeTextes_2016 : System.Web.UI.Page
    {
        CahietTextService service = new CahietTextService();

        string id_ens;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UP"] == null || Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }


            if (!IsPostBack)
            {
                id_ens = Session["ID_ENS"].ToString();
                tempcl.Update();
                bindniv_cl();
                bindcrennomenclature();
            }
        }
        public void bindniv_cl()
        {
            
                id_ens = Session["ID_ENS"].ToString();
                ddlclasses.DataTextField = "code_cl";
                ddlclasses.DataValueField = "code_cl";

                ddlclasses.DataSource = service.bind_cdclens(id_ens, Convert.ToInt32(RadioButtonList3.SelectedValue));

                ddlclasses.DataBind();

                ddlclasses.Items.Insert(0, new ListItem("Veuillez choisir", "0"));
                ddlclasses.SelectedItem.Selected = false;
                ddlclasses.Items.FindByText("Veuillez choisir").Selected = true;
                tempcl.Update();
            
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            //TextBox1.Text = Calendar1.SelectedDate.ToString();
        }

        protected void ddlclasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            if (ddlclasses.SelectedValue != null)
            {
                id_ens = Session["ID_ENS"].ToString();
                ddlmodule.DataTextField = "designation";
                ddlmodule.DataValueField = "CODE_MODULE";
                
                    ddlmodule.DataSource = service.bind_module(id_ens, ddlclasses.SelectedValue, Convert.ToInt32(RadioButtonList3.SelectedValue));
                    ddlmodule.DataBind();
               
                //ddlmodule.Items.Insert(0, new ListItem("Veuillez choisir", "0"));
                //ddlmodule.SelectedItem.Selected = false;
                //ddlmodule.Items.FindByText("Veuillez choisir").Selected = true;
                //tempcl.Update();
            }
            //}
        }


        public void bindcrennomenclature()
        {
            ddlcren1.DataTextField = "lib_nome";
            ddlcren1.DataValueField = "CODE_NOME";
            ddlcren2.DataTextField = "lib_nome";
            ddlcren2.DataValueField = "CODE_NOME";

            ddlcren1.DataSource = service.bind_CRENEAU();
            ddlcren1.DataBind();
            ddlcren2.DataSource = service.bind_CRENEAU();
            ddlcren2.DataBind();

            ddlcren1.Items.Insert(0, new ListItem("Veuillez choisir", "0"));
            ddlcren1.SelectedItem.Selected = false;
            ddlcren1.Items.FindByText("Veuillez choisir").Selected = true;
            tempcl.Update();

            ddlcren2.Items.Insert(0, new ListItem("Veuillez choisir", "0"));
            ddlcren2.SelectedItem.Selected = false;
            ddlcren2.Items.FindByText("Veuillez choisir").Selected = true;
            tempcl.Update();
        }

        protected void btnvalid_Click(object sender, EventArgs e)
        { 
            id_ens = Session["ID_ENS"].ToString();
            //valider votre choix.
        try
        {
            if (ddlcren1.SelectedValue == "0" || ddlcren2.SelectedValue == "0" || ddlduree.SelectedValue=="0"|| ddlclasses.SelectedValue=="0" || ddlmodule.SelectedValue=="0")
            {

                string message;
                message = "alert('Veuillez choisir !')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }

            else 
            {
                DataTable dts = new DataTable();
                dts = service.VerifCahier(id_ens, ddlmodule.SelectedValue, ddlclasses.SelectedValue,Convert.ToInt32(RadioButtonList3.SelectedValue));
                if(dts.Rows.Count != 0)
                {
                string message;
            message = "alert('Donnée existe !')";
            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }

                else
                {

            service.add_cahierTEXT(ddlclasses.SelectedValue, ddlmodule.SelectedValue, Convert.ToDateTime(TBdateseance.Text), ddlduree.SelectedValue, txtmessage.Text, txtremarque.Text, ddlcren1.Text, ddlcren2.Text, id_ens, "2015",Convert.ToInt32(RadioButtonList3.SelectedValue));
            //Response.Write(@"<script language='javascript'>alert('Ajout avec succès');</script>");
            string message;
            message = "alert('Ajout avec succès !')";
            ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
            }
            
        }
    
            catch
            {
                //Response.Write(@"<script language='javascript'>alert('Erreur de serveur');</script>");

                string message;
                message = "alert('Erreur de serveur !')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            
            }
        }

        protected void ddlcren2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int s1;
            int s2;
            
            
             s1 = Convert.ToInt32(ddlcren2.SelectedValue);
             s2 = Convert.ToInt32(ddlcren1.SelectedValue);

            if (ddlcren2.SelectedValue == ddlcren1.SelectedValue || s1 < s2)
            {
                string message;
                message = "alert('Veuillez entrer une heure supérieure !')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            
            }
            else 
                if(ddlcren2.SelectedValue=="0")
                {
                    string message;
                    message = "alert('Veuillez choisir !')";
                    ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                }
        }
        protected void ddlcren1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlcren1.SelectedValue == "0")
            {
                string message;
                message = "alert('Veuillez choisir !')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
        }

        protected void ddlduree_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlduree.SelectedValue == "0")
            {
                string message;
                message = "alert('Veuillez choisir !')";
                ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
            }
        }

    }
}
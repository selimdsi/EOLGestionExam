﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ajout_projet.aspx.cs" 
Inherits="ESPOnline.Enseignants.Ajout_projet"   MasterPageFile="~/Enseignants/Ens.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Contents/Css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Contents/Scripts/JScript1.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link  href="../Contents/cssencadrement/encadrement.css" type="text/javascript"/>
    


    <style type="text/css">
        .successMerit
        {
            background-color: #1fa756;
            border: medium none;
            color: White;
        }
        .defaultColor
        {
            background-color: white;
            color: black;
        }
        .dangerFailed
        {
            background-color: #f2283a;
            color: White;
        }
    </style>
    
     <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
     
        <link rel="stylesheet" href="../Contents/Encadrement/css/style.css">
         <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

        <script src="../Contents/Encadrement/js/index.js"></script>


         <link rel="stylesheet" href="../Contents/Css/StyleSheet1.css" type="text/css" media="all" />

</asp:Content>

   <asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div id="content">
        <center>
            <h3>
                FORMULAIRE D'INSCRIPTION</h3>
        </center>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel ID="Panel1" runat="server">
                    <%-- <div  class="clearfix">
    <div class="panel panel-default" >
  <div class="panel-heading" >--%>
                    <%--  <h3 class="panel-title"  align="center" >INFORMATIONS 
        PERSONNELLES</h3>--%>
                    <%-- </div>--%>
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <div class="span5">

                                    <table class="style1">
                                        <tr>
                                            <td class="style3">
                                                <div class="form-group">
                                                    <asp:TextBox ID="Tbnom" runat="server" TextMode="SingleLine" Height="31px" Width="200px"
                                                        required="" placeholder="Nom"></asp:TextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <asp:TextBox ID="Tbprenom" runat="server" TextMode="SingleLine" Height="31px" Width="192px"
                                                        required="" placeholder="Prénoms"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="style1">
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <asp:TextBox ID="Tbmail" runat="server" Height="31px" Width="405px" required="" placeholder="E-mail"
                                                        TextMode="Email"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="style1">
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <asp:TextBox ID="Tbverifmail" runat="server" TextMode="Email" Height="31px" Width="404px"
                                                        required="" placeholder="Confirmer E-mail"></asp:TextBox>
                                                </div>
                                                <asp:CompareValidator ID="cvEmail" runat="server" ControlToValidate="Tbverifmail"
                                                    ControlToCompare="Tbmail" Operator="Equal" ErrorMessage="Veuillez verifier votre e-mail"
                                                    ForeColor="Red" Display="Dynamic"> </asp:CompareValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="style1">
                                        <tr>
                                            <td class="style4">
                                                <div class="form-group">
                                                    Civilité</div>
                                            </td>
                                            <td class="style4">
                                                <div class="form-group">
                                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" Height="30px" RepeatDirection="Horizontal"
                                                        Width="250px" CellPadding="-1">
                                                        <asp:ListItem Selected="True" Value="M"> M.</asp:ListItem>
                                                        <asp:ListItem Value="F">Mme</asp:ListItem>
                                                        <asp:ListItem Value="F">Mlle</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="style1">
                                        <tr>
                                            <td class="style5">
                                                <div class="form-group">
                                                    Date de Naissance</div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <asp:TextBox ID="Tbdatenai" runat="server" Height="31px" Width="190px" required=""
                                                        TextMode="Date"></asp:TextBox>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="style1">
                                        <tr>
                                            <td class="style4">
                                                <div class="form-group">
                                                    <asp:TextBox ID="Tblieunaiss" runat="server" TextMode="SingleLine" Height="31px"
                                                        Width="403px" required="" placeholder="Lieu de naissance"></asp:TextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="style1">
                                        <tr>
                                            <td class="style4">
                                                <div class="form-group">
                                                    <asp:TextBox ID="TextBox2" runat="server" TextMode="SingleLine" Height="31px" Width="403px"
                                                        required="" placeholder="Votre fonction"></asp:TextBox>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <table class="style1">
                                        <tr>
                                            <td class="style4">
                                                <asp:Label ID="Label8" runat="server" Text="Nationalité"></asp:Label>
                                                <div class="form-group">
                                                    <asp:DropDownList ID="DropDownList4" runat="server" CssClass="dropdown-toggle" dir="ltr"
                                                        Height="31px" required="true" Width="200px" 
                                                        >
                                                        <asp:ListItem Value="99">Tunisienne</asp:ListItem>
                                                        <asp:ListItem Value="01">Algerienne</asp:ListItem>
                                                        <asp:ListItem Value="24">Andoranne</asp:ListItem>
                                                        <asp:ListItem Value="02">Beninoise</asp:ListItem>
                                                        <asp:ListItem Value="03">Burkinabé</asp:ListItem>
                                                        <asp:ListItem Value="05">Centre africaine</asp:ListItem>
                                                        <asp:ListItem Value="04">Camerounaise</asp:ListItem>
                                                        <asp:ListItem Value="06">Comorienne</asp:ListItem>
                                                        <asp:ListItem Value="07">Congolaise</asp:ListItem>
                                                        <asp:ListItem Value="08">Djiboutienne</asp:ListItem>
                                                        <asp:ListItem Value="26">Française</asp:ListItem>
                                                        <asp:ListItem Value="09">Gabonaise</asp:ListItem>
                                                        <asp:ListItem Value="10">Guinienne</asp:ListItem>
                                                        <asp:ListItem Value="13">Ivoirienne</asp:ListItem>
                                                        <asp:ListItem Value="14">Malienne</asp:ListItem>
                                                        <asp:ListItem Value="15">Marocaine</asp:ListItem>
                                                        <asp:ListItem Value="16">Mauritanienne</asp:ListItem>
                                                        <asp:ListItem Value="17">Nigerienne</asp:ListItem>
                                                        <asp:ListItem Value="18">Palestinienne</asp:ListItem>
                                                        <asp:ListItem Value="19">Sénégalaise</asp:ListItem>
                                                        <asp:ListItem Value="20">Syrienne</asp:ListItem>
                                                        <asp:ListItem Value="21">Tchadienne</asp:ListItem>
                                                        <asp:ListItem Value="22">Togolaise</asp:ListItem>
                                                        <asp:ListItem Value="23">Yemenite</asp:ListItem>
                                                        <asp:ListItem Value="12">Irakienne</asp:ListItem>
                                                        <asp:ListItem Value="11">Indienne</asp:ListItem>
                                                        <asp:ListItem Value="00">--Autre--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </td>
                                            <td>
                                                Num Téléphone
                                                <div class="form-group">
                                                    <table class="style1">
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="TextBox1" MaxLength="4" runat="server" Height="31px" Width="50px" placeholder="code" ></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="Tbphone" runat="server" Height="31px" Width="140px" required="" 
                                                                    placeholder="Num Tel"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Tbphone"
                                                    ErrorMessage="Numéro invalide" Display="Dynamic" ForeColor="Red" ValidationExpression="[0-9]{8}$"> </asp:RegularExpressionValidator>
                                </div>
                                </td> </tr> </table>
                                <table class="style1">
                                    <tr>
                                        <td class="style6">
                                            <div class="form-group">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                        </td>
                                        <td class="style2">
                                            <div class="form-group">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="style1">
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="style1">
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputEmail2">
                                                    Email address</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">
                                                    Password</label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="span2">
                            </div>
                            <div class="span5">
                                <table class="style1">
                                    <tr>
                                        <td class="style4">
                                            <div class="form-group">
                                                <asp:DropDownList ID="DropDownList5" runat="server" Height="31px"
                                                AutoPostBack="true"
                                                 Width="202px" required="" 
                                                   >
                                                    <asp:ListItem Value="V">Choisir num cin/passeport</asp:ListItem>
                                                   
                                                    <asp:ListItem Value="C">Cin</asp:ListItem>
                                                    <asp:ListItem Value="P">Passeport</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </td>

                                        <td>
                                            <div class="form-group">
                                                <asp:TextBox ID="TbNUM_CIN_PASSEPORT" runat="server" Height="31px" Width="190px"
                                                    required="" placeholder="Numéro CIN" MaxLength="8" Visible="false"  > </asp:TextBox>

                                                     <asp:TextBox ID="tbpassport" runat="server" Height="31px" Width="190px"
                                                    required="" placeholder="Numéro Passeport" Visible="false" > </asp:TextBox>
                                            </div>

                                            <asp:RegularExpressionValidator ID="Reg" runat="server" 
                                            ControlToValidate="TbNUM_CIN_PASSEPORT"
                                                ErrorMessage="Numéro invalide" Display="Dynamic" 
                                                 
                                                ForeColor="Red" ValidationExpression="^[A-Z0-9]+$"> </asp:RegularExpressionValidator>

                                                <asp:RegularExpressionValidator 
                                                ID="Reg3" runat="server" ControlToValidate="tbpassport"

                                                ErrorMessage="Numéro invalide" Display="Dynamic" 
                                                 
                                                ForeColor="Red" ValidationExpression="^[A-Z0-9]+$"> </asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                                <table class="style1">
                                    <tr>
                                        <td class="style8">
                                            <div class="form-group">
                                                Date de délivrance</div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <asp:TextBox ID="Tbdateliver" runat="server" TextMode="Date" Height="31px" Width="190px"
                                                    required=""></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="style1">
                                    <tr>
                                        <td class="style4">
                                            <div class="form-group">
                                                Lieu de délivrance</div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <asp:TextBox ID="Tbdelivrea" runat="server" TextMode="SingleLine" Height="31px" Width="190px"
                                                    required=""></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <table class="style1">
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <asp:TextBox ID="Tbadress_et" runat="server" TextMode="MultiLine" Width="401px" Height="50px"
                                                    placeholder="Adresse à Tunis"></asp:TextBox>
                                            </div>
                                        </td>
                                        <%--  <td>
                    
                    <div class="form-group">
    
  </div>--%></td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <br />
                                <br />
                                <table class="style1">
                                    <tr>
                                        <td class="style5" style="height: 38px">
                                            <div class="form-group">
                                            </div>
                                        </td>
                                        <td class="style5" style="height: 38px">
                                            <div class="form-group">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <br />
                                <table class="style1">
                                    <tr>
                                        <td class="style10">
                                            <div class="form-group">
                                                <asp:TextBox ID="Tbcodepostal" runat="server" Height="31px" Width="199px" placeholder="Code postal" MaxLength="4"></asp:TextBox>
                                            </div>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Tbcodepostal"
                                                ErrorMessage="Numéro invalide" Display="Dynamic" ForeColor="Red" ValidationExpression="[0-9]+$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td class="style9">
                                            <div class="form-group">
                                                <asp:TextBox ID="Tbville" runat="server" TextMode="SingleLine" Height="31px" Width="190px"
                                                    placeholder="Ville"></asp:TextBox>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="style1">
                                    <tr>
                                        <td class="style4">
                                            <div class="form-group">
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <table class="style1">
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputEmail2">
                                                    Email address</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">
                                                    Password</label>
                                            </div>
                                        </td>
                                    </tr>
                          </table>
                              <center>
                                 <table>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <asp:Button ID="Button2" runat="server" 
                                                CssClass="form-control" 
                                                Text="Valider" Width="150px"
                                                    Height="50px" AutoPostBack="True" />
              
                                                   
                                            </div>
                                     </td>

                                         <%-- modifier--%>

                                       <%--<td >
                                         <div class="form-group">
                                         <asp:Button ID="BtnModif" runat="server" 
                                                CssClass="form-control" 
                                                Text="Modifier" Width="150px"
                                                    Height="50px" AutoPostBack="True" onclick="BtnModif_Click" />
                                                    </div>
                                        </td>--%>

                                        <td >
                                         <div class="form-group">
                                         <asp:Button ID="Button1" runat="server" 
                                                CssClass="form-control" 
                                                Text="Retour" Width="150px"
                                                    Height="50px" AutoPostBack="True" />
                                                    </div>
                                        </td>




                                   

                                        <td>
                                            <div class="form-group">
                                                <label class="sr-only" for="exampleInputPassword2">
                                                    Password</label>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                                
                              </center>
                             
                                  
                                
                                
                               
                            </div>
                        </div>
                    </div>
                    <%--</div></div></div>--%>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
         
    <asp:Label ID="Label111" runat="server" Text="Inscription avec succée" Visible="false" ></asp:Label>

         <%--<p class="text-center">
                    © 2016 Esprit Entreprise, Inc. · <a href="#">Privacy</a> · <a href="#">Terms</a></p>
--%>
        <p class="indication"> Les champs avec  <span class="required">*</span> sont requis</p>
       
        
    </div>
   </asp:Content>
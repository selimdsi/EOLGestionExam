﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CahierDeTextes_2016.aspx.cs"
 Inherits="ESPOnline.Enseignants.CahierDeTextes_2016"MasterPageFile="~/Enseignants/Ens.Master" %>


 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">



<style type="text/css">


.custom-calendar .ajax__calendar_container   
{
    background-color:#ffc; /* pale yellow */ border:solid 1px #666;z-index : 1000 ; 
    font-family:arial,helvetica,clean,sans-serif;
 
    } 
 
.custom-calendar .ajax__calendar_title {  background-color:#cf9; /* pale green */ height:20px;  color:#333; } 
 
.custom-calendar .ajax__calendar_prev, .custom-calendar .ajax__calendar_next {  background-color:#aaa; /* darker gray */ height:20px;  width:20px;
} 
 
.custom-calendar .ajax__calendar_day {  color:#333; /* normal day - darker gray color */} .custom-calendar .ajax__calendar_other .ajax__calendar_day {  color:#666; /* day not actually in this month - lighter gray color */} 
 
.custom-calendar .ajax__calendar_today {  background-color:#cff;  /* pale blue */ height:20px; }

</style>
<style type="text/css">

.btn {
  background: #d93434;
  background-image: -webkit-linear-gradient(top, #d93434, #313c42);
  background-image: -moz-linear-gradient(top, #d93434, #313c42);
  background-image: -ms-linear-gradient(top, #d93434, #313c42);
  background-image: -o-linear-gradient(top, #d93434, #313c42);
  background-image: linear-gradient(to bottom, #d93434, #313c42);
  -webkit-border-radius: 28;
  -moz-border-radius: 28;
  border-radius: 28px;
  font-family: Arial;
  color: #ffffff;
  font-size: 20px;
  padding: 10px 20px 10px 20px;
  text-decoration: none;
}

.btn:hover {
  background: #fc3c3c;
  background-image: -webkit-linear-gradient(top, #fc3c3c, #d97634);
  background-image: -moz-linear-gradient(top, #fc3c3c, #d97634);
  background-image: -ms-linear-gradient(top, #fc3c3c, #d97634);
  background-image: -o-linear-gradient(top, #fc3c3c, #d97634);
  background-image: linear-gradient(to bottom, #fc3c3c, #d97634);
  text-decoration: none;
}
</style>
    <link href="../Contents/Css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap.js" type="text/javascript"></script>
        <script type="text/javascript" src="../Contents/Scripts/JScript1.js"></script>
        <style type="text/css">
        
        
        .grise {
  border:1px solid black; 
  background-color:silver;
}

.blue {
  border:1px solid blue; 
}

.none {
  border-style:none;
}
        
        
        </style>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript">
            $("[src*=plus]").live("click", function () {
                $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                $(this).attr("src", "../EnseignantsCUP/Styles/minus.png");
            });
            $("[src*=minus]").live("click", function () {
                $(this).attr("src", "../EnseignantsCUP/Styles/plus.png");
                $(this).closest("tr").next().remove();
            });
        </script>
              
         <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: Black;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}
 #hurfDurf table
 .Gridstudent
    {
      
        margin-right:80px;
    }
      
          </style>

         <%-- style row color chnged--%>
         <style type="text/css">
  .successMerit {
    background-color: #1fa756;
    border: medium none;
    color: White;
  }
    .defaultColor
    {
        background-color: white;
        color: black;
    }
  .dangerFailed {
    background-color: #f2283a;
    color: White;
  }
             </style>


         <style type="text/css"> 
.styled-button-10 {
	background:#8cce57;
	background:-moz-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#5CCD00),color-stop(100%,#4AA400));
	background:-webkit-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-o-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-ms-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#5CCD00', endColorstr='#4AA400',GradientType=0);
	padding:10px 15px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:16px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #459A00
}
             .style13
             {
                 color: #660033;
                 font-size: medium;
             }
             .style15
             {
                 width: 161px;
             }
             .style16
             {
                 color: #800000;
             }
             .style17
             {
                 color: #660033;
             }
         </style>
 

 <link media="screen" href="../Contents/Cahier.css" type="text/css" rel="stylesheet">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br /><br /><br />
<br />
<br />


<div id="page" class="espace_enseignant">
<div id="header">
  <p>&nbsp;</p>
    <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" >
   
    <tr>
      <td valign="middle"><div align="center">
                    
                </div></td>
      <td ><div align="center">

          <span align="center">
  
               <%--<img src="../images/Composition-1.gif" alt="ss" width="600px" height="80px" />--%>

          </span>
           
        <%--<div class="header_description">
          <div align="center">
                            
          </div>
        </div>--%>
      </div>
  </div>
<p class="erreur">
</p>
<hr/>
	<script language="JavaScript" type="text/JavaScript">

	    function formfocus() {
	        document.form2.passe.focus()
	        document.form2.passe.select()
	    }
	</script>
<form method="post" name="form2" id="form2" action="">
<input type='hidden' name='md5' />
<asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>
<asp:UpdatePanel ID="tempcl" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<table width="92%" border="0" align="center" cellpadding="0" cellspacing="0" class="espace_enseignant">
<tr><td class="style15"><br /></td><td><br /></td></tr>
<tr><td class="style15"><br /></td><td><br /></td></tr>
<tr>
            <td class="style2">
                
                </td>
            <td class="style5">
            
                                                        <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatDirection="Horizontal"
                                                            AutoPostBack="True" >
                                                            <asp:ListItem Value="1" >Semestre 1</asp:ListItem>
                                                            <asp:ListItem Value="2" Selected="True">Semestre 2</asp:ListItem>
                                                       
                                                        </asp:RadioButtonList>
                                                                    </td>
           
        </tr>

<tr>
          <td class="style2">
          <asp:Label ID="lblll" runat="server" Text="Date séance :" CssClass="style13" ></asp:Label>
              
          </td>
    
          <td align="left">
         <asp:TextBox ID="TBdateseance" runat="server"  Width= "150px" ></asp:TextBox>
                <asp:CalendarExtender ID="TBdateseance_CalendarExtender" runat="server" 
                     Format="dd/MM/yyyy" TargetControlID="TBdateseance" CssClass="custom-calendar">
                </asp:CalendarExtender>
          </td>
         
        </tr>

       <tr><td><br /></td><td><br /></td></tr>
<tr>
<td class="style15">
<asp:Label ID="Label3" runat="server" Text="Horaire de travail:" 
        CssClass="style13" ></asp:Label></td><td align="left">

        <span class="style16">
        De</span>:<asp:DropDownList runat="server" ID="ddlcren1" AutoPostBack="true"
         DataTextField="lib_nome" AppendDataBoundItems="true"
    DataValueField="code_nome" onselectedindexchanged="ddlcren1_SelectedIndexChanged">
    
    </asp:DropDownList> &nbsp; <span class="style16">À</span> 
    <asp:DropDownList runat="server" ID="ddlcren2" DataTextField="lib_nome" AutoPostBack="true"
            DataValueField="code_nome" AppendDataBoundItems="true" 
            onselectedindexchanged="ddlcren2_SelectedIndexChanged">
    
    </asp:DropDownList>
        &nbsp;&nbsp;<span class="style17">Durée</span>
      <asp:DropDownList runat="server" ID="ddlduree" AppendDataBoundItems="true" 
            AutoPostBack="true" onselectedindexchanged="ddlduree_SelectedIndexChanged">
    <asp:ListItem Value="0">Veuillez choisir</asp:ListItem>
    <asp:ListItem Value="1.5">Une heure et demi</asp:ListItem>
    <asp:ListItem Value="3">Trois heures</asp:ListItem>
    </asp:DropDownList>

    </td>
</tr>
<tr><td class="style15"></td><td><br /></td></tr>
<tr>
<td class="style15">
<asp:Label ID="lblclasse" runat="server" Text="Votre classe:" 
        CssClass="style13" ></asp:Label></td><td align="left">
       
<asp:DropDownList ID="ddlclasses" runat="server" AutoPostBack="true" 
        
        onselectedindexchanged="ddlclasses_SelectedIndexChanged" Width= "150px" Height="30px">

</asp:DropDownList></td>
</tr>
<tr><td class="style15"><br /></td><td><br /></td></tr>
<tr>
<td class="style15">
<asp:Label ID="Label1" runat="server" Text="Matiére:" CssClass="style13" ></asp:Label></td>
<td align="left">

<asp:DropDownList Width= "150px" Height="30px" ID="ddlmodule" runat="server"  
AppendDataBoundItems="true" DataTextField="DESIGNATION" 
            DataValueField="CODE_MODULE"  >
            <asp:ListItem Value="0">Veuillez choisir</asp:ListItem>
</asp:DropDownList>
</td>
</tr>
<tr><td class="style15"><br /><br /></td>
    <td><br /></td></tr>
<tr>

<td class="style15">

<asp:Label ID="lblmessage" runat="server" Text="Contenu traité" CssClass="style13"></asp:Label>
    </td>
<td align="left">
<asp:TextBox ID="txtmessage" runat="server" TextMode="MultiLine" Width="500px" Height="120px"></asp:TextBox>
</td>
</tr>

<tr><td><br /></td><td><br /></td></tr>
<tr>

<td class="style15">

<asp:Label ID="Label2" runat="server" Text="Remarques:" CssClass="style13"></asp:Label>
    </td>
<td align="left">
<asp:TextBox ID="txtremarque" runat="server" TextMode="MultiLine" Width="500px" Height="80px"></asp:TextBox>
</td>
</tr>

    <tr><td><br /></td><td><br /></td></tr>
    <tr><td></td><td align="center">  <asp:Button  ID="btnvalid" runat="server" Text="Valider" CssClass="btn" 
        onclick="btnvalid_Click"/></td></tr>
  <tr><td><br /></td><td><br /></td></tr><tr><td><br /></td><td><br /></td></tr>
</table>
</ContentTemplate>

</asp:UpdatePanel>
</form>

<br />
<br />

<br />
<br />

<br />


</asp:Content>
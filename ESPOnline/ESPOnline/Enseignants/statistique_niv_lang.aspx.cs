﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Enseignants
{
    public partial class statistique_niv_lang : System.Web.UI.Page
    {
        StatService service = new StatService();
        ToiecService code = new ToiecService();
        string code_cl;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }
            lblclasse.Visible = false;
            maha.Visible = false;
        }

        public void bindNBR()
        {
            Gridtoiec.DataSource = service.GetNbre_niv_langue(ddlchoix.Text);
            Gridtoiec.DataBind();

        }

        protected void gridViewtoiec_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Gridtoiec.PageIndex = e.NewPageIndex;
            Gridtoiec.DataBind();
            Gridtoiec.DataSource = service.GetNbre_niv_langue(ddlchoix.SelectedValue);
            Gridtoiec.DataBind();
        }

        protected void ddlchoix_SelectedIndexChanged(object sender, EventArgs e)
        {
            code_cl = service.returnCode_cl() ;

            if (code_cl.StartsWith("1"))
            {
                bindNBR();
                lblclasse.Visible = true;
                maha.Visible = true;
                lblclasse.Text = ddlchoix.SelectedValue;

            }
            else
                if (code_cl.StartsWith("2"))
                {
                    bindNBR();
                    lblclasse.Visible = true;
                    maha.Visible = true;
                    lblclasse.Text = ddlchoix.SelectedValue;
                }
                else
                    if (code_cl.StartsWith("3"))
                    {
                        bindNBR();
                        lblclasse.Visible = true;
                        maha.Visible = true;
                        lblclasse.Text = ddlchoix.SelectedValue;
                    }
                    else
                        if (code_cl.StartsWith("4"))
                        {
                            bindNBR();
                            lblclasse.Visible = true;
                            maha.Visible = true;
                            lblclasse.Text = ddlchoix.SelectedValue;
                        }
                        else
                            if (code_cl.StartsWith("5"))
                            {
                                bindNBR();
                                lblclasse.Visible = true;
                                maha.Visible = true;
                                lblclasse.Text = ddlchoix.SelectedValue;
                              
                            }

        }

        protected void Gridtoiec_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Enseignants
{
    public partial class Add_project2017 : System.Web.UI.Page
    {
        ServiceEncadrement service = new ServiceEncadrement();
        string ID_ENS;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UP"] == null || Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

            ID_ENS = Session["ID_ENS"].ToString();

            if (!IsPostBack)
            {
                bindModule();
                bindmethod();
                bindtypeprojet();
                bindtechnologie();
            }
        }


        public void bindModule()
        {


            ddlmodule.DataTextField = "DESIGNATION";
            ddlmodule.DataValueField = "CODE_MODULE";
            ddlmodule.DataSource = service.Getmodule();
            ddlmodule.DataBind();
        }

        public void bindmethod()
        {


            ddlmethodologie.DataTextField = "LIB_NOME";
            ddlmethodologie.DataValueField = "CODE_NOME";
            ddlmethodologie.DataSource = service.GetMethodologie();
            ddlmethodologie.DataBind();
        }

        public void bindtypeprojet()
        {


            ddltypeprojet.DataTextField = "LIB_NOME";
            ddltypeprojet.DataValueField = "CODE_NOME";
            ddltypeprojet.DataSource = service.Gettypeprojet();
            ddltypeprojet.DataBind();
        }

        public void bindtechnologie()
        {


            ddltechnologie.DataTextField = "LIB_NOME";
            ddltechnologie.DataValueField = "CODE_NOME";
            ddltechnologie.DataSource = service.Gettechnologie();
            ddltechnologie.DataBind();
        }



        protected void ddlmatricule_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            //ajouter un rapport
           

        }

        protected void Btnadd_Click(object sender, EventArgs e)
        {
            ID_ENS = Session["ID_ENS"].ToString();
            string an = service.get_an();
            string x = service.inc_id_projet();
            decimal xx = Convert.ToDecimal(x) + 1;

            String _ID_PROJET = "PROJ_17_" + xx;


            try
            {
                service.insert_Project(_ID_PROJET, an, Tbnom.Text, Tbadress_et.Text, ddltechnologie.SelectedValue, ddlmethodologie.SelectedValue, ddltypeprojet.SelectedValue, ddlduree.SelectedValue, ddlmodule.SelectedValue, ID_ENS, "2", "2");


                //string message = "alert('AJOUT AVEC SUCCEÉ')";
                //ScriptManager.RegisterClientScriptBlock((sender as Control), this.GetType(), "alert", message, true);
                Response.Redirect("~/Enseignants/Server_Page.aspx");

            }
            catch
            {
                Response.Write(@"<script language='javascript'>alert('ERREUR DE SERVEUR');</script>");

            }
        }

        protected void ad_Click(object sender, EventArgs e)
        {
            Response.Write(@"<script language='javascript'>alert('ERREUR DE SERVEUR');</script>");

        }


    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Enseignants
{
    public partial class SaisirCahietTexte_2016 : System.Web.UI.Page
    {
        CahietTextService service = new CahietTextService();

       string id_ens;
       protected void Page_Load(object sender, EventArgs e)
       {
           if (Session["UP"] == null || Session["ID_ENS"] == null)
           {
               Response.Redirect("~/Online/default.aspx");
           }


           if (!IsPostBack)
           {
               id_ens = Session["ID_ENS"].ToString();
               tempcl.Update();
               bindniv_cl();
               bindcrennomenclature();
           }
       }
        public void bindniv_cl()
       {
           id_ens = Session["ID_ENS"].ToString();
            ddlclasses.DataTextField = "code_cl";
            ddlclasses.DataValueField = "code_cl";

           // ddlclasses.DataSource = service.bind_cdclens(id_ens);

            ddlclasses.DataBind();

            ddlclasses.Items.Insert(0, new ListItem("Veuillez choisir", "0"));
            ddlclasses.SelectedItem.Selected = false;
           // ddlclasses.Items.FindByText("Veuillez choisir").Selected = true;
            tempcl.Update();
        }

        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            //TextBox1.Text = Calendar1.SelectedDate.ToString();
        }

        protected void ddlclasses_SelectedIndexChanged(object sender, EventArgs e)
        {
            id_ens = Session["ID_ENS"].ToString();
            ddlmodule.DataTextField = "designation";
            ddlmodule.DataValueField = "designation";

            ddlmodule.DataSource = service.bind_module(id_ens,ddlclasses.SelectedValue,Convert.ToInt32("2"));

            ddlmodule.DataBind();

            ddlmodule.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            ddlmodule.SelectedItem.Selected = false;
           // ddlmodule.Items.FindByText("Veuillez choisir").Selected = true;
            tempcl.Update();
        }


        public void bindcrennomenclature()

        {
            ddlcren1.DataTextField = "lib_nome";
            ddlcren1.DataValueField = "lib_nome";
            ddlcren2.DataTextField = "lib_nome";
            ddlcren2.DataValueField = "lib_nome";

            ddlcren1.DataSource = service.bind_CRENEAU();
            ddlcren1.DataBind();
            ddlcren2.DataSource = service.bind_CRENEAU();
            ddlcren2.DataBind();

            ddlcren1.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            ddlcren1.SelectedItem.Selected = false;
           // ddlcren1.Items.FindByText("Veuillez choisir").Selected = true;
            tempcl.Update();

            ddlcren2.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            ddlcren2.SelectedItem.Selected = false;
            //ddlcren2.Items.FindByText("Veuillez choisir").Selected = true;
            tempcl.Update();
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BLL;

namespace ESPOnline.Enseignants
{
    public partial class AjoutPEncadv0 : System.Web.UI.Page
    {
        ServiceEncadrement service = new ServiceEncadrement();
        string ID_ENS;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UP"] == null || Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

            ID_ENS = Session["ID_ENS"].ToString();

            if (!IsPostBack)
            {

                string an = service.get_an();
                //Gridexam.DataSource = service.getlistProjet(an);
                //Gridexam.DataBind();
                Gridstudent.DataSource = service.bindlsprof();
                Gridstudent.DataBind();

                ddlprojet.DataTextField = "nom_projet";
                ddlprojet.DataValueField = "id_projet";
                ddlprojet.DataSource=service.getlistProjet(an,ID_ENS);
                ddlprojet.DataBind();

                ddlstudentt.DataTextField = "nom";
                ddlstudentt.DataValueField = "id_et";
                ddlstudentt.DataSource = service.getliststudents(an);
                ddlstudentt.DataBind();


                

            }
          
        }

        protected void btnOK_Click1(object sender, ImageClickEventArgs e)
        {

        }

        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        { }

        protected void Gridexam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void GrdEmpData_RowEditing(object sender, GridViewEditEventArgs e)
        { }

        protected void GrdEmpData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
        }


        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            //switch back to default mode
            Gridstudent.EditIndex = -1;
            //Bind the grid
            string an = service.get_an();
            Gridstudent.DataSource = service.getlistProjet(an,ID_ENS);
            Gridstudent.DataBind(); 
        }

        protected void ddlprojet_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
        {
            string an = service.get_an();
            ddltype.DataTextField = "lib_nome";
            ddltype.DataValueField = "type_projet";
            ddltype.DataSource = service.gettypeprojet(an, ddlprojet.SelectedValue);
            ddltype.DataBind();
        }

        protected void btnoki_Click(object sender, EventArgs e)
        {
            //insert into esp_projet_etudiant
            try
            {
                service.affecter_project(ddlprojet.SelectedValue, ddlstudentt.SelectedValue);
                Gridstudent.Visible = true;
                Response.Write(@"<script language='javascript'>alert('AJOUT AVEC SUSSEÉ');</script>");


            }
            catch
            {
                Response.Write(@"<script language='javascript'>alert('ERREUR DE SERVEUR');</script>");

            }
        }

    }
}
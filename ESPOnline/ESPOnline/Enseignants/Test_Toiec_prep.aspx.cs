﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;

namespace ESPOnline.Enseignants
{
    public partial class Test_Toiec_prep : System.Web.UI.Page
    {
        string nbenregtoiec;
        string nbenregtpreptoiec;
        string id_ens;
        ToiecService service = new ToiecService();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

                lblanneedeb.Text = service.getANNEEDEBs();

                lblanneefin.Text = service.getAnneeFiN();
                nbenregtoiec = service.countNB_TOIEC();
                nbenregtpreptoiec = service.countNBPrep_TOIEC();
                id_ens = Session["ID_ENS"].ToString();
                string veriflabeltoeic = service.selectEtatTTOIECenseign(id_ens);
                string veriflabelprepTOEIC = service.selectPreparationEtatTTOIECenseign(id_ens);

                panelmsg.Visible = false;
                if (!IsPostBack)
                {

                    lblcountpreptoiec.Text = nbenregtpreptoiec;

                    lblcounttoiec.Text = nbenregtoiec;

                    int nbtoiec = Convert.ToInt32(nbenregtoiec);

                    int nbprep = Convert.ToInt32(nbenregtpreptoiec);

                    if (veriflabeltoeic == "O" || veriflabelprepTOEIC == "O")
                    {
                        Response.Write(@"<script language='javascript'>alert('Vous êtes déjà inscrit');</script>");
                        Panelfrang.Visible = false;


                    }
                    else
                    {
                        if (nbtoiec < 10 && nbprep < 10)
                        {
                            Panelfrang.Visible = true;
                        }

                        if (nbtoiec < 10 || nbprep < 10)
                        {
                            if (nbtoiec < 10)
                            {
                                paneltoiec.Visible = true;
                                Panelfrang.Visible = true;
                            }
                            else
                            {
                                paneltoiec.Visible = false;
                                chkTOIEC.Enabled = false;
                                //Panelfrang.Visible = true;
                                panelmsg.Visible = true;
                                lblchoix.Visible = false;
                                // Label2.Text = "le nombre est atteint 300 candidats au test toiec";
                                Response.Write(@"<script language='javascript'>alert('le nombre est atteint 300 candidats au certif toeic');</script>");
                            
                            }
                            if (nbprep < 10)
                            {
                                panelprep.Visible = true;
                                Panelfrang.Visible = true;

                            }
                            else
                            {
                                //Panelfrang.Visible = true;
                                panelprep.Visible = false;
                                panelmsg.Visible = true;
                                lblchoixprep.Visible = false;
                                //Label2.Text = "le nombre est atteint 300 candidats au test PREPARATION toiec";
                                Response.Write(@"<script language='javascript'>alert('le nombre est atteint 300 candidats au  prep certification toeic,passer certif toeic');</script>");

                            }

                        }
                        else
                        {
                            panelmsg.Visible = false;
                            Panelfrang.Visible = false;
                            Response.Write(@"<script language='javascript'>alert('Session fermée,le nombre est atteint 300 candidats dans les deux certifs toeic et preparation toeic');</script>");

                        }
                    }

                }

                    }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (chkTOIEC.Checked)
            {
                service.UpdatETOICenseign(id_ens);
                lbltoiec.Text = "Vous êtes inscrit au test toeic";
                paneltoiec.Visible = false;
                Panelfrang.Visible = false;
                panelmsg.Visible = true;

            }
            else
            {
                service.UpdatETOICToNoenseig(id_ens);
                lbltoiec.Text = "Vous n'êtes pas inscrit au test toeic";

                paneltoiec.Visible = false;
                Panelfrang.Visible = false;
                panelmsg.Visible = true;
            }


            if (chkprepTOIEC.Checked)
            {
                service.UpdatEPreparationTOICenseign(id_ens);
                lblpreptoiec.Text = "Vous êtes inscrit au test prep toeic";
                //plrep.Visible = true;
                // paneltest.Visible = false;
                panelprep.Visible = false;
                Panelfrang.Visible = false;
                panelmsg.Visible = true;

            }
            else
            {
                service.UpdatEPREPTOICToNoenseig(id_ens);
                lblpreptoiec.Text = "Vous n'êtes pas inscrit au test prep toeic";
                //plrep.Visible = true;
                //paneltest.Visible = false;
                panelprep.Visible = false;

                panelmsg.Visible = true;
                Panelfrang.Visible = false;

            }

        }
    }
}
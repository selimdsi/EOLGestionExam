﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Data;
namespace ESPOnline.Enseignants
{
    public partial class INSCRIT_FR_ANG_2015aspx : System.Web.UI.Page
    {
        ToiecService service = new ToiecService();
        string id_ens;
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UP"] == null || Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }

            
            lblanneedeb.Text = "2015";
            lblanneefin.Text = "2016";
            if (!IsPostBack)
            {
                panel1.Visible =false;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable(); ;
                id_ens = Session["ID_ENS"].ToString();
                dt = service.verifier(id_ens);

                if (dt.Rows.Count != 0)
                {

                    Response.Write(@"<script language='javascript'>alert('Vous êtes déjà inscrit!');</script>");
                    panelddr.Visible = false;
                    panel1.Visible = true;
                }
                else
                {
                    service.Enreg_etud_FORMATION_enseign(id_ens, ddlchoix.SelectedValue);
                    Response.Write(@"<script language='javascript'>alert('Vous êtes enregistré avec succès');</script>");


                    panelddr.Visible = false;
                    panel1.Visible = true;
                }
            }
           

            catch
            {
                Response.Write(@"<script language='javascript'>alert('Erreur de serveur');</script>");

            }
            
        }
    }
}
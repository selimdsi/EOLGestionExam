﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ESPSuiviEncadrement;
using Oracle.ManagedDataAccess.Types;

namespace ESPOnline.Enseignants
{
    public partial class encadrementGroupe : System.Web.UI.Page
    {
         
        string up;
        string id;
        string nom;
        string cup;
        protected void Page_Load(object sender, EventArgs e)
        {


            if (Session["UP"] == null || Session["ID_ENS"] == null)
            {
                Response.Redirect("~/Online/default.aspx");
            }
            up = Session["UP"].ToString();
            id = Session["ID_ENS"].ToString();
            nom = Session["NOM_ENS"].ToString();
            cup = Session["CUP"].ToString();
            //Panel3.Visible = false;
            if (!Page.IsPostBack)
            {
                List<Classes> cl = new List<Classes>();
                cl = Classes.GetList();
                DropDownList2.DataSource = cl;
                DropDownList2.DataTextField = "CODE_CL";
                DropDownList2.DataValueField = "CODE_CL";
                DropDownList2.DataBind();
                DropDownList3.Items.Clear();
                DropDownList5.Items.Clear();
                DropDownList2.Items.Insert(0, new ListItem("Choisir", "0"));
                DropDownList2.Items.Insert(1, new ListItem("All", "All"));
                DropDownList3.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList5.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList8.Items.Insert(0, new ListItem("N/A", "0"));
                LinkButton2.Visible = false;
                LinkButton3.Visible = false;
                LinkButton1.Visible = false;
                Panel3.Visible = false;
            }
            if (Session["SUIVI"] == "true")
            {
                Session["SUIVI"] = "false";
                PostSuivi();
            }
        }
        /*
         * Panel 1
         * 
         */
        //Classe
        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

            LinkButton3.Visible = false;
            LinkButton1.Visible = false;
            LinkButton2.Visible = false;
            Panel3.Visible = false;
            //PanelEncadrementGROUPE.Visible = false;
            IntiPanel3();

            if (DropDownList2.SelectedValue == "0")
            {
                DropDownList3.Items.Clear();
                DropDownList4.Items.Clear();
                DropDownList5.Items.Clear();
                DropDownList7.Items.Clear();
                DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList5.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList3.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList8.Items.Insert(0, new ListItem("N/A", "0"));
                //PanelEncadrementGROUPE.Visible = false;

            }
            else
            {
                if (DropDownList2.SelectedValue == "All")
                {
                    //EtudiantBind();
                    ModuleAllBind();
                    DropDownList3.Items.Clear();
                    DropDownList3.Items.Insert(0, new ListItem("N/A", "0"));

                    DropDownList4.Items.Clear();
                    DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));

                    if(DropDownList3.SelectedValue=="0")
                        PanelEncadrementGROUPE.Visible = false;
                    
                }
                else
                {
                    DropDownList3.Items.Clear();
                    DropDownList3.Items.Insert(0, new ListItem("N/A", "0"));
                    DropDownList4.Items.Clear();
                    DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));
                    DropDownList7.Items.Clear();
                    DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                    ModuleClassBind(DropDownList2.SelectedValue);

                    if (DropDownList3.SelectedValue == "0")
                        PanelEncadrementGROUPE.Visible = false;
                    
                    //EtudiantClassBind(DropDownList2.SelectedValue);

                }
            }
        }
        //Module
        protected void DropDownList5_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            LinkButton3.Visible = false;
            LinkButton1.Visible = false;
            LinkButton2.Visible = false;
            Panel3.Visible = false;
            PanelEncadrementGROUPE.Visible = false;
            IntiPanel3();

            DropDownList3.Items.Clear();
            DropDownList3.Items.Insert(0, new ListItem("N/A", "0"));
            DropDownList4.Items.Clear();
            DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));
            DropDownList7.Items.Clear();
            DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
            if (DropDownList5.SelectedValue == "0")
            {
                DropDownList3.Items.Clear();
                DropDownList3.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList4.Items.Clear();
                DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList7.Items.Clear();
                DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                //PanelEncadrementGROUPE.Visible = false;

                if (DropDownList3.SelectedValue != "0")
                    PanelEncadrementGROUPE.Visible = true;
                else
                    PanelEncadrementGROUPE.Visible = false;
            }
            else
            {
                if (DropDownList2.SelectedValue != "0" && DropDownList5.SelectedValue != "0")
                {
                    EtudiantClassBind(DropDownList2.SelectedValue);
                    //PanelEncadrementGROUPE.Visible = true;

                    if (DropDownList3.SelectedValue != "0")
                        PanelEncadrementGROUPE.Visible = true;
                    else
                        PanelEncadrementGROUPE.Visible = false;
                }
                if (DropDownList2.SelectedValue == "All" && DropDownList5.SelectedValue != "0")
                {
                    EtudiantBind();
                    //PanelEncadrementGROUPE.Visible = true;

                    if (DropDownList3.SelectedValue != "0")
                        PanelEncadrementGROUPE.Visible = true;
                    else
                        PanelEncadrementGROUPE.Visible = false;
                }
            }
        }
        //Etudiant
        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            LinkButton3.Visible = false;
            LinkButton1.Visible = false;
            LinkButton2.Visible = false;
            LinkButton4.Visible = false;
            LinkButton5.Visible = false;
            PanelProjet.Visible = false;
            PanelEncadrementGROUPE.Visible = false;

            Panel3.Visible = false;
            IntiPanel3();

            if (DropDownList3.SelectedValue == "0")
            {
                DropDownList4.Items.Clear();
                DropDownList7.Items.Clear();
                DropDownList4.Items.Insert(0, new ListItem("N/A", "0"));
                DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                PanelProjet.Visible = false;
            }
            else
            {
                DropDownList7.Items.Clear();
                DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                Label21.Text = Convert.ToString(EtudiantClasses.Instance.getNiveauEtudiant(DropDownList3.SelectedValue.ToString()));
                DropDownList4.Items.Clear();
                TypeProjetBind();
                PanelProjet.Visible = true;
            }

            if (RadioButtonList4.SelectedValue == "group")
            {
                string ID_ET = DropDownList3.SelectedValue.ToString().Trim();
                
                bool status = false;
                ESPSuiviEncadrement.EtudiantClasses.Instance.openconntrans();
                string type_projet = DropDownList4.SelectedValue.ToString().Trim();
                status = ESPSuiviEncadrement.EtudiantClasses.Instance.statusEtudiantaffecter(ID_ET,type_projet);
                ESPSuiviEncadrement.EtudiantClasses.Instance.closeConnection();
                Label36.Text = Convert.ToString(GroupeProjet.Instance.getNomEtudiant(DropDownList3.SelectedValue.ToString()));
                if (status == false)
                {
                    LabelAffectationEtudiant.Text = "n'est pas affecter";
                    LabelAffectationEtudiant.CssClass = "control-label text-danger";
                    LinkButton4.Visible = true;
                }
                else
                {
                    LabelAffectationEtudiant.Text = "dèjà affecter";
                    LabelAffectationEtudiant.CssClass = "control-label text-success";
                    LinkButton4.Visible = false;
                }
            }
            else
            {
                LabelAffectationEtudiant.Visible = false;
                LabelStatusEtudiant.Visible = false;
            }
        }

        private void ModuleAllBind()
        {
            List<Modules> mod = new List<Modules>();
            mod = Modules.GetListAllModules();
            DropDownList5.DataSource = mod;
            DropDownList5.DataTextField = "DESIGNATION";
            DropDownList5.DataValueField = "CODE_MODULE";
            DropDownList5.DataBind();
            DropDownList5.Items.Insert(0, new ListItem("Choisir", "0"));
        }

        protected void EtudiantBind()
        {
            List<EtudiantClasses> ls = new List<EtudiantClasses>();
            ls = EtudiantClasses.GetList();
            DropDownList3.DataSource = ls;
            DropDownList3.DataTextField = "NOM_ET";
            DropDownList3.DataValueField = "ID_ET";
            DropDownList3.DataBind();
            DropDownList3.Items.Insert(0, new ListItem("Choisir", "0"));
        }

        protected void EtudiantClassBind(string codecl)
        {
            List<EtudiantClasses> ls = new List<EtudiantClasses>();
            ls = EtudiantClasses.GetListEtudiantClasse(codecl);
            DropDownList3.DataSource = ls;
            DropDownList3.DataTextField = "NOM_ET";
            DropDownList3.DataValueField = "ID_ET";
            DropDownList3.DataBind();
            DropDownList3.Items.Insert(0, new ListItem("Choisir", "0"));
        }

        
        protected void ModuleClassBind(string codecl)
        {
            List<Modules> ls = new List<Modules>();
            ls = Modules.GetList(codecl);
            DropDownList5.DataSource = ls;
            DropDownList5.DataTextField = "DESIGNATION";
            DropDownList5.DataValueField = "CODE_MODULE";
            DropDownList5.DataBind();
            DropDownList5.Items.Insert(0, new ListItem("Choisir", "0"));
        }

        protected void TypeProjetBind()
        {
            DropDownList4.Items.Insert(0, new ListItem("Choisir", "0"));
            DropDownList4.Items.Insert(1, new ListItem("Mini Projet", "01"));
            DropDownList4.Items.Insert(2, new ListItem("Projet d'intégration", "12"));
            DropDownList4.Items.Insert(3, new ListItem("P.F.E", "23"));
        }

        /*
        * Fin Panel 1
        * 
        */
        /*
        * Panel 2
        * 
        */
        /*panel Evaluation recherche*/
        protected void afichierPanel(object sender, object e)
        {
            if (RadioButtonList7.SelectedValue == "oui")
            {
                Button13.Visible = true;
                Button14.Visible = true;
                Button1.Visible = false;
                Button2.Visible = false;
                Panel3.Visible = true;
            }
            else
            {
                Button13.Visible = false;
                Button14.Visible = false;
                Button1.Visible = false;
                Button2.Visible = false;
                Panel3.Visible = false;
            }
        }

        //Type
        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {
            LinkButton2.Visible = false;
            LinkButton3.Visible = false;
            LinkButton1.Visible = false;
            LinkButton4.Visible = false;
            LinkButton5.Visible = false;

            Panel3.Visible = false;
            IntiPanel3();

            if (RadioButtonList4.SelectedValue == "indiv")
            {
                LinkButton2.Visible = true;
            }
            else
            {
                LinkButton2.Visible = false;
            }

            if (DropDownList4.SelectedValue == "01" || DropDownList4.SelectedValue == "23")
            {
                

                DropDownList7.Items.Clear();
                PanelEncadrementGROUPE.Visible = true;
                PanelREchercheListeGroupeParEt.Visible = false;
                PanelajoutGP.Visible = false;
                RadioButtonList4.Enabled = true;
                if (RadioButtonList4.SelectedValue == "indiv")
                    BindAllProjects(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                else
                {
                    LabelAffectationEtudiant.Visible = true;
                    LabelStatusEtudiant.Visible = true;
                    BindAllProjects_Groupe(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                    bool status = false;
                    ESPSuiviEncadrement.EtudiantClasses.Instance.openconntrans();
                    string type_projet = DropDownList4.SelectedValue.ToString().Trim();
                    status = ESPSuiviEncadrement.EtudiantClasses.Instance.statusEtudiantaffecter(DropDownList3.SelectedValue,type_projet);
                    ESPSuiviEncadrement.EtudiantClasses.Instance.closeConnection();
                    Label36.Text = Convert.ToString(GroupeProjet.Instance.getNomEtudiant(DropDownList3.SelectedValue.ToString()));
                    if (status == false)
                    {
                        LabelAffectationEtudiant.Text = "n'est pas affecter";
                        LinkButton4.Visible = true;
                    }
                    else
                    {
                        LabelAffectationEtudiant.Text = "dèjà affecter";
                        LabelAffectationEtudiant.CssClass = "control-label text-success";
                        LinkButton4.Visible = false;
                    }
                }
                
                //if (DropDownList3.SelectedValue != "0" && DropDownList2.SelectedValue == "All")
                //{
                //    DropDownList7.Items.Clear();
                //    BindAllProjects(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                //}
                //else
                //{

                //    if (DropDownList3.SelectedValue != "0" && DropDownList2.SelectedValue != "All")
                //    {
                //        DropDownList7.Items.Clear();
                //        BindProjectsByClass(DropDownList3.SelectedValue.ToString(), DropDownList2.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                //    }
                //}
            }
            else if (DropDownList4.SelectedValue == "12")
            {
                
                DropDownList7.Items.Clear();
                RadioButtonList4.SelectedValue = "group";
                RadioButtonList4.Enabled = false;
                PanelEncadrementGROUPE.Visible = true;
                PanelREchercheListeGroupeParEt.Visible = true;
                PanelajoutGP.Visible = true;
                LabelAffectationEtudiant.Visible = true;
                LabelStatusEtudiant.Visible = true;
                BindAllProjects_Groupe(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                //BindAllProjects(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                //Label21.Text = Convert.ToString(EtudiantClasses.Instance.getNiveauEtudiant(DropDownList3.SelectedValue.ToString()));
                bool status = false;
                //ESPSuiviEncadrement.EtudiantClasses.Instance.openconntrans();
                string type_projet = DropDownList4.SelectedValue.ToString().Trim();
                status = ESPSuiviEncadrement.EtudiantClasses.Instance.statusEtudiantaffecter(DropDownList3.SelectedValue,type_projet);
                ESPSuiviEncadrement.EtudiantClasses.Instance.closeConnection();
                Label36.Text = Convert.ToString(GroupeProjet.Instance.getNomEtudiant(DropDownList3.SelectedValue.ToString()));
                if (status == false)
                {
                    LabelAffectationEtudiant.Text = "n'est pas affecter";
                    LinkButton4.Visible = true;
                }
                else
                {
                    LabelAffectationEtudiant.Text = "dèjà affecter";
                    LabelAffectationEtudiant.CssClass = "control-label text-success";
                    LinkButton4.Visible = false;
                }
                
                

               
            }
            else
            {

                Panel3.Visible = false;
                DropDownList7.Items.Clear();
                DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
            }
        }

        //Titre Des Projets
        protected void DropDownList7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList4.SelectedValue == "indiv")
            {
                if (DropDownList7.SelectedValue != "0")
                {
                    IntiPanel3();
                    LinkButton3.Visible = true;
                    LinkButton1.Visible = true;
                    Button13.Visible = false;
                    Button14.Visible = false;
                    //List<ESP_PROJET_DETAILS> proj = ESP_PROJET_DETAILS.GetProj(DropDownList7.SelectedValue.ToString());
                    HiddenField1.Value = DropDownList4.SelectedValue;
                    Panel3.Visible = true;
                }
                else
                {
                    Button13.Visible = false;
                    Button14.Visible = false;
                    LinkButton3.Visible = false;
                    LinkButton1.Visible = false;
                    Panel3.Visible = false;
                }
            }
            else
            {
                LinkButton3.Visible = false;
                LinkButton1.Visible = false;
                Panel3.Visible = false;
            }

            

        }
        //Valider Creation Projet

        protected void DropDownList8_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        protected void Button3_Click(object sender, EventArgs e)
        {
            string CODE_MODULE = DropDownList5.SelectedValue.ToString().Trim();
            string ID_ET = DropDownList3.SelectedValue.ToString().Trim();
            string ID_ENS = nom.ToString().Trim();
            string CODE_CL = DropDownList2.SelectedValue.ToString().Trim();
            //Creation projet

            string TYPE_PROJET = DropDownList4.SelectedValue.ToString().Trim();
            string NOM_PROJET = TextBox5.Text.ToString().Trim();

            string TECHNOLOGIES = DDlistTech.SelectedValue.ToString();
            string METHODOLOGIE = DDlistMethod.SelectedValue.ToString();
            string DESCRIPTION_PROJET = TextBox15.Text.ToString().Trim();
            decimal PERIODE = Convert.ToDecimal(RadioButtonList3.SelectedValue.ToString().Trim());
            decimal SEMESTRE = Convert.ToDecimal(RadioButtonList2.SelectedValue.ToString().Trim());
            decimal NIVEAU_ETUDIANT = Convert.ToDecimal(Label21.Text.ToString());
            OracleDate date = OracleDate.GetSysDate();
            OracleTimeStamp he = OracleTimeStamp.GetSysDate();
            int v = 0;

            if (DropDownList4.SelectedValue != "0" && DropDownList5.SelectedValue != "0" && DropDownList2.SelectedValue != "0" && DropDownList3.SelectedValue != "0")
            {
                if (ESP_PROJET.Instance.verif_projet("2013", NOM_PROJET, CODE_MODULE))
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert('Le projet est deja existant !')</script>");
                }
                else
                    if (DropDownList6.SelectedValue.ToString() == "Mois" || TextBox5.Text == "")
                    {
                        Response.Write("<script LANGUAGE='JavaScript'> alert('verifier le titre du projet ou la durée du projet !')</script>");
                    }
                    else
                    {
                        decimal DUREE = Convert.ToDecimal(DropDownList6.SelectedValue.ToString().Trim());
                        ESP_PROJET.Instance.openconntrans();
                        ESP_PROJET.Instance.create_esp_projet("2013", NOM_PROJET, CODE_MODULE, TYPE_PROJET, DESCRIPTION_PROJET, TECHNOLOGIES, METHODOLOGIE, NIVEAU_ETUDIANT, DUREE, SEMESTRE, PERIODE);
                        ESP_PROJET.Instance.closeConnection();
                        List<ESP_PROJET> id = ESP_PROJET.Instance.getIdproj("2013", NOM_PROJET, CODE_MODULE);
                        string proj = id.FirstOrDefault<ESP_PROJET>().ID_PROJET;
                        if (proj != "")
                        {
                            ESP_ENCADDREMENT.Instance.openconntrans();
                            ESP_ENCADDREMENT.Instance.create_Encadrement_ESP(proj, "2013", TYPE_PROJET, ID_ET, ID_ENS, CODE_CL, date, he, he, he, 0, 0, 0, 0, 0, "", "", "");
                            ESP_ENCADDREMENT.Instance.closeConnection();
                        }

                        TextBox5.Text = "";
                        TextBox15.Text = "";
                        Label21.Text = "";
                        Response.Write("<script LANGUAGE='JavaScript'> alert('Projet crée avec succes !')</script>");
                        DropDownList4.SelectedIndex = 0;
                        DropDownList6.SelectedIndex = 0;
                        DDlistTech.SelectedIndex = 0;
                        DDlistMethod.SelectedIndex = 0;
                        if (DropDownList4.SelectedValue != "0")
                        {

                            if (DropDownList3.SelectedValue != "0" && DropDownList2.SelectedValue == "All")
                            {
                                DropDownList7.Items.Clear();
                                BindAllProjects(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                            }
                            else
                            {
                                if (DropDownList3.SelectedValue != "0" && DropDownList2.SelectedValue != "All")
                                {
                                    DropDownList7.Items.Clear();
                                    BindProjectsByClass(DropDownList3.SelectedValue.ToString(), DropDownList2.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());
                                }
                            }
                        }
                        else
                        {
                            DropDownList7.Items.Clear();
                            DropDownList7.Items.Insert(0, new ListItem("N/A", "0"));
                        }

                    }
            }
            else
            {
                TextBox5.Text = "";
                TextBox15.Text = "";
                Label21.Text = "";
                DropDownList4.SelectedIndex = 0;
                DropDownList6.SelectedIndex = 0;
                DDlistTech.SelectedIndex = 0;
                DDlistMethod.SelectedIndex = 0;
                if (DropDownList4.SelectedValue == "0")
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner le type de projet !')</script>");
                }
                else
                {
                    if (DropDownList5.SelectedValue == "0")
                    {
                        Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner le module !')</script>");
                    }
                    else
                    {
                        if (DropDownList2.SelectedValue == "0")
                        {
                            Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner la classe !')</script>");
                        }
                        else
                        {
                            if (DropDownList3.SelectedValue == "0")
                            {
                                Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner l'etudiant !')</script>");
                            }
                        }
                    }
                }
            }
        }
        //Valider Modification Projet
        protected void Button5_Click(object sender, EventArgs e)
        {
            string CODE_MODULE = DropDownList5.SelectedValue.ToString().Trim();

            //Modification projet
            string ID_PROJET;
            string TYPE_PROJET;
            string NOM_PROJET;
            decimal DUREE;
            string TECHNOLOGIES;
            string METHODOLOGIE;
            string DESCRIPTION_PROJET;
            decimal PERIODE;
            decimal SEMESTRE;
            decimal NIVEAU_ETUDIANT;


            TYPE_PROJET = DropDownList4.SelectedValue.ToString().Trim();

            foreach (RepeaterItem i in Repeater1.Items)
            {
                TextBox txt = (TextBox)i.FindControl("TextBox18");
                ID_PROJET = txt.Text;
                TextBox txt1 = (TextBox)i.FindControl("TextBox25");
                NOM_PROJET = txt1.Text;

                TextBox txt4 = (TextBox)i.FindControl("TextBox15");
                DESCRIPTION_PROJET = txt4.Text;

                DropDownList ddl4 = (DropDownList)i.FindControl("DDlistTech2");
                TECHNOLOGIES = ddl4.SelectedValue.ToString().Trim();
                DropDownList ddl5 = (DropDownList)i.FindControl("DDlistMethod2");
                METHODOLOGIE = ddl5.SelectedValue.ToString().Trim();

                //DropDownList ddl = (DropDownList)i.FindControl("DropDownList4");
                //TYPE_PROJET = ddl.SelectedValue.ToString().Trim();
                DropDownList ddl1 = (DropDownList)i.FindControl("DropDownList8");
                Label txtN = (Label)i.FindControl("TextBoxNiveau");

                NIVEAU_ETUDIANT = Convert.ToDecimal(txtN.Text);
                DropDownList ddl2 = (DropDownList)i.FindControl("DropDownList9");
                DUREE = Convert.ToDecimal(ddl2.SelectedValue.ToString().Trim());
                RadioButtonList rbl2 = (RadioButtonList)i.FindControl("RadioButtonList2");
                SEMESTRE = Convert.ToDecimal(rbl2.SelectedValue.ToString().Trim());
                RadioButtonList rbl3 = (RadioButtonList)i.FindControl("RadioButtonList3");
                PERIODE = Convert.ToDecimal(rbl3.SelectedValue.ToString().Trim());
                ESP_PROJET.Instance.openconntrans();
                ESP_PROJET.Instance.update_esp_suivi_projet("2013", ID_PROJET, NOM_PROJET, CODE_MODULE, TYPE_PROJET, DESCRIPTION_PROJET, TECHNOLOGIES, METHODOLOGIE, NIVEAU_ETUDIANT, DUREE, SEMESTRE, PERIODE);
                ESP_PROJET.Instance.closeConnection();
                Response.Write("<script LANGUAGE='JavaScript'> alert('La mise à jour du projet a été effectué avec succes !!')</script>");
                BindAllProjects(DropDownList3.SelectedValue.ToString(), DropDownList4.SelectedValue.ToString());

            }


        }
        //Button Modifier
        protected void LinkButton3_Click(object sender, EventArgs e)
        {
            //Response.Write("<script LANGUAGE='JavaScript'> alert('Le projet est deja existant !')</script>");

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalModifier();", true);
            //ScriptManager.RegisterClientScriptBlock(this, typeof(System.Web.UI.Page), "LoginFail", "$('#modifier_projet').modal('show');", true);
            //ClientScript.RegisterStartupScript(Page.GetType(), "key", "alert('Button Clicked')", true);
        }

        //button affectation au groupe
        protected void Button7_Click(object sender, EventArgs e)
        {
            string[] me = { "", "", "", "" };
            
            string id_et = DropDownList3.SelectedValue.ToString();
            ESP_GROUPE_ETUDIANT.Instance.openconntrans();
            me = ESP_GROUPE_ETUDIANT.Instance.REchercheGroupeParNomGROUPE(DropDownList11.SelectedValue.ToString().Trim());
            ESP_GROUPE_ETUDIANT.Instance.closeConnection();
            string NOM_GROUPE = me[0];
            string desc_groupe = me[1];
            decimal note_et = Convert.ToDecimal(me[2]);
            decimal numl_groupe = Convert.ToDecimal(me[3]);
            string ID_PROJ= "PROJGROUP";
            decimal id = ESP_GROUPE_ETUDIANT.Instance.inc_id_groupe_etudiant();
            ID_PROJ= ID_PROJ+ id;
            //Label2.Text = me[3];
            ESP_GROUPE_ETUDIANT.Instance.openconntrans(); ;

            ESP_GROUPE_ETUDIANT.Instance.create_Groupe_ETUDIANT(id_et, ID_PROJ, NOM_GROUPE, desc_groupe, note_et, numl_groupe, "2013");
            ESP_GROUPE_ETUDIANT.Instance.closeConnection();
            Response.Write("<script LANGUAGE='JavaScript'> alert('Affectation Réusite !!!!!!')</script>");
        }

        //Button Ajouter
        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            if (DropDownList2.SelectedValue == "0")
            {

                Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner la classe !')</script>");
            }
            else
            {

                if (DropDownList3.SelectedValue == "0")
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner l'etudiant !')</script>");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalAjouter();", true);

                }

            }

        }

        private void BindProjectsByClass(string p, string p_1, string p_2)
        {
            List<ESP_PROJET_DETAILS> ls = new List<ESP_PROJET_DETAILS>();
            ls = ESP_PROJET_DETAILS.GetProjEtudiantClass(p, p_1, p_2);
            DropDownList7.DataSource = ls;
            DropDownList7.DataTextField = "NOM_PROJET";
            DropDownList7.DataValueField = "ID_PROJET";
            DropDownList7.DataBind();
            if (DropDownList7.Items.Count != 0)
            {
                DropDownList7.Items.Insert(0, new ListItem("Choisir", "0"));
            }
            else
            {
                DropDownList7.Items.Insert(0, new ListItem("Aucun", "0"));
            }

        }

        private void BindProjectsByClass_Groupe(string p, string p_1, string p_2)
        {
            List<ESP_PROJET_DETAIL_GROUPE> ls = new List<ESP_PROJET_DETAIL_GROUPE>();
            ls = ESP_PROJET_DETAIL_GROUPE.GetProjEtudiantClass_Groupe(p, p_1, p_2);
            DropDownList7.DataSource = ls;
            DropDownList7.DataTextField = "NOM_PROJET";
            DropDownList7.DataValueField = "ID_PROJET";
            DropDownList7.DataBind();
            if (DropDownList7.Items.Count != 0)
            {
                DropDownList7.Items.Insert(0, new ListItem("Choisir", "0"));
            }
            else
            {
                DropDownList7.Items.Insert(0, new ListItem("Aucun", "0"));
            }

        }

        private void BindAllProjects(string p, string p_1)
        {
            List<ESP_PROJET_DETAILS> ls = new List<ESP_PROJET_DETAILS>();
            ls = ESP_PROJET_DETAILS.GetProjEtudiant(p, p_1);
            DropDownList7.DataSource = ls;
            DropDownList7.DataTextField = "NOM_PROJET";
            DropDownList7.DataValueField = "ID_PROJET";
            DropDownList7.DataBind();
            if (DropDownList7.Items.Count != 0)
            {
                DropDownList7.Items.Insert(0, new ListItem("Choisir", "0"));
            }
            else
            {
                DropDownList7.Items.Insert(0, new ListItem("Aucun", "0"));
            }
        }

        private void BindAllProjects_Groupe(string p, string p_1)
        {
            List<ESP_PROJET_DETAIL_GROUPE> ls = new List<ESP_PROJET_DETAIL_GROUPE>();
            ls = ESP_PROJET_DETAIL_GROUPE.GetProjEtudiant_Groupe(p, p_1);
            DropDownList7.DataSource = ls;
            DropDownList7.DataTextField = "NOM_PROJET";
            DropDownList7.DataValueField = "ID_PROJET";
            DropDownList7.DataBind();
            if (DropDownList7.Items.Count != 0)
            {
                DropDownList7.Items.Insert(0, new ListItem("Choisir", "0"));
            }
            else
            {
                DropDownList7.Items.Insert(0, new ListItem("Aucun", "0"));
            }
        }
        //Suivi
        protected void LinkButton1_Click1(object sender, EventArgs e)
        {
            Session["CODE_CL"] = DropDownList2.SelectedValue.ToString();
            Session["NOM_ET"] = DropDownList3.SelectedItem.ToString();
            Session["ID_ET"] = DropDownList3.SelectedValue.ToString();
            Session["ID_ENS"] = DropDownList3.SelectedValue.ToString();
            
            Response.Redirect("Suivi.aspx");
        }


        protected void suivi_groupe(object sender, EventArgs e)
        {
           
            string nom_proj = DropDownList8.SelectedValue;
            Session["ID_ENS"] = DropDownList3.SelectedValue.ToString();
            string[] ls = { "", "", "", "", "" };
            ESP_GROUPE_ETUDIANT.Instance.openconntrans();
            ls = ESP_GROUPE_ETUDIANT.Instance.REchercheGroupeParNomGROUPE(DropDownList8.Text);

            string ID_PROJET = ESP_PROJET_DETAIL_GROUPE.Instance.GetIDProjGroupe(DropDownList8.Text);
            ESP_GROUPE_ETUDIANT.Instance.closeConnection();
            Session["NOM_GROUPE"] = ls[0];
            Session["DESCRIPTIO_GROUPE"] = ls[1];
            Session["NUMERO_GROUPE"] = ls[3];
            Session["ID_PROJ_GP"] = ls[4];

            Session["ID_PROJET"] = ID_PROJET;

            Response.Write("<script>");
            Response.Write("window.open('suivigroupe.aspx','_blanck')");
            Response.Write("</script>"); 
            //Response.Write(@"<script language='javascript'>alert('" + ID_PROJET + "!!')</script>");
            
           
        }


        // <summary>
        /// ////////////////////////////////////////////////
        // </summary>
        // <param name="sender"></param>
        // <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            string h_entre = dtp_input2.Value.ToString();
            h_entre = dtp_input2.Value.ToString().Substring(0, 2);
            string h_sortie = dtp_input3.Value.ToString().Substring(0, 2);
            string m_entre = dtp_input2.Value.ToString().Substring(3, 2);
            string m_sortie = dtp_input3.Value.ToString().Substring(3, 2);

            int duree_entre = int.Parse(h_entre) * 60 + int.Parse(m_entre);
            int duree_sortir = int.Parse(h_sortie) * 60 + int.Parse(m_sortie);
            int dure = duree_sortir - duree_entre;

            if (dure < 60)
            {
                Response.Write(@"<script language='javascript'>alert('verifier les heures !!')</script>");
            }
            else
            {

                if (RadioButtonList4.SelectedValue == "indiv")
                {
                    string dure_heure_string = (dure / 60).ToString();
                    string dure_minute_string = (dure % 60).ToString();
                    Convert.ToInt32(dure_heure_string);
                    OracleTimeStamp date;
                    date =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();
                    OracleTimeStamp dureee = new OracleTimeStamp(date.Year, date.Month, date.Day, Convert.ToInt32(dure_heure_string), Convert.ToInt32(dure_minute_string), 0);
                    string ID_PROJET = DropDownList7.SelectedValue.ToString().Trim();
                    string ANNEE_DEB = "2013";
                    string TYPE_PROJET = HiddenField1.Value.ToString().Trim();
                    string ID_ET = DropDownList3.SelectedValue.ToString().Trim();
                    string ID_ENS = nom.ToString().Trim();
                    string CODE_CL = DropDownList2.SelectedValue.ToString().Trim();
                    /* ************************************************************** */

                    string date_from_input = dtp_input1.Value.ToString();
                   Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate();
                    OracleDate dateoracle;
                    OracleDate time =Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate();

                    dateoracle = new OracleDate(int.Parse(date_from_input.Substring(6, 4)), int.Parse(date_from_input.Substring(3, 2)), int.Parse(date_from_input.Substring(0, 2)), Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Hour, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Minute, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Second);
                    //dateoracle = new OracleDate(int.Parse(date_from_input.Substring(0, 4)), int.Parse(date_from_input.Substring(5, 2)), int.Parse(date_from_input.Substring(8, 2)), Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Hour, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Minute, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Second);

                    OracleDate DATE_ENC = dateoracle;

                    //Label1.Text = dateoracle.ToString();
                    /********************************************************************/
                    //OracleTimeStamp HEURE_DEB = OracleTimeStamp.Parse(dtp_input2.Value.ToString());
                    string datetime2 = dtp_input2.Value.ToString();
                    OracleTimeStamp time1;
                    time1 =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();

                    double milii = 0;
                    OracleTimeStamp HEURE_DEB = new OracleTimeStamp(time1.Year, time1.Month, time1.Day, int.Parse(datetime2.Substring(0, 2)), int.Parse(datetime2.Substring(3, 2)), 1
                        , milii);

                    //OracleTimeStamp HEURE_FIN = OracleTimeStamp.Parse(dtp_input3.Value.ToString());
                    string datetime = dtp_input3.Value.ToString();
                    OracleTimeStamp time11;
                    time11 =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();

                    double mili = 0;
                    OracleTimeStamp HEURE_FIN = new OracleTimeStamp(time11.Year, time11.Month, time11.Day, int.Parse(datetime.Substring(0, 2)), int.Parse(datetime.Substring(3, 2)), 1
                        , mili);



                    /**************************************************/



                    decimal AV_TECH = Convert.ToDecimal(TextBox3.Text.Trim());
                    decimal AV_GLOBAL = Convert.ToDecimal(TextBox9.Text.Trim());
                    decimal AV_ANG = Convert.ToDecimal(TextBox4.Text.Trim());
                    decimal AV_FR = Convert.ToDecimal(TextBox6.Text.Trim());
                    decimal AV_RAPPORT = Convert.ToDecimal(TextBox8.Text.Trim());
                    decimal AV_CC = Convert.ToDecimal(TextBox7.Text.Trim());
                    string AV_COMPORTEMENT = RadioButtonList1.SelectedValue.ToString();
                    string REMARQUE = TextBox10.Text;
                    string TRAVAIL = TextBox11.Text;




                    ESP_ENCADDREMENT.Instance.openconntrans();
                    ESP_ENCADDREMENT.Instance.create_Encadrement_ESP(ID_PROJET, ANNEE_DEB, TYPE_PROJET, ID_ET, ID_ENS, CODE_CL, DATE_ENC, HEURE_DEB, HEURE_FIN, dureee, AV_TECH, AV_ANG, AV_FR, AV_RAPPORT, AV_CC, AV_COMPORTEMENT, REMARQUE, TRAVAIL);
                    ESP_ENCADDREMENT.Instance.closeConnection();
                    Response.Write("<script LANGUAGE='JavaScript'> alert('La suivi du projet a été effectué avec succes !!')</script>");
                }
                else
                {
                    string dure_heure_string = (dure / 60).ToString();
                    string dure_minute_string = (dure % 60).ToString();
                    Convert.ToInt32(dure_heure_string);
                    OracleTimeStamp date;
                    date =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();
                    OracleTimeStamp dureee = new OracleTimeStamp(date.Year, date.Month, date.Day, Convert.ToInt32(dure_heure_string), Convert.ToInt32(dure_minute_string), 0);
                    string ID_PROJET = DropDownList7.SelectedValue.ToString().Trim();
                    string ANNEE_DEB = "2013";
                    string TYPE_PROJET = HiddenField1.Value.ToString().Trim();
                    string ID_ET = DropDownList3.SelectedValue.ToString().Trim();
                    string ID_ENS = nom.ToString().Trim();
                    string CODE_CL = DropDownList2.SelectedValue.ToString().Trim();
                    /* ************************************************************** */

                    string date_from_input = dtp_input1.Value.ToString();
                   Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate();
                    OracleDate dateoracle;
                    OracleDate time =Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate();

                    dateoracle = new OracleDate(int.Parse(date_from_input.Substring(6, 4)), int.Parse(date_from_input.Substring(3, 2)), int.Parse(date_from_input.Substring(0, 2)), Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Hour, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Minute, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Second);
                    //dateoracle = new OracleDate(int.Parse(date_from_input.Substring(0, 4)), int.Parse(date_from_input.Substring(5, 2)), int.Parse(date_from_input.Substring(8, 2)), Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Hour, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Minute, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Second);

                    OracleDate DATE_ENC = dateoracle;

                    //Label1.Text = dateoracle.ToString();
                    /********************************************************************/
                    //OracleTimeStamp HEURE_DEB = OracleTimeStamp.Parse(dtp_input2.Value.ToString());
                    string datetime2 = dtp_input2.Value.ToString();
                    OracleTimeStamp time1;
                    time1 =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();

                    double milii = 0;
                    OracleTimeStamp HEURE_DEB = new OracleTimeStamp(time1.Year, time1.Month, time1.Day, int.Parse(datetime2.Substring(0, 2)), int.Parse(datetime2.Substring(3, 2)), 1
                        , milii);

                    //OracleTimeStamp HEURE_FIN = OracleTimeStamp.Parse(dtp_input3.Value.ToString());
                    string datetime = dtp_input3.Value.ToString();
                    OracleTimeStamp time11;
                    time11 =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();

                    double mili = 0;
                    OracleTimeStamp HEURE_FIN = new OracleTimeStamp(time11.Year, time11.Month, time11.Day, int.Parse(datetime.Substring(0, 2)), int.Parse(datetime.Substring(3, 2)), 1
                        , mili);



                    /**************************************************/



                    decimal AV_TECH = Convert.ToDecimal(TextBox3.Text.Trim());
                    decimal AV_GLOBAL = Convert.ToDecimal(TextBox9.Text.Trim());
                    decimal AV_ANG = Convert.ToDecimal(TextBox4.Text.Trim());
                    decimal AV_FR = Convert.ToDecimal(TextBox6.Text.Trim());
                    decimal AV_RAPPORT = Convert.ToDecimal(TextBox8.Text.Trim());
                    decimal AV_CC = Convert.ToDecimal(TextBox7.Text.Trim());
                    string AV_COMPORTEMENT = RadioButtonList1.SelectedValue.ToString();
                    string REMARQUE = TextBox10.Text;
                    string TRAVAIL = TextBox11.Text;
                    decimal noteGroupe = decimal.Parse(TextBoxNoteGROUPE.Text);
                    string id_groupe_projet = DropDownList8.SelectedValue;
                    string type_projet = DropDownList4.SelectedValue;


                    string id_endacrement = "ENCGRP";
                    decimal id2 = ESP_ENCADREMENT_GROUPE.Instance.inc_id_groupe_encadrement();
                    id_endacrement = id_endacrement + id2;

                    string ID_PROJ = "PROJ";
                    decimal id = ESP_PROJET.Instance.inc_id_projet();
                    ID_PROJ = ID_PROJ + id;


                    ESP_ENCADREMENT_GROUPE.Instance.openconntrans();
                    ESP_ENCADREMENT_GROUPE.Instance.create_Encadrement_ESP_GROUPE(ID_PROJ, ID_ENS, ID_ET, id_endacrement, noteGroupe, CODE_CL, AV_TECH, AV_ANG, AV_FR, AV_RAPPORT, AV_CC, AV_COMPORTEMENT, REMARQUE, TRAVAIL, DATE_ENC, HEURE_DEB, HEURE_FIN, dureee, ANNEE_DEB, type_projet);
                    ESP_ENCADREMENT_GROUPE.Instance.closeConnection();
                    Response.Write("<script LANGUAGE='JavaScript'> alert('La suivi du projet a été effectué avec succes !!')</script>");
                }
            }




        }

        protected void Button2_Click(object sender, EventArgs e)
        {

        }




        protected void cancel_create_groupe(object sender, EventArgs e)
        {
            textboxNomGroupe.Text = "";
            textboxSujetPI.Text= "";
            DropDownList1.SelectedValue = "";
            RadioButtonList5.SelectedValue = "non";

        }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       
        protected void select_type_encadrement(object sender, EventArgs e)
        {
            
            //PanelAjoutGroupe.Visible = false;
            RadioButtonList5.SelectedValue = "non";
            string ID_ET = DropDownList3.SelectedValue;

            if (DropDownList4.SelectedValue == "01" || DropDownList4.SelectedValue == "23")
            {
                if (RadioButtonList4.SelectedValue == "indiv")
                {
                    
                    LinkButton2.Visible = true;
                    LinkButton3.Visible = true;
                    LinkButton4.Visible = false;
                    LinkButton5.Visible = false;
                    Label36.Text = Convert.ToString(GroupeProjet.Instance.getNomEtudiant(DropDownList3.SelectedValue.ToString()));
                    LabelAffectationEtudiant.Visible = false;
                    LabelStatusEtudiant.Visible = false;
                    PanelHeadEncadrement.Visible = true;
                    PanelHeadEncadrementParGroupe.Visible = false;
                    
                    PanelProjet.Visible = true;
                    LinkButton4.Visible = false;

                    LinkButton3.Visible = false;
                    PanelajoutGP.Visible = false;
                    PanelREchercheListeGroupeParEt.Visible = false;


                }
                else 
                    if (RadioButtonList4.SelectedValue == "group")
                {
                    LinkButton1.Visible = false;
                    LinkButton2.Visible = false;
                    LinkButton3.Visible = false;
                    LinkButton4.Visible = true;
                    LinkButton5.Visible = true;

                    bool status = false;
                    string type_projet = DropDownList4.SelectedValue.ToString().Trim();
                    ESPSuiviEncadrement.EtudiantClasses.Instance.openconntrans();
                    status = ESPSuiviEncadrement.EtudiantClasses.Instance.statusEtudiantaffecter(DropDownList3.SelectedValue,type_projet);
                    ESPSuiviEncadrement.EtudiantClasses.Instance.closeConnection();
                    Label36.Text = Convert.ToString(GroupeProjet.Instance.getNomEtudiant(DropDownList3.SelectedValue.ToString()));
                    if (status == false)
                    {
                        LabelAffectationEtudiant.Text = "n'est pas affecter";
                        LinkButton4.Visible = true;
                    }
                    else
                    {
                        LabelAffectationEtudiant.Text = "dèjà affecter";
                        LabelAffectationEtudiant.CssClass = "control-label text-success";
                        LinkButton4.Visible = false;
                    }


                    LabelAffectationEtudiant.Visible = true;
                    LabelStatusEtudiant.Visible = true;
                    PanelREchercheListeGroupeParEt.Visible = true;
                    PanelajoutGP.Visible = true;
                    PanelHeadEncadrement.Visible = false;
                    LinkButton1.Visible = false;
                    LinkButton2.Visible = false;
                    LinkButton3.Visible = false;
                    Panel3.Visible = false;
                   


                    PanelHeadEncadrementParGroupe.Visible = true;
                    
                    PanelProjet.Visible = true;


                }
            }
            else if (DropDownList4.SelectedValue == "12")
            {
                LinkButton1.Visible = false;
                LinkButton2.Visible = false;
                LinkButton3.Visible = false;
                LinkButton4.Visible = true;
                LinkButton5.Visible = true;
                LabelStatusEtudiant.Visible = true;
                LabelAffectationEtudiant.Visible = true;

                PanelREchercheListeGroupeParEt.Visible = true;
                PanelajoutGP.Visible = true;
                PanelHeadEncadrement.Visible = false;
                LinkButton1.Visible = false;
                LinkButton2.Visible = false;
                LinkButton3.Visible = false;
                Panel3.Visible = false;

                PanelHeadEncadrementParGroupe.Visible = true;
                
                PanelProjet.Visible = true;

                bool status = false;
                string type_projet = DropDownList4.SelectedValue.ToString().Trim();
                ESPSuiviEncadrement.EtudiantClasses.Instance.openconntrans();
                status = ESPSuiviEncadrement.EtudiantClasses.Instance.statusEtudiantaffecter(DropDownList3.SelectedValue,type_projet);
                ESPSuiviEncadrement.EtudiantClasses.Instance.closeConnection();
                Label36.Text = Convert.ToString(GroupeProjet.Instance.getNomEtudiant(DropDownList3.SelectedValue.ToString()));
                if (status == false)
                {
                    LabelAffectationEtudiant.Text = "n'est pas affecter";
                    LinkButton4.Visible = true;
                }
                else
                {
                    LabelAffectationEtudiant.Text = "dèjà affecter";
                    LabelAffectationEtudiant.CssClass = "control-label text-success";
                    LinkButton4.Visible = false;
                }


                
                

                
            }

        }
        

        public void IntiPanel3()
        {
            //AV_TECH 
            TextBox3.Text = "";
            //AV_GLOBAL 
            TextBox9.Text = "";
            //AV_ANG 
            TextBox4.Text = "";
            //AV_FR 
            TextBox6.Text = "";
            //AV_RAPPORT 
            TextBox8.Text = "";
            //AV_CC
            TextBox7.Text = "";
            //AV_COMPORTEMENT
            RadioButtonList1.SelectedValue = "B";
            //REMARQUE
            TextBox10.Text = "";
            //TRAVAIL
            TextBox11.Text = "";
        }

        public void PostSuivi()
        {
            //string codecl;
            //string id;
            //string nomet;
            //string idet;
            //string idproj;

            ////Session["SUIVI"] = "true";
            //codecl = Session["CODE_CL"].ToString();
            //nomet = Session["NOM_ET"].ToString();
            //idet = Session["ID_ET"].ToString();
            //id = Session["ID_ENS"].ToString();
            //idproj = Session["ID_PROJ"].ToString();

            //DropDownList2.SelectedValue = codecl;
            //DropDownList2.AutoPostBack = true;

            //DropDownList2.SelectedIndexChanged += new EventHandler(DropDownList2_SelectedIndexChanged);
        }

        protected void LinkButton4_Click(object sender, EventArgs e)
        {
            if (DropDownList2.SelectedValue == "0")
            {

                Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner la classe !')</script>");
            }
            else
            {

                if (DropDownList3.SelectedValue == "0")
                {
                    Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner l'etudiant !')</script>");
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalAffecterGroupe();", true);

                }

            }
        }

        protected void create_groupe(object sender, EventArgs e)
        {
            
            if (DropDownList4.SelectedValue == "0")
            {
                
                Response.Write("<script LANGUAGE='JavaScript'> alert('Veuillez selectionner un type de projet !')</script>");
                RadioButtonList5.SelectedValue = "non";
            }
            else
            {
                if (RadioButtonList5.SelectedValue == "oui")
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalcreate_Groupe();", true);
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalcreate_Groupe();", false);
            }
            
            
        }


        //ajouter un projet par groupe
        protected void Button11_Click(object sender, EventArgs e)
        {
            string CODE_MODULE = DropDownList5.SelectedValue.ToString().Trim();
            string ID_ET = DropDownList3.SelectedValue.ToString().Trim();
            string ID_ENS = nom.ToString().Trim();
            string CODE_CL = DropDownList2.SelectedValue.ToString().Trim();
            string TYPE_PROJET = DropDownList4.SelectedValue.ToString().Trim();
            

            string TECHNOLOGIES = DropDownListtechnol.SelectedValue.ToString();
            string METHODOLOGIE = DropDownListMethod.SelectedValue.ToString();
            string DESCRIPTION_PROJET = TextBox1Descrip.Text.ToString().Trim();
            decimal PERIODE = Convert.ToDecimal(RadioButtonListPriode.SelectedValue.ToString().Trim());
            decimal SEMESTRE = Convert.ToDecimal(RadioButtonListSimestre.SelectedValue.ToString().Trim());


            string ID_PROJ= "PROJGROUP";
            string id_endacrement = "ENCGRP";
            string nom_groupe = textboxNomGroupe.Text;
            string sujet = textboxSujetPI.Text;
            decimal numero_groupe = decimal.Parse(DropDownList1.SelectedValue.ToString());
            decimal id = ESP_GROUPE_ETUDIANT.Instance.inc_id_groupe_etudiant();
            decimal id2 = ESP_ENCADREMENT_GROUPE.Instance.inc_id_groupe_encadrement();
            ID_PROJ= ID_PROJ+ id;
            id_endacrement = id_endacrement + id2;
            OracleDate date1 = OracleDate.GetSysDate();
            OracleTimeStamp date2 = OracleTimeStamp.GetSysDate();
            

            if (ESP_GROUPE_ETUDIANT.Instance.verif_Groupe_ETUDIANT(nom_groupe, numero_groupe) == true)
            {
                Response.Write("<script LANGUAGE='JavaScript'> alert('ce projet existe déjà')</script>");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModalcreate_Groupe();", true);

            }
            else
            {
                //Response.Write("<script LANGUAGE='JavaScript'> alert('n'esiste pas !')</script>");
                ESP_GROUPE_ETUDIANT.Instance.openconntrans();
                ESP_GROUPE_ETUDIANT.Instance.create_Groupe_ETUDIANT("", ID_PROJ, nom_groupe, sujet, 0, numero_groupe, "2013");
                ESP_GROUPE_ETUDIANT.Instance.closeConnection();

                decimal DUREE = Convert.ToDecimal(DropDownList10.SelectedValue.ToString().Trim());

                //ESP_PROJET.Instance.openconntrans();
                //ESP_PROJET.Instance.create_esp_projet_par_groupe("2013", TextBoxtitreprojet.Text, CODE_MODULE, TYPE_PROJET, DESCRIPTION_PROJET, TECHNOLOGIES, METHODOLOGIE, DUREE, SEMESTRE, PERIODE, ID_PROJ);
                //ESP_PROJET.Instance.closeConnection();

                ESP_ENCADREMENT_GROUPE.Instance.openconntrans();
                ESP_ENCADREMENT_GROUPE.Instance.create_Encadrement_ESP_GROUPE(id_endacrement, ID_ENS, ID_ET, ID_PROJ, 0, CODE_CL, 0, 0, 0, 0, 0, "", "", "", date1, date2, date2, date2, "2013", TYPE_PROJET);
                ESP_ENCADREMENT_GROUPE.Instance.closeConnection();

                Response.Write("<script LANGUAGE='JavaScript'> alert('ajout avec succès !')</script>");
                RadioButtonList5.SelectedValue = "non";
                textboxSujetPI.Text = "";
                textboxNomGroupe.Text = "";
                DropDownList1.SelectedValue = "";
            }
            

        }

        protected void Afficher_suivi_Groupe(object sender, EventArgs e)
        {
            string[] ls = { "", "", "", "" };
            ESP_GROUPE_ETUDIANT.Instance.openconntrans();
            ls = ESP_GROUPE_ETUDIANT.Instance.REchercheGroupeParNomGROUPE(DropDownList8.SelectedValue);
            string ID_PROJ_GP = ls[0];
            ESP_GROUPE_ETUDIANT.Instance.closeConnection();
            Session["ID_PROJ_GP"] = ID_PROJ_GP;

            if (RadioButtonList6.SelectedValue == "oui")
            {
                PanelGroupeProjet.Visible = true;
                PanelEvaluation.Visible = true;
            }
            else
            {
                PanelGroupeProjet.Visible = false;
                RadioButtonList7.SelectedValue = "non";
                PanelEvaluation.Visible = false;
            }
                
        }

        protected void Button13_Click(object sender, EventArgs e)
        {
            string h_entre = dtp_input2.Value.ToString();
            h_entre = dtp_input2.Value.ToString().Substring(0, 2);
            string h_sortie = dtp_input3.Value.ToString().Substring(0, 2);
            string m_entre = dtp_input2.Value.ToString().Substring(3, 2);
            string m_sortie = dtp_input3.Value.ToString().Substring(3, 2);

            int duree_entre = int.Parse(h_entre) * 60 + int.Parse(m_entre);
            int duree_sortir = int.Parse(h_sortie) * 60 + int.Parse(m_sortie);
            int dure = duree_sortir - duree_entre;

            if (dure < 60)
            {
                Response.Write(@"<script language='javascript'>alert('verifier les heures !!')</script>");
            }
            else
            {
                string dure_heure_string = (dure / 60).ToString();
                string dure_minute_string = (dure % 60).ToString();
                Convert.ToInt32(dure_heure_string);
                OracleTimeStamp date;
                date =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();
                OracleTimeStamp dureee = new OracleTimeStamp(date.Year, date.Month, date.Day, Convert.ToInt32(dure_heure_string), Convert.ToInt32(dure_minute_string), 0);
                string ID_PROJET = DropDownList7.SelectedValue.ToString().Trim();
                string ANNEE_DEB = "2013";
                string TYPE_PROJET = HiddenField1.Value.ToString().Trim();
                string ID_ET = DropDownList3.SelectedValue.ToString().Trim();
                string ID_ENS = nom.ToString().Trim();
                string CODE_CL = DropDownList2.SelectedValue.ToString().Trim();
                /* ************************************************************** */

                string date_from_input = dtp_input1.Value.ToString();
               Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate();
                OracleDate dateoracle;
                OracleDate time =Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate();

                dateoracle = new OracleDate(int.Parse(date_from_input.Substring(6, 4)), int.Parse(date_from_input.Substring(3, 2)), int.Parse(date_from_input.Substring(0, 2)), Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Hour, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Minute, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Second);
                //dateoracle = new OracleDate(int.Parse(date_from_input.Substring(0, 4)), int.Parse(date_from_input.Substring(5, 2)), int.Parse(date_from_input.Substring(8, 2)), Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Hour, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Minute, Oracle.ManagedDataAccess.Types.OracleDate.GetSysDate().Second);

                OracleDate DATE_ENC = dateoracle;

                //Label1.Text = dateoracle.ToString();
                /********************************************************************/
                //OracleTimeStamp HEURE_DEB = OracleTimeStamp.Parse(dtp_input2.Value.ToString());
                string datetime2 = dtp_input2.Value.ToString();
                OracleTimeStamp time1;
                time1 =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();

                double milii = 0;
                OracleTimeStamp HEURE_DEB = new OracleTimeStamp(time1.Year, time1.Month, time1.Day, int.Parse(datetime2.Substring(0, 2)), int.Parse(datetime2.Substring(3, 2)), 1
                    , milii);

                //OracleTimeStamp HEURE_FIN = OracleTimeStamp.Parse(dtp_input3.Value.ToString());
                string datetime = dtp_input3.Value.ToString();
                OracleTimeStamp time11;
                time11 =Oracle.ManagedDataAccess.Types.OracleTimeStamp.GetSysDate();

                double mili = 0;
                OracleTimeStamp HEURE_FIN = new OracleTimeStamp(time11.Year, time11.Month, time11.Day, int.Parse(datetime.Substring(0, 2)), int.Parse(datetime.Substring(3, 2)), 1
                    , mili);



                /**************************************************/



                decimal AV_TECH = Convert.ToDecimal(TextBox3.Text.Trim());
                decimal AV_GLOBAL = Convert.ToDecimal(TextBox9.Text.Trim());
                decimal AV_ANG = Convert.ToDecimal(TextBox4.Text.Trim());
                decimal AV_FR = Convert.ToDecimal(TextBox6.Text.Trim());
                decimal AV_RAPPORT = Convert.ToDecimal(TextBox8.Text.Trim());
                decimal AV_CC = Convert.ToDecimal(TextBox7.Text.Trim());
                string AV_COMPORTEMENT = RadioButtonList1.SelectedValue.ToString();
                string REMARQUE = TextBox10.Text;
                string TRAVAIL = TextBox11.Text;
                decimal noteGroupe = decimal.Parse(TextBoxNoteGROUPE.Text);
                string id_groupe_projet = DropDownList8.SelectedValue;
                string type_projet = DropDownList4.SelectedValue;

                string id_endacrement = "ENCGRP";
                decimal id2 = ESP_ENCADREMENT_GROUPE.Instance.inc_id_groupe_encadrement();
                id_endacrement = id_endacrement + id2;

                string ID_PROJ= "PROJGROUP";
                decimal id = ESP_PROJET.Instance.inc_id_projet();
                ID_PROJ= ID_PROJ+ id;


                ESP_ENCADREMENT_GROUPE.Instance.openconntrans();
                ESP_ENCADREMENT_GROUPE.Instance.create_Encadrement_ESP_GROUPE(ID_PROJ, ID_ENS, ID_ET, id_endacrement, noteGroupe, CODE_CL, AV_TECH, AV_ANG, AV_FR, AV_RAPPORT, AV_CC, AV_COMPORTEMENT, REMARQUE, TRAVAIL, DATE_ENC, HEURE_DEB, HEURE_FIN, dureee, ANNEE_DEB, type_projet);
                ESP_ENCADREMENT_GROUPE.Instance.closeConnection();
                Response.Write("<script LANGUAGE='JavaScript'> alert('La suivi du projet a été effectué avec succes !!')</script>");
            }
        }

       

       

       
        
        /*
        * Panel 2
        * 
        */
       
        
    }
}
        
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add_project2017.aspx.cs"
    Inherits="ESPOnline.Enseignants.Add_project2017" MasterPageFile="~/Enseignants/Ens.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Contents/Css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Contents/Scripts/JScript1.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <link href="../Contents/cssencadrement/encadrement.css" type="text/javascript" />
    <style type="text/css">
        .successMerit
        {
            background-color: #1fa756;
            border: medium none;
            color: White;
        }
        .defaultColor
        {
            background-color: white;
            color: black;
        }
        .dangerFailed
        {
            background-color: #f2283a;
            color: White;
        }
    </style>
    <script src="http://s.codepen.io/assets/libs/modernizr.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../Contents/Encadrement/css/style.css">
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="../Contents/Encadrement/js/index.js"></script>
    <link rel="stylesheet" href="../Contents/Css/SMH.css" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <div id="content">
        <br />
        <center>
            <asp:Image ImageUrl="~/bootstrap-3.1.1-dist/css/Sans titre2.png" runat="server" />
           <br />
            <br />
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>

            
            <table align="left">
                <tr>
                    <td>
                        <asp:TextBox ID="Tbnom" runat="server" TextMode="SingleLine" Height="31px" Width="250px"
                            required="" placeholder="Titre de projet"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp
                    </td>
                    <td>
                        <telerik:RadComboBox ID="ddlmodule" runat="server"  EnableLoadOnDemand="True"
                            EmptyMessage="Tappez le module" Filter="Contains" Width="290px" Height="120px"
                            OnSelectedIndexChanged="ddlmatricule_SelectedIndexChanged">
                        </telerik:RadComboBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlmodule"
                            ForeColor="Red" ErrorMessage="Value Required!" InitialValue="--Select--"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp
                    </td>
                 
                </tr>
            </table>
            <br />
            <br />
            <hr  />
            <table align="left">
                <tr>
                    <td>
                        Type de projet:<asp:RadioButtonList ID="ddltypeprojet" runat="server" DataTextField="lib_nome"
                            DataValueField="code_nome">
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        Technologie:<asp:RadioButtonList ID="ddltechnologie" runat="server" DataTextField="lib_nome"
                            DataValueField="code_nome">
                        </asp:RadioButtonList>
                    </td>
                    <td>
                        Methodologie:<asp:RadioButtonList ID="ddlmethodologie" runat="server" 
                             DataTextField="lib_nome" DataValueField="code_nome">
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <hr />
            <table align="left">
            
            <tr>
            <td>
            Durée: <asp:DropDownList ID="ddlduree" runat="server">
                                                    <asp:ListItem Value="0">Veuillez choisir</asp:ListItem>
                                                      <asp:ListItem Value="1">Janvier</asp:ListItem>
                                                        <asp:ListItem Value="2">Fevrier</asp:ListItem>
                                                          <asp:ListItem Value="3">Mars</asp:ListItem>
                                                           <asp:ListItem Value="4">Avril</asp:ListItem>
                                                            <asp:ListItem Value="5">Mai</asp:ListItem>
                                                             <asp:ListItem Value="6">Juin</asp:ListItem>
                                                               <asp:ListItem Value="7">Juillet</asp:ListItem>
                                                                 <asp:ListItem Value="8">Aout</asp:ListItem>
                                                                   <asp:ListItem Value="9">Septembre</asp:ListItem>
                                                                     <asp:ListItem Value="10">Octobre</asp:ListItem>
                                                                       <asp:ListItem Value="11">Novembre</asp:ListItem>
                                                                         <asp:ListItem Value="12">Decembre</asp:ListItem>
                                                    
                                                    </asp:DropDownList>
                                            
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</td>
            <td>  
                     <asp:TextBox ID="Tbadress_et" runat="server" 
                     TextMode="MultiLine" placeholder="Description et vos remarques..." BackColor="#cbdce3"></asp:TextBox>
                    </td>


                    
<%--
                                          
   <td>     
        Semestre<asp:RadioButtonList ID="Rdsemestre" runat="server"   
            >
            <asp:ListItem Value="1">S1</asp:ListItem>
                 <asp:ListItem Value="2">S2</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 







<td>Période<asp:RadioButtonList ID="rdperiode" runat="server"   
            >
            <asp:ListItem Value="1">P1</asp:ListItem>
                 <asp:ListItem Value="2">P2</asp:ListItem>               
            </asp:RadioButtonList></td>
           --%>






            <tr><td></td><td></td> <td></td>
            <td>
                <asp:Button ID="Btnadd" runat="server" 
                                                                                           
                                                    Text="Valider" Width="150px"
                                                    Height="50px"  onclick="Btnadd_Click" 
                                                    /></td>
                                                    
           
           <td>  <asp:Button ID="Button1" runat="server"  
                                                Text="Annuler" Width="150px"
                                                    Height="50px"  onclick="Button1_Click" /></td>
            </tr>
              </table>
           <br />

         
         
            <asp:Label ID="Label111" runat="server" Text="Inscription avec succée" Visible="false"></asp:Label>
        </center>
    </div>

    <table>
    
    <tr>
          </tr>
    </table>
</asp:Content>

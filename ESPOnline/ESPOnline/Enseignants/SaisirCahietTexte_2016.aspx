﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaisirCahietTexte_2016.aspx.cs" 
Inherits="ESPOnline.Enseignants.SaisirCahietTexte_2016" MasterPageFile="~/Enseignants/Ens.Master" %>


 <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Contents/Css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="../Contents/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap.js" type="text/javascript"></script>
        <script type="text/javascript" src="../Contents/Scripts/JScript1.js"></script>
        <style type="text/css">
        
        
        .grise {
  border:1px solid black; 
  background-color:silver;
}

.blue {
  border:1px solid blue; 
}

.none {
  border-style:none;
}
        
        
        </style>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript">
            $("[src*=plus]").live("click", function () {
                $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
                $(this).attr("src", "../EnseignantsCUP/Styles/minus.png");
            });
            $("[src*=minus]").live("click", function () {
                $(this).attr("src", "../EnseignantsCUP/Styles/plus.png");
                $(this).closest("tr").next().remove();
            });
        </script>
              
         <style type="text/css">
      
      table.grid tbody tr:hover {background-color:#e5ecf9;}
.GridHeaderStyle{color:#FEF7F7;background-color: #877d7d;font-weight: bold;}
.GridItemStyle{background-color:#eeeeee;color: Black;}
.GridAlternatingStyle{background-color:#dddddd;color: black;}
.GridSelectedStyle{background-color:#d6e6f6;color: black;}


.GridStyle
{
	border-bottom: white 2px ridge; 
	border-left: white 2px ridge; 
	background-color: white; 
	width: 100%; 
	border-top: white 2px ridge; 
	border-right: white 2px ridge; 
}
.ItemStyle {
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}

.ItemStyle td{
	BACKGROUND-COLOR: #eeeeee; 
	COLOR: black;
	padding-bottom: 5px;
	padding-right: 3px;
	padding-top: 5px;
	padding-left: 3px;
	height: 25px
}
.FixedHeaderStyle {
	BACKGROUND-COLOR:  #7591b1; 
	COLOR: #FFFFFF; 
	FONT-WEIGHT: bold;
	position:relative ;   
	top:expression(this.offsetParent.scrollTop);  
	z-index: 10;  
}
.Caption_1_Customer
{
	background-color:#beccda;
	color: #000000;
	width: 30%;
	height: 20px;
}
 #hurfDurf table
 .Gridstudent
    {
      
        margin-right:80px;
    }
      
          </style>

         <%-- style row color chnged--%>
         <style type="text/css">
  .successMerit {
    background-color: #1fa756;
    border: medium none;
    color: White;
  }
    .defaultColor
    {
        background-color: white;
        color: black;
    }
  .dangerFailed {
    background-color: #f2283a;
    color: White;
  }
             </style>


         <style type="text/css"> 
.styled-button-10 {
	background:#8cce57;
	background:-moz-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-webkit-gradient(linear,left top,left bottom,color-stop(0%,#5CCD00),color-stop(100%,#4AA400));
	background:-webkit-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-o-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:-ms-linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	background:linear-gradient(top,#5CCD00 0%,#4AA400 100%);
	filter: progid: DXImageTransform.Microsoft.gradient( startColorstr='#5CCD00', endColorstr='#4AA400',GradientType=0);
	padding:10px 15px;
	color:#fff;
	font-family:'Helvetica Neue',sans-serif;
	font-size:16px;
	border-radius:5px;
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
	border:1px solid #459A00
}
             .style13
             {
                 color: #660033;
                 font-weight: bold;
                 font-size: medium;
             }
         </style>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<br />
<br />
<br />
<center>
<br />
<table border="1px" cellpadding="2px" cellspacing="2px" width="600px">
<tr>
<td>
 <img src="../images/Composition-1.gif" alt="ss" width="600px" height="80px" />
</td>
</tr>

</table>
<br />
<asp:ToolkitScriptManager ID="ToolkitScriptManager2" runat="server">
        </asp:ToolkitScriptManager>
<asp:UpdatePanel ID="tempcl" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<table>
<tr>
          <td>
          <asp:Label ID="lblll" runat="server" Text="Date séance :" CssClass="style13"></asp:Label>
              
          </td>
    
          <td>
           <asp:TextBox ID="TBdateseance" runat="server"  Width= "150px" ></asp:TextBox>
                <asp:CalendarExtender ID="TBdateseance_CalendarExtender" runat="server" 
                     Format="dd/MM/yy" TargetControlID="TBdateseance">
                </asp:CalendarExtender>
          </td>
         
        </tr>
        <tr><td><td></td></td></tr>

<tr>
<td>
<asp:Label ID="Label3" runat="server" Text="Horaire de travail:" 
        CssClass="style13" ></asp:Label></td><td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    de:<asp:DropDownList runat="server" ID="ddlcren1" DataTextField="lib-nome" AppendDataBoundItems="true"
    DataValueField="code_nome">
    
    </asp:DropDownList> &nbsp; à
    <asp:DropDownList runat="server" ID="ddlcren2" DataTextField="lib-nome" DataValueField="code_nome" AppendDataBoundItems="true">
    
    </asp:DropDownList>
    &nbsp;&nbsp;durée
      <asp:DropDownList runat="server" ID="DropDownList1" AppendDataBoundItems="true">
    <asp:ListItem Value="0">Veuillez choisir</asp:ListItem>
    <asp:ListItem Value="1.5">Une heure et demie</asp:ListItem>
    <asp:ListItem Value="3">Trois heures</asp:ListItem>
    </asp:DropDownList>

    </td>
</tr>
<tr><td></td><td></td></tr>
<tr>
<td>
<asp:Label ID="lblclasse" runat="server" Text="Votre classe:" 
        CssClass="style13" ></asp:Label></td><td>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:DropDownList ID="ddlclasses" runat="server" AutoPostBack="true" 
        AppendDataBoundItems="true" 
        onselectedindexchanged="ddlclasses_SelectedIndexChanged" Width= "150px" Height="30px">

</asp:DropDownList></td>
</tr>
<tr><td></td><td<</tr>
<tr>
<td>
<asp:Label ID="Label1" runat="server" Text="Matiére:" CssClass="style13" ></asp:Label></td><td>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<asp:DropDownList Width= "150px" Height="30px" ID="ddlmodule" runat="server" AutoPostBack="true"  DataTextField="DESIGNATION" DataValueField="code_module" >

</asp:DropDownList></td>
</tr>
<tr><td></td><td></td></tr>
<tr>

<td>

<asp:Label ID="lblmessage" runat="server" Text="Contenu traité" CssClass="style13"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td>
<asp:TextBox ID="txtmessage" runat="server" TextMode="MultiLine" Width="500px" Height="120px"></asp:TextBox>
</td>
</tr>
</table>
</ContentTemplate>
</asp:UpdatePanel>

</center>
<br />
<center>
<asp:Button  ID="btnvalid" runat="server" Text="Valider"/>
</center>
</asp:Content>
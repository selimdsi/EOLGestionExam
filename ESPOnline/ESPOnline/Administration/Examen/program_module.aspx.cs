﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Data;
using ESPSuiviEncadrement;
using System.Drawing;



namespace ESPOnline.Administration.Examen
{
    public partial class program_module : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();

        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //divMain.Style.Add("background-color", "gray");
                //get_annee_univ();
                ddlan_univer.Items.Insert(0, "---Veuillez choisir---");

                string ss = service.get_annee_parametr();

                string anne0 = service.get_annee_0();

                int annee00 = Convert.ToInt32(anne0);
                string ss2 = ss.Substring(0, 4);
                int ssi = Convert.ToInt32(ss2);
                int index = 1;
                for (int i = ssi; i >= annee00; i--)
                {

                    int rr = Convert.ToInt32(i.ToString());

                    int ff = rr + 1;
                    ddlan_univer.Items.Insert(index, new ListItem(i.ToString() + "-" + ff, i.ToString()));
                    index++;
                    //*************lblanneedeb.Text = service.get_annee_univ();

                    lblanneedeb.Text = service.get_annee_parametr();
                }
            }
        }

        public void get_annee_univ()
        {
            ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataSource = service.List_etud_2015();
            ddlan_univer.DataBind();


        }


        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }


        }

        protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlsite.SelectedValue.ToString() != null)
            {
                if (ddlsite.SelectedValue.ToString() != "ALL")
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_niveauGH("lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'");
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;
                    Label2.Text = " lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
                }
                else
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_niveauGH(Label1.Text.ToString());
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                }
            }
        }

        protected void ddlniv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlniv.SelectedValue != null)
            {
                if (ddlniv.SelectedValue.ToString() != "ALL")
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    if (Label2.Text == "")
                    {

                        ddclasse.DataSource = service.bind_classes("lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString());
                        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    }
                    else
                    {
                        Label1.Text = "lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";


                        ddclasse.DataSource = service.bind_classes("lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() + " ");

                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;

                        Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    }
                }
                else
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    if (Label2.Text.ToString() != "")
                    {

                        ddclasse.DataSource = service.bind_classes(Label1.Text.ToString() + " and " + Label2.Text.ToString());
                    }
                    else
                    {
                        ddclasse.DataSource = service.bind_classes(Label1.Text.ToString());
                    }
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddclasse.DataBind();
                    ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddclasse.SelectedItem.Selected = false;
                    ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                    Label3.Text = "";
                }

            }
        }

        protected void ddclasse_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (ddclasse.SelectedValue != null)
            {
                if (rdperiode.SelectedValue == "1")
                {
                    if (ddclasse.SelectedValue.ToString() != "ALL")
                    {
                        Gridexam.DataSource = service.bind_exam_aprogrammer("upper(code_cl) like '" + ddclasse.SelectedValue + "%'", "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");
                        //+ " " + "and" + " " + Label3.Text.ToString()
                        Gridexam.DataBind();
                        Gridexam.Visible = true;
                        Label1.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'";
                        Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                        Label3.Text = "";



                    }

                    else
                    {
                        Label1.Text = "lower(a.code_cl) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                        Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                        Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text.ToString(), "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");

                    }

                    Gridexam.DataBind();
                    Gridexam.Visible = true;
                    Label4.Text = "";
                    btnUpdate.Visible = true;
                }




                else

                    if (rdperiode.SelectedValue == "2")
                    {
                        if (ddclasse.SelectedValue.ToString() != "ALL")
                        {
                            Gridexam.DataSource = service.bind_exam_aprogrammerP2("UPPER(code_cl) like '" + ddclasse.SelectedValue + "%'", "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");
                            //+ " " + "and" + " " + Label3.Text.ToString()
                            Gridexam.DataBind();
                            Gridexam.Visible = true;
                            Label1.Text = "UPPER(a.code_cl) like '" + ddclasse.SelectedValue.ToString() + "%'";
                            Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                            Label3.Text = "";


                        }

                        else
                        {
                            Label1.Text = "lower(a.code_cl) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                            Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                            Gridexam.DataSource = service.bind_exam_aprogrammerP2(Label1.Text.ToString(), "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");

                        }
                        Gridexam.DataBind();
                        Gridexam.Visible = true;
                        Label4.Text = "";
                        btnUpdate.Visible = true;


                    }

            }
        }

        private void BindData()
        {
            Gridexam.DataSource = service.bind_exam_aprogrammer(ddclasse.SelectedValue, Rdsemestre.SelectedValue);
            Gridexam.DataBind();
        }

        protected void EditCustomer(object sender, GridViewEditEventArgs e)
        {
            Gridexam.EditIndex = e.NewEditIndex;
            this.BindData();
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Gridexam.EditIndex = -1;
            BindData();
        }


        protected void Gridexam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {
            if (CheckBox22.Checked == false)
            {
                GridexambyModule.Enabled = false;
                GridexambyModule.Visible = false;
                Gridexam.Enabled = true;
                Gridexam.Visible = true;
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Rdcours.SelectedValue == "C")
                    {


                        if (DropDownList4.SelectedValue.ToString() != "ALL")
                        {

                            OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM ESP_MODULE_PANIER_CLASSE_SAISO a where  code_cl like '" + DropDownList4.SelectedValue + "%' AND CODE_MODULE=:code_module and( charge_p1=:charge_p1 or charge_p2=:charge_p2) and ANNEE_DEB like '2016' and num_semestre='" + Rdsemestre.SelectedValue + "' order by FN_TRI_CLASSE(code_cl)");

                            cmd.Parameters.Add(":code_module", e.Row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text);

                            cmd.Parameters.Add(":charge_p1", e.Row.Cells[5].Controls.OfType<Label>().FirstOrDefault().Text);

                            cmd.Parameters.Add(":charge_p2", e.Row.Cells[6].Controls.OfType<Label>().FirstOrDefault().Text);
                            //string annee_deb = ddlan_univer.SelectedValue;



                            //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                            ListBox ddlCountries = (e.Row.FindControl("ddlCountries") as ListBox);
                            ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
                            ddlCountries.DataTextField = "code_cl";
                            ddlCountries.DataValueField = "code_cl";
                            // ddlCountries.Items.Add("NN");
                            // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                            ddlCountries.DataBind();




                            OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");

                            DropDownList ddlCountriesS = (e.Row.FindControl("ddlCountriesS") as DropDownList);
                            ddlCountriesS.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                            ddlCountriesS.DataTextField = "num_seance";
                            ddlCountriesS.DataValueField = "num_seance";
                            ddlCountriesS.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                            //ddlCountriesS.SelectedItem.Selected = false;
                            ddlCountriesS.Items.FindByText("Choisir").Selected = true;

                            // ddlCountries.Items.Add("NN");
                            // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                            ddlCountriesS.DataBind();



                        }

                        else
                        {
                            OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM ESP_MODULE_PANIER_CLASSE_SAISO a where code_cl like '" + DropDownList3.SelectedValue + "%' AND CODE_MODULE=:code_module and (charge_p1=:charge_p1 or charge_p2=:charge_p2) and ANNEE_DEB like '2016' and num_semestre='" + Rdsemestre.SelectedValue + "' order by FN_TRI_CLASSE(code_cl)");
                            //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM ESP_MODULE_PANIER_CLASSE_SAISO a where a.code_cl like '" + DropDownList3.SelectedValue + "%' AND a.CODE_MODULE=:code_module  and a.ANNEE_DEB like '2015' and a.num_semestre='" + Rdsemestre.SelectedValue + "' order by FN_TRI_CLASSE(code_cl)");

                            cmd.Parameters.Add(":code_module", e.Row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text);

                            cmd.Parameters.Add(":charge_p1", e.Row.Cells[5].Controls.OfType<Label>().FirstOrDefault().Text);

                            cmd.Parameters.Add(":charge_p2", e.Row.Cells[6].Controls.OfType<Label>().FirstOrDefault().Text);
                            //string annee_deb = ddlan_univer.SelectedValue;



                            //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                            DropDownList ddlCountries = (e.Row.FindControl("ddlCountries") as DropDownList);
                            ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
                            ddlCountries.DataTextField = "code_cl";
                            ddlCountries.DataValueField = "code_cl";

                            // ddlCountries.Items.Add("NN");
                            // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                            ddlCountries.DataBind();



                            OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");



                            //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                            DropDownList ddlCountriesS = (e.Row.FindControl("ddlCountriesS") as DropDownList);
                            ddlCountriesS.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                            ddlCountriesS.DataTextField = "num_seance";
                            ddlCountriesS.DataValueField = "num_seance";
                            ddlCountriesS.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                            //ddlCountriesS.SelectedItem.Selected = false;
                            ddlCountriesS.Items.FindByText("Choisir").Selected = true;
                            ddlCountriesS.DataBind();





                        }

                    }

                    else
                    {
                        if (Rdcours.SelectedValue == "A")
                        {

                            if (ddclasse.SelectedValue.ToString() != "ALL")
                            {

                                OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM ESP_MODULE_PANIER_CLASSE_SAISO a where code_cl like '" + ddclasse.SelectedValue + "%' AND CODE_MODULE=:code_module and (charge_p1=:charge_p1 or charge_p2=:charge_p2) and ANNEE_DEB like '2016' and num_semestre='" + Rdsemestre.SelectedValue + "' order by FN_TRI_CLASSE(code_cl)");

                                cmd.Parameters.Add(":code_module", e.Row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text);

                                cmd.Parameters.Add(":charge_p1", e.Row.Cells[5].Controls.OfType<Label>().FirstOrDefault().Text);

                                cmd.Parameters.Add(":charge_p2", e.Row.Cells[6].Controls.OfType<Label>().FirstOrDefault().Text);
                                //string annee_deb = ddlan_univer.SelectedValue;



                                //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                                DropDownList ddlCountries = (e.Row.FindControl("ddlCountries") as DropDownList);
                                ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
                                ddlCountries.DataTextField = "code_cl";
                                ddlCountries.DataValueField = "code_cl";
                                // ddlCountries.Items.Add("NN");
                                // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                                ddlCountries.DataBind();




                                //bind seance
                                OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");


                                //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                                DropDownList ddlCountriesS = (e.Row.FindControl("ddlCountriesS") as DropDownList);
                                ddlCountriesS.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                                ddlCountriesS.DataTextField = "num_seance";
                                ddlCountriesS.DataValueField = "num_seance";
                                ddlCountriesS.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                                ddlCountriesS.SelectedItem.Selected = true;
                                ddlCountriesS.Items.FindByText("Choisir").Selected = true;
                                ddlCountriesS.DataBind();




                            }

                            else
                            {
                                OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM ESP_MODULE_PANIER_CLASSE_SAISO a where code_cl like '" + ddlniv.SelectedValue + "%' AND CODE_MODULE=:code_module and charge_p1=:charge_p1 and charge_p2=:charge_p2 and ANNEE_DEB like '2016' and num_semestre='" + Rdsemestre.SelectedValue + "' order by FN_TRI_CLASSE(code_cl)");

                                cmd.Parameters.Add(":code_module", e.Row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text);

                                cmd.Parameters.Add(":charge_p1", e.Row.Cells[5].Controls.OfType<Label>().FirstOrDefault().Text);

                                cmd.Parameters.Add(":charge_p2", e.Row.Cells[6].Controls.OfType<Label>().FirstOrDefault().Text);
                                //string annee_deb = ddlan_univer.SelectedValue;



                                //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                                DropDownList ddlCountries = (e.Row.FindControl("ddlCountries") as DropDownList);
                                ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
                                ddlCountries.DataTextField = "code_cl";
                                ddlCountries.DataValueField = "code_cl";
                                // ddlCountries.Items.Add("NN");
                                // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                                ddlCountries.DataBind();


                                OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");

                                //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                                DropDownList ddlCountriesS = (e.Row.FindControl("ddlCountriesS") as DropDownList);
                                ddlCountriesS.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                                ddlCountriesS.DataTextField = "num_seance";
                                ddlCountriesS.DataValueField = "num_seance";
                                ddlCountriesS.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                                ddlCountriesS.SelectedItem.Selected = true;
                                ddlCountriesS.Items.FindByText("Choisir").Selected = true;
                                ddlCountriesS.DataBind();



                            }
                        }
                    }
                    //string country = (e.Row.FindControl("lblCountry") as Label).Text;
                    // ddlCountries.Items.FindByValue(country).Selected = true;


                    Label p1 = (e.Row.FindControl("lblPrice12") as Label);
                    Label p2 = (e.Row.FindControl("lblPrice1") as Label);
                    RadioButtonList exds = (e.Row.FindControl("RadioButtonds") as RadioButtonList);

                    if (p1.Text == "")
                    {
                        p1.Text = "0";
                    }

                    else
                    {
                        p1 = (e.Row.FindControl("lblPrice12") as Label);

                    }

                    if (p2.Text == "")
                    {
                        p2.Text = "0";
                    }

                    else
                    {
                        p2 = (e.Row.FindControl("lblPrice1") as Label);
                    }
                    decimal valp1 = Convert.ToDecimal(p1.Text);
                    //int valp1 = Convert.ToInt32(p1);
                    decimal valp2 = Convert.ToDecimal(p2.Text);
                    if (rdperiode.SelectedValue == "1")
                    {



                        if (valp1 > 0 && valp2 == 0)
                        {
                            exds.SelectedValue = "EX";



                        }

                        else
                        {
                            if (valp1 > 0 && valp2 > 0)
                            {
                                exds.SelectedValue = "DS";
                            }

                        }

                    }


                    else
                    {
                        if (rdperiode.SelectedValue == "2")
                        {
                            if (valp2 > 0)
                            {

                                exds.SelectedValue = "EX";
                            }

                        }
                    }


                }

            }
            else if (CheckBox22.Checked == true)
            {
                GridexambyModule.Enabled = true;
                GridexambyModule.Visible = true;
                Gridexam.Enabled = false;
                Gridexam.Visible = false;
            }
        }

        protected void OnCheckedChangedDDD(object sender, EventArgs e)
        {

        }

        protected void OnCheckedChanged(object sender, EventArgs e)
        {

            //foreach (GridViewRow row in Gridexam.Rows)
            //{


            //    DropDownList ddlR = (DropDownList)row.Cells[2].FindControl("ddlCountriesS");
            //    //CheckBox chkBox = (CheckBox)Gridexam.FindControl("CheckBox1");
            //    CheckBox ck1 = (CheckBox)row.Cells[1].FindControl("CheckBox1");

            //    if (ck1.Checked == true)
            //    {

            //        ddlR.Enabled = true;


            //    }
            //    else { ddlR.Enabled = false; }  

            //}

        }



        protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        { }


        protected void ddlDropSEANCE_SelectedIndexChanged(object sender, EventArgs e)
        {
            int x = 0;

            foreach (GridViewRow row in Gridexam.Rows)
            {

                if (row.RowType == DataControlRowType.DataRow)
                {
                    //DropDownList ddlCountries = (row.FindControl("ddlCountriesS") as DropDownList);

                    //string num_seance = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value;



                    for (int i = 0; i < Gridexam.Rows.Count - 1; i++)
                    {
                        for (int j = 1; j < Gridexam.Rows.Count; j++)
                        {

                            //int j = i + 1;
                            if (i != j)
                            {
                                if (Gridexam.Rows[i].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                                {

                                    if (Gridexam.Rows[i].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                                    {
                                        x = 99;

                                        OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");


                                        Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                                        Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "num_seance";
                                        Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "num_seance";
                                        Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmd2, "SELECT");
                                        Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                                        Gridexam.Rows[j].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));

                                    }
                                }
                            }

                        }
                    }

                }
            }
            if (x == 99)
            {
                Response.Write(@"<script language='javascript'>alert('Séance déjà affectée');</script>");

            }

        }

        //editer in a gridview$

        protected void Enregistrer(object sender, EventArgs e)
        {
            for (int d = 0; d < Gridexam.Rows.Count - 1; d++)
            {
                if (Gridexam.Rows[d].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                {

                    if (Rdcours.SelectedValue == "C")
                    {
                        foreach (GridViewRow row in Gridexam.Rows)
                        {

                            if (row.RowType == DataControlRowType.DataRow)
                            {
                                ListBox ddlCountries = (row.FindControl("ddlCountries") as ListBox);



                                bool isChecked = row.Cells[1].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                                if (isChecked)
                                {

                                    // string a_programmer,string type_exam,string code_cl,string code_module,string num_semestre,string num_periode, string DS_EXAM,string anne

                                    //for (int i = 0; i < ddlCountries.Selected.Count; i++)
                                    //{


                                    for (int i = 0; i < ddlCountries.Items.Count; i++)
                                    {
                                        if (ddlCountries.Items[i].Selected)
                                        {
                                            string num_seance = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value;

                                            string a_programmer = row.Cells[9].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                            string type_exam = row.Cells[10].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                            string code_cl = ddlCountries.Items[i].Value;
                                            //string code_cl = row.Cells[8].Controls.OfType<ListBox>().FirstOrDefault().SelectedItem.Value;
                                            //ListBox myListBox = (ListBox)Gridexam.row.Cells[].FindControl("ddlCountries");


                                            string code_module = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;
                                            string num_semestre = Rdsemestre.SelectedValue;
                                            string num_periode = rdperiode.SelectedValue;
                                            string DS_EXAM = row.Cells[7].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;


                                            string anne = lblanneedeb.Text.Substring(0, 4);
                                            service.affecter_module_examen(num_seance, a_programmer, type_exam, code_cl, code_module, Convert.ToInt32(num_semestre), num_periode, DS_EXAM, anne, DropDownList2.SelectedValue);

                                        }
                                    }




                                }
                            }

                        }

                        Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");
                        // BindData();
                        /////////////////*************************************************/////////////////////
                        if (rdperiode.SelectedValue == "1")
                        {


                            if (DropDownList4.SelectedValue != "ALL")
                            {
                                Label1.Text = "lower(a.code_cl) like '" + DropDownList4.SelectedValue.ToString().ToLower() + "%'";
                                Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";


                                Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text.ToString(), Label2.Text.ToString());
                                Gridexam.DataBind();

                                btnUpdate.Visible = true;
                            }

                            else
                            {
                                Label1.Text = "UPPER(a.code_cl) like '" + DropDownList4.SelectedValue.ToString().ToLower() + "%'";
                                Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";


                                Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text.ToString(), Label2.Text.ToString());
                                Gridexam.DataBind();

                                btnUpdate.Visible = true;
                            }

                        }

                        else
                        {

                            if (rdperiode.SelectedValue == "2")
                            {
                                Label1.Text = "lower(a.code_cl) like '" + DropDownList4.SelectedValue.ToString().ToLower() + "%'";
                                Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                                Gridexam.DataSource = service.bind_exam_aprogrammerP2(Label1.Text, Label2.Text);
                                Gridexam.DataBind();

                                btnUpdate.Visible = true;
                            }

                        }


                    }

                    else
                    {

                        if (Rdcours.SelectedValue == "A")
                        {

                            foreach (GridViewRow row in Gridexam.Rows)
                            {

                                if (row.RowType == DataControlRowType.DataRow)
                                {
                                    ListBox ddlCountries = (row.FindControl("ddlCountries") as ListBox);



                                    bool isChecked = row.Cells[1].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                                    if (isChecked)
                                    {



                                        for (int i = 0; i < ddlCountries.Items.Count; i++)
                                        {
                                            if (ddlCountries.Items[i].Selected)
                                            {
                                                string num_seance = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value;

                                                string a_programmer = row.Cells[9].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                                string type_exam = row.Cells[10].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                                string code_cl = ddlCountries.Items[i].Value;
                                                //string code_cl = row.Cells[8].Controls.OfType<ListBox>().FirstOrDefault().SelectedItem.Value;
                                                //ListBox myListBox = (ListBox)Gridexam.row.Cells[].FindControl("ddlCountries");


                                                string code_module = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;
                                                string num_semestre = Rdsemestre.SelectedValue;
                                                string num_periode = rdperiode.SelectedValue;
                                                string DS_EXAM = row.Cells[7].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;


                                                string anne = lblanneedeb.Text.Substring(0, 4);
                                                service.affecter_module_examen(num_seance, a_programmer, type_exam, code_cl, code_module, Convert.ToInt32(num_semestre), num_periode, DS_EXAM, anne, DropDownList2.SelectedValue);

                                            }
                                        }

                                    }
                                }

                            }

                            Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");
                            if (rdperiode.SelectedValue == "1")
                            {

                                if (ddclasse.SelectedValue != "ALL")
                                {
                                    Label1.Text = "UPPER(a.code_cl) like '" + ddclasse.SelectedValue.ToString() + "%'";
                                    Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                                    Gridexam.DataSource = service.bind_exam_aprogrammer(ddclasse.SelectedValue, Rdsemestre.SelectedValue);
                                    Gridexam.DataBind();

                                    btnUpdate.Visible = true;
                                }

                                else
                                {
                                    Label1.Text = "upper(a.code_cl) like '" + ddlniv.SelectedValue.ToString() + "%'";
                                    Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                                    Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text, Label2.Text);
                                    Gridexam.DataBind();

                                    btnUpdate.Visible = true;
                                }

                            }

                            else
                            {
                                if (rdperiode.SelectedValue == "2")
                                {
                                    if (ddclasse.SelectedValue != "ALL")
                                    {

                                        ///////////////ici no all
                                        Label1.Text = "upper(a.code_cl) like '" + ddclasse.SelectedValue.ToString() + "%'";
                                        Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";


                                        Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text, Label2.Text);
                                        Gridexam.DataBind();

                                        btnUpdate.Visible = true;
                                    }
                                    else
                                    {

                                        Label1.Text = "upper(a.code_cl) like '" + ddclasse.SelectedValue.ToString() + "%'";
                                        Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                                        Gridexam.DataSource = service.bind_exam_aprogrammerP2(Label1.Text, Label2.Text);
                                        Gridexam.DataBind();

                                        btnUpdate.Visible = true;
                                    }
                                }

                            }

                        }

                    }

                }
                else Response.Write(@"<script language='javascript'>alert('sélectionner la bonne séance');</script>");
            }
        }
        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void btnvalid_Click(object sender, EventArgs e)
        {
            if (Rdsemestre.SelectedValue == "" || rdperiode.SelectedValue == "")
            {
                Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

            }
            //else
            //{
            //    lbls.Text = Rdsemestre.SelectedValue;
            //    lbls.ForeColor = System.Drawing.Color.Chocolate;
            //    lblp.Text = rdperiode.SelectedValue;

            //    lblp.ForeColor = System.Drawing.Color.Chocolate;

            //    plvalider.Visible = false;
            //    plannee.Visible = true;
            //}

            else
            {


                plvalider.Visible = false;

                Plannee2.Visible = true;
                plannee.Visible = false;
                Plchoixannee.Visible = false;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";
                string annee_deb = service.get_annee_parametr();

                Label8.Text = Rdsemestre.SelectedValue;
                Label9.Text = rdperiode.Text;
                //DropDownList2.DataSource = service.bind_site2(annee_deb.Substring(0, 4));

                //DropDownList2.DataBind();

                Label1.Text = "a.ANNEE_DEB= '" + annee_deb.ToString().Substring(0, 4) + "'";
                //Label1.Text = "a.ANNEE_DEB= '" + lblanneedeb.Text.ToString().Substring(0, 4) + "'";

            }
        }


        //protected void Submit(object sender, EventArgs e)
        //{
        //    string message = "";
        //    foreach (ListItem item in ddlCountries.Items)
        //    {
        //        if (item.Selected)
        //        {
        //            message += item.Text + " " + item.Value + "\\n";
        //        }
        //    }
        //    ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('" + message + "');", true);
        //}



        protected void Btnsuivant_Click(object sender, EventArgs e)
        {

            if (Rdcours.SelectedValue == "C")
            {
                Plannee2.Visible = true;
                plannee.Visible = false;
                Plchoixannee.Visible = false;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";
                string annee_deb = service.get_annee_parametr();

                Label8.Text = Rdsemestre.SelectedValue;
                Label9.Text = rdperiode.Text;
                //DropDownList2.DataSource = service.bind_site2(annee_deb.Substring(0, 4));

                //DropDownList2.DataBind();

                Label1.Text = "a.ANNEE_DEB= '" + annee_deb.ToString().Substring(0, 4) + "'";
                //Label1.Text = "a.ANNEE_DEB= '" + lblanneedeb.Text.ToString().Substring(0, 4) + "'";

            }
            else
            {
                if (Rdcours.SelectedValue == "A")
                {
                    Plannee2.Visible = false;
                    plannee.Visible = true;
                    Plchoixannee.Visible = false;


                    lbls.Text = Rdsemestre.SelectedValue;
                    lblp.Text = rdperiode.SelectedValue;
                }

                else
                {

                    Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

                }
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bind niveau

            DropDownList3.Enabled = true;

            if (DropDownList2.SelectedValue.ToString() != null)
            {
                if (DropDownList2.SelectedValue.ToString() != "ALL")
                {
                    DropDownList3.DataTextField = "niveau";
                    DropDownList3.DataValueField = "niveau";
                    DropDownList3.DataSource = service.bind_niveauGH(DropDownList2.SelectedValue.ToString().ToLower());
                    ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList3.DataBind();

                    DropDownList3.SelectedItem.Selected = false;
                    //DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;
                    Label2.Text = DropDownList2.SelectedValue.ToString().ToLower();

                    Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                }
                else
                {
                    DropDownList3.DataTextField = "niveau";
                    DropDownList3.DataValueField = "niveau";
                    DropDownList3.DataSource = service.bind_niveauGH(Label1.Text.ToString());
                    ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList3.DataBind();
                    DropDownList3.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    DropDownList3.SelectedItem.Selected = false;
                    DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                    Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                }


            }
            else
            {

                if (DropDownList2.SelectedValue.ToString() != "Charguia")
                {
                    DropDownList3.DataTextField = "niveau";
                    DropDownList3.DataValueField = "niveau";
                    DropDownList3.DataSource = service.bind_niveauGH(Label1.Text.ToString());
                    ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList3.DataBind();
                    DropDownList3.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    DropDownList3.SelectedItem.Selected = false;
                    DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                    Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                }

                else
                {
                    if (DropDownList2.SelectedValue.ToString() == "Ghazala")
                    {
                        DropDownList3.DataTextField = "niveau";
                        DropDownList3.DataValueField = "niveau";
                        DropDownList3.DataSource = service.bind_niveauGH(DropDownList2.SelectedValue.ToString().ToLower());
                        ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList3.DataBind();

                        DropDownList3.SelectedItem.Selected = false;
                        //DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;
                        Label2.Text = DropDownList2.SelectedValue.ToString().ToLower();

                        Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                    }
                }
            }

        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList4.Enabled = true;
            //if (DropDownList2.SelectedValue == "2")

            /*
              if (DropDownList3.SelectedValue != null)
            {
                if (DropDownList3.SelectedValue.ToString() != "ALL")
                {
                           
                    DropDownList4.DataTextField = "code_cl";
                    DropDownList4.DataValueField = "code_cl";
                    if (Label2.Text == "")
                    {

                        DropDownList4.DataSource = service.bind_classes(DropDownList3.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString());
                         ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList4.DataBind();
                        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        DropDownList4.SelectedItem.Selected = false;
                        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

                    }
                    else
                    {
                        Label1.Text = "lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";


                       DropDownList4.DataSource = service.bind_classes("lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'" + " " + "and" + " " + Label1.Text.ToString() + " ");

                        DropDownList4.DataBind();
                        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        DropDownList4.SelectedItem.Selected = false;
                        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;

                        Label3.Text = "lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

                    }
                }
                else
                {
                    DropDownList4.DataTextField = "code_cl";
                    DropDownList4.DataValueField = "code_cl";
                    if (Label2.Text.ToString() != "")
                    {
                        Label1.Text = "lower(SITE) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";


                        DropDownList4.DataSource = service.bind_classes(Label1.Text.ToString());
                    }
                    else
                    {
                        DropDownList4.DataSource = service.bind_classes(Label1.Text.ToString());
                    }
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList4.DataBind();
                    DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    DropDownList4.SelectedItem.Selected = false;
                    DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
                    Label3.Text = "";
                }

            }
             */
            if (DropDownList3.SelectedValue != null)
            {
                if (DropDownList3.SelectedValue.ToString() != "ALL")
                {
                    DropDownList4.DataTextField = "code_cl";
                    DropDownList4.DataValueField = "code_cl";
                    if (DropDownList2.SelectedValue == "Ghazala")
                    {

                        DropDownList4.DataSource = service.bind_classesgh(DropDownList3.SelectedValue.ToString().ToLower());
                        ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList4.DataBind();
                        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        DropDownList4.SelectedItem.Selected = false;
                        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

                    }
                    else
                    {

                        if (DropDownList2.SelectedValue == "Charguia")
                        {
                            // Label1.Text = "lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";


                            DropDownList4.DataSource = service.bind_classesCH(DropDownList3.SelectedValue.ToString().ToLower());

                            DropDownList4.DataBind();
                            DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                            DropDownList4.SelectedItem.Selected = false;
                            DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;

                            Label3.Text = DropDownList3.SelectedValue.ToString().ToLower();
                        }
                    }
                }
                else
                {
                    if (DropDownList3.SelectedValue.ToString() == "ALL")
                    {
                        DropDownList4.DataTextField = "code_cl";
                        DropDownList4.DataValueField = "code_cl";
                        DropDownList4.DataSource = service.bind_classesALL();

                        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList4.DataBind();
                        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        DropDownList4.SelectedItem.Selected = false;
                        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
                        Label3.Text = "";
                    }
                }

            }





        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (ddclasse.SelectedValue != null)
            {
                if (rdperiode.SelectedValue == "1")
                {
                    if (DropDownList4.SelectedValue.ToString() != "ALL")  /////////////////////////////////////
                    {
                        Gridexam.DataSource = service.bind_exam_aprogrammer("upper(code_cl) like '" + DropDownList4.SelectedValue + "%'", "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");
                        //+ " " + "and" + " " + Label3.Text.ToString()
                        Gridexam.DataBind();
                        btnUpdate.Visible = true;
                        Gridexam.Visible = true;
                        Label1.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'";
                        Label2.Text = "NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                        Label3.Text = "";

                        OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");


                        //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                        //DropDownList ddlCountriesS = (e.Row.FindControl("ddlCountriesS") as DropDownList);
                        //ddlCountriesS.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                        //ddlCountriesS.DataTextField = "num_seance";
                        //ddlCountriesS.DataValueField = "num_seance";
                        //ddlCountriesS.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                        //ddlCountriesS.SelectedItem.Selected = true;
                        //ddlCountriesS.Items.FindByText("Choisir").Selected = true;
                        //ddlCountriesS.DataBind();


                    }

                    else
                    {
                        Label1.Text = "lower(a.code_cl) like '" + DropDownList3.SelectedValue.ToString() + "%'";

                        Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                        Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text.ToString(), Label2.Text);
                        Gridexam.DataBind();
                        btnUpdate.Visible = true;


                        OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");


                        //OracleCommand cmd = new OracleCommand("SELECT  code_cl FROM Classes1516 where code_cl like '" + ddclasse.SelectedValue + "%'order by FN_TRI_CLASSE(code_cl)");
                        //DropDownList ddlCountriesS = (e.Row.FindControl("ddlCountriesS") as DropDownList);
                        //ddlCountriesS.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                        //ddlCountriesS.DataTextField = "num_seance";
                        //ddlCountriesS.DataValueField = "num_seance";
                        //ddlCountriesS.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                        //ddlCountriesS.SelectedItem.Selected = true;
                        //ddlCountriesS.Items.FindByText("Choisir").Selected = true;
                        //ddlCountriesS.DataBind();


                    }
                    Gridexam.DataBind();
                    Gridexam.Visible = true;
                    Label4.Text = "";

                }




                else

                    if (rdperiode.SelectedValue == "2")
                    {
                        if (DropDownList4.SelectedValue.ToString() != "ALL")
                        {
                            Gridexam.DataSource = service.bind_exam_aprogrammerP2("UPPER(code_cl) like '" + DropDownList4.SelectedValue + "%'", "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");
                            //+ " " + "and" + " " + Label3.Text.ToString()
                            Gridexam.DataBind();

                            btnUpdate.Visible = true;
                            Gridexam.Visible = true;
                            Label1.Text = "UPPER(a.code_cl) like '" + DropDownList4.SelectedValue.ToString() + "%'";
                            Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                            Label3.Text = "";


                        }

                        else
                        {
                            Label1.Text = "lower(a.code_cl) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

                            Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

                            Gridexam.DataSource = service.bind_exam_aprogrammerP2(Label1.Text.ToString(), Label2.Text);
                            Gridexam.DataBind();
                            btnUpdate.Visible = true;
                        }
                    }
            }

        }

        protected void OnChecked22Changedmodule(object sender, EventArgs e)
        {
            if (CheckBox22.Checked == true)
            {
                //DropDownList2.Visible = false;
                //LabelSite.Visible = false;

                DropDownList3.Visible = false;
                LabelNiv.Visible = false;

                DropDownList4.Visible = false;
                LabelNivDet.Visible = false;

                DropDownListmodule.Visible = true;
                LabelMod.Visible = true;

                GridexambyModule.Enabled = true;
                GridexambyModule.Visible = true;
                Gridexam.Enabled = false;
                Gridexam.Visible = false;


            }
            else if (CheckBox22.Checked == false)
            {
                //DropDownList2.Visible = true;
                //LabelSite.Visible = true;

                DropDownList3.Visible = true;
                LabelNiv.Visible = true;

                DropDownList4.Visible = true;
                LabelNivDet.Visible = true;

                DropDownListmodule.Visible = false;
                LabelMod.Visible = false;

                GridexambyModule.Enabled = false;
                GridexambyModule.Visible = false;
                Gridexam.Enabled = true;
                Gridexam.Visible = true;

            }

        }

        protected void DropDownListmoduleonselect(object sender, EventArgs e)
        {
            Gridexam.Enabled = false;
            GridexambyModule.Enabled = true;
            enregbymodule.Visible = true;
            GridexambyModule.DataSource = service.bind_exam_aprogrammerbymodule(DropDownListmodule.SelectedValue, Rdsemestre.SelectedValue);
            GridexambyModule.DataBind();

            foreach (GridViewRow row in GridexambyModule.Rows)
            {
                OracleCommand cmd2 = new OracleCommand("SELECT distinct num_seance FROM esp_seance_examen");

                DropDownList ddlCountriesS0 = (DropDownList)row.Cells[2].FindControl("ddlCountriesS0");

                ddlCountriesS0.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                ddlCountriesS0.DataTextField = "num_seance";
                ddlCountriesS0.DataValueField = "num_seance";
                ddlCountriesS0.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                ddlCountriesS0.Items.FindByText("Choisir").Selected = true;
                ddlCountriesS0.DataBind();
            }

        }

        protected void EnregistrerByModule(object sender, EventArgs e)
        {
            foreach (GridViewRow row in GridexambyModule.Rows)
            {
                for (int d = 0; d < GridexambyModule.Rows.Count - 1; d++)
                {
                    if (GridexambyModule.Rows[d].Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                    {

                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            ListBox ddlCountries = (row.FindControl("ddlCountriesS0") as ListBox);
                            bool isChecked = row.Cells[1].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                            if (isChecked)
                            {
                                string num_seance = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value;

                                string a_programmer = row.Cells[9].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                string type_exam = row.Cells[10].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                string code_cl = row.Cells[8].Controls.OfType<Label>().FirstOrDefault().Text;

                                string code_module = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;
                                string num_semestre = Rdsemestre.SelectedValue;
                                string num_periode = rdperiode.SelectedValue;
                                string DS_EXAM = row.Cells[7].Controls.OfType<RadioButtonList>().FirstOrDefault().SelectedItem.Value;

                                string anne = lblanneedeb.Text.Substring(0, 4);

                                service.affecter_module_examen_by_module(num_seance, a_programmer, type_exam, code_cl, code_module, Convert.ToInt32(num_semestre), num_periode, DS_EXAM, anne, DropDownList2.SelectedValue);

                            };
                        }

                    }

                    Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");

                }
                Response.Write(@"<script language='javascript'>alert('sélectionner la bonne séance');</script>");
            }

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;
using BLL;

namespace ESPOnline.Administration.Examen
{
    public partial class program_module_MAJ : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();

        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                  ddlan_univer.Items.Insert(0, "---Veuillez choisir---");

                string ss = service.get_annee_parametr();

                string anne0 = service.get_annee_0();

                int annee00 = Convert.ToInt32(anne0);
                string ss2 = ss.Substring(0,4);
                int ssi = Convert.ToInt32(ss2);
                int index = 1;
                for (int i = ssi; i >= annee00; i--)
                {

                    int rr = Convert.ToInt32(i.ToString());

                    int ff = rr + 1;
                    ddlan_univer.Items.Insert(index, new ListItem(i.ToString() + "-" + ff, i.ToString()));
                    index++;
                    //*************lblanneedeb.Text = service.get_annee_univ();

                    lblanneedeb.Text = service.get_annee_parametr();
                }        
            }
            }
    

        public void get_annee_univ()
        {
            ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataSource = service.List_etud_2015();
            ddlan_univer.DataBind();
        }

        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }
        }



        protected void ddlniv_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlniv.SelectedValue != null)
            {
                if (ddlniv.SelectedValue.ToString() != "ALL")
                {
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                    //if (Label2.Text == "")
                    //{

                        ddclasse.DataSource = service.bind_cls("lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'", "and niveau like '" + ddlniv.SelectedValue + "%'");
                        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        ddclasse.DataBind();
                        ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        ddclasse.SelectedItem.Selected = false;
                        ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
                        Label4.Text = "and niveau like '" + ddlniv.SelectedValue + "%'";

                    //}
                    //else
                    //{

                    //    ddclasse.DataSource = service.bind_cls("lower(site) like '" + ddlsite.SelectedValue.ToString() + "%'", "and niveau like '" + ddlniv.SelectedValue + "%'");
                      
                    //    ddclasse.DataBind();
                    //    ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    //    ddclasse.SelectedItem.Selected = false;
                    //    ddclasse.Items.FindByText("Veuillez choisir").Selected = true;

                    //    Label3.Text = "lower(niveau) like '" + ddlniv.SelectedValue.ToString().ToLower() + "%'";

                    //}
                }
                else
                {
                    Label3.Text = "lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
                    Label4.Text = "";
                    ddclasse.DataTextField = "code_cl";
                    ddclasse.DataValueField = "code_cl";
                   

                        ddclasse.DataSource = service.bind_cls(Label3.Text.ToString(), Label4.Text.ToString());
                    //}
                    //else
                    //{
                    //    ddclasse.DataSource = service.bind_classesbyclasses(Label1.Text.ToString());
                    //}
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddclasse.DataBind();
                    ddclasse.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddclasse.SelectedItem.Selected = false;
                    ddclasse.Items.FindByText("Veuillez choisir").Selected = true;
                    Label3.Text = "";
                }

            }
        }


        protected void ddclasse_SelectedIndexChanged(object sender, EventArgs e)
        {

            BindData();
            btnUpdate.Visible = true;



        }
        private void BindDataX()
        {
            Gridexam.DataSource = service.bind_exam_byclassesord(DropDownList4.SelectedValue, Rdsemestre.SelectedValue);
            Gridexam.DataBind();
        }

        private void BindData()
        {
            Gridexam.DataSource = service.bind_exam_byclassesord(ddclasse.SelectedValue, Rdsemestre.SelectedValue);
            Gridexam.DataBind();
        }

        protected void EditCustomer(object sender, GridViewEditEventArgs e)
        {
            Gridexam.EditIndex = e.NewEditIndex;
            this.BindData();
        }

        protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Gridexam.EditIndex = -1;
            BindData();
        }

        protected void UpdateCustomer(object sender, GridViewUpdateEventArgs e)
        {
            //string shipperId = (Gridexam.Rows[e.RowIndex].FindControl("rblShippers") as RadioButtonList).SelectedItem.Value;
            //string orderId = Gridexam.DataKeys[e.RowIndex].Value.ToString();
            //string strConnString = ConfigurationManager.ConnectionStrings["conString"].ConnectionString;
            //using (OracleConnection con = new OracleConnection(strConnString))
            //{
            //    string query = "UPDATE Orders SET Shipvia = @ShipperId WHERE OrderId = @OrderId";
            //    using (OracleCommand cmd = new OracleCommand(query))
            //    {
            //        cmd.Connection = con;
            //        // a voir  cmd.Parameters.AddWithValue("@OrderId", orderId);
            //        // a voir   cmd.Parameters.AddWithValue("@ShipperId", shipperId);
            //        con.Open();
            //        cmd.ExecuteNonQuery();
            //        con.Close();
            //        Response.Redirect(Request.Url.AbsoluteUri);
            //    }
            //}
        }

        protected void Gridexam_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                DropDownList ddlaprogrammer = (e.Row.FindControl("ddlaprogrammer") as DropDownList);


                OracleCommand cmdd = new OracleCommand("SELECT distinct CODE_nome FROM CODE_NOMENCLATURE WHERE  CODE_STR='96' and (code_nome='1' or code_nome='0')");

                ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
                ddlaprogrammer.DataTextField = "CODE_nome";
                ddlaprogrammer.DataValueField = "CODE_nome";

                ddlaprogrammer.DataBind();




                DropDownList ddltypeexam = (e.Row.FindControl("ddltypeexam") as DropDownList);


                OracleCommand cmdx = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='97' and (code_nome='01' or code_nome='02' or code_nome='03') ");

                ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
                ddltypeexam.DataTextField = "LIB_NOME";
                ddltypeexam.DataValueField = "LIB_NOME";

                ddltypeexam.DataBind();




            }
        }

        protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        {

            //foreach (GridViewRow row in Gridexam.Rows)
            //{
            //    if (row.RowType == DataControlRowType.DataRow)
            //    {

            //        DropDownList ddldesign = (row.FindControl("ddldesign") as DropDownList);
            //        DropDownList ddlCountries1 = (row.FindControl("ddlCountries") as DropDownList);
            //        Label lblcodecl = (row.FindControl("lblcodecl") as Label);
            //        OracleCommand cmd2 = new OracleCommand("SELECT  distinct designation from esp_module b inner join ESP_MODULE_PANIER_CLASSE_SAISO a on a.CODE_MODULE=b.CODE_MODULE where a.code_module like '"+ddlCountries1.SelectedValue+"' and code_cl like '"+lblcodecl.Text+"'");

            //        ddldesign.DataSource = this.ExecuteQuery(cmd2, "SELECT");
            //        ddldesign.DataTextField = "designation";
            //        ddldesign.DataValueField = "designation";

            //        ddldesign.DataBind();




            //        DropDownList ddlaprogrammer = (row.FindControl("ddlaprogrammer") as DropDownList);

            //        Label lblcodecl11 = (row.FindControl("lblcodecl") as Label);
            //        OracleCommand cmdd = new OracleCommand("SELECT A_programmer from esp_ds_exam where code_cl like '" + lblcodecl11.Text + "' and code_module like '" + ddlaprogrammer.SelectedValue + "'");

            //        ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
            //        ddlaprogrammer.DataTextField = "A_programmer";
            //        ddlaprogrammer.DataValueField = "A_programmer";

            //        ddlaprogrammer.DataBind();



            //        //bind type exam

            //        DropDownList ddltypeexam = (row.FindControl("ddltypeexam") as DropDownList);

            //        Label lblco = (row.FindControl("lblcodecl") as Label);
            //        OracleCommand cmdx = new OracleCommand("SELECT type_exam from esp_ds_exam where code_cl like '" + lblco.Text + "' and code_module like '" + ddlaprogrammer.SelectedValue + "'");

            //        ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
            //        ddltypeexam.DataTextField = "type_exam";
            //        ddltypeexam.DataValueField = "type_exam";

            //        ddltypeexam.DataBind();

            //    }
            //}
        }


        protected void ddlmodule_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void Update(object sender, EventArgs e)
        {

            //if exist faire le update
            foreach (GridViewRow row in Gridexam.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                    {

                        OracleCommand cmd = new OracleCommand("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam where code_cl =:code_cl and code_module=:code_module");

                        cmd.Parameters.Add("a_programmer", row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value);
                            cmd.Parameters.Add(":type_exam", row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value);

                            cmd.Parameters.Add(":code_cl", row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text);
                            cmd.Parameters.Add(":code_module", row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text);
                            
                            this.ExecuteQuery(cmd, "SELECT");
                            Response.Write(@"<script language='javascript'>alert('Modification avec succès');</script>");

                        if(Rdcours.SelectedValue=="C")
                        {
                            BindDataX();
                        
                        }

                        else
                        {
                            BindData();
                        }
                        //else
                        //{
                        //    Response.Write(@"<script language='javascript'>alert('Module non affecté');</script>");
                        //}

                    }
                }

            }
        }


        protected void btnvalid_Click(object sender, EventArgs e)
        {
            if (Rdsemestre.SelectedValue == "" || rdperiode.SelectedValue == "")
            {
                Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

            }
            //else
            //{
            //    lbls.Text = Rdsemestre.SelectedValue;
            //    lbls.ForeColor = System.Drawing.Color.Chocolate;
            //    lblp.Text = rdperiode.SelectedValue;

            //    lblp.ForeColor = System.Drawing.Color.Chocolate;

            //    plvalider.Visible = false;
            //    plannee.Visible = true;
            //}

            else
            {

                //Plchoixannee.Visible = true;
                plvalider.Visible = false;

                Plannee2.Visible = true;
                plannee.Visible = false;
                Plchoixannee.Visible = false;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";
                string annee_deb = service.get_annee_parametr();

                Label8.Text = Rdsemestre.SelectedValue;
                Label9.Text = rdperiode.Text;
                //DropDownList2.DataSource = service.bind_site2(annee_deb.Substring(0, 4));

                //DropDownList2.DataBind();

                Label1.Text = "a.ANNEE_DEB= '" + annee_deb.ToString().Substring(0, 4) + "'";
                //Label1.Text = "a.ANNEE_DEB= '" + lblanneedeb.Text.ToString().Substring(0, 4) + "'";
            }
        }

        protected void Btnsuivant_Click(object sender, EventArgs e)
        {

            if (Rdcours.SelectedValue == "C")
            {
                Plannee2.Visible = true;
                plannee.Visible = false;
                Plchoixannee.Visible = false;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";
                string annee_deb = service.get_annee_parametr();

                Label8.Text = Rdsemestre.SelectedValue;
                Label9.Text = rdperiode.Text;
                DropDownList2.DataSource = service.bind_site2(annee_deb.Substring(0, 4));

                DropDownList2.DataBind();

                Label1.Text = "a.ANNEE_DEB= '" + annee_deb.ToString().Substring(0, 4) + "'";
                //Label1.Text = "a.ANNEE_DEB= '" + lblanneedeb.Text.ToString().Substring(0, 4) + "'";

            }
            else
            {
                if (Rdcours.SelectedValue == "A")
                {
                    Plannee2.Visible = false;
                    plannee.Visible = true;
                    Plchoixannee.Visible = false;


                    lbls.Text = Rdsemestre.SelectedValue;
                    lblp.Text = rdperiode.SelectedValue;
                }

                else
                {

                    Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

                }
            }
        }

       

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {


            //if (DropDownList3.SelectedValue != null)
            //{
            //    if (DropDownList3.SelectedValue.ToString() != "ALL")
            //    {
            //        DropDownList4.DataTextField = "code_cl";
            //        DropDownList4.DataValueField = "code_cl";
            //        if (Label2.Text == "")
            //        {

            //            DropDownList4.DataSource = service.bind_cls("lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'", "and niveau like '" + DropDownList3.SelectedValue + "%'");
            //            // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
            //            DropDownList4.DataBind();
            //            DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            //            DropDownList4.SelectedItem.Selected = false;
            //            DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
            //            // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
            //            Label3.Text = "lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

            //        }
            //        else
            //        {
            //            Label1.Text = "lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";


            //            DropDownList4.DataSource = service.bind_cls("lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'", "and niveau like '" + DropDownList3.SelectedValue + "%'");

            //            DropDownList4.DataBind();
            //            DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            //            DropDownList4.SelectedItem.Selected = false;
            //            DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;

            //            Label3.Text = "lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

            //        }
            //    }
            //    else
            //    {
            //        Label3.Text = "lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
            //        Label4.Text = "";

            //        DropDownList4.DataTextField = "code_cl";
            //        DropDownList4.DataValueField = "code_cl";
            //        //if (Label2.Text.ToString() != "")
            //        //{
            //            Label1.Text = "lower(SITE) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";


            //             DropDownList4.DataSource = service.bind_cls(Label3.Text,Label4.Text);
            //        //}
            //        //else
            //        //{
            //        //    DropDownList4.DataSource = service.bind_classes(Label1.Text.ToString());
            //        //}
            //        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
            //        DropDownList4.DataBind();
            //        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            //        DropDownList4.SelectedItem.Selected = false;
            //        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
            //        Label3.Text = "";
            //    }

            //}

            if (DropDownList3.SelectedValue != null)
            {
                if (DropDownList3.SelectedValue.ToString() != "ALL")
                {
                    DropDownList4.DataTextField = "code_cl";
                    DropDownList4.DataValueField = "code_cl";
                    if (DropDownList2.SelectedValue == "2")
                    {

                        DropDownList4.DataSource = service.bind_classesgh(DropDownList3.SelectedValue.ToString().ToLower());
                        ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList4.DataBind();
                        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        DropDownList4.SelectedItem.Selected = false;
                        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
                        // Label1.Text=Label1.Text.ToString()+" and lower(niveau) like '" +ddlniv.SelectedValue.ToString().ToLower() + "%'";
                        Label3.Text = "lower(niveau) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

                    }
                    else
                    {

                        if (DropDownList2.SelectedValue == "1")
                        {
                            // Label1.Text = "lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";


                            DropDownList4.DataSource = service.bind_classesCH(DropDownList3.SelectedValue.ToString().ToLower());

                            DropDownList4.DataBind();
                            DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                            DropDownList4.SelectedItem.Selected = false;
                            DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;

                            Label3.Text = DropDownList3.SelectedValue.ToString().ToLower();
                        }
                    }
                }
                else
                {
                    if (DropDownList3.SelectedValue.ToString() == "ALL")
                    {
                        DropDownList4.DataTextField = "code_cl";
                        DropDownList4.DataValueField = "code_cl";
                        DropDownList4.DataSource = service.bind_classesALL();

                        // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList4.DataBind();
                        DropDownList4.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                        DropDownList4.SelectedItem.Selected = false;
                        DropDownList4.Items.FindByText("Veuillez choisir").Selected = true;
                        Label3.Text = "";
                    }
                }

            }

        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {


            //if (ddclasse.SelectedValue != null)
            //{
            //    if (rdperiode.SelectedValue == "1")
            //    {
            //        if (DropDownList4.SelectedValue.ToString() != "ALL")
            //        {
            //            Gridexam.DataSource = service.bind_exam_aprogrammer("upper(code_cl) like '" + DropDownList4.SelectedValue + "%'", "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");
            //            //+ " " + "and" + " " + Label3.Text.ToString()
            //            Gridexam.DataBind();
            //            btnUpdate.Visible = true;
            //            Gridexam.Visible = true;
            //            Label1.Text = "lower(a.code_cl) like '" + ddclasse.SelectedValue.ToString().ToLower() + "%'";
            //            Label2.Text = "NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

            //            Label3.Text = "";


            //        }

            //        else
            //        {
            //            Label1.Text = "lower(a.code_cl) like '" + DropDownList3.SelectedValue.ToString() + "%'";

            //            Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

            //            Gridexam.DataSource = service.bind_exam_aprogrammer(Label1.Text.ToString(), Label2.Text);
            //            Gridexam.DataBind();
            //            btnUpdate.Visible = true;
            //        }
            //        Gridexam.DataBind();
            //        Gridexam.Visible = true;
            //        Label4.Text = "";

            //    }




            //    else

            //        if (rdperiode.SelectedValue == "2")
            //        {
            //            if (DropDownList4.SelectedValue.ToString() != "ALL")
            //            {
            //                Gridexam.DataSource = service.bind_exam_aprogrammerP2("UPPER(code_cl) like '" + DropDownList4.SelectedValue + "%'", "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'");
            //                //+ " " + "and" + " " + Label3.Text.ToString()
            //                Gridexam.DataBind();

            //                btnUpdate.Visible = true;
            //                Gridexam.Visible = true;
            //                Label1.Text = "UPPER(a.code_cl) like '" + DropDownList4.SelectedValue.ToString() + "%'";
            //                Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

            //                Label3.Text = "";


            //            }

            //            else
            //            {
            //                Label1.Text = "lower(a.code_cl) like '" + DropDownList3.SelectedValue.ToString().ToLower() + "%'";

            //                Label2.Text = "and NUM_SEMESTRE like '" + Rdsemestre.SelectedValue + "'";

            //                Gridexam.DataSource = service.bind_exam_aprogrammerP2(Label1.Text.ToString(), Label2.Text);
            //                Gridexam.DataBind();
            //                btnUpdate.Visible = true;
            //            }
            //        }
            //}
            BindDataX();
            btnUpdate.Visible = true;

        }


        protected void OnCheckedChangedDDD(object sender, EventArgs e)
        {
            bool isUpdateVisible = false;
            CheckBox chk = (sender as CheckBox);
            if (chk.ID == "chkAll")
            {
                foreach (GridViewRow row in Gridexam.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked = chk.Checked;
                    }
                }
            }
            CheckBox chkAll = (Gridexam.HeaderRow.FindControl("chkAll") as CheckBox);
            chkAll.Checked = true;
            foreach (GridViewRow row in Gridexam.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    for (int i = 3; i < row.Cells.Count; i++)
                    {
                        row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                        }
                        if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                        }
                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }
                        if (!isChecked)
                        {
                            chkAll.Checked = false;
                        }
                    }
                }
            }
            btnUpdate.Visible = isUpdateVisible;
        }

        protected void OnCheckedChanged(object sender, EventArgs e)
        {
           

                bool isUpdateVisible = false;
                Label1.Text = string.Empty;

                //Loop through all rows in GridView
                foreach (GridViewRow row in Gridexam.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                        if (isChecked)
                            row.RowState = DataControlRowState.Edit;

                        for (int i = 6; i < row.Cells.Count; i++)
                        {
                            row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                            if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                            {
                                row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                            }
                            if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                            {
                                row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                            }

                            if (isChecked && !isUpdateVisible)
                            {
                                isUpdateVisible = true;
                            }

                            //bind a prog


                        }

                    }



                    DropDownList ddlaprogrammer = (row.FindControl("ddlaprogrammer") as DropDownList);


                    OracleCommand cmdd = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='96' and (code_nome='01' or code_nome='02')");

                    ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
                    ddlaprogrammer.DataTextField = "LIB_NOME";
                    ddlaprogrammer.DataValueField = "LIB_NOME";

                    ddlaprogrammer.DataBind();



                    //bind type exam

                    DropDownList ddltypeexam = (row.FindControl("ddltypeexam") as DropDownList);


                    OracleCommand cmdx = new OracleCommand("SELECT distinct LIB_NOME FROM CODE_NOMENCLATURE WHERE  CODE_STR='97' and (code_nome='01' or code_nome='02' or code_nome='03') ");

                    ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
                    ddltypeexam.DataTextField = "LIB_NOME";
                    ddltypeexam.DataValueField = "LIB_NOME";

                    ddltypeexam.DataBind();



                }




                // UpdatePanel1.Update();
                btnUpdate.Visible = isUpdateVisible;
            }


        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //bind niveau

            //if (DropDownList2.SelectedValue.ToString() != null)
            //{
            //    if (DropDownList2.SelectedValue.ToString() != "ALL")
            //    {
            //        DropDownList3.DataTextField = "code_cl";
            //        DropDownList3.DataValueField = "code_cl";
            //        DropDownList3.DataSource = service.bind_classesgh(DropDownList2.SelectedValue.ToString().ToLower());
            //         ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
            //        DropDownList3.DataBind();
            //        DropDownList3.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            //        DropDownList3.SelectedItem.Selected = false;
            //        DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;
            //        Label2.Text = " lower(site) like '" + DropDownList2.SelectedValue.ToString().ToLower() + "%'";
            //    }
            //    else
            //    {
            //        DropDownList3.DataTextField = "niveau";
            //        DropDownList3.DataValueField = "niveau";
            //        DropDownList3.DataSource = service.bind_classesgh(Label1.Text.ToString());
            //         ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
            //        DropDownList3.DataBind();
            //        DropDownList3.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            //        DropDownList3.SelectedItem.Selected = false;
            //        DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;

            //        Label2.Text = "";
            //    }
            //}
            //bind niveau

            DropDownList3.Enabled = true;

            if (DropDownList2.SelectedValue.ToString() != null)
            {
                if (DropDownList2.SelectedValue.ToString() != "ALL")
                {
                    DropDownList3.DataTextField = "niveau";
                    DropDownList3.DataValueField = "niveau";
                    DropDownList3.DataSource = service.bind_niveauGH(DropDownList2.SelectedValue.ToString().ToLower());
                    ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList3.DataBind();

                    DropDownList3.SelectedItem.Selected = false;
                    //DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;
                    Label2.Text = DropDownList2.SelectedValue.ToString().ToLower();

                    Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                }
                else
                {
                    DropDownList3.DataTextField = "niveau";
                    DropDownList3.DataValueField = "niveau";
                    DropDownList3.DataSource = service.bind_niveauGH(Label1.Text.ToString());
                    ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList3.DataBind();
                    DropDownList3.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    DropDownList3.SelectedItem.Selected = false;
                    DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                    Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                }


            }
            else
            {

                if (DropDownList2.SelectedValue.ToString() != "1")
                {
                    DropDownList3.DataTextField = "niveau";
                    DropDownList3.DataValueField = "niveau";
                    DropDownList3.DataSource = service.bind_niveauGH(Label1.Text.ToString());
                    ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    DropDownList3.DataBind();
                    DropDownList3.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    DropDownList3.SelectedItem.Selected = false;
                    DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                    Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                }

                else
                {
                    if (DropDownList2.SelectedValue.ToString() == "2")
                    {
                        DropDownList3.DataTextField = "niveau";
                        DropDownList3.DataValueField = "niveau";
                        DropDownList3.DataSource = service.bind_niveauGH(DropDownList2.SelectedValue.ToString().ToLower());
                        ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                        DropDownList3.DataBind();

                        DropDownList3.SelectedItem.Selected = false;
                        //DropDownList3.Items.FindByText("Veuillez choisir").Selected = true;
                        Label2.Text = DropDownList2.SelectedValue.ToString().ToLower();

                        Labelzzz.Text = DropDownList2.SelectedValue.ToString();
                    }
                }
            }
        }


        protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlsite.SelectedValue.ToString() != null)
            {
                if (DropDownList2.SelectedValue.ToString() != "ALL")
                {
                    ddlniv.DataTextField = "code_cl";
                    ddlniv.DataValueField = "code_cl";
                    ddlniv.DataSource = service.bind_classesgh(DropDownList2.SelectedValue.ToString().ToLower());
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;
                    Label2.Text = " lower(site) like '" + ddlsite.SelectedValue.ToString().ToLower() + "%'";
                }
                else
                {
                    ddlniv.DataTextField = "niveau";
                    ddlniv.DataValueField = "niveau";
                    ddlniv.DataSource = service.bind_classesgh(Label1.Text.ToString());
                    // ddlsite.Items.Add(new ListItem("Veuillez choisir un site", "0"));
                    ddlniv.DataBind();
                    ddlniv.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                    ddlniv.SelectedItem.Selected = false;
                    ddlniv.Items.FindByText("Veuillez choisir").Selected = true;

                    Label2.Text = "";
                }
            }
        }


        

    }
}


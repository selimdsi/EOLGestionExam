﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using BLL;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;
using System.Globalization;
using Telerik;
using Telerik.Web.UI;
using System.Drawing;

namespace ESPOnline.Administration.Examen
{
    public partial class Gestion_Surveillant : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        protected void DISPSURVEILLANT_Click(object sender, EventArgs e)
        {
            GridviewSurveillant.Visible = false;
            Label12.Visible = true;
            DropDownList15.Visible = true;
            SurveillantDisp.Visible = true;
            
            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            OracleConnection connection = new OracleConnection(AppConfiguration.ConnectionString);
            connection.Open();
            OracleCommand cmd1 = new OracleCommand("TRUNCATE TABLE ESP_SURVEILLANT_DISPO", connection);
            cmd1.ExecuteNonQuery();
            connection.Close();

            foreach (RadComboBoxItem item in RadComboBox1.Items)
            {

                if (item.Checked == true)
                {
                   
                    connection.Open();
                    OracleCommand cmd = new OracleCommand("insert into ESP_SURVEILLANT_DISPO (ID_ENS,NOM_ENS) values('" + item.Value + "','" + item.Text + "')", connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
            Response.Write("<script>alert('Inserted')</script>");
        }



        protected void OnRowDataBoundSalle(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                OracleCommand cmdSurv = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");

                DropDownList DropDownListSALLE1 = (e.Row.FindControl("DropDownListSurv21") as DropDownList);

                DropDownListSALLE1.DataSource = this.ExecuteQuery(cmdSurv, "SELECT");
                DropDownListSALLE1.DataTextField = "NOM_ENS";
                DropDownListSALLE1.DataValueField = "ID_ENS";
                DropDownListSALLE1.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                DropDownListSALLE1.Items.FindByText("Choisir").Selected = true;
                DropDownListSALLE1.DataBind();


                OracleCommand cmdSurv2 = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");

                DropDownList DropDownListSALLE2 = (e.Row.FindControl("DropDownListSurv2") as DropDownList);

                DropDownListSALLE2.DataSource = this.ExecuteQuery(cmdSurv2, "SELECT");
                DropDownListSALLE2.DataTextField = "NOM_ENS";
                DropDownListSALLE2.DataValueField = "ID_ENS";
                DropDownListSALLE2.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                DropDownListSALLE2.Items.FindByText("Choisir").Selected = true;
                DropDownListSALLE2.DataBind();


            }
        }


        protected void Button1_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in GridviewSurveillant.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    DataTable dtt;

                    if (Rdcours.SelectedValue == "C")
                    {

                        string seance = row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text;
                        string code_cl = row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text;
                        string module = row.Cells[2].Controls.OfType<Label>().FirstOrDefault().Text;
                        string semestre = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;
                        string periode = row.Cells[4].Controls.OfType<Label>().FirstOrDefault().Text;

                        string surv1 = row.Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                        string surv2 = row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Text;


                        OracleCommand cmd = new OracleCommand("insert into ESP_SURVEILLANT_EXAMEN (NUM_SEANCE,SEMESTRE,PERIODE,MODULE,CODE_CL,SURVEILLANT_1,SURVEILLANT_2) values ('" + seance + "','" + semestre + "','" + periode + "','" + module + "','" + code_cl + "','" + surv1 + "','" + surv2 + "') ");

                        this.ExecuteQuery(cmd, "UPDATE");
                        Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");


                        //("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam,code_module=:code_module where code_cl =:code_cl");



                        //cmd.Parameters.Add(":num_seance", row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text);
                        ////cmd.Parameters.Add(":annee_deb", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add("to_date(:date_seance,'dd/mm/yy')", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        ////cmd.Parameters.Add(":semestre", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":num_jours", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":jours", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":heure_debut", row.Cells[4].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":min_debut", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);


                        //cmd.Parameters.Add(":heure_fin", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":min_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":site", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":periode", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);




                    }

                }
            }


        
        }




        protected void btnvalid9_Click(object sender, EventArgs e)
        {
            RadComboBox1.Visible = true;
            RadButton1.Visible = true;
            Button2.Visible = false;

            int semestre = Convert.ToInt16(Rdsemestre.SelectedValue);
            int periode = Convert.ToInt16(rdperiode.SelectedValue);
            //DataTable dt = new DataTable();

            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            OracleConnection connection = new OracleConnection(AppConfiguration.ConnectionString);
            connection.Open();
            OracleCommand cmd1 = new OracleCommand("TRUNCATE TABLE ESP_SURVEILLANT_DISPO", connection);
            cmd1.ExecuteNonQuery();
            connection.Close();

            //connection.Open();
            //OracleCommand cmd3 = new OracleCommand("SELECT CODE_CL, CODE_MODULE FROM ESP_DS_EXAM where NUM_SEMESTRE = '" + semestre + "' and NUM_PERIODE = '" + periode + "'", connection);
            //cmd3.ExecuteNonQuery();
            //OracleDataAdapter od = new OracleDataAdapter(cmd3);
            //od.Fill(dt);
            //connection.Close();

            //connection.Open();
            //OracleCommand cmd2 = new OracleCommand("INSERT INTO ESP_AFFECT_SALLE ( CODE_CL, CODE_MODULE ) SELECT CODE_CL, CODE_MODULE FROM ESP_DS_EXAM where ESP_DS_EXAM.NUM_PERIODE = '" + periode + "' and ESP_DS_EXAM.NUM_SEMESTRE = '" + semestre + "'", connection);
            //cmd2.ExecuteNonQuery();
            //connection.Close();


        }

        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void DISPSALLE_Click2(object sender, EventArgs e)
        {

            GridviewSurveillant.DataSource = service.getsurv(Convert.ToInt32(DropDownList15.SelectedValue), Convert.ToInt32(Rdsemestre.SelectedValue), Convert.ToInt32(rdperiode.SelectedValue));
            GridviewSurveillant.DataBind();
            GridviewSurveillant.Visible = true;
            ButtonAS.Visible = true;

        }




        protected void ddlDropSurv1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int x = 0;
            int y = 0;
            int z = 0;
            foreach (GridViewRow row in GridviewSurveillant.Rows)
            {
                for (int i = 0; i < GridviewSurveillant.Rows.Count - 1; i++)
                {
                    if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir" || GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                    {
                        if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                        {
                            y = 99;
                            OracleCommand cmdSurv = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");


                            GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                            GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "NOM_ENS";
                            GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "ID_ENS";
                            GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmdSurv, "SELECT");
                            GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                            GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));

                        }
                    }
                }
            }

            if (y == 99)
            {
                Response.Write(@"<script language='javascript'>alert('Surveillant déjà affecté');</script>");

            }

            foreach (GridViewRow row in GridviewSurveillant.Rows)
            {

                if (row.RowType == DataControlRowType.DataRow)
                {
                    //DropDownList ddlCountries = (row.FindControl("ddlCountriesS") as DropDownList);

                    //string num_seance = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value;



                    for (int i = 0; i < GridviewSurveillant.Rows.Count - 1; i++)
                    {
                        for (int j = 1; j < GridviewSurveillant.Rows.Count; j++)
                        {

                            //int j = i + 1;
                            if (i != j)
                            {
                                if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                                {

                                    if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                                    {
                                        x = 99;

                                        OracleCommand cmdSurv = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");


                                        GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                                        GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "NOM_ENS";
                                        GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "ID_ENS";
                                        GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmdSurv, "SELECT");
                                        GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                                        GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));


                                    }
                                    else if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                                    {
                                        z = 99;

                                        OracleCommand cmdSALLE = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");


                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "NOM_ENS";
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "ID_ENS";
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmdSALLE, "SELECT");
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));

                                    }
                                }
                            }

                        }
                    }

                }
            }
            if (x == 99 || z == 99)
            {
                Response.Write(@"<script language='javascript'>alert('Surveillant déjà affecté');</script>");
            }

        }

        protected void ddlDropSurv2_SelectedIndexChanged(object sender, EventArgs e)
        {
            int a = 0;
            int b = 0;
            int d = 0;
            foreach (GridViewRow row in GridviewSurveillant.Rows)
            {
                for (int i = 0; i < GridviewSurveillant.Rows.Count - 1; i++)
                {
                    if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir" || GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                    {
                        if (GridviewSurveillant.Rows[i].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                        {
                            a = 99;

                            OracleCommand cmdSurv = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");


                            GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                            GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "NOM_ENS";
                            GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "ID_ENS";
                            GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmdSurv, "SELECT");
                            GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                            GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));
                        }
                    }
                }
            }

            if (a == 99)
            {
                Response.Write(@"<script language='javascript'>alert('Surveillant déjà affecté');</script>");

            }

            foreach (GridViewRow row in GridviewSurveillant.Rows)
            {

                if (row.RowType == DataControlRowType.DataRow)
                {
                    //DropDownList ddlCountries = (row.FindControl("ddlCountriesS") as DropDownList);

                    //string num_seance = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value;



                    for (int i = 0; i < GridviewSurveillant.Rows.Count - 1; i++)
                    {
                        for (int j = 1; j < GridviewSurveillant.Rows.Count; j++)
                        {

                            //int j = i + 1;
                            if (i != j)
                            {
                                if (GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value != "Choisir")
                                {

                                    if (GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                                    {
                                        b = 99;

                                        OracleCommand cmdSurv = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");


                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "NOM_ENS";
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "ID_ENS";
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmdSurv, "SELECT");
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));
                                  
                                    }
                                    else if (GridviewSurveillant.Rows[i].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value == GridviewSurveillant.Rows[j].Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value)
                                    {
                                        d = 99;

                                        OracleCommand cmdSALLE = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO order by NOM_ENS");


                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Clear();
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataTextField = "NOM_ENS";
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataValueField = "ID_ENS";
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataSource = this.ExecuteQuery(cmdSALLE, "SELECT");
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().DataBind();
                                        GridviewSurveillant.Rows[j].Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Items.Insert(0, new ListItem("Choisir", "Choisir"));

                                    }
                                }
                            }

                        }
                    }

                }
            }
            if (b == 99 || d == 99)
            {
                Response.Write(@"<script language='javascript'>alert('Surveillant déjà affecté');</script>");
            }

        }



        protected void DropDownList15OnSelect(object sender, EventArgs e)
        {

        }
       
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using BLL;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;
using System.Globalization;
using System.Drawing;


namespace ESPOnline.Administration.Examen
{
    public partial class Gestion_Surveillant_Modif : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnvalid9_Click(object sender, EventArgs e)
        {

            Button2.Visible = false;
            GridviewSALLE.Visible = false;
            Label12.Visible = true;
            DropDownList15.Visible = true;
            DISPSALLE.Visible = true;

        }

        protected void OnRowDataBoundSalle(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //OracleCommand cmdSALLE = new OracleCommand("SELECT salle from ESP_SALLE_DISPO where SALLE not in (select SALLE_1 from ESP_SALLE_EXAMEN) and SALLE not in (select SALLE_2 from ESP_SALLE_EXAMEN)");

                //DropDownList DropDownListSALLE1 = (e.Row.FindControl("DropDownListSALLE1") as DropDownList);

                //DropDownListSALLE1.DataSource = this.ExecuteQuery(cmdSALLE, "SELECT");
                //DropDownListSALLE1.DataTextField = "SALLE";
                //DropDownListSALLE1.DataValueField = "SALLE";
                //DropDownListSALLE1.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                //DropDownListSALLE1.Items.FindByText("Choisir").Selected = true;
                //DropDownListSALLE1.DataBind();


                //OracleCommand cmdSALLE2 = new OracleCommand("SELECT salle from ESP_SALLE_DISPO where SALLE not in (select SALLE_1 from ESP_SALLE_EXAMEN) and SALLE not in (select SALLE_2 from ESP_SALLE_EXAMEN)");

                //DropDownList DropDownListSALLE2 = (e.Row.FindControl("DropDownListSALLE2") as DropDownList);

                //DropDownListSALLE2.DataSource = this.ExecuteQuery(cmdSALLE, "SELECT");
                //DropDownListSALLE2.DataTextField = "SALLE";
                //DropDownListSALLE2.DataValueField = "SALLE";
                //DropDownListSALLE2.Items.Insert(0, new ListItem("Choisir", "Choisir"));
                //DropDownListSALLE2.Items.FindByText("Choisir").Selected = true;
                //DropDownListSALLE2.DataBind();


            }

        }

        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DISPSALLE_Click2(object sender, EventArgs e)
        {

            GridviewSALLE.DataSource = service.getsurvmodif(Convert.ToInt32(DropDownList15.SelectedValue), Convert.ToInt32(Rdsemestre.SelectedValue), Convert.ToInt32(rdperiode.SelectedValue));
            GridviewSALLE.DataBind();
            GridviewSALLE.Visible = true;

        }

        protected void DropDownList15OnSelect(object sender, EventArgs e)
        {

        }

        protected void GridviewSALLE_SelectedIndexChanged(object sender, EventArgs e)
        {

        }





        protected void OnCheckedChangedDDD(object sender, EventArgs e)
        {
            bool isUpdateVisible = false;
            CheckBox chk = (sender as CheckBox);
            if (chk.ID == "chkAll")
            {
                foreach (GridViewRow row in GridviewSALLE.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked = chk.Checked;
                    }
                }
            }
            CheckBox chkAll = (GridviewSALLE.HeaderRow.FindControl("chkAll") as CheckBox);
            chkAll.Checked = true;
            foreach (GridViewRow row in GridviewSALLE.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    for (int i = 0; i < row.Cells.Count; i++)
                    {
                        row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                        }
                        if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                        }
                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }
                        if (!isChecked)
                        {
                            chkAll.Checked = false;
                        }
                    }
                }
            }
            btnUpdate.Visible = true;
        }

        protected void OnCheckedChanged(object sender, EventArgs e)
        {


            bool isUpdateVisible = false;
            //Label1.Text = string.Empty;
            btnUpdate.Visible = true;
            //Loop through all rows in GridView
            foreach (GridViewRow row in GridviewSALLE.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                        row.RowState = DataControlRowState.Edit;

                    for (int i = 6; i < row.Cells.Count; i++)
                    {
                        row.Cells[i].Controls.OfType<Label>().FirstOrDefault().Visible = !isChecked;
                        if (row.Cells[i].Controls.OfType<TextBox>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<TextBox>().FirstOrDefault().Visible = isChecked;
                        }
                        if (row.Cells[i].Controls.OfType<DropDownList>().ToList().Count > 0)
                        {
                            row.Cells[i].Controls.OfType<DropDownList>().FirstOrDefault().Visible = isChecked;
                        }

                        if (isChecked && !isUpdateVisible)
                        {
                            isUpdateVisible = true;
                        }

                        //bind a prog


                    }

                }



                DropDownList ddlaprogrammer = (row.FindControl("DropDownListSurveillant1") as DropDownList);


                OracleCommand cmdd = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO where ID_ENS not in (select SURVEILLANT_1 from ESP_SURVEILLANT_EXAMEN) and ID_ENS not in (select SURVEILLANT_2 from ESP_SURVEILLANT_EXAMEN) ");

                ddlaprogrammer.DataSource = this.ExecuteQuery(cmdd, "SELECT");
                ddlaprogrammer.DataTextField = "NOM_ENS";
                ddlaprogrammer.DataValueField = "ID_ENS";

                ddlaprogrammer.DataBind();



                //bind type exam

                DropDownList ddltypeexam = (row.FindControl("DropDownListSurveillant2") as DropDownList);


                OracleCommand cmdx = new OracleCommand("SELECT DISTINCT NOM_ENS, ID_ENS from ESP_SURVEILLANT_DISPO where ID_ENS not in (select SURVEILLANT_1 from ESP_SURVEILLANT_EXAMEN) and ID_ENS not in (select SURVEILLANT_2 from ESP_SURVEILLANT_EXAMEN) ");

                ddltypeexam.DataSource = this.ExecuteQuery(cmdx, "SELECT");
                ddltypeexam.DataTextField = "NOM_ENS";
                ddltypeexam.DataValueField = "ID_ENS";

                ddltypeexam.DataBind();



            }




            // UpdatePanel1.Update();
            //btnUpdate.Visible = isUpdateVisible;
        }


        protected void Update(object sender, EventArgs e)
        {

            //if exist faire le update
            foreach (GridViewRow row in GridviewSALLE.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    bool isChecked = row.Cells[0].Controls.OfType<CheckBox>().FirstOrDefault().Checked;
                    if (isChecked)
                    {

                        OracleCommand cmd = new OracleCommand("update ESP_SURVEILLANT_EXAMEN set SURVEILLANT_1=:SURVEILLANT_1,SURVEILLANT_2=:SURVEILLANT_2 where code_cl =:CODE_CL and MODULE=:MODULE");

                        cmd.Parameters.Add("SURVEILLANT_1", row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value);
                        cmd.Parameters.Add("SURVEILLANT_2", row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Value);

                        cmd.Parameters.Add(":CODE_CL", row.Cells[2].Controls.OfType<Label>().FirstOrDefault().Text);
                        cmd.Parameters.Add(":MODULE", row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text);

                        this.ExecuteQuery(cmd, "UPDATE");
                        Response.Write(@"<script language='javascript'>alert('Modification');</script>");
                    }

                        //if (Rdcours.SelectedValue == "C")
                    //{
                    //    BindDataX();

                        //}

                        //else
                    //{
                    //    BindData();
                    //}
                    else
                    {
                        Response.Write(@"<script language='javascript'>alert('Modification avec succès');</script>");
                    }

                }
            }

        }
    }







}
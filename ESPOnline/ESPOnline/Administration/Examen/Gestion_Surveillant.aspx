﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gestion_Surveillant.aspx.cs" Inherits="ESPOnline.Administration.Examen.Gestion_Surveillant" MasterPageFile="~/Administration/Examen/Exam.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <style type="text/css">
        table.grid tbody tr:hover
        {
            background-color: #e5ecf9;
        }
        .GridHeaderStyle
        {
            color: #FEF7F7;
            background-color: #877d7d;
            font-weight: bold;
        }
        .GridItemStyle
        {
            background-color: #eeeeee;
            color: white;
        }
        .GridAlternatingStyle
        {
            background-color: #dddddd;
            color: black;
        }
        .GridSelectedStyle
        {
            background-color: #d6e6f6;
            color: black;
        }
        
        
        .GridStyle
        {
            border-bottom: white 2px ridge;
            border-left: white 2px ridge;
            background-color: white;
            width: 100%;
            border-top: white 2px ridge;
            border-right: white 2px ridge;
        }
        .ItemStyle
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        
        .ItemStyle td
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        .FixedHeaderStyle
        {
            background-color: #7591b1;
            color: #FFFFFF;
            font-weight: bold;
            position: relative;
            top: expression(this.offsetParent.scrollTop);
            z-index: 10;
        }
        .Caption_1_Customer
        {
            background-color: #beccda;
            color: #000000;
            width: 30%;
            height: 20px;
        }
        
        
        .style3
        {
            color: #000000;
        }
        .style4
        {
            color: #CC0000;
        }
        .style5
        {
            color: #0066FF;
        }
        .style6
        {
            color: #0000FF;
        }
        
        
        .style7
        {
            color: #FF0000;
        }
        
        
        .style8
        {
            color: #003300;
        }
        
        .grid td, .grid th
        {
            text-align: center;

    </style> 




<h3 style="font-family: Calibri; margin-left: 640px;" __designer:mapid="6db">Gestion des Surveillants </h3>
    <center>
    <br />
    
    <asp:Panel ID="Panel1" runat="server">
        <center>
        <table>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Semestre: "
              class="style5">
              </asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList ID="Rdsemestre" runat="server"   
            RepeatDirection="Horizontal" width="120px" >
                        <asp:ListItem Value="1">S1</asp:ListItem>
                        <asp:ListItem Value="2">S2</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Periode: "
      class="style5"    ></asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList ID="rdperiode" runat="server"   
             RepeatDirection="Horizontal" width="120px" >
                        <asp:ListItem Value="1">P1</asp:ListItem>
                        <asp:ListItem Value="2">P2</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:Button  ID="Button2" runat="server" Text="Valider" Height="35px"
                    Width="120px" onclick="btnvalid9_Click"/>
                </td>
            </tr>
        </table>
        </center>
    </asp:Panel>
    <br />
    </center>
<asp:Panel ID="Plchoixannee" runat="server" Visible="false">
<table><tr><td><asp:Label ID="Label7" runat="server" Text="Veuillez choisir l'année auquel vous avez faire l'affectation: "
                            ></asp:Label>
</td>
<td> <asp:RadioButtonList ID="Rdcours" runat="server"   
            RepeatDirection="Horizontal" width="180px" >
            <asp:ListItem Selected="True" value="C">Année en cours</asp:ListItem>
                 <asp:ListItem value="A">Autre</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

              <td><asp:Button  ID="Btnsuivant" runat="server" Text="Suivant" Height="35px" 
                    Width="120px" /></td>
</tr></table>
</asp:Panel>
<div>
  
        <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
            <div class="combobox-wraper">
                <telerik:RadComboBox RenderMode="Lightweight" Visible="false" ID="RadComboBox1" CheckedItemsTexts="DisplayAllInInput" runat="server" 
                    DataSourceID="SqlDataSource2"  CheckBoxes="True" EnableCheckAllItemsCheckBox="True" 
                    Width="700px" Label="Surveillants Disponibles :" DataTextField="NOM_ENS" 
                    DataValueField="ID_ENS">
                     
                </telerik:RadComboBox>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                   ConnectionString="<%$ ConnectionStrings:DefaultConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:DefaultConnectionString.ProviderName %>" 
                    SelectCommand="SELECT DISTINCT ID_ENS, NOM_ENS from esp_enseignant where TYPE_ENS='P' and ETAT='A'">
                </asp:SqlDataSource>
                <telerik:RadButton RenderMode="Lightweight" ID="RadButton1" Visible="false" runat="server" onclick="DISPSURVEILLANT_Click"  
                    Text="Valider"  />


                <br />
                <asp:SqlDataSource ID="sqlDataSourceSALLE" runat="server" 
                  ConnectionString="<%$ ConnectionStrings:DefaultConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:DefaultConnectionString.ProviderName %>" 
                    SelectCommand="SELECT DISTINCT SALLE from ESP_SALLE_DISPO order by SALLE">
                </asp:SqlDataSource>
                
                <asp:SqlDataSource ID="sqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:DefaultConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:DefaultConnectionString.ProviderName %>" 
                    SelectCommand="SELECT DISTINCT NUM_SEANCE from ESP_SEANCE_EXAMEN order by NUM_SEANCE">
                </asp:SqlDataSource>
   
                <br />
                <asp:Label ID="Label12" class="style5" runat="server" Text="Séance" Visible="false"></asp:Label>
    <asp:DropDownList ID="DropDownList15" runat="server" Width="111px" OnSelectedIndexChanged="DropDownList15OnSelect" Visible="false" DataSourceID="sqlDataSource1" 
        DataTextField="NUM_SEANCE" DataValueField="NUM_SEANCE">
        
    </asp:DropDownList>
                
   
    &nbsp;&nbsp;&nbsp;

     <asp:Button ID="SurveillantDisp" runat="server" 
                    Text="Valider les seances" OnClick="DISPSALLE_Click2" Visible="false"/>
                
   
    <center>            
    <asp:GridView ID="GridviewSurveillant" OnRowDataBound="OnRowDataBoundSalle" Visible="true" HeaderStyle-BackColor="RosyBrown" HeaderStyle-BorderColor="White" 
        runat="server"  CssClass="carousel"   ShowFooter="True" Width="1262px"  AutoGenerateColumns="False" >
        <Columns>        
            <%--<asp:BoundField DataField="NUM_SEANCE" HeaderText="Seance" SortExpression="NUM_SEANCE" />
            <asp:BoundField DataField="CODE_CL" HeaderText="Classe" SortExpression="CODE_CL" />
            <asp:BoundField DataField="CODE_MODULE" HeaderText="Module" SortExpression="CODE_MODULE" />
            <asp:BoundField DataField="NUM_SEMESTRE" HeaderText="Semestre" SortExpression="NUM_SEMESTRE" />
            <asp:BoundField DataField="NUM_PERIODE" HeaderText="Periode" SortExpression="NUM_PERIODE" />--%>

            <asp:TemplateField HeaderText="Seance" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("NUM_SEANCE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice1" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Classe" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice12" runat="server" Text='<%# Eval("CODE_CL")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice2" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Module" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice13" runat="server" Text='<%# Eval("DESIGNATION")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice3" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Semestre" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice14" runat="server" Text='<%# Eval("NUM_SEMESTRE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice4" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Periode" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice15" runat="server" Text='<%# Eval("NUM_PERIODE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice5" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="Surveillant 1">
                <ItemTemplate>
                    <center>
                    <asp:DropDownList ID="DropDownListSurv21" runat="server" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDropSurv1_SelectedIndexChanged" Width="220px">
                    </asp:DropDownList>
                      <%--  <asp:DropDownList ID="DropDownListSurv2" runat="server" AppendDataBoundItems="true"  Width="220px">
                    </asp:DropDownList>--%>
                </center>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Surveillant 2">
                <ItemTemplate>
                    <center>
                    <asp:DropDownList ID="DropDownListSurv2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlDropSurv2_SelectedIndexChanged" AppendDataBoundItems="true"  Width="220px">
                    </asp:DropDownList>
                </center>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>

<HeaderStyle BackColor="Red" BorderColor="White" ForeColor="White"></HeaderStyle>
    </asp:GridView>
</center> 

               <%-- <telerik:RadGrid ID="RadGrid1" runat="server" DataSourceID="SqlDataSource55"  AllowPaging="True" AllowSorting="True"
                     AllowFilteringByColumn="True">
                    
                    <MasterTableView>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Salle 1">
                                <ItemTemplate>
                                    <telerik:RadComboBox RenderMode="Lightweight" ID="RadComboBox1" OnSelectedIndexChanged="radbox1SS" CheckedItemsTexts="DisplayAllInInput" runat="server" Visible="true"
                                        DataSourceID="sqlDataSourceSALLE" 
                                        Width="100px" DataTextField="SALLE" 
                                        DataValueField="SALLE">
                                    </telerik:RadComboBox>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="Salle 2">
                                <ItemTemplate>
                                    <telerik:RadComboBox RenderMode="Lightweight" ID="RadComboBox2" CheckedItemsTexts="DisplayAllInInput" runat="server" Visible="true"
                                        DataSourceID="sqlDataSourceSALLE" 
                                        Width="100px" DataTextField="SALLE" 
                                        DataValueField="SALLE">

                                    </telerik:RadComboBox>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>

                </telerik:RadGrid>--%>

                <asp:SqlDataSource ID="SqlDataSource555" runat="server"
                    ConnectionString="<%$ ConnectionStrings:DefaultConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:DefaultConnectionString.ProviderName %>" 
                    SelectCommand="SELECT CODE_CL, CODE_MODULE, NUM_SEMESTRE, NUM_PERIODE, NUM_SEANCE FROM ESP_DS_EXAM ORDER BY NUM_SEANCE"></asp:SqlDataSource>

                <br />


                <asp:Button ID="ButtonAS" runat="server" Visible="false" OnClick="Button1_Click" Text="Affecter Surveillant" />


</div>
    
    
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
       
</asp:Content>
 


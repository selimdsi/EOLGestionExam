﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Affecter_seance_exam.aspx.cs" 
Inherits="ESPOnline.Administration.Examen.Affecter_seance_exam" MasterPageFile="~/Administration/Examen/Exam.Master" %>


<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="../Contents/jquery.js" type="text/javascript"></script>
    <link href="../Contents/Css/datetimepicker.css" rel="stylesheet" type="text/css" />
   <%-- <link href="../Contents/animate.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../Contents/Css/bootstrap-theme.css" rel="stylesheet" type="text/css" />--%>
    <script src="../Contents/bootstrap.js" type="text/javascript"></script>
    <script src="../Contents/bootstrap.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <script src="../Contents/Scripts/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <style type="text/css">
         .footer td
        {
            border: none;
        }
   .table     td 
   {
border-bottom: 1pt solid black;
}     
  .footer      tr {
border-bottom: 1pt solid black;
}
        .footer th
        {
            border: none;
        }
    </style>

    <style type="text/css">
        table.grid tbody tr:hover
        {
            background-color: #e5ecf9;
        }
        .GridHeaderStyle
        {
            color: #FEF7F7;
            background-color: #877d7d;
            font-weight: bold;
        }
        .GridItemStyle
        {
            background-color: #eeeeee;
            color: white;
        }
        .GridAlternatingStyle
        {
            background-color: #dddddd;
            color: black;
        }
        .GridSelectedStyle
        {
            background-color: #d6e6f6;
            color: black;
        }
        
        
        .GridStyle
        {
            border-bottom: white 2px ridge;
            border-left: white 2px ridge;
            background-color: white;
            width: 100%;
            border-top: white 2px ridge;
            border-right: white 2px ridge;
        }
        .ItemStyle
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        
        .ItemStyle td
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        .FixedHeaderStyle
        {
            background-color: #7591b1;
            color: #FFFFFF;
            font-weight: bold;
            position: relative;
            top: expression(this.offsetParent.scrollTop);
            z-index: 10;
        }
        .Caption_1_Customer
        {
            background-color: #beccda;
            color: #000000;
            width: 30%;
            height: 20px;
        }
        
        
        .style3
        {
            color: #000000;
        }
        .style4
        {
            color: #CC0000;
        }
        .style5
        {
            color: #0066FF;
        }
        .style6
        {
            color: #0000FF;
        }
        
        
        .style7
        {
            color: #FF0000;
        }
        
        
        .style8
        {
            color: #003300;
        }
        
        .grid td, .grid th
        {
            text-align: center;

    </style> 

        
  <script type="text/javascript">


      function calendarShown(sender, args) {
          sender._popupBehavior._element.style.zIndex = 1000;
      }
  </script>


   <script src="styles/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
<script src="styles/Scripts/jquery.dynDateTime.min.js" type="text/javascript"></script>
<script src="styles/Scripts/JScript1.js" type="text/javascript"></script>
<link href="styles/Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<link href="styles/Styles/calendar-blue.css" rel="stylesheet" type="text/css" />
<script type = "text/javascript">
    $(document).ready(function () {
        $(".Calender").dynDateTime({
            showsTime: true,
            ifFormat: "%d/%m/%Y",
           
            align: "BR",
            electric: false,
            singleClick: false,
            displayArea: ".siblings('.dtcDisplayArea')",
            button: ".next()"
        });
    });
</script> 
    <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <asp:Panel ID="plvalider" runat="server">
<center>
<h3 style="font-family: Calibri">Affectetion des séances des examens</h3>

</center>
        <center>
<table>
<tr>
<td>
    
<asp:Label ID="Label11" runat="server" class="style5" Text="Semestre: ">
              </asp:Label></td>

                                                     
   <td>          <asp:RadioButtonList ID="Rdsemestre" runat="server"   
            RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="1">S1</asp:ListItem>
                 <asp:ListItem Value="2">S2</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

 





<td>
<asp:Label ID="Label6" runat="server" class="style5" Text="Periode: "></asp:Label></td>

<td><asp:RadioButtonList ID="rdperiode" runat="server"   
             RepeatDirection="Horizontal" width="120px" >
            <asp:ListItem Value="1">P1</asp:ListItem>
                 <asp:ListItem Value="2">P2</asp:ListItem>               
            </asp:RadioButtonList></td>
            <td><asp:Button  ID="Button1" runat="server" Text="Valider" Height="35px" 
                    Width="120px" onclick="btnvalid_Click"/></td>
</tr>
</table>
    </asp:Panel>

<asp:Panel ID="Plchoixannee" runat="server" Visible="false">
<table><tr><td><asp:Label ID="Label7" runat="server" Text="Veuillez choisir l'année auquel vous avez faire l'affectation: "
                             ForeColor="#044652"  ></asp:Label>
</td>
<td> <asp:RadioButtonList ID="Rdcours" runat="server"   
            RepeatDirection="Horizontal" width="180px"   >
            <asp:ListItem Selected="True" value="C">Année en cours</asp:ListItem>
                 <asp:ListItem value="A">Autre</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

              <td><asp:Button  ID="Btnsuivant" runat="server" Text="Suivant" Height="35px" 
                    Width="120px" onclick="Btnsuivant_Click" /></td>
</tr></table>

                                                     
           

</asp:Panel>


<asp:Panel ID="plannee" runat="server" Visible="false">


<table>

<tr><td>
    <span class="style5">Année Universitaire:
</span>
<asp:DropDownList ID="ddlan_univer" runat="server" AppendDataBoundItems="true"  Width="150px"
   Height="30px"     AutoPostBack="true" 
        onselectedindexchanged="ddlan_univer_SelectedIndexChanged" CssClass="style3">

</asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;
</td>


<td>
    <span class="style5">Site:</span><span class="style3"> </span>
<asp:DropDownList ID="ddlsite" runat="server" 
        onselectedindexchanged="ddlsite_SelectedIndexChanged"  Width="150px"
   Height="30px"
    AutoPostBack="true" CssClass="style3"    >

</asp:DropDownList>

    a</td>



</tr>


</table>


</asp:Panel>

<br />
    <center>
<asp:Panel ID="Plannee2" runat="server" Visible="false">


<table>


<tr>




<td>
    <center>
    <span class="style5">Site:</span><span class="style3"> </span>
<asp:DropDownList ID="DropDownList2" runat="server"  AppendDataBoundItems="true"  Width="150px" Height="30px" AutoPostBack="true" CssClass="style3" onselectedindexchanged="DropDownList2_SelectedIndexChanged">
    <asp:ListItem>Veuillez choisir</asp:ListItem>
    <asp:ListItem Value="Charguia">Charguia</asp:ListItem>
    <asp:ListItem Value="Ghazala">Ghazala</asp:ListItem>
    
</asp:DropDownList>
    </center>
</td>




</tr>


</table>


</asp:Panel>
</center>
<br />

<asp:Panel ID="pl2"  runat="server" Visible="false">


<center>
<h2>Site: <asp:Label ID="lblSite" runat="server" ForeColor="CadetBlue"></asp:Label></h2>
<h2 style="font-family: Calibri">Affectation des séances d&#39;examens pour l&#39;année :
<asp:Label ID="lblan" runat="server" ForeColor="CadetBlue"></asp:Label>
, Semestre : <asp:Label ID="lblsem" runat="server" ForeColor="CadetBlue"></asp:Label>

,Periode: <asp:Label ID="lblperiod" runat="server" ForeColor="CadetBlue"></asp:Label></h2></center>

 <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>


       
    <br />
    <center>
                    &nbsp;
    <asp:Label ID="Label12" runat="server" Text="Date début : " class="style5"></asp:Label>
    <asp:TextBox ID="TextBoxdeb" runat="server" AutoPostBack="false" class="Calender" TextMode="Date" />
    &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
    <asp:Label ID="Label13" runat="server" Text="Date fin : " class="style5"></asp:Label>
                <asp:TextBox ID="TextBoxfin" runat="server" TextMode="Date"  class = "Calender"  AutoPostBack="false" />
                    <br />
        <br />
        <asp:Label ID="Label1" runat="server" Text="Numero de séance par jour : " class="style5"></asp:Label>
        <asp:DropDownList ID="DropDownListNumSeance" Width="80px"  runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="DropDownListNumSeance_SelectedIndexChanged">
                <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                <asp:ListItem Value="1">1</asp:ListItem>            
                <asp:ListItem Value="2">2</asp:ListItem>
                <asp:ListItem Value="3">3</asp:ListItem>
                <asp:ListItem Value="4">4</asp:ListItem>
                <asp:ListItem Value="5">5</asp:ListItem>
                <asp:ListItem Value="6">6</asp:ListItem>
                    </asp:DropDownList>
                    <br />
        <br />
         <asp:Label ID="Label2" runat="server" Text="Heur début séance 1 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="heurDebS1" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
        <asp:Label ID="Label10" runat="server" Text="Minute début séance 1 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="MinDebS1" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Selected="True" Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <br />
         <asp:Label ID="Label3" runat="server" Text="Heur début séance 2 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="heurDebS2" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
         <asp:Label ID="Label15" runat="server" Text="Minute début séance 2 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="MinDebS2" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Selected="True" Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <br />
         <asp:Label ID="Label4" runat="server" Text="Heur début séance 3 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="heurDebS3" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
        <asp:Label ID="Label16" runat="server" Text="Minute début séance 3 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="MinDebS3" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Selected="True" Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <br />
         <asp:Label ID="Label5" runat="server" Text="Heur début séance 4 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="heurDebS4" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
         <asp:Label ID="Label17" runat="server" Text="Minute début séance 4 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="MinDebS4" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Selected="True" Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <br />
         <asp:Label ID="Label8" runat="server" Text="Heur début séance 5 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="heurDebS5" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
        <asp:Label ID="Label18" runat="server" Text="Minute début séance 5 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="MinDebS5" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Selected="True" Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <br />
         <asp:Label ID="Label9" runat="server" Text="Heur début séance 6 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="heurDebS6" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Value="-1"> Choisir </asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
         <asp:Label ID="Label19" runat="server" Text="Minute début séance 6 : " class="style5" Visible="false"></asp:Label>
        <asp:DropDownList ID="MinDebS6" Width="80px"  runat="server" AppendDataBoundItems="true" Visible="false">
                        <asp:ListItem Selected="True" Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                    <br />

     <asp:Button ID="Button2" runat="server" Width="80px" OnClick="Button2_Click" Text="Valider"  Visible="false" />
   </center>
       
    <br />
    <center>
    <asp:GridView ID="Gridview1" runat="server" AutoGenerateColumns="false"  Style="border-bottom: black 2px ridge;
                                border-left: black 2px ridge; background-color: white; border-top: black 2px ridge;
                                border-right: black 2px ridge;"  OnRowDataBound="OnRowDataBoundS" BorderWidth="0px" BorderColor="Red" CssClass="grid"
                                RowStyle-CssClass="ItemStyle" HeaderStyle-CssClass="FixedHeaderStyle" GridLines="Both"
                                EmptyDataRowStyle-CssClass="ItemStyle" BackColor="#0099CC" onselectedindexchanged="Gridview1_SelectedIndexChanged" >
        <Columns>
            <%--  <asp:BoundField DataField="RowNumber" HeaderText="Nº séance" />--%>
            <asp:TemplateField HeaderText="Nº séance">
                <ItemTemplate>
                    <%-- <asp:BoundField DataField="RowNumber" HeaderText="Nº séance"  />--%>
                    <asp:Label ID="lb" runat="server" Text='<%# Eval("RowNumber")%>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--  <asp:TemplateField HeaderText="Date Examen">
        <ItemTemplate>
            <asp:TextBox ID="TextBox1" runat="server" Width="175px" />
            <asp:Calendar ID="DateTakenCalendar" runat="server" OnSelectionChanged="Calendar_Changed">
            
            
            </asp:Calendar>
        
        
        </ItemTemplate>
        </asp:TemplateField>
--%>
            <asp:TemplateField HeaderText="Date Examen">
                
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text="Label"></asp:Label>
                </ItemTemplate>
                
            </asp:TemplateField>
            <%--<asp:TemplateField HeaderText="Date">
            <ItemTemplate>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                 <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="TextBox1" 
                                                          PopupButtonID="TextBox1" Format="dd/MM/yyyy">
                                                          </asp:CalendarExtender>

            </ItemTemplate>
        </asp:TemplateField>--%><%--<asp:TemplateField HeaderText="Nº Jours">
            

            <ItemTemplate>
                <asp:DropDownList ID="DropDownList1" runat="server"
                                  AppendDataBoundItems="true">
                     <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>

        </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Nº Jours" Visible="false" >
                <ItemTemplate>
                    <asp:DropDownList ID="ddlCountries" runat="server" AppendDataBoundItems="true" AutoPostBack="true">
                        <asp:ListItem Value="01">01</asp:ListItem>
                        <asp:ListItem Value="02">02</asp:ListItem>
                        <asp:ListItem Value="03">03</asp:ListItem>
                        <asp:ListItem Value="04">04</asp:ListItem>
                        <asp:ListItem Value="05">05</asp:ListItem>
                        <asp:ListItem Value="06">06</asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Jours" Visible="true">
                <ItemTemplate>
                    <asp:Label ID="Labeljour" runat="server" Text="Label"></asp:Label>
                    <%--<asp:DropDownList ID="ddlCountries2" runat="server" AppendDataBoundItems="true" AutoPostBack="false" Enabled="true">
                        <asp:ListItem Value="Lundi">Lundi</asp:ListItem>
                        <asp:ListItem Value="Mardi">Mardi</asp:ListItem>
                        <asp:ListItem Value="Mercredi">Mercredi</asp:ListItem>
                        <asp:ListItem Value="Jeudi">Jeudi</asp:ListItem>
                        <asp:ListItem Value="Vendredi">Vendredi</asp:ListItem>
                        <asp:ListItem Value="Samedi">Samedi</asp:ListItem>
                    </asp:DropDownList>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <%-- <asp:TemplateField  HeaderText="Jours">
            <ItemTemplate>
                <asp:DropDownList ID="DropDownList2" runat="server"
                         AppendDataBoundItems="true">
                     <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </ItemTemplate>
        </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Heure début">
                <ItemTemplate>
                    <center>
                    <asp:DropDownList ID="DropDownList3" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                    </asp:DropDownList>
                        </center>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Minute début">
                <ItemTemplate>
                    <center>
                    <asp:DropDownList ID="DropDownList4" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                        <asp:ListItem Value="00">00</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="30">30</asp:ListItem>
                        <asp:ListItem Value="45">45</asp:ListItem>
                    </asp:DropDownList>
                        </center>
                </ItemTemplate>
            </asp:TemplateField>
           <%-- <asp:TemplateField HeaderText="Heure fin">
                <ItemTemplate>
                    <asp:DropDownList ID="DropDownList5" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                        <asp:ListItem Value="08">08</asp:ListItem>
                        <asp:ListItem Value="09">09</asp:ListItem>
                        <asp:ListItem Value="10">10</asp:ListItem>
                        <asp:ListItem Value="11">11</asp:ListItem>
                        <asp:ListItem Value="12">12</asp:ListItem>
                        <asp:ListItem Value="13">13</asp:ListItem>
                        <asp:ListItem Value="14">14</asp:ListItem>
                        <asp:ListItem Value="15">15</asp:ListItem>
                        <asp:ListItem Value="16">16</asp:ListItem>
                        <asp:ListItem Value="16">17</asp:ListItem>
                        <asp:ListItem Value="16">18</asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Durée">
                <ItemTemplate>
                    <center>
                    <asp:DropDownList ID="DropDownList6" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Value="-1">Select</asp:ListItem>
                        <asp:ListItem Value="1h">1h</asp:ListItem>
                        <asp:ListItem Value="1h30">1h30</asp:ListItem>
                        <asp:ListItem Value="2h">2h</asp:ListItem>
                    </asp:DropDownList>
                        </center>
                </ItemTemplate>
                <%--<FooterStyle HorizontalAlign="Right" />
                <FooterTemplate>
                    <asp:Button ID="ButtonAdd" runat="server" onclick="ButtonAdd_Click" Text="Ajouter séance" />
                </FooterTemplate>--%>
            </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Button ID="ButtonSupp" runat="server" onclick="ButtonSupp_Click"  ForeColor="Red" Text="(-)" />
                   <%-- <asp:CheckBox ID="Button3" runat="server" AutoPostBack="true" OnCheckedChanged="ButtonSupp_Click"  ForeColor="Red" Text="(-)" />--%>
                 </ItemTemplate>
            </asp:TemplateField>
        </Columns>
      
        <HeaderStyle BackColor="#CC0000" CssClass="FixedHeaderStyle" ForeColor="White" />
        <RowStyle CssClass="ItemStyle" />
    </asp:GridView>
    </center>

<br />
<br />

<center>
<asp:Button  ID="btnaffecter" runat="server" Text="Affecter" Visible="false" Height="40px" 
        Width="100px" onclick="btnaffecter_Click" />
</center>
<br />
<br />
<br />

    </asp:Panel>

</asp:Content>
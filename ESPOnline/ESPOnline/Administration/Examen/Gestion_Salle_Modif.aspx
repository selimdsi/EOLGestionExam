﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Gestion_Salle_Modif.aspx.cs" Inherits="ESPOnline.Administration.Examen.Gestion_Salle_Modif" MasterPageFile="~/Administration/Examen/Exam.Master"%>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <style type="text/css">
        table.grid tbody tr:hover
        {
            background-color: #e5ecf9;
        }
        .GridHeaderStyle
        {
            color: #FEF7F7;
            background-color: #877d7d;
            font-weight: bold;
        }
        .GridItemStyle
        {
            background-color: #eeeeee;
            color: white;
        }
        .GridAlternatingStyle
        {
            background-color: #dddddd;
            color: black;
        }
        .GridSelectedStyle
        {
            background-color: #d6e6f6;
            color: black;
        }
        
        
        .GridStyle
        {
            border-bottom: white 2px ridge;
            border-left: white 2px ridge;
            background-color: white;
            width: 100%;
            border-top: white 2px ridge;
            border-right: white 2px ridge;
        }
        .ItemStyle
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        
        .ItemStyle td
        {
            background-color: #eeeeee;
            color: black;
            padding-bottom: 5px;
            padding-right: 3px;
            padding-top: 5px;
            padding-left: 3px;
            height: 25px;
        }
        .FixedHeaderStyle
        {
            background-color: #7591b1;
            color: #FFFFFF;
            font-weight: bold;
            position: relative;
            top: expression(this.offsetParent.scrollTop);
            z-index: 10;
        }
        .Caption_1_Customer
        {
            background-color: #beccda;
            color: #000000;
            width: 30%;
            height: 20px;
        }
        
        
        .style3
        {
            color: #000000;
        }
        .style4
        {
            color: #CC0000;
        }
        .style5
        {
            color: #0066FF;
        }
        .style6
        {
            color: #0000FF;
        }
        
        
        .style7
        {
            color: #FF0000;
        }
        
        
        .style8
        {
            color: #003300;
        }
        
        .grid td, .grid th
        {
            text-align: center;

    </style> 


<h3 style="font-family: Calibri; margin-left: 640px;" __designer:mapid="6db">Modification des salles </h3>
    <center>
    <br />
    
    <asp:Panel ID="Panel1" runat="server">
        <center>
        <table>
            
            <tr>
                <td>
                    <asp:Label ID="Label11" class="style5" runat="server" Text="Semestre: ">
              </asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList ID="Rdsemestre" runat="server"   
            RepeatDirection="Horizontal" width="120px" >
                        <asp:ListItem Value="1">S1</asp:ListItem>
                        <asp:ListItem Value="2">S2</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Periode: "
    class="style5"  ></asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList ID="rdperiode" runat="server"   
             RepeatDirection="Horizontal" width="120px" >
                        <asp:ListItem Value="1">P1</asp:ListItem>
                        <asp:ListItem Value="2">P2</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td>
                    <asp:Button  ID="Button2" runat="server" Text="Valider" Height="35px"
                    Width="120px" onclick="btnvalid9_Click"/>
                </td>
            </tr>
        </table>
        </center>
    </asp:Panel>
    </center>
    <br />
    <asp:Panel ID="Plchoixannee" runat="server" Visible="false">
<table><tr><td><asp:Label ID="Label7" runat="server" Text="Veuillez choisir l'année auquel vous avez faire l'affectation: "
                            ></asp:Label>
</td>
<td> <asp:RadioButtonList ID="Rdcours" runat="server"   
            RepeatDirection="Horizontal" width="180px" >
            <asp:ListItem Selected="True" value="C">Année en cours</asp:ListItem>
                 <asp:ListItem value="A">Autre</asp:ListItem>
              
                
            </asp:RadioButtonList></td>

              <td><asp:Button  ID="Btnsuivant" runat="server" Text="Suivant" Height="35px" 
                    Width="120px" /></td>
</tr></table>
</asp:Panel>

<div>

    <br />
    <br />
     <asp:SqlDataSource ID="sqlDataSource1" runat="server" 
                   ConnectionString="<%$ ConnectionStrings:DefaultConnectionString %>" 
                    ProviderName="<%$ ConnectionStrings:DefaultConnectionString.ProviderName %>" 
                    SelectCommand="SELECT DISTINCT NUM_SEANCE from ESP_SEANCE_EXAMEN order by NUM_SEANCE">
                </asp:SqlDataSource>

                <asp:Label ID="Label12" class="style5" runat="server" Text="Séance" Visible="false"></asp:Label>
    :<asp:DropDownList ID="DropDownList15" runat="server" Width="111px" OnSelectedIndexChanged="DropDownList15OnSelect" Visible="false" DataSourceID="sqlDataSource1" 
        DataTextField="NUM_SEANCE" DataValueField="NUM_SEANCE">
        
    </asp:DropDownList>
                
   
     <asp:Button ID="DISPSALLE" runat="server" 
                    Text="Valider les seances" OnClick="DISPSALLE_Click2" Visible="false"/>
                
   
    <center>            
    <asp:GridView ID="GridviewSALLE"  HeaderStyle-BackColor="RosyBrown" HeaderStyle-BorderColor="White" 
        runat="server"  CssClass="carousel"   ShowFooter="True" Width="1262px"  AutoGenerateColumns="False" OnSelectedIndexChanged="GridviewSALLE_SelectedIndexChanged" >
        <Columns>        
            <%--<asp:BoundField DataField="NUM_SEANCE" HeaderText="Seance" SortExpression="NUM_SEANCE" />
            <asp:BoundField DataField="CODE_CL" HeaderText="Classe" SortExpression="CODE_CL" />
            <asp:BoundField DataField="CODE_MODULE" HeaderText="Module" SortExpression="CODE_MODULE" />
            <asp:BoundField DataField="NUM_SEMESTRE" HeaderText="Semestre" SortExpression="NUM_SEMESTRE" />
            <asp:BoundField DataField="NUM_PERIODE" HeaderText="Periode" SortExpression="NUM_PERIODE" />--%>
             <asp:TemplateField HeaderText="Modifier Salle" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px" >
                <HeaderTemplate>
                <asp:CheckBox ID = "chkAll" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChangedDDD" Visible="false"/>
            </HeaderTemplate>
                 <ItemTemplate>
                     <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" OnCheckedChanged="OnCheckedChanged"/>
                 </ItemTemplate>
<HeaderStyle Width="30px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
               
            


            <asp:TemplateField HeaderText="Seance" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice11" runat="server" Text='<%# Eval("NUM_SEANCE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice1" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Classe" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice12" runat="server" Text='<%# Eval("CODE_CL")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice2" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Module" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice13" runat="server" Text='<%# Eval("MODULE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice3" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Semestre" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice14" runat="server" Text='<%# Eval("SEMESTRE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice4" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Periode" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice15" runat="server" Text='<%# Eval("PERIODE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice5" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="150px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice159" runat="server" Text='<%# Eval("DATE_SEANCE")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice59" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="70px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>


             <asp:TemplateField HeaderText="Heur" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblPrice158" runat="server" Text='<%# Eval("HEURE_DEBUT")%>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label ID="lblTotalPrice58" runat="server" />
                </FooterTemplate>

<HeaderStyle Width="70px"></HeaderStyle>

<ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>


            <asp:TemplateField HeaderText="Salle 1">
                <ItemTemplate>
                    <center>

                    <asp:Label ID="lblPrice155" runat="server" Text='<%# Eval("SALLE_1")%>' />
                    <asp:DropDownList ID="DropDownListSALLE1" runat="server" Visible="false" AutoPostBack="true" AppendDataBoundItems="true"  Width="150px">
                    </asp:DropDownList>
                        <%-- <asp:DropDownList ID="DropDownListSALLE2" runat="server" AppendDataBoundItems="true"  Width="100px">
                    </asp:DropDownList>--%>
                </center>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Salle 2">
                <ItemTemplate>
                    <center>
                        <asp:Label ID="lblPrice156" runat="server" Text='<%# Eval("SALLE_2")%>' />
                    <asp:DropDownList ID="DropDownListSALLE2" runat="server" Visible="false" AppendDataBoundItems="true" AutoPostBack="true"  Width="150px">
                    </asp:DropDownList>
                </center>
                </ItemTemplate>
            </asp:TemplateField>
           
        </Columns>

<HeaderStyle BackColor="#CC0000" BorderColor="White" ForeColor="White"></HeaderStyle>
    </asp:GridView>
</center> 
    <center>
               <asp:Button ID="btnUpdate" runat="server" Text="Modifier" OnClick = "Update"  height="44px" Visible="true"/>
        </center>
     <link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
</asp:Content>
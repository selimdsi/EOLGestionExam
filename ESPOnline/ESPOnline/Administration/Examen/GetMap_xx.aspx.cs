﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using System.Configuration;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using ESPSuiviEncadrement;


namespace ESPOnline.Administration.Examen
{
    public partial class GetMap_xx : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                plvalider.Visible = true;
                plgenerate.Visible = false;
            }
        }

        protected void btnSimple_Click(object sender, EventArgs e)
        {
            /*
                        String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
                        OracleConnection con = new OracleConnection(strConnString);
                        OracleCommand cmd = new OracleCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "PS_MAP_EXAMEN";
                        cmd.Parameters.Add(":vannee", OracleDbType.Varchar2).Value = txt1.Text.Trim();
                        cmd.Parameters.Add(":vsemestre", OracleDbType.Int32).Value = txt2.Text.Trim();
                        cmd.Parameters.Add(":vperiode", OracleDbType.Int32).Value = txt3.Text.Trim();
                        cmd.Parameters.Add(":vsite", OracleDbType.Varchar2).Value = txt4.Text.Trim(); 
                    cmd.Connection = con;
                        cmd.Connection = con;
                        //try
                        //{
                            con.Open();
                           // GridView1.EmptyDataText = "No Records Found";
                            GridView1.DataSource = cmd.ExecuteReader();
                            GridView1.DataBind();
                        //}
                        //catch (Exception ex)
                        //{
                        //    throw ex;
                        //}
                        //finally
                        //{
                        //    con.Close();
                        //    con.Dispose();
                        //}
     
                        */
            /*
            OracleCommand command = new  OracleCommand();
		 OracleDataAdapter adapter = new  OracleDataAdapter();
		DataSet ds = new DataSet();
		int i = 0;
		string  Oracle = null;
		//string connetionString = "Data Source=.;Initial Catalog=pubs;User ID=sa;Password=*****";
		 OracleConnection connection = new  OracleConnection(AppConfiguration.ConnectionString);
        
		connection.Open();
		command.Connection = connection;
		command.CommandType = CommandType.StoredProcedure;
        command.CommandText = "PS_MAP_EXAMEN";

		adapter = new  OracleDataAdapter(command);
		adapter.Fill(ds);
		connection.Close();
		GridView1.DataSource = ds.Tables[0];
		GridView1.DataBind();
             * */

        }

        protected void btnSimpleParam_Click(object sender, EventArgs e)
        {

        }

        protected void btnvalid_Click(object sender, EventArgs e)
        {
            if (Rdsemestre.SelectedValue == "" || rdperiode.SelectedValue == "")
            {
                Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

            }
            else
            {
                plgenerate.Visible = false;
                //Plchoixannee.Visible = true;
                plvalider.Visible = false;

                Plannee2.Visible = true;
                plannee.Visible = false;
                Plchoixannee.Visible = false;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";
                string annee_deb = service.get_annee_parametr();

                Label8.Text = Rdsemestre.SelectedValue;
                Label9.Text = rdperiode.Text;
                DropDownList2.DataSource = service.bind_site2(annee_deb.Substring(0, 4));

                DropDownList2.DataBind();

                Label1.Text = "a.ANNEE_DEB= '" + annee_deb.ToString().Substring(0, 4) + "'";
                //Label1.Text = "a.ANNEE_DEB= '" + lblanneedeb.Text.ToString().Substring(0, 4) + "'";
                plgenerate.Visible = true;
                get_annee_univ();
            }
        }

        protected void Btnsuivant_Click(object sender, EventArgs e)
        {
            if (Rdcours.SelectedValue == "C")
            {
                Plannee2.Visible = true;
                plannee.Visible = false;
                Plchoixannee.Visible = false;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";
                string annee_deb = service.get_annee_parametr();

                Label8.Text = Rdsemestre.SelectedValue;
                Label9.Text = rdperiode.Text;
                DropDownList2.DataSource = service.bind_site2(annee_deb.Substring(0, 4));

                DropDownList2.DataBind();

                Label1.Text = "a.ANNEE_DEB= '" + annee_deb.ToString().Substring(0, 4) + "'";
                //Label1.Text = "a.ANNEE_DEB= '" + lblanneedeb.Text.ToString().Substring(0, 4) + "'";
                plgenerate.Visible = true;
                get_annee_univ();

            }
            else
            {
                if (Rdcours.SelectedValue == "A")
                {
                    Plannee2.Visible = false;
                    plannee.Visible = true;
                    Plchoixannee.Visible = false;


                    lbls.Text = Rdsemestre.SelectedValue;
                    lblp.Text = rdperiode.SelectedValue;
                    plgenerate.Visible = true;
                    get_annee_univ();
                }

                else
                {

                    Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

                }
            }

        }

        public void get_annee_univ()
        {
            ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
            ddlan_univer.DataSource = service.List_etud_2015();
            ddlan_univer.DataBind();
            ddlan_univer.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
            ddlan_univer.SelectedItem.Selected = false;
            ddlan_univer.Items.FindByText("Veuillez choisir").Selected = true;


        }


        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }


        }

        protected void btngenerate_Click(object sender, EventArgs e)
        {
            btnSupprimer.Visible = true;
            enreg.Visible = true;


            if (Rdcours.SelectedValue == "C")
            {
                string annee_deb = service.get_annee_parametr();
                string annee_fin = service.get_annee_parametrfin();

                string ad = annee_deb.Substring(0, 4);
                string af = annee_deb.Substring(5, 4);

                string aad = ad.Substring(2, 2).ToString();
                string aaf = af.Substring(2, 2).ToString();

                string annedf = aad + aaf;
                service.CreateMapExamenx(ad, Convert.ToInt32(Rdsemestre.SelectedValue), Convert.ToInt32(rdperiode.SelectedValue), DropDownList2.SelectedValue);

                geidmapx.DataSource = service.getmap_x(annedf, Convert.ToInt32(Rdsemestre.SelectedValue), Convert.ToInt32(rdperiode.SelectedValue), DropDownList2.SelectedValue);
                geidmapx.DataBind();
                lblname.Text = "MAPX_" + annedf + "_" + Convert.ToInt32(Rdsemestre.SelectedValue) + "" + Convert.ToInt32(rdperiode.SelectedValue) + "_" + DropDownList2.SelectedValue + "";

            }

            else
                if (Rdcours.SelectedValue == "A")
                {
                    //geidmapx.DataSource = service.getmap_x(ddlan_univer.SelectedValue,Convert.ToInt32(Rdsemestre.SelectedValue),Convert.ToInt32(rdperiode.SelectedValue),ddlsite.SelectedValue);
                    geidmapx.DataBind();

                }

            ScriptManager.RegisterStartupScript(Page, this.GetType(), "Key", "<script>MakeStaticHeader('" + geidmapx.ClientID + "', 400, 950 , 40 ,true); </script>", false);
       
        }



        protected void btnSUPPRIMER(object sender, EventArgs e)
        {
            btnSupprimer.Visible = false;
            enreg.Visible = false;
            geidmapx.Visible = false;
            //lblname.Text = "mapdsex__" + annedf + "_" + Convert.ToInt32(Rdsemestre.SelectedValue) + "" + Convert.ToInt32(rdperiode.SelectedValue) + "_" + DropDownList2.SelectedValue + "";

            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            OracleConnection connection = new OracleConnection(AppConfiguration.ConnectionString);
            connection.Open();
            OracleCommand cmd1 = new OracleCommand("DROP TABLE " + lblname.Text + "", connection);
            cmd1.ExecuteNonQuery();
            connection.Close();
        }

        protected void onclickenreg(object sender, EventArgs e)
        {
            btnSupprimer.Visible = false;
        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using BLL;
using Oracle.ManagedDataAccess.Client;
using ESPSuiviEncadrement;
using System.Globalization;

namespace ESPOnline.Administration.Examen
{
    public partial class Affecter_seance_exam : System.Web.UI.Page
    {
        ServiceExamen service = new ServiceExamen();
        #region static data
        //private ArrayList GetDummyData()
        //{

        //    ArrayList arr = new ArrayList();

        //    arr.Add(new ListItem("Item1", "1"));
        //    arr.Add(new ListItem("Item2", "2"));
        //    arr.Add(new ListItem("Item3", "3"));
        //    arr.Add(new ListItem("Item4", "4"));
        //    arr.Add(new ListItem("Item5", "5"));

        //    return arr;
        //}

        //private void FillDropDownList(DropDownList ddl)
        //{
        //    ArrayList arr = GetDummyData();

        //    foreach (ListItem item in arr)
        //    {
        //        ddl.Items.Add(item);
        //    }
        //} 
        #endregion


          protected void DropDownListNumSeance_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownListNumSeance.SelectedItem.Text == "6")
            {
                Button2.Visible = true;

                //heur, minute S1
                Label2.Visible = true;
                heurDebS1.Visible = true;
                Label10.Visible = true;
                MinDebS1.Visible = true;

                //heur, minute S2
                Label3.Visible = true;
                heurDebS2.Visible = true;
                Label15.Visible = true;
                MinDebS2.Visible = true;

                //heur, minute S3
                Label4.Visible = true;
                heurDebS3.Visible = true;
                Label16.Visible = true;
                MinDebS3.Visible = true;

                //heur, minute S4
                Label5.Visible = true;
                heurDebS4.Visible = true;
                Label17.Visible = true;
                MinDebS4.Visible = true;

                //heur, minute S5
                Label8.Visible = true;
                heurDebS5.Visible = true;
                Label18.Visible = true;
                MinDebS5.Visible = true;

                //heur, minute S6
                Label9.Visible = true;
                heurDebS6.Visible = true;
                Label19.Visible = true;
                MinDebS6.Visible = true;
 
            }
            if (DropDownListNumSeance.SelectedItem.Text == "5")
            {
                Button2.Visible = true;

                //heur, minute S1
                Label2.Visible = true;
                heurDebS1.Visible = true;
                Label10.Visible = true;
                MinDebS1.Visible = true;

                //heur, minute S2
                Label3.Visible = true;
                heurDebS2.Visible = true;
                Label15.Visible = true;
                MinDebS2.Visible = true;

                //heur, minute S3
                Label4.Visible = true;
                heurDebS3.Visible = true;
                Label16.Visible = true;
                MinDebS3.Visible = true;

                //heur, minute S4
                Label5.Visible = true;
                heurDebS4.Visible = true;
                Label17.Visible = true;
                MinDebS4.Visible = true;

                //heur, minute S5
                Label8.Visible = true;
                heurDebS5.Visible = true;
                Label18.Visible = true;
                MinDebS5.Visible = true;

                //heur, minute S6
                Label9.Visible = false;
                heurDebS6.Visible = false;
                Label19.Visible = false;
                MinDebS6.Visible = false;
            }
            if (DropDownListNumSeance.SelectedItem.Text == "4")
            {
                Button2.Visible = true;

                //heur, minute S1
                Label2.Visible = true;
                heurDebS1.Visible = true;
                Label10.Visible = true;
                MinDebS1.Visible = true;

                //heur, minute S2
                Label3.Visible = true;
                heurDebS2.Visible = true;
                Label15.Visible = true;
                MinDebS2.Visible = true;

                //heur, minute S3
                Label4.Visible = true;
                heurDebS3.Visible = true;
                Label16.Visible = true;
                MinDebS3.Visible = true;

                //heur, minute S4
                Label5.Visible = true;
                heurDebS4.Visible = true;
                Label17.Visible = true;
                MinDebS4.Visible = true;

                //heur, minute S5
                Label8.Visible = false;
                heurDebS5.Visible = false;
                Label18.Visible = false;
                MinDebS5.Visible = false;

                //heur, minute S6
                Label9.Visible = false;
                heurDebS6.Visible = false;
                Label19.Visible = false;
                MinDebS6.Visible = false;
            }
            if (DropDownListNumSeance.SelectedItem.Text == "3")
            {
                Button2.Visible = true;

                //heur, minute S1
                Label2.Visible = true;
                heurDebS1.Visible = true;
                Label10.Visible = true;
                MinDebS1.Visible = true;

                //heur, minute S2
                Label3.Visible = true;
                heurDebS2.Visible = true;
                Label15.Visible = true;
                MinDebS2.Visible = true;

                //heur, minute S3
                Label4.Visible = true;
                heurDebS3.Visible = true;
                Label16.Visible = true;
                MinDebS3.Visible = true;

                //heur, minute S4
                Label5.Visible = false;
                heurDebS4.Visible = false;
                Label17.Visible = false;
                MinDebS4.Visible = false;

                //heur, minute S5
                Label8.Visible = false;
                heurDebS5.Visible = false;
                Label18.Visible = false;
                MinDebS5.Visible = false;

                //heur, minute S6
                Label9.Visible = false;
                heurDebS6.Visible = false;
                Label19.Visible = false;
                MinDebS6.Visible = false;
            }
            if (DropDownListNumSeance.SelectedItem.Text == "2")
            {
                Button2.Visible = true;

                //heur, minute S1
                Label2.Visible = true;
                heurDebS1.Visible = true;
                Label10.Visible = true;
                MinDebS1.Visible = true;

                //heur, minute S2
                Label3.Visible = true;
                heurDebS2.Visible = true;
                Label15.Visible = true;
                MinDebS2.Visible = true;

                //heur, minute S3
                Label4.Visible = false;
                heurDebS3.Visible = false;
                Label16.Visible = false;
                MinDebS3.Visible = false;

                //heur, minute S4
                Label5.Visible = false;
                heurDebS4.Visible = false;
                Label17.Visible = false;
                MinDebS4.Visible = false;

                //heur, minute S5
                Label8.Visible = false;
                heurDebS5.Visible = false;
                Label18.Visible = false;
                MinDebS5.Visible = false;

                //heur, minute S6
                Label9.Visible = false;
                heurDebS6.Visible = false;
                Label19.Visible = false;
                MinDebS6.Visible = false;
            }
            if (DropDownListNumSeance.SelectedItem.Text == "1")
            {
                Button2.Visible = true;

                //heur, minute S1
                Label2.Visible = true;
                heurDebS1.Visible = true;
                Label10.Visible = true;
                MinDebS1.Visible = true;

                //heur, minute S2
                Label3.Visible = false;
                heurDebS2.Visible = false;
                Label15.Visible = false;
                MinDebS2.Visible = false;

                //heur, minute S3
                Label4.Visible = false;
                heurDebS3.Visible = false;
                Label16.Visible = false;
                MinDebS3.Visible = false;

                //heur, minute S4
                Label5.Visible = false;
                heurDebS4.Visible = false;
                Label17.Visible = false;
                MinDebS4.Visible = false;

                //heur, minute S5
                Label8.Visible = false;
                heurDebS5.Visible = false;
                Label18.Visible = false;
                MinDebS5.Visible = false;

                //heur, minute S6
                Label9.Visible = false;
                heurDebS6.Visible = false;
                Label19.Visible = false;
                MinDebS6.Visible = false;
            }

        }




        private void SetInitialRow()
        {

            DataTable dt = new DataTable();
            DataRow dr = null;
            var Days = (Convert.ToDateTime(TextBoxfin.Text) - Convert.ToDateTime(TextBoxdeb.Text)).TotalDays;
            int day = (int)Days;



            dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Column1", typeof(string)));//for TextBox value 
            dt.Columns.Add(new DataColumn("Column2", typeof(string)));//for TextBox value 
            dt.Columns.Add(new DataColumn("Column3", typeof(string)));//for DropDownList selected item 
            dt.Columns.Add(new DataColumn("Column4", typeof(string)));//for DropDownList selected item 
            dt.Columns.Add(new DataColumn("Column5", typeof(string)));//for DropDownList selected item 

            //dt.Columns.Add(new DataColumn("Column6", typeof(string)));//for DropDownList selected item 

            dt.Columns.Add(new DataColumn("Column6", typeof(string)));//for DropDownList selected item 
            DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
            DataRow drCurrentRow = null;


            int rownum = 0;


            if (DropDownListNumSeance.SelectedItem.Text == "6")
            {
                for (int i = 0; i <= day; i++)         //gggggggggggggggggggggg
                {
                    for (int j = 0; j < 6; j++)
                    {
                        rownum++;
                        dr = dt.NewRow();
                        //Label date = (Label)Gridview1.Rows[2].Cells[1].FindControl("lbdate");


                        dr["RowNumber"] = rownum;
                        dr["Column1"] = string.Empty;

                        //date.Text=TextBoxdeb.Text;
                        dr["Column2"] = string.Empty;
                        dr["Column3"] = string.Empty;
                        dr["Column4"] = string.Empty;
                        dr["Column5"] = string.Empty;


                        dt.Rows.Add(dr);
                    }

                }
            }

            if (DropDownListNumSeance.SelectedItem.Text == "5")
            {
                for (int i = 0; i <= day; i++)         //gggggggggggggggggggggg
                {
                    for (int j = 0; j < 5; j++)
                    {
                        rownum++;
                        dr = dt.NewRow();
                        //Label date = (Label)Gridview1.Rows[2].Cells[1].FindControl("lbdate");


                        dr["RowNumber"] = rownum;
                        dr["Column1"] = string.Empty;

                        //date.Text=TextBoxdeb.Text;
                        dr["Column2"] = string.Empty;
                        dr["Column3"] = string.Empty;
                        dr["Column4"] = string.Empty;
                        dr["Column5"] = string.Empty;


                        dt.Rows.Add(dr);
                    }

                }
            }

            if (DropDownListNumSeance.SelectedItem.Text == "4")
            {
                for (int i = 0; i <= day; i++)         //gggggggggggggggggggggg
                {
                    for (int j = 0; j < 4; j++)
                    {
                        rownum++;
                        dr = dt.NewRow();
                        //Label date = (Label)Gridview1.Rows[2].Cells[1].FindControl("lbdate");


                        dr["RowNumber"] = rownum;
                        dr["Column1"] = string.Empty;

                        //date.Text=TextBoxdeb.Text;
                        dr["Column2"] = string.Empty;
                        dr["Column3"] = string.Empty;
                        dr["Column4"] = string.Empty;
                        dr["Column5"] = string.Empty;


                        dt.Rows.Add(dr);
                        
                    }

                }
            }

            if (DropDownListNumSeance.SelectedItem.Text == "3")
            {
                for (int i = 0; i <= day; i++)         //gggggggggggggggggggggg
                {
                    for (int j = 0; j < 3; j++)
                    {
                        rownum++;
                        dr = dt.NewRow();
                        //Label date = (Label)Gridview1.Rows[2].Cells[1].FindControl("lbdate");


                        dr["RowNumber"] = rownum;
                        dr["Column1"] = string.Empty;

                        //date.Text=TextBoxdeb.Text;
                        dr["Column2"] = string.Empty;
                        dr["Column3"] = string.Empty;
                        dr["Column4"] = string.Empty;
                        dr["Column5"] = string.Empty;


                        dt.Rows.Add(dr);
                    }

                }
            }

            if (DropDownListNumSeance.SelectedItem.Text == "2")
            {
                for (int i = 0; i <= day; i++)         //gggggggggggggggggggggg
                {
                    for (int j = 0; j < 2; j++)
                    {
                        rownum++;
                        dr = dt.NewRow();
                        //Label date = (Label)Gridview1.Rows[2].Cells[1].FindControl("lbdate");


                        dr["RowNumber"] = rownum;
                        dr["Column1"] = string.Empty;

                        //date.Text=TextBoxdeb.Text;
                        dr["Column2"] = string.Empty;
                        dr["Column3"] = string.Empty;
                        dr["Column4"] = string.Empty;
                        dr["Column5"] = string.Empty;


                        dt.Rows.Add(dr);
                    }

                }
            }

            if (DropDownListNumSeance.SelectedItem.Text == "1")
            {
                for (int i = 0; i <= day; i++)         //gggggggggggggggggggggg
                {
                    for (int j = 0; j < 1; j++)
                    {
                        rownum++;
                        dr = dt.NewRow();
                        //Label date = (Label)Gridview1.Rows[2].Cells[1].FindControl("lbdate");


                        dr["RowNumber"] = rownum;
                        dr["Column1"] = string.Empty;

                        //date.Text=TextBoxdeb.Text;
                        dr["Column2"] = string.Empty;
                        dr["Column3"] = string.Empty;
                        dr["Column4"] = string.Empty;
                        dr["Column5"] = string.Empty;


                        dt.Rows.Add(dr);
                    }

                }
            }


            //Store the DataTable in ViewState for future reference 

            ViewState["CurrentTable"] = dt;

            //Bind the Gridview 
            Gridview1.DataSource = dt;
            Gridview1.DataBind();

            if (DropDownListNumSeance.SelectedItem.Text == "6")
            {

                foreach (GridViewRow row in Gridview1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int z = 0;
                        for (int i = 0; i <= day; i++)
                        {
                            DropDownList ddl31 = (DropDownList)Gridview1.Rows[z].Cells[4].FindControl("DropDownList3");
                            ddl31.SelectedItem.Text = heurDebS1.Text;
                            DropDownList ddl41 = (DropDownList)Gridview1.Rows[z].Cells[5].FindControl("DropDownList4");
                            ddl41.SelectedItem.Text = MinDebS1.Text;
                            DropDownList ddl51 = (DropDownList)Gridview1.Rows[z].Cells[6].FindControl("DropDownList6");
                            ddl51.SelectedItem.Text = "1h";

                            DropDownList ddl32 = (DropDownList)Gridview1.Rows[z + 1].Cells[4].FindControl("DropDownList3");
                            ddl32.SelectedItem.Text = heurDebS2.Text;
                            DropDownList ddl42 = (DropDownList)Gridview1.Rows[z + 1].Cells[5].FindControl("DropDownList4");
                            ddl42.SelectedItem.Text = MinDebS2.Text;
                            DropDownList ddl52 = (DropDownList)Gridview1.Rows[z + 1].Cells[6].FindControl("DropDownList6");
                            ddl52.SelectedItem.Text = "1h";

                            DropDownList ddl33 = (DropDownList)Gridview1.Rows[z + 2].Cells[4].FindControl("DropDownList3");
                            ddl33.SelectedItem.Text = heurDebS3.Text;
                            DropDownList ddl43 = (DropDownList)Gridview1.Rows[z + 2].Cells[5].FindControl("DropDownList4");
                            ddl43.SelectedItem.Text = MinDebS3.Text;
                            DropDownList ddl53 = (DropDownList)Gridview1.Rows[z + 2].Cells[6].FindControl("DropDownList6");
                            ddl53.SelectedItem.Text = "1h";

                            DropDownList ddl34 = (DropDownList)Gridview1.Rows[z + 3].Cells[4].FindControl("DropDownList3");
                            ddl34.SelectedItem.Text = heurDebS4.Text;
                            DropDownList ddl44 = (DropDownList)Gridview1.Rows[z + 3].Cells[5].FindControl("DropDownList4");
                            ddl44.SelectedItem.Text = MinDebS4.Text;
                            DropDownList ddl54 = (DropDownList)Gridview1.Rows[z + 3].Cells[6].FindControl("DropDownList6");
                            ddl54.SelectedItem.Text = "1h";

                            DropDownList ddl35 = (DropDownList)Gridview1.Rows[z + 4].Cells[4].FindControl("DropDownList3");
                            ddl35.SelectedItem.Text = heurDebS5.Text;
                            DropDownList ddl45 = (DropDownList)Gridview1.Rows[z + 4].Cells[5].FindControl("DropDownList4");
                            ddl45.SelectedItem.Text = MinDebS5.Text;
                            DropDownList ddl55 = (DropDownList)Gridview1.Rows[z + 4].Cells[6].FindControl("DropDownList6");
                            ddl55.SelectedItem.Text = "1h";

                            DropDownList ddl36 = (DropDownList)Gridview1.Rows[z + 5].Cells[4].FindControl("DropDownList3");
                            ddl36.SelectedItem.Text = heurDebS6.Text;
                            DropDownList ddl46 = (DropDownList)Gridview1.Rows[z + 5].Cells[5].FindControl("DropDownList4");
                            ddl46.SelectedItem.Text = MinDebS6.Text;
                            DropDownList ddl56 = (DropDownList)Gridview1.Rows[z + 5].Cells[6].FindControl("DropDownList6");
                            ddl56.SelectedItem.Text = "1h";

                            for (int j = 0; j < 6; j++)
                            {
                                z++;
                                DateTime date1 = Convert.ToDateTime(TextBoxdeb.Text);
                                Label date = (Label)Gridview1.Rows[j + (6 * i)].FindControl("Label14");
                                DateTime answer = date1.AddDays(i);
                                date.Text = answer.ToString("dd/MM/yyyy");
                                
                                Label jour = (Label)Gridview1.Rows[j + (6 * i)].FindControl("Labeljour");
                                DateTime dateValue = date1.AddDays(i);
                                string jourr = dateValue.ToString("ddd", new CultureInfo("fr-FR"));
                                jour.Text = jourr;
                            }
                        }
                    }
                }
            }

            if (DropDownListNumSeance.SelectedItem.Text == "5")
            {

                foreach (GridViewRow row in Gridview1.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        int z = 0;
                        for (int i = 0; i <= day; i++)
                        {
                            DropDownList ddl31 = (DropDownList)Gridview1.Rows[z].Cells[4].FindControl("DropDownList3");
                            ddl31.SelectedItem.Text = heurDebS1.Text;
                            DropDownList ddl41 = (DropDownList)Gridview1.Rows[z].Cells[5].FindControl("DropDownList4");
                            ddl41.SelectedItem.Text = MinDebS1.Text;
                            DropDownList ddl51 = (DropDownList)Gridview1.Rows[z].Cells[6].FindControl("DropDownList6");
                            ddl51.SelectedItem.Text = "1h";

                            DropDownList ddl32 = (DropDownList)Gridview1.Rows[z + 1].Cells[4].FindControl("DropDownList3");
                            ddl32.SelectedItem.Text = heurDebS2.Text;
                            DropDownList ddl42 = (DropDownList)Gridview1.Rows[z + 1].Cells[5].FindControl("DropDownList4");
                            ddl42.SelectedItem.Text = MinDebS2.Text;
                            DropDownList ddl52 = (DropDownList)Gridview1.Rows[z + 1].Cells[6].FindControl("DropDownList6");
                            ddl52.SelectedItem.Text = "1h";

                            DropDownList ddl33 = (DropDownList)Gridview1.Rows[z + 2].Cells[4].FindControl("DropDownList3");
                            ddl33.SelectedItem.Text = heurDebS3.Text;
                            DropDownList ddl43 = (DropDownList)Gridview1.Rows[z + 2].Cells[5].FindControl("DropDownList4");
                            ddl43.SelectedItem.Text = MinDebS3.Text;
                            DropDownList ddl53 = (DropDownList)Gridview1.Rows[z + 2].Cells[6].FindControl("DropDownList6");
                            ddl53.SelectedItem.Text = "1h";

                            DropDownList ddl34 = (DropDownList)Gridview1.Rows[z + 3].Cells[4].FindControl("DropDownList3");
                            ddl34.SelectedItem.Text = heurDebS4.Text;
                            DropDownList ddl44 = (DropDownList)Gridview1.Rows[z + 3].Cells[5].FindControl("DropDownList4");
                            ddl44.SelectedItem.Text = MinDebS4.Text;
                            DropDownList ddl54 = (DropDownList)Gridview1.Rows[z + 3].Cells[6].FindControl("DropDownList6");
                            ddl54.SelectedItem.Text = "1h";

                            DropDownList ddl35 = (DropDownList)Gridview1.Rows[z + 4].Cells[4].FindControl("DropDownList3");
                            ddl35.SelectedItem.Text = heurDebS5.Text;
                            DropDownList ddl45 = (DropDownList)Gridview1.Rows[z + 4].Cells[5].FindControl("DropDownList4");
                            ddl45.SelectedItem.Text = MinDebS5.Text;
                            DropDownList ddl55 = (DropDownList)Gridview1.Rows[z + 4].Cells[6].FindControl("DropDownList6");
                            ddl55.SelectedItem.Text = "1h";

                         

                            for (int j = 0; j < 5; j++)
                            {
                                z++;
                                DateTime date1 = Convert.ToDateTime(TextBoxdeb.Text);
                                Label date = (Label)Gridview1.Rows[j + (5 * i)].FindControl("Label14");
                                DateTime answer = date1.AddDays(i);
                                date.Text = answer.ToString("dd/MM/yyyy");

                                Label jour = (Label)Gridview1.Rows[j + (5 * i)].FindControl("Labeljour");
                                DateTime dateValue = date1.AddDays(i);
                                string jourr = dateValue.ToString("ddd", new CultureInfo("fr-FR"));
                                jour.Text = jourr;
                            }
                        }
                    }
                }
            }

            
                if (DropDownListNumSeance.SelectedItem.Text == "4")
                {

                    foreach (GridViewRow row in Gridview1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            int z = 0;
                            for (int i = 0; i <= day; i++)
                            {
                                DropDownList ddl31 = (DropDownList)Gridview1.Rows[z].Cells[4].FindControl("DropDownList3");
                                ddl31.SelectedItem.Text = heurDebS1.Text;
                                DropDownList ddl41 = (DropDownList)Gridview1.Rows[z].Cells[5].FindControl("DropDownList4");
                                ddl41.SelectedItem.Text = MinDebS1.Text;
                                DropDownList ddl51 = (DropDownList)Gridview1.Rows[z].Cells[6].FindControl("DropDownList6");
                                ddl51.SelectedItem.Text = "1h30";

                                DropDownList ddl32 = (DropDownList)Gridview1.Rows[z + 1].Cells[4].FindControl("DropDownList3");
                                ddl32.SelectedItem.Text = heurDebS2.Text;
                                DropDownList ddl42 = (DropDownList)Gridview1.Rows[z + 1].Cells[5].FindControl("DropDownList4");
                                ddl42.SelectedItem.Text = MinDebS2.Text;
                                DropDownList ddl52 = (DropDownList)Gridview1.Rows[z + 1].Cells[6].FindControl("DropDownList6");
                                ddl52.SelectedItem.Text = "1h30";

                                DropDownList ddl33 = (DropDownList)Gridview1.Rows[z + 2].Cells[4].FindControl("DropDownList3");
                                ddl33.SelectedItem.Text = heurDebS3.Text;
                                DropDownList ddl43 = (DropDownList)Gridview1.Rows[z + 2].Cells[5].FindControl("DropDownList4");
                                ddl43.SelectedItem.Text = MinDebS3.Text;
                                DropDownList ddl53 = (DropDownList)Gridview1.Rows[z + 2].Cells[6].FindControl("DropDownList6");
                                ddl53.SelectedItem.Text = "1h30";

                                DropDownList ddl34 = (DropDownList)Gridview1.Rows[z + 3].Cells[4].FindControl("DropDownList3");
                                ddl34.SelectedItem.Text = heurDebS4.Text;
                                DropDownList ddl44 = (DropDownList)Gridview1.Rows[z + 3].Cells[5].FindControl("DropDownList4");
                                ddl44.SelectedItem.Text = MinDebS4.Text;
                                DropDownList ddl54 = (DropDownList)Gridview1.Rows[z + 3].Cells[6].FindControl("DropDownList6");
                                ddl54.SelectedItem.Text = "1h30";

                                

                                for (int j = 0; j < 4; j++)
                                {
                                    z++;
                                    DateTime date1 = Convert.ToDateTime(TextBoxdeb.Text);
                                    Label date = (Label)Gridview1.Rows[j + (4 * i)].FindControl("Label14");
                                    DateTime answer = date1.AddDays(i);
                                    date.Text = answer.ToString("dd/MM/yyyy");

                                    Label jour = (Label)Gridview1.Rows[j + (4 * i)].FindControl("Labeljour");
                                    DateTime dateValue = date1.AddDays(i);
                                    string jourr = dateValue.ToString("ddd", new CultureInfo("fr-FR"));
                                    jour.Text = jourr;
                                }
                            }
                        }
                    }
                }

                if (DropDownListNumSeance.SelectedItem.Text == "3")
                {

                    foreach (GridViewRow row in Gridview1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            int z = 0;
                            for (int i = 0; i <= day; i++)
                            {
                                DropDownList ddl31 = (DropDownList)Gridview1.Rows[z].Cells[4].FindControl("DropDownList3");
                                ddl31.SelectedItem.Text = heurDebS1.Text;
                                DropDownList ddl41 = (DropDownList)Gridview1.Rows[z].Cells[5].FindControl("DropDownList4");
                                ddl41.SelectedItem.Text = MinDebS1.Text;
                                DropDownList ddl51 = (DropDownList)Gridview1.Rows[z].Cells[6].FindControl("DropDownList6");
                                ddl51.SelectedItem.Text = "1h30";

                                DropDownList ddl32 = (DropDownList)Gridview1.Rows[z + 1].Cells[4].FindControl("DropDownList3");
                                ddl32.SelectedItem.Text = heurDebS2.Text;
                                DropDownList ddl42 = (DropDownList)Gridview1.Rows[z + 1].Cells[5].FindControl("DropDownList4");
                                ddl42.SelectedItem.Text = MinDebS2.Text;
                                DropDownList ddl52 = (DropDownList)Gridview1.Rows[z + 1].Cells[6].FindControl("DropDownList6");
                                ddl52.SelectedItem.Text = "1h30";

                                DropDownList ddl33 = (DropDownList)Gridview1.Rows[z + 2].Cells[4].FindControl("DropDownList3");
                                ddl33.SelectedItem.Text = heurDebS3.Text;
                                DropDownList ddl43 = (DropDownList)Gridview1.Rows[z + 2].Cells[5].FindControl("DropDownList4");
                                ddl43.SelectedItem.Text = MinDebS3.Text;
                                DropDownList ddl53 = (DropDownList)Gridview1.Rows[z + 2].Cells[6].FindControl("DropDownList6");
                                ddl53.SelectedItem.Text = "1h30";

                         


                                for (int j = 0; j < 3; j++)
                                {
                                    z++;
                                    DateTime date1 = Convert.ToDateTime(TextBoxdeb.Text);
                                    Label date = (Label)Gridview1.Rows[j + (3 * i)].FindControl("Label14");
                                    DateTime answer = date1.AddDays(i);
                                    date.Text = answer.ToString("dd/MM/yyyy");

                                    Label jour = (Label)Gridview1.Rows[j + (3 * i)].FindControl("Labeljour");
                                    DateTime dateValue = date1.AddDays(i);
                                    string jourr = dateValue.ToString("ddd", new CultureInfo("fr-FR"));
                                    jour.Text = jourr;
                                }
                            }
                        }
                    }
                }

                if (DropDownListNumSeance.SelectedItem.Text == "2")
                {

                    foreach (GridViewRow row in Gridview1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            int z = 0;
                            for (int i = 0; i <= day; i++)
                            {
                                DropDownList ddl31 = (DropDownList)Gridview1.Rows[z].Cells[4].FindControl("DropDownList3");
                                ddl31.SelectedItem.Text = heurDebS1.Text;
                                DropDownList ddl41 = (DropDownList)Gridview1.Rows[z].Cells[5].FindControl("DropDownList4");
                                ddl41.SelectedItem.Text = MinDebS1.Text;
                                DropDownList ddl51 = (DropDownList)Gridview1.Rows[z].Cells[6].FindControl("DropDownList6");
                                ddl51.SelectedItem.Text = "2h";

                                DropDownList ddl32 = (DropDownList)Gridview1.Rows[z + 1].Cells[4].FindControl("DropDownList3");
                                ddl32.SelectedItem.Text = heurDebS2.Text;
                                DropDownList ddl42 = (DropDownList)Gridview1.Rows[z + 1].Cells[5].FindControl("DropDownList4");
                                ddl42.SelectedItem.Text = MinDebS2.Text;
                                DropDownList ddl52 = (DropDownList)Gridview1.Rows[z + 1].Cells[6].FindControl("DropDownList6");
                                ddl52.SelectedItem.Text = "2h";




                                for (int j = 0; j < 2; j++)
                                {
                                    z++;
                                    DateTime date1 = Convert.ToDateTime(TextBoxdeb.Text);
                                    Label date = (Label)Gridview1.Rows[j + (2 * i)].FindControl("Label14");
                                    DateTime answer = date1.AddDays(i);
                                    date.Text = answer.ToString("dd/MM/yyyy");

                                    Label jour = (Label)Gridview1.Rows[j + (2 * i)].FindControl("Labeljour");
                                    DateTime dateValue = date1.AddDays(i);
                                    string jourr = dateValue.ToString("ddd", new CultureInfo("fr-FR"));
                                    jour.Text = jourr;
                                }
                            }
                        }
                    }
                }

                if (DropDownListNumSeance.SelectedItem.Text == "1")
                {

                    foreach (GridViewRow row in Gridview1.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            int z = 0;
                            for (int i = 0; i <= day; i++)
                            {
                                DropDownList ddl31 = (DropDownList)Gridview1.Rows[z].Cells[4].FindControl("DropDownList3");
                                ddl31.SelectedItem.Text = heurDebS1.Text;
                                DropDownList ddl41 = (DropDownList)Gridview1.Rows[z].Cells[5].FindControl("DropDownList4");
                                ddl41.SelectedItem.Text = MinDebS1.Text;
                                DropDownList ddl51 = (DropDownList)Gridview1.Rows[z].Cells[6].FindControl("DropDownList6");
                                ddl51.SelectedItem.Text = "2h";

                          
                                for (int j = 0; j < 1; j++)
                                {
                                    z++;
                                    DateTime date1 = Convert.ToDateTime(TextBoxdeb.Text);
                                    Label date = (Label)Gridview1.Rows[j + (1 * i)].FindControl("Label14");
                                    DateTime answer = date1.AddDays(i);
                                    date.Text = answer.ToString("dd/MM/yyyy");

                                    Label jour = (Label)Gridview1.Rows[j + (1 * i)].FindControl("Labeljour");
                                    DateTime dateValue = date1.AddDays(i);
                                    string jourr = dateValue.ToString("ddd", new CultureInfo("fr-FR"));
                                    jour.Text = jourr;
                                }
                            }
                        }
                    }
                }


            //After binding the gridview, we can then extract and fill the DropDownList with Data 

            //DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[2].FindControl("DropDownList1");
            //DropDownList ddl2 = (DropDownList)Gridview1.Rows[0].Cells[3].FindControl("DropDownList2");
            //DropDownList ddl3 = (DropDownList)Gridview1.Rows[0].Cells[4].FindControl("DropDownList3");
            //DropDownList ddl4 = (DropDownList)Gridview1.Rows[0].Cells[5].FindControl("DropDownList4");
            //// DropDownList ddl5 = (DropDownList)Gridview1.Rows[0].Cells[6].FindControl("DropDownList5");
            //DropDownList ddl6 = (DropDownList)Gridview1.Rows[0].Cells[6].FindControl("DropDownList6");
            //FillDropDownList(ddl1);
            //FillDropDownList(ddl2);

        }

        //private void AddNewRowToGrid()
        //{

        //    if (ViewState["CurrentTable"] != null)
        //    {

        //        DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
        //        DataRow drCurrentRow = null;

        //        if (dtCurrentTable.Rows.Count > 0)
        //        {
        //            drCurrentRow = dtCurrentTable.NewRow();
        //            drCurrentRow["RowNumber"] = dtCurrentTable.Rows.Count + 1;

        //            //add new row to DataTable 
        //            dtCurrentTable.Rows.Add(drCurrentRow);
        //            //Store the current data to ViewState for future reference 

        //            ViewState["CurrentTable"] = dtCurrentTable;


        //            for (int i = 0; i < dtCurrentTable.Rows.Count - 1; i++)
        //            {

        //                //extract the TextBox values 

        //                TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[1].FindControl("TextBox1");
        //                //TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox2");

        //                dtCurrentTable.Rows[i]["Column1"] = box1.Text;
        //                //dtCurrentTable.Rows[i]["Column2"] = box2.Text;

        //                //extract the DropDownList Selected Items 

        //                DropDownList ddl1 = (DropDownList)Gridview1.Rows[i].Cells[2].FindControl("ddlCountries");
        //                DropDownList ddl2 = (DropDownList)Gridview1.Rows[i].Cells[3].FindControl("ddlCountries2");

        //                DropDownList ddl3 = (DropDownList)Gridview1.Rows[i].Cells[4].FindControl("DropDownList3");

        //                DropDownList ddl4 = (DropDownList)Gridview1.Rows[i].Cells[5].FindControl("DropDownList4");

        //                DropDownList ddl5 = (DropDownList)Gridview1.Rows[i].Cells[6].FindControl("DropDownList5");
        //                DropDownList ddl6 = (DropDownList)Gridview1.Rows[i].Cells[7].FindControl("DropDownList6");



        //                // Update the DataRow with the DDL Selected Items 

        //                dtCurrentTable.Rows[i]["Column2"] = ddl1.SelectedItem.Text;
        //                //dtCurrentTable.Rows[i]["Column3"] = ddl2.SelectedItem.Text;
        //                dtCurrentTable.Rows[i]["Column4"] = ddl3.SelectedItem.Text;
        //                dtCurrentTable.Rows[i]["Column5"] = ddl4.SelectedItem.Text;
        //                dtCurrentTable.Rows[i]["Column6"] = ddl5.SelectedItem.Text;
        //                dtCurrentTable.Rows[i]["Column7"] = ddl6.SelectedItem.Text;

        //            }
        //            //fillJours();
        //            // fillnumJours();

        //            //Rebind the Grid with the current data to reflect changes 
        //            Gridview1.DataSource = dtCurrentTable;
        //            Gridview1.DataBind();
        //        }
        //    }
        //    else
        //    {
        //        Response.Write("ViewState is null");

        //    }
        //    //Set Previous Data on Postbacks 
        //    SetPreviousData();
        //}



        private void SetPreviousData()
        {

            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {

                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        TextBox box1 = (TextBox)Gridview1.Rows[i].Cells[1].FindControl("TextBox1");
                        //TextBox box2 = (TextBox)Gridview1.Rows[i].Cells[2].FindControl("TextBox2");

                        DropDownList ddl1 = (DropDownList)Gridview1.Rows[rowIndex].Cells[2].FindControl("ddlCountries");
                        //DropDownList ddl2 = (DropDownList)Gridview1.Rows[rowIndex].Cells[3].FindControl("ddlCountries2");

                        DropDownList ddl3 = (DropDownList)Gridview1.Rows[rowIndex].Cells[4].FindControl("DropDownList3");
                        DropDownList ddl4 = (DropDownList)Gridview1.Rows[rowIndex].Cells[5].FindControl("DropDownList4");


                        DropDownList ddl5 = (DropDownList)Gridview1.Rows[rowIndex].Cells[6].FindControl("DropDownList5");
                        DropDownList ddl6 = (DropDownList)Gridview1.Rows[rowIndex].Cells[7].FindControl("DropDownList6");



                        //Fill the DropDownList with Data 
                        //FillDropDownList(ddl1);
                        //FillDropDownList(ddl2);
                        // fillnumJours();
                        //fillJours();

                        if (i < dt.Rows.Count - 1)
                        {

                            //Assign the value from DataTable to the TextBox 
                            box1.Text = dt.Rows[i]["Column1"].ToString();
                            //box2.Text = dt.Rows[i]["Column2"].ToString();

                            //Set the Previous Selected Items on Each DropDownList  on Postbacks 
                            ddl1.ClearSelection();
                            ddl1.Items.FindByText(dt.Rows[i]["Column2"].ToString()).Selected = true;

                            //ddl2.ClearSelection();
                            //ddl2.Items.FindByText(dt.Rows[i]["Column3"].ToString()).Selected = true;

                            ddl3.ClearSelection();
                            ddl3.Items.FindByText(dt.Rows[i]["Column4"].ToString()).Selected = true;


                            ddl4.ClearSelection();
                            ddl4.Items.FindByText(dt.Rows[i]["Column5"].ToString()).Selected = true;


                            ddl5.ClearSelection();
                            ddl5.Items.FindByText(dt.Rows[i]["Column6"].ToString()).Selected = true;

                            ddl6.ClearSelection();
                            ddl6.Items.FindByText(dt.Rows[i]["Column7"].ToString()).Selected = true;


                        }

                        rowIndex++;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!Page.IsPostBack)
            //{
            //    SetInitialRow();
            //    //fillJours();
            //    //fillnumJours();
            //}
        }

        //protected void ButtonAdd_Click(object sender, EventArgs e)
        //{
        //    AddNewRowToGrid();
        //}


        public void fillJours()
        {
            DropDownList ddl2 = (DropDownList)Gridview1.Rows[0].Cells[3].FindControl("DropDownList2");
            ddl2.DataSource = service.fill_jours();
            ddl2.DataBind();
        }


        public void fillnumJours()
        {
            DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[2].FindControl("DropDownList1");

            ddl1.DataSource = service.fill_Num_jours();
            ddl1.DataBind();
        }


        public void fillHD()
        {

        }


        public void fillMD()
        {

        }

        public void fillHF()
        {
        }


        public void fillMF()
        {

        }



        protected void Calendar_Changed(object sender, EventArgs e)
        {
            //Calendar DateTakenCalendar = (Calendar)sender;
            //TextBox DateTakenTextBox = (TextBox)((GridViewRow)((Calendar)(sender)).Parent.Parent).FindControl("TextBox1");

            //DateTakenTextBox.Text = DateTakenCalendar.SelectedDate.ToString();

        }




        protected void OnRowDataBoundS(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                OracleCommand cmd = new OracleCommand("select code_nome from code_nomenclature where code_str='92'");
                DropDownList ddlCountries = (e.Row.FindControl("ddlCountries") as DropDownList);
                ddlCountries.DataSource = this.ExecuteQuery(cmd, "SELECT");
                ddlCountries.DataTextField = "code_nome";
                ddlCountries.DataValueField = "code_nome";
                // ddlCountries.Items.Add("NN");
                // ddlCountries.Items.Add(new ListItem("NN", "NN"));
                ddlCountries.DataBind();


                //OracleCommand cmd2 = new OracleCommand("select lib_nome from code_nomenclature where code_str='92'");
                //DropDownList ddlCountries2 = (e.Row.FindControl("ddlCountries2") as DropDownList);
                //ddlCountries2.DataSource = this.ExecuteQuery(cmd2, "SELECT");
                //ddlCountries2.DataTextField = "lib_nome";
                //ddlCountries2.DataValueField = "lib_nome";
                //// ddlCountries.Items.Add("NN");
                //// ddlCountries.Items.Add(new ListItem("NN", "NN"));
                //ddlCountries2.DataBind();
            }





        }





        private DataTable ExecuteQuery(OracleCommand cmd, string action)
        {
            //string conString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            using (OracleConnection con = new OracleConnection(AppConfiguration.ConnectionString))
            {
                cmd.Connection = con;
                switch (action)
                {
                    case "SELECT":
                        using (OracleDataAdapter sda = new OracleDataAdapter())
                        {
                            sda.SelectCommand = cmd;
                            using (DataTable dt = new DataTable())
                            {
                                sda.Fill(dt);
                                return dt;
                            }
                        }
                    case "UPDATE":
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                        break;
                }
                return null;
            }
        }

        protected void btnaffecter_Click(object sender, EventArgs e)
        {

            string conString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;
            OracleConnection connection = new OracleConnection(AppConfiguration.ConnectionString);
            connection.Open();
            OracleCommand cmd1 = new OracleCommand("TRUNCATE TABLE ESP_SEANCE_EXAMEN", connection);
            cmd1.ExecuteNonQuery();
            connection.Close();

            foreach (GridViewRow row in Gridview1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    DataTable dtt;

                    if (Rdcours.SelectedValue == "C")
                    {


                        string semestre = Rdsemestre.SelectedValue;

                        string anneesub = service.Get_annee_deb();
                        string annee = anneesub.Substring(0, 4);

                        string periode = rdperiode.SelectedValue;
                        string site = DropDownList2.SelectedValue;
                        string heuredeb = row.Cells[4].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Text;

                        string mindebut = row.Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Text;
                        string dp = ":";
                        string hdeb = string.Concat(heuredeb, dp, mindebut);

                        string duree = row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().SelectedItem.Text;

                        //string mindefin = row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                        //string hfin = string.Concat(heurefin, dp, mindefin) ;


                        string numseance = row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text;
                        string date_seanceup = row.Cells[1].Controls.OfType<Label>().FirstOrDefault().Text;

                        DateTime date_seance2 = Convert.ToDateTime(date_seanceup);

                        string date_seance = date_seance2.ToString("dd/MM/yyyy");

                        //string num_jours = row.Cells[2].Controls.OfType<Label>().FirstOrDefault().Text;
                        string jours = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;




                        dtt = service.existSeance(numseance, periode, Convert.ToInt32(semestre), date_seance, annee);


                        if (dtt.Rows.Count != 0)
                        {
                            Response.Write(@"<script language='javascript'>alert('Affectation dejà existe');</script>");
                        }


                        else
                        {
                            OracleCommand cmd = new OracleCommand("insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,jours,heure_debut,DUREE,site,periode) values ('" + numseance + "','" + annee + "',to_date('" + date_seance + "','dd/MM/yy'),'" + semestre + "','" + jours + "','" + hdeb + "','" + duree + "','" + site + "','" + periode + "') ");

                            this.ExecuteQuery(cmd, "UPDATE");
                            // Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");

                        }
                        //("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam,code_module=:code_module where code_cl =:code_cl");



                        //cmd.Parameters.Add(":num_seance", row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text);
                        ////cmd.Parameters.Add(":annee_deb", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add("to_date(:date_seance,'dd/mm/yy')", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        ////cmd.Parameters.Add(":semestre", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":num_jours", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":jours", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":heure_debut", row.Cells[4].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":min_debut", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);


                        //cmd.Parameters.Add(":heure_fin", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":min_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);

                        //cmd.Parameters.Add(":site", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);
                        //cmd.Parameters.Add(":periode", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);




                    }

                    else
                    {

                        if (Rdcours.SelectedValue == "A")
                        {
                            string semestre1 = Rdsemestre.SelectedValue;
                            string annee1 = ddlan_univer.SelectedValue.Substring(0, 4);
                            string periode = rdperiode.SelectedValue;
                            string site = ddlsite.SelectedValue;


                            string heuredeb = row.Cells[4].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            string mindebut = row.Cells[5].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                            string dp = ":";
                            string hdeb = string.Concat(heuredeb, dp, mindebut);
                            string heurefin = row.Cells[6].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            string mindefin = row.Cells[7].Controls.OfType<DropDownList>().FirstOrDefault().Text;

                            string hfin = string.Concat(heurefin, dp, mindefin);


                            string numseance = row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text;
                            string date_seanceup = row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text;

                            DateTime date_seance2 = Convert.ToDateTime(date_seanceup);

                            string date_seance = date_seance2.ToString("dd/MM/yyyy");
                            string num_jours = row.Cells[2].Controls.OfType<DropDownList>().FirstOrDefault().Text;
                            string jours = row.Cells[3].Controls.OfType<Label>().FirstOrDefault().Text;

                            dtt = service.existSeance(numseance, periode, Convert.ToInt32(semestre1), date_seance, annee1);


                            if (dtt.Rows.Count != 0)
                            {
                                Response.Write(@"<script language='javascript'>alert('Affectation dejà existe');</script>");
                            }


                            else
                            {


                                OracleCommand cmd = new OracleCommand("insert into esp_seance_examen (num_seance,annee_deb,date_seance,semestre,num_jours,jours,heure_debut,heure_fin,site,periode) values ('" + numseance + "','" + annee1 + "',to_date('" + date_seance + "','dd/MM/yy'),'" + semestre1 + "','" + num_jours + "','" + jours + "','" + hdeb + "','" + hfin + "','" + site + "','" + periode + "') ");
                                //("update esp_ds_exam set a_programmer=:a_programmer,type_exam=:type_exam,code_module=:code_module where code_cl =:code_cl");
                                this.ExecuteQuery(cmd, "UPDATE");
                                //Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");
                            }
                            //cmd.Parameters.Add(":num_seance", row.Cells[0].Controls.OfType<Label>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":annee_deb", row.Cells[1].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add("to_date(:date_seance,'dd/mm/yy')", row.Cells[6].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":semestre", row.Cells[2].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":num_jours", row.Cells[4].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":jours", row.Cells[5].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":heure_debut", row.Cells[7].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            //cmd.Parameters.Add(":heure_fin", row.Cells[8].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":site", row.Cells[9].Controls.OfType<TextBox>().FirstOrDefault().Text);
                            ////cmd.Parameters.Add(":periode", row.Cells[3].Controls.OfType<TextBox>().FirstOrDefault().Text);        

                        }
                    }

                }


                //rebind grid maha
                //gvCustomers.DataSource = service.bind_niveau_etudiantbycl(ddclasse2.SelectedValue, ddlannee_debM.SelectedValue);
                //gvCustomers.DataBind();
            }
            Response.Write(@"<script language='javascript'>alert('Affectation avec succès');</script>");

        }

        protected void btnvalid_Click(object sender, EventArgs e)
        {
            if (Rdsemestre.SelectedValue == "" || rdperiode.SelectedValue == "")
            {
                Response.Write(@"<script language='javascript'>alert('Veuillez choisir!');</script>");

            }
            //

            else
                //plvalider.Visible = false;
                //Plchoixannee.Visible = true;

                plvalider.Visible = false;
            Plchoixannee.Visible = false;

            if (Rdcours.SelectedValue == "C")
            {

                Plannee2.Visible = true;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";

                string annee = service.get_annee_parametr();

                //DropDownList2.DataSource = service.bind_site2(annee.Substring(0, 4));

                //DropDownList2.DataBind();

            }
        }

        protected void Btnsuivant_Click(object sender, EventArgs e)
        {
            plvalider.Visible = false;
            Plchoixannee.Visible = false;

            if (Rdcours.SelectedValue == "C")
            {

                Plannee2.Visible = true;

                DropDownList2.DataTextField = "site";
                DropDownList2.DataValueField = "site";

                string annee = service.get_annee_parametr();

                DropDownList2.DataSource = service.bind_site2(annee.Substring(0, 4));

                DropDownList2.DataBind();

            }

            else
            {
                if (Rdcours.SelectedValue == "A")
                    plannee.Visible = true;

                //ddlan_univer.DataTextField = "ANNEE_UNIVERSITAIRE";
                //ddlan_univer.DataValueField = "ANNEE_UNIVERSITAIRE";
                //ddlan_univer.DataSource = service.List_etud_2015();
                //ddlan_univer.DataBind();


                ddlan_univer.Items.Insert(0, "---Veuillez choisir---");

                string ss = service.get_annee_parametr();

                string anne0 = service.get_annee_0();

                int annee00 = Convert.ToInt32(anne0);
                string ss2 = ss.Substring(0, 4);
                int ssi = Convert.ToInt32(ss2);
                int index = 1;
                for (int i = ssi; i >= annee00; i--)
                {

                    int rr = Convert.ToInt32(i.ToString());

                    int ff = rr + 1;
                    ddlan_univer.Items.Insert(index, new ListItem(i.ToString() + "-" + ff, i.ToString()));
                    index++;


                    {

                    }
                }
            }

        }

        protected void ddlan_univer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlan_univer.SelectedValue != null)
            {
                ddlsite.DataTextField = "SITE";
                ddlsite.DataValueField = "SITE";

                ddlsite.DataSource = service.bind_site(ddlan_univer.SelectedValue.ToString().Substring(0, 4));

                ddlsite.DataBind();
                ddlsite.Items.Insert(0, new ListItem("Veuillez choisir", "Veuillez choisir"));
                ddlsite.SelectedItem.Selected = false;
                ddlsite.Items.FindByText("Veuillez choisir").Selected = true;
                //Label1.Text = "a.ANNEE_DEB= '" + ddlan_univer.SelectedValue.ToString().Substring(0, 4) + "'";


            }
        }

        protected void ddlsite_SelectedIndexChanged(object sender, EventArgs e)
        {
            pl2.Visible = true;
            plannee.Visible = false;

            lblan.Text = ddlan_univer.SelectedValue.Substring(0, 4);
            lblperiod.Text = rdperiode.SelectedValue;
            lblsem.Text = Rdsemestre.SelectedValue;
            lblSite.Text = ddlsite.SelectedValue;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            pl2.Visible = true;
            Plannee2.Visible = false;

            /////////////////////////
            string annee = service.get_annee_parametr();
            lblan.Text = annee;
            lblperiod.Text = rdperiode.SelectedValue;
            lblsem.Text = Rdsemestre.SelectedValue;
            lblSite.Text = DropDownList2.SelectedValue;

        }

        //ajouter les seances

        protected void Gridview1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txt_TextChanged(object sender, EventArgs e)
        {



            TextBox TextBox1 = (TextBox)Gridview1.Rows[0].Cells[1].FindControl("TextBox1");

            DropDownList ddl1 = (DropDownList)Gridview1.Rows[0].Cells[2].FindControl("DropDownList1");

            DropDownList ddl2 = (DropDownList)Gridview1.Rows[0].Cells[3].FindControl("DropDownList2");

            DateTime dateValue = Convert.ToDateTime(TextBox1.Text);

            //CultureInfo myCIintl = new CultureInfo("FR-fr");

            //ddl2.SelectedValue = dateValue.ToString("ddl1", new CultureInfo("fr-FR"));



        }

        //protected void ddlDropDownList122_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
        //    DropDownList duty = (DropDownList)gvr.FindControl("ddlCountries");
        //    //DropDownList duty2 = (DropDownList)gvr.FindControl("ddlCountries2");

        //    if (duty.SelectedValue == "01")
        //    {
        //        duty2.SelectedValue = "Lundi";

        //    }
        //    else
        //        if (duty.SelectedValue == "02")
        //        {

        //            duty2.SelectedValue = "Mardi";
        //        }
        //        else
        //            if (duty.SelectedValue == "03")
        //            {
        //                duty2.SelectedValue = "Mercredi";

        //            }
        //            else
        //                if (duty.SelectedValue == "04")
        //                {
        //                    duty2.SelectedValue = "Jeudi";

        //                }
        //                else
        //                    if (duty.SelectedValue == "05")
        //                    {
        //                        duty2.SelectedValue = "Vendredi";


        //                    }
        //                    else if (duty.SelectedValue == "06")
        //                    {
        //                        duty2.SelectedValue = "Samedi";

        //                    }
        //}

        protected void Button2_Click(object sender, EventArgs e)
        {
            SetInitialRow();

            btnaffecter.Visible = true;
        }

        protected void ButtonSupp_Click(object sender, EventArgs e)
        {
           
            //    //// GridView1.Rows[i].Visible = false;

            //    //myDt.Rows.RemoveAt(i);
            //    //DataTable myDt1 = (DataTable)Session["tabel"];
            //    //GridView1.DataSource = myDt1;
            //    //GridView1.DataBind();

            //for (int i = Gridview1.Rows.Count - 1; i >= 0; i--)
            //{
            //     if (Gridview1.Rows[i].Cells[7].Controls.OfType<Button>().FirstOrDefault()==)

            //    Button btn = (Button)Gridview1.Rows[i].Cells[7].FindControl("ButtonSupp");
               
            //        // GridView1.Rows[i].Visible = false;

            //        //DataTable myDt1 = (DataTable)Session["tabel"];
            //        //myDt1.Rows[i].Delete();
            //        //Gridview1.DataSource = myDt1;
            //        //Gridview1.DataBind();

            //        Gridview1.Rows[i].Enabled = false;
            //        Gridview1.Rows[i].Visible = false;


                
            //}

            
           
        }

      
        


        

    }



}
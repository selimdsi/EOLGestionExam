﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data;
using System.ComponentModel;

namespace ESPSuiviEncadrement
{
    public class encadrementProf
    {
        #region sing
        static encadrementProf instance;
        static Object locker = new Object();
        public static encadrementProf Instance
        {
            get
            {
                lock (locker)
                {
                    if (instance == null)
                    {
                        instance = new encadrementProf();
                    }

                    return encadrementProf.instance;
                }
            }

        }
        private encadrementProf() { }
        #endregion sing
    }
}
